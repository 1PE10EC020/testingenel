<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>CFR</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>CFR</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>CWB Toolkit</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Concat_Object_Field_Names__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>false</externalId>
        <label>Concat Object Field Names</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Configuration_Environment__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Configuration Environment</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Custom_Objects__c</fullName>
        <externalId>false</externalId>
        <label>Custom Objects</label>
        <referenceTo>CustomObjects__c</referenceTo>
        <relationshipName>Custom_Fields</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Data_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Select a Data Type for the Custom Field</inlineHelpText>
        <label>Data Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Checkbox</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Currency</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Date</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Date/Time</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Email</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Number</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Percent</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Phone</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Picklist</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Picklist Multiselect</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Text</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Text Area</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Text Area Long</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Text Area Rich</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>URL</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Decimal_Places__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter the no. of decimal places to be displayed</inlineHelpText>
        <label>Decimal Places</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Default_Checked__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Select True / False if the checkbox should be checked by default or not.</inlineHelpText>
        <label>Default Checked</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>False</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>True</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Default_Value__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;if(1&gt;2,&apos;test pass&apos;,&apos;test fail&apos;)&quot;</formula>
        <inlineHelpText>Default Value that should be displayed</inlineHelpText>
        <label>Default Value</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <defaultValue>&quot;&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Development_Environment__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Development Environment</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_History__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Enable History</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>External ID</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Field_Label__c</fullName>
        <defaultValue>&quot;&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Field Label</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field_Name__c</fullName>
        <defaultValue>&quot;&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Field Name</label>
        <length>40</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>First_Value_Default__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Check if you wish for the first value to be used as Default Value</inlineHelpText>
        <label>First Value Default</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Help_Text__c</fullName>
        <defaultValue>&quot;&quot;</defaultValue>
        <description>Sample field Description 1</description>
        <externalId>false</externalId>
        <inlineHelpText>Sample Help Text 1</inlineHelpText>
        <label>Help Text</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Length__c</fullName>
        <externalId>false</externalId>
        <label>Length</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ObjectName__c</fullName>
        <externalId>false</externalId>
        <label>ObjectName</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Picklist_Values__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Values of the picklist separated by &apos;Next Line Character&apos;</inlineHelpText>
        <label>Picklist Values</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Production_Environment__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Production Environment</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Refrence_No__c</fullName>
        <externalId>false</externalId>
        <label>Req.Ref#</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Required__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Check if this field should be Mandatory</inlineHelpText>
        <label>Required</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFDC_Track_History__c</fullName>
        <description>Sample field Description 1</description>
        <externalId>false</externalId>
        <formula>&quot;if(1&gt;2,&apos;test pass&apos;,&apos;test fail&apos;)&quot;</formula>
        <inlineHelpText>Sample Help Text 1</inlineHelpText>
        <label>SFDC Track History</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sort__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Check if the values of the picklist if the values should be sorted in Alphabetical order</inlineHelpText>
        <label>Sort</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>UAT_Environment__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>UAT Environment</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Unique__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Check if this field should not allow duplicate values</inlineHelpText>
        <label>Unique</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Visible_Lines__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter the no. of lines of the picklist that should be visible</inlineHelpText>
        <label>Visible Lines</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Custom Field</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>CF-{0000}</displayFormat>
        <label>Record Name</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Custom Fields</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Field_Label__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Field_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Data_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>OBJECT_ID</searchFilterFields>
        <searchFilterFields>Field_Label__c</searchFilterFields>
        <searchFilterFields>Field_Name__c</searchFilterFields>
        <searchFilterFields>Data_Type__c</searchFilterFields>
        <searchFilterFields>Description__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
