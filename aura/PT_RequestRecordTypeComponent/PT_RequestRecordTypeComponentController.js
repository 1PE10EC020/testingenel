({
	// Queries Record type details from srver based on the attribute value set at the component lebel
    doInit : function(component, event, helper) {
       //debugger;
        // helper.validateUser(component);	
       helper.queryRecordType(component);
            
	},
    // this method sets the attribute value on selection of a specific record type radio button. This attribute will
    // be passed as input attribute to next Request Insertion Component(RIC).
    updateRadioValue:function(component,event,helper){
        //debugger;
        var selected = event.getSource().get("v.value");
        //debugger;
		component.set("v.recordId",selected); 
        if(selected!== ''){
         component.set("v.buttonvisible",true);    
        }
    },
    hideModal : function(component, event, helper) {
        document.getElementById("extensionPopup").style.display = "none";
        //debugger;
    },
    //On click of Continue button, user is navigated to Request RIC component page which displays the list of Request
    //RFI components to complete request creation.
    navigateToRIC : function(component, event, helper) {
        debugger;
        //alert('hi');
        //window.location.href = $A.get("$Label.c.PT_Request_RIC_CommunityPage_URL")+component.get("v.recordId");
        component.set("v.renderRIC",true);
        //alert('hi 1');
        //debugger;		
	},
    cancel : function(component, event, helper) 
    {
        //Navigates user back to community home page on click of cancel button.
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");        
    },
    
})