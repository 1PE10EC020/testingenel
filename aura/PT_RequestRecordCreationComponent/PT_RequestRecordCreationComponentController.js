({
	//This method reads the record type id from the Community page URL parameter and same is being set as attribute.
	//This alsoinvokes helper class method to initialize picklist values for all the picklist type fields in request object.
    doInit : function(component, event, helper) {
		debugger;
        //alert('hi1');
        /*var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'recordId') { //lets say you are looking for param name - firstName
                sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
            }
        }
        
        component.set("v.recordId",sParameterName[1]);
        var value=component.get("v.recordId");
        var recTypeId = component.get("v.recordId");*/
        
        helper.initilizePicklistValues(component, event, helper);
        //helper.hideuploadcomponent(component, event, helper);
        component.set("v.Spinner", true);        
        
	},
    validateDatiIntestatarioRFI1 : function(component, event, helper) {
      
       var selectedRFI= event.getParam("selected");
        //alert('RFI1'+selectedRFI);
        component.set("v.selected",selectedRFI);
    },
    
formatMismatchChange : function(component,event,helper){
     	debugger;
       console.log('format mismatch 1 : '+component.get('v.formatMismatch1')+'\n format mismatch 2 : '+component.get('v.formatMismatch2') + '\n format mismatch 3 : '+component.get('v.formatMismatch3'));
       if(component.get('v.formatMismatch1') ===$A.get("$Label.c.PT_True") || 
          component.get('v.formatMismatch2') ===$A.get("$Label.c.PT_True")|| 
          component.get('v.formatMismatch3') ===$A.get("$Label.c.PT_True")){
           component.set('v.formatError',true);
     	}
     	else{
          if(component.get('v.formatMismatch1') !==$A.get("$Label.c.PT_True") && 
             component.get('v.formatMismatch2') !==$A.get("$Label.c.PT_True") && 
             component.get('v.formatMismatch3') !==$A.get("$Label.c.PT_True")) {
           component.set('v.formatError',false);
          }     	
       }
    },
    
   noFileSelected : function(component,event,helper){
        var fileSelected = component.get('v.fileNotselected');
        if(fileSelected===$A.get("$Label.c.PT_FileSelectedvframe1")){
            component.set('v.file1Notselected',true);
        }
        if(fileSelected===$A.get("$Label.c.PT_FileSelectedvframe2")){
            component.set('v.file2Notselected',true);
        }
        if(fileSelected===$A.get("$Label.c.PT_FileSelectedvframe3")){
            component.set('v.file3Notselected',true);
        }
	
        var file1 = component.get('v.file1Notselected');
        var file2 = component.get('v.file2Notselected');
        var file3 = component.get('v.file3Notselected');
       
        if(file1===true && file2===true && file3===true){
            component.set('v.noFilesSelected',$A.get("$Label.c.PT_True"));
        }
        
    },
    
    fileUploadSequenceHandler : function(component,event,helper){
        debugger;
        var request = component.get('v.request');
        var ammissibilita = null;
		var file1Status = component.get('v.uploadFlag1');
        var file2Status = component.get('v.uploadFlag2');
		var file3Status = component.get('v.uploadFlag3');
        var file1selected = component.get('v.file1selected');
        var file2selected = component.get('v.file2selected');
        var file3selected = component.get('v.file3selected');

   		if((file1selected === $A.get("$Label.c.PT_True") && file1Status === $A.get("$Label.c.PT_Uploaded"))||file1selected === $A.get("$Label.c.PT_False")){
        	  if((file2selected === $A.get("$Label.c.PT_True") && file2Status === $A.get("$Label.c.PT_Uploaded"))||file2selected === $A.get("$Label.c.PT_False")){
            	    if((file3selected === $A.get("$Label.c.PT_True") && file3Status === $A.get("$Label.c.PT_Uploaded"))||file3selected === $A.get("$Label.c.PT_False")){
                     debugger;
                     //alert('request'+JSON.stringify(request));
        
        var action = component.get("c.updateFileUploadCheckbox");
        action.setParams({
            "request" : request, 
        });
        
        action.setCallback(this,function(response){
            helper.interactiveOutcomeManagment(component,event,helper);

        });
        $A.enqueueAction(action);        
              } 
           }  
        }
    },
    hideuploadcomponent : function(component, event, helper){
        //  If the user chooses Tipo Richiesta Cliente Finale = 1 / 2 / 5 .
      var TipoRichiestaClienteFinale = event.getParam("TipoRichiestaClienteFinaleEvent");
        //alert('onchange'+TipoRichiestaClienteFinale);
        if((TipoRichiestaClienteFinale === "1" )||
           (TipoRichiestaClienteFinale === "2" )||
           (TipoRichiestaClienteFinale === "5"))
        {
            component.set("v.TipoRichiestaCliente",$A.get("$Label.c.PT_True"));
            
        }
        else {
           component.set("v.TipoRichiestaCliente",$A.get("$Label.c.PT_False")); 
        }
    },
    
    
    
    // this function performs all the UI validations, Pre-validation checks and on successful validation it invokes Apex
    // Controller method to create request record.

    validateandcreate : function(component, event, helper) {
       debugger;
        //alert('entered validate and create');
        var errorMsg_ric = helper.validateDatiRichiestaRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_ric))
        {
            helper.throwErrorMsg(component,errorMsg_ric,helper) ;
            return;
        } 
        //Calling a helper method to validate Dati Venditore RFI field values.
        var errorMsg_ven = helper.validateDatiVenditoreRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_ven))
        {
            helper.throwErrorMsg(component,errorMsg_ven,helper) ;
            return;
        } 
        
        //Calling a helper method to validate Dati Intestatario RFI field values. 
        var errorMsg_Int = helper.validateDatiIntestatarioRFI(component,event,helper);
        if(!$A.util.isEmpty(errorMsg_Int))
        {
            helper.throwErrorMsg(component,errorMsg_Int,helper) ;
            return;
        }
        //Calling a helper method to validate Fornitura Address
        var errorMsg_pod = helper.validatePODFornituraAddress(component,helper);
        if(!$A.util.isEmpty(errorMsg_pod))
        {
            helper.throwErrorMsg(component,errorMsg_pod,helper) ;
            return;
        }
        //Calling a helper method to validate Dati Contrattuali RFI field values.                
        var errorMsg_con = helper.validateDatiContrattualiRFI(component,helper); 
        if(!$A.util.isEmpty(errorMsg_con))
        {
            helper.throwErrorMsg(component,errorMsg_con,helper) ;
            return;
        } 
        //Calling a helper method to validate Disalimentabile RFI field values.
        var errorMsg_dis = helper.validateDisalimentabileRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_dis))
        {
            helper.throwErrorMsg(component,errorMsg_dis,helper) ;
            return;
        }
        //Calling a helper method to validate Altri Dati RFI field values.
      	var errorMsg_AltriDatiRFI = helper.validateAltriDatiRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_AltriDatiRFI))
        {
            helper.throwErrorMsg(component,errorMsg_AltriDatiRFI,helper) ;
            return;
        } 
       // Calling a helper method to validate Parametri di Ricerca RFI field values.      
       var errorMsg_par = helper.validateParametri(component,helper);
        if(!$A.util.isEmpty(errorMsg_par))
        {
            helper.throwErrorMsg(component,errorMsg_par,helper) ;
            return;
        } 
       // Calling a helper method to validate DatiMisura  RFI field values.  
        var errorMsg_DatiMisura = helper.validateDatiMisuraRFI(component,helper); 
        if(!$A.util.isEmpty(errorMsg_DatiMisura))
        {
            helper.throwErrorMsg(component,errorMsg_DatiMisura,helper) ;
            return;
        } 
        //for Datirichestacliente
        var errorMsg_Tipo = helper.validateDatiRichestaclienteRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_Tipo)){
            helper.throwErrorMsg(component,errorMsg_Tipo,helper) ;
            return;
        }
        var errorMsg_Datservcurv = helper.validateDatiServCurveRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_Datservcurv)){
            helper.throwErrorMsg(component,errorMsg_Datservcurv,helper) ; 
            return;
        }
        // Calling a helper method to validate Letture Contestate RFI field values.  
		var errorMsg_letconts = helper.validateLettureContestate(component,helper);
        if(!$A.util.isEmpty(errorMsg_letconts))
        {
            helper.throwErrorMsg(component,errorMsg_letconts,helper) ;
            return;
        }
        // Calling a helper method to validate File upload RFI field values.  
		var errorMsg_file = helper.validateFileUpload(component,helper);
        if(!$A.util.isEmpty(errorMsg_file))
        {
            helper.throwErrorMsg(component,errorMsg_file,helper) ;
            return;
        } 
        debugger;
        //Calling a helper method to perform Pre-validation checks across RFI fields values.        
        //helper.checkPrevalidationErrors(component,helper);
        helper.createRequest(component,helper);
    },
	cancel : function(component, event, helper) 
    {
        //debugger;
        //Navigates user back to community home page on click of cancel button.
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");        
    },
   showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner      
       component.set("v.Spinner", true);
      
   },
 hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
   },
})