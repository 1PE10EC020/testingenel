({
    
    doinit : function(component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1")))
        {
            component.set("v.reqCodFisc","true");
        }
        else{
            component.set("v.requiredCodFisc","true");
        }
    },  
    
    onClick: function (component, event, helper) {
        //debugger;
        var selected = event.target.value;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        //alert(selected);
        //event.getSource().get("v.value");
        //component.set("v.recordId",selected);
        component.set("v.ipselection",selected);
        
        if(selected==="NameSurname"){
            component.find("RagSol").set("v.value","");
            component.set("v.displayNameSurname","true");
            component.set("v.displayBusinessName","false");
        }
        
        if(selected==="BusinessName"){
            component.find("Nome").set("v.value","");
            component.find("Cognome").set("v.value","");
            component.set("v.displayBusinessName","true");
            component.set("v.displayNameSurname","false");        
        }     
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           //recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_I01"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))||
           //recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01")))
        {
            var compEvent = $A.get("e.c:PT_RequestIptypeEvent");
            
            compEvent.setParams({
                "selected": selected
            });
            compEvent.fire(); 
        } 
    },
    
    checkCodiceFormat : function(component, event, helper) {
        //debugger;
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        var codiceFiscale = component.find("CodFisc_cl__c").get("v.value");
        var codiceFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_CodiceFiscale_REGEX"));
        
        if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))&&codiceFiscale!==null){
            var validateCodiceFormat = codiceFormat.test(codiceFiscale);
            if(validateCodiceFormat){
                var inputCmp = component.find("CodFisc_cl__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: Codice fiscale' ;
                var inputCmp1 = component.find("CodFisc_cl__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }
        }else{
            var inputCmp = component.find("Piva_cl__c");
            inputCmp.set("v.errors", null);
        }     
    },
    
    checkPartitaIVAFormat : function(component, event, helper) {
        //debugger;
        
        var partitaIva = component.find("Piva_cl__c").get("v.value");        
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))&&partitaIva!==null){
            if(!$A.util.isEmpty(partitaIva) && partitaIva!==null){ 
                //alert('piva cl--->'+partitaIva);
                var partitaIvaFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_PartitaIva_REGEX"));
                var validatepartitaIvaFormat = partitaIvaFormat.test(partitaIva);
                if(validatepartitaIvaFormat){
                    var inputCmp = component.find("Piva_cl__c");
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Errore di controllo sul formato per i seguenti campi: Partita IVA' ;
                    var inputCmp1 = component.find("Piva_cl__c");
                    inputCmp1.set("v.errors", [{message:errMsg}]);
                }    
            }
            else{
                var inputCmp = component.find("Piva_cl__c");
                inputCmp.set("v.errors", null);
            }
        } 
        
    },
    
})