({ 
    /**
    * This method is called to perform server action which intern calls the getSobjectRecord helper method.
    **/
    doInit : function(component, event, helper) 
    {
        var FieldAPIs = component.get("v.FieldAPIs"); 
        var FieldLabel = component.get("v.FieldLabel");
        var searchFieldApiName = component.get("v.searchFieldApiName");
        component.set('v.sObjectFieldApiName', FieldAPIs.split(','));
        component.set('v.sObjectFieldLabel', FieldLabel.split(','));
        //component.set('v.sObjectSearchFieldApiName', searchFieldApiName.split(','));

        component.set('v.sObjectFieldOther', FieldAPIs.split(','));
        console.log("*** sObjectFieldOther: " +component.get('v.sObjectFieldOther'));
       
        var offObj = component.get('v.sObjOffset');         
        helper.getSobjectRecord(component, offObj, true, false);

        /***[G.T.] - Download Column - start***/   
        var noColumn = component.get("v.numberCol");
        var labelColumns = component.get("v.labelCol");
        console.log("*** numero colonne: " +noColumn);
        console.log("*** label colonne: " +labelColumns);

        if(noColumn != 0){

            component.set('v.DownloadColumns', labelColumns.split(','));
            console.log("*** lista header: " + component.get('v.DownloadColumns'));
            var sourceField = component.get('v.sourceField');
            component.set('v.DownloadFields', sourceField.split(','));
            component.set('v.isDownloadable', true);
            console.log('*** isDownloadable: ' +component.get('v.isDownloadable'));
            var downloadApiField = component.get('v.DownloadFields');
            console.log('*** downloadApiField: ' +component.get('v.DownloadFields'));


           }

        /***[G.T.] - Download Column - end***/ 
    },
    
    /**
    * This method is called when user clicks on 'Next' hyperlink and will display the next records in the list
    **/
    nextRec : function(component, event, helper) 
    {        
        var maxNoOfRecords = component.get('v.maxNoRecords');   
        var offObj =  component.get("v.sObjOffset") + maxNoOfRecords;                           
        var isSearchable =  component.get("v.isSearchable");
        if(isSearchable)
        {    
            helper.getSobjectFilteredRecords(component, offObj,true);           
        }
        else            
        {
            helper.getSobjectRecord(component, offObj, true,true);         
        }    
    },
    
    /**
    * This method is called when user clicks on 'Search' button and will show the records in the list that match search term
    **/
    search  : function(component, event, helper) 
    {
        var maxNoOfRecords = component.get('v.maxNoRecords');  
        
        component.set('v.sObjOffset',-maxNoOfRecords); 
        helper.getSobjectFilteredRecords(component,0,true);     
        
    },
    
    /**
    * This method is called when user clicks enter after entering search term in search box
    *  and will show the records in the list that match search term
    **/
    searchOnEnter : function(component, event, helper){
        if(event.getParams().keyCode == 13){
            var maxNoOfRecords = component.get('v.maxNoRecords');
            component.set('v.sObjOffset',-maxNoOfRecords);
            helper.getSobjectFilteredRecords(component,0,true);
        }
    },
    
    /**
    * This method is called when user clicks on 'Previous' hyperlink and will display the previous records in the list
    **/
    previous  : function(component, event, helper) 
    {
        var maxNoOfRecords = component.get('v.maxNoRecords');   
        var offObj =  component.get("v.sObjOffset") - maxNoOfRecords;                   
        offObj =  (offObj <= 0) ? 0 : offObj;      
        var isSearchable =  component.get("v.isSearchable");
        if(isSearchable)
        {    
            helper.getSobjectFilteredRecords(component, offObj,false);          
        }
        else            
        {
            helper.getSobjectRecord(component, offObj, false,true);         
        }    
    },
    
    /**
    * This method is called when user clicks on > Symbol beside each record 
    * and will navigate user to the detail page of the record
    **/
    openRecord : function(component, event, helper) 
    {
        var recID = event.target.id;       
        var device = $A.get("$Browser.formFactor");
        if(device != 'DESKTOP')
        {
            var navEvent = $A.get("e.force:navigateToSObject");
            navEvent.setParams({
                recordId: recID,
                slideDevName: "detail"
            });
            navEvent.fire(); 
        }
        else
        {
            var navEvent = $A.get("e.force:navigateToSObject");
            navEvent.setParams({
                recordId: recID,
                slideDevName: "detail"
            });
            navEvent.fire(); 
        }           
    }

    

})