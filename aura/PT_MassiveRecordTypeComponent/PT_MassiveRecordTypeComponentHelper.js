({
	// this fuction invokes the recordTypeSelection method in the Apex controller PT_RequestRecordTypeController 
	// to fetch record type details
    queryRecordType : function(component) {
        //debugger;
        var action = component.get("c.getRecordTypeDetails");
        var rcdTypeStrind = component.get("v.rcdTypeName");
        action.setParams({
            	 "rcdTypeName": rcdTypeStrind
        })
        //debugger;
        action.setCallback(this,function(response){
           var State = response.getState();
            if(response.getReturnValue() != null && response.getReturnValue().length > 0){
                component.set("v.requestRecordTypes", response.getReturnValue());
                document.getElementById("extensionPopup").style.display = "block";
                component.set("v.rcdTypeListflag",true);
            	//debugger;
                component.set("v.Spinner", false);
            }
         });
		$A.enqueueAction(action);
        //debugger;
		component.set("v.Spinner", true);
    },
    // this fuction invokes the recordTypeSelection method in the Apex controller PT_RequestRecordTypeController 
	// to fetch record type details
    queryMasRecordType : function(component) {
        //debugger;
        var action = component.get("c.getMasRecordTypeDetails");
        //debugger;
        action.setCallback(this,function(response){
           var State = response.getState();
            //alert(response.getReturnValue());
            if(response.getReturnValue() != null && response.getReturnValue().length > 0){
                component.set("v.massiveRecordTypes", response.getReturnValue());var masrecType = component.get("v.massiveRecordTypes");
                component.set("v.masrcdTypeContributi",masrecType[0].Id);
                var masrecTypeRic = component.get("v.massiveRecordTypes");
                component.set("v.masrcdTypeRicheste",masrecTypeRic[1].Id);component.set("v.Spinner", false);
            }
         });
		$A.enqueueAction(action);
    },
})