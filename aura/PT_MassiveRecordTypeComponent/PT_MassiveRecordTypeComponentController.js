({
	// Queries Record type details from srver based on the attribute value set at the component lebel
    doInit : function(component, event, helper) {
       //debugger;	
       helper.queryRecordType(component);
       helper.queryMasRecordType(component);
            
	},
    // this method sets the attribute value on selection of a specific record type radio button. This attribute will
    // be passed as input attribute to next Massive Insertion Component(MIC).
    updateRadioValue:function(component,event,helper){
        //debugger;
        var selected = event.getSource().get("v.value");
        component.set("v.recordId",selected);
        component.set('v.recordIdMas',selected);
        var recType = component.get("v.requestRecordTypes");
        var masrecTypeContributiIDVal = component.get("v.masrcdTypeContributi");
        var masrecTypeRichesteIDVal=component.get("v.masrcdTypeRicheste");
        for(var i=0;i<recType.length;i++){
            if(recType[i].Id===selected){
                if(recType[i].Name==="E01" || recType[i].Name==="Conferma_A03_N01_N02_S02_VL2_MC1"){
                      component.set("v.recordId",masrecTypeContributiIDVal);
                      component.set("v.tipoRic",recType[i].Name);//line added by Priyanka
                }
                else{
                    component.set("v.recordId",masrecTypeRichesteIDVal);
                    component.set("v.tipoRic",recType[i].Name);//line added by Priyanka
                }
            }
        }
		 
        if(selected!== ''){
         component.set("v.buttonvisible",true);    
        }
    },
    hideModal : function(component, event, helper) {
        document.getElementById("extensionPopup").style.display = "none";
        //debugger;
    },
    //On click of Continue button, user is navigated to Request MIC component page which displays the list of Request
    //MFI components to complete request creation.
    navigateToRIC : function(component, event, helper) {
        component.set("v.renderRIC",true);
        var test = component.get("v.renderRIC");
        //debugger;		
	},

    cancel : function(component, event, helper) 
    {
        //Navigates user back to community home page on click of cancel button.
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");        
    },
    
})