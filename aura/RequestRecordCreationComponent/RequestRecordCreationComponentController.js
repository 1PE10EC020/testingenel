({

    doInit : function(component, event, helper) {
      /*  debugger;
        console.log('start****');
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'recordId') { //lets say you are looking for param name - firstName
                sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
            }
        }
        component.set("v.recordId",sParameterName[1]);
        var value=component.get("v.recordId");
        debugger;
        
        helper.initilizePicklistValues(component, event, helper);
        
        var action = component.get("c.renderRFIComponents");
        debugger;
        action.setParams({
            recordTypeName : value,
        });
        action.setCallback(this,function(a){
            var state = a.getState();
            debugger;
            if(state == "SUCCESS"){
                var retValue = a.getReturnValue();
                console.log('retValue' +retValue);
                component.set("v.checkRFIVisibility",retValue);
                document.getElementById("requestPopup").style.display = "block";
               
            }    
        });
        $A.enqueueAction(action);      */  
	},
    
	create : function(component, event, helper) {
        debugger;
        
        var errorMsg_ven = helper.validateDatiVenditoreRFI(component,helper); 
        console.log('errorMsg' +errorMsg_ven);
        if(!$A.util.isEmpty(errorMsg_ven))
        {
            console.log('errorMsg' +errorMsg_ven);
            helper.throwErrorMsg(component,errorMsg_ven,helper) ;
            return;
        } 
        
        helper.validateDatiIntestatarioRFI(component,event,helper);
        
        
        var errorMsg_con = helper.validateDatiContrattualiRFI(component,helper); 
        console.log('errorMsg' +errorMsg_con);
        if(!$A.util.isEmpty(errorMsg_con))
        {
            console.log('errorMsg' +errorMsg_con);
            helper.throwErrorMsg(component,errorMsg_con,helper) ;
            return;
        }
        
        var errorMsg_dis = helper.validateDisalimentabileRFI(component,helper); 
        console.log('errorMsg' +errorMsg_dis);
        if(!$A.util.isEmpty(errorMsg_dis))
        {
            console.log('errorMsg' +errorMsg_dis);
            helper.throwErrorMsg(component,errorMsg_dis,helper) ;
            return;
        } 
        var errorMsg_dis = helper.validateDatiMisuraRFI(component,helper); 
        console.log('errorMsg' +errorMsg_dis);
        if(!$A.util.isEmpty(errorMsg_dis))
        {
            console.log('errorMsg' +errorMsg_dis);
            helper.throwErrorMsg(component,errorMsg_dis,helper) ;
            return;
        } 
        
        var preValError = helper.checkPrevalidationErrors(component,helper); 
        return; 
        
        var request = component.get("v.request");
		var action = component.get("c.createRecord");
        //helper.queryRecordType(component);
        
         action.setParams({
            request : request,
            // requestTwo:requestEventParam             
        });
        
        action.setCallback(this,function(a){
            var state = a.getState();
            var response = a.getReturnValue();
            debugger;
            if(state == "SUCCESS"){
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.Id,
                    "slideDevName": "detail"
                });
                navEvt.fire();
             }
        });
        $A.enqueueAction(action);

	},
	cancel : function(component, event, helper) 
    {
        window.location.href = "https://four420-openkbms.cs86.force.com/s/";        
    },

})