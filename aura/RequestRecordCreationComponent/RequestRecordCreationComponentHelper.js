({
	validateDatiVenditoreRFI : function(component,helper) {
        debugger;
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        /*var strRagionesocialeCod = requestObj.DescrTraderEstesa__c;
        var strContrattodidispacciamento = requestObj.DescrDispaccEstesa__c; */
        var strCodiceRichiestaVenditore = requestObj.IdRichTrader__c;
        debugger;
        
        /*if($A.util.isEmpty(strRagionesocialeCod)){
            return 'Il campo Ragione sociale - Cod è obbligatorio';
        }		
        if($A.util.isEmpty(strContrattodidispacciamento)){
            return 'Il campo Contratto di dispacciamento è obbligatorio';
        } */	
        if($A.util.isEmpty(strCodiceRichiestaVenditore)){
            console.log('Error Message :: DatiVenditoreRFI Inside IF');
            return 'Il campo Codice Richiesta Venditore è obbligatorio';
        }
        return errMsg;
    },
    
    
    validateDatiIntestatarioRFI : function(component, event, helper) {
		debugger;       
        
        
        var compEvent = $A.get("e.c:RequestIptypeEvent");//component.getEvent("compEvent");
        compEvent.setParams({
            	 });
        debugger;
        compEvent.fire();        
	},
    
    validateDatiContrattualiRFI : function(component,helper) {
		debugger;
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        var strTipologiadiconnessione = requestObj.IdConnessione__c;
        var strTipologiacontrattorichiesta = requestObj.IdTipoContr__c;
        var strTensioneFaserichiesta = requestObj.Tensione__c;
        var strUsoEnergia = requestObj.IdUsoEnergia__c;
        var strStagionaleRicorrenteatto = requestObj.FlagStagRicAtto__c;
        var strSettoreIndustrialeMerceologico = requestObj.IdSettore__c;
        var strAutocertFormContServdiConn = requestObj.AutocertContrConn__c;
        
        debugger;
        
        if($A.util.isEmpty(strTipologiadiconnessione)){
            return 'Il campo Tipologia di connessione è obbligatorio';
        }
        if($A.util.isEmpty(strTipologiacontrattorichiesta)){
            return 'Il campo Tipologia contratto richiesta è obbligatorio';
        }
        if($A.util.isEmpty(strTensioneFaserichiesta)){
            return 'Il campo Tensione/Fase richiesta è obbligatorio';
        }
        if($A.util.isEmpty(strUsoEnergia)){
            return 'Il campo Uso Energia è obbligatorio';
        }
        if($A.util.isEmpty(strStagionaleRicorrenteatto)){
            return 'Il campo Stagionale Ricorrente in atto è obbligatorio';
        }
        if($A.util.isEmpty(strSettoreIndustrialeMerceologico)){
            return 'Il campo Settore Industriale/Merceologico è obbligatorio';
        }
        if($A.util.isEmpty(strAutocertFormContServdiConn)){
            return 'Il campo Autocertificazione Formalizzazione Contratto Servizio di Connessione è obbligatorio';
        }
        return errMsg;
    },
    
    validateDisalimentabileRFI : function(component,helper) {
		debugger;
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        var strDisalimentabile = requestObj.FlagDisalimentabile__c;
        debugger;
        if($A.util.isEmpty(strDisalimentabile)){
            return 'Il campo Disalimentabile è obbligatorio';
        }
        return errMsg;
    },
    throwErrorMsg : function(component,errMsg,helper) 
    {
        console.log('Error Message :: ' + errMsg);    
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
			resultsToast.setParams({"title": "Error","message": errMsg,"type":"error"});
			resultsToast.fire();
        }            
    },
    initilizePicklistValues :function(component, event, helper)
    {
        // get all the picklist from  the fields.
		var action = component.get("c.getRichiestaPicklistValues");   
        debugger;
        action.setCallback(this, function(a) 
		{     
			var returnValue = a.getReturnValue();   
            // Check if server response is null.
            if(!$A.util.isEmpty(a.getReturnValue()))
            {
                // initlize Richiesta picklist values
                var pickListValue = a.getReturnValue().pickListValues;
                console.log(">>>> Picklist Value :" + pickListValue);
                debugger;
                component.set("v.picklistValues",pickListValue);
                console.log(">>>> Picklist attribute Values :" + component.get("v.picklistValues"));
                
            }    
        });
        $A.enqueueAction(action);
    },
    
    checkPrevalidationErrors : function(component,helper) {
      debugger;
      var errMsg = null;
      var request = component.get("v.request");
      var action = component.get("c.PT_Prevalidations");
       
      action.setParams({
                "request" : request, 
      });
      
       action.setCallback(this,function(response){
       		var state = response.getState();   
           if(state === "SUCCESS"){
               errMsg = response.getReturnValue();
               console.log('Prevalidation errMsg' + errMsg);
               if(!$A.util.isEmpty(errMsg))
        	{
                console.log('preValError : ' +errMsg);
                helper.throwErrorMsg(component,errMsg,helper) ;
                return;
        	}
           }   
           return errMsg;
        });
		$A.enqueueAction(action);        
	}, 
    validateDatiMisuraRFI : function(component,helper) {
        debugger;
        var errMsg = null;
        var requestObj = component.get('v.request');
        var strEnergiaAttMonorario = requestObj.EnergiaAttMonorario__c;
        if($A.util.isEmpty(strEnergiaAttMonorario)){
            return 'Il campo Energia Attiva Monorario è obbligatorio';
        }
        return errMsg;
    },
})