({
    doInit: function(component, event, helper) {
        helper.getIntialization(component);
        helper.getEmpList(component);
        var action = component.get("c.tiporichValues");
        action.setCallback(this, function(response) {
            component.set("v.lstOftiporich", response.getReturnValue());
            console.log('response'+response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    
    search: function(component, event, helper) {
        //PREV_REP_01 Validation
        /*  var ERR_PREV_REP_01 = helper.validatePREV_REP_01(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_REP_01)) {
            helper.throwErrorMsg(component, ERR_PREV_REP_01, helper);
            return;
        }*/
        //Display Search records if no Validation
        helper.getListOfRecords(component);
        
        
    },
    //For the cancel button
    cancel: function(component, event, helper) {
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");
    },
    
    //For the navigating into Record
    navigateToRecord: function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: event.target.id,
            slideDevName: "detail"
        });
        navEvent.fire();
    }
    
    
})