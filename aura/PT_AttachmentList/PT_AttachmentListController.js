({
	doInit : function(component, event, helper) {
        alert(component.get("v.recordId"));
		  var controllerValues = component.get("c.attachmentDetails");
          controllerValues.setParams({
           strObjectName : component.get("v.objectName"),
           fieldname : component.get("v.FieldAPIs"),
           parentId :component.get("v.recordId")
              
           });
        controllerValues.setCallback(this, function(response) {
                    var state = response.getState();
           // alert(state);
                    if (state === "SUCCESS") {
                        var responseData = response.getReturnValue();
                        component.set("v.RecordsList",responseData);
           				component.set("v.attachId", responseData.Id);
                       }
                });
                $A.enqueueAction(controllerValues);    
          
    }
     
    
})