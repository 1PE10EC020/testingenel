({ 
		doInit : function(component, event, helper) {
      //      alert("debugger");
        //Send LC Host as parameter to VF page so VF page can send message to LC; make it all dynamic
        component.set('v.lcHost', window.location.hostname);
           // debugger;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName"); 
      //  alert("Record Type Name"+recTypeName);
         if(recTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03")){      
        	component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_M03") );
         }
         if(recTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1")){      
        	component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_GD1") );
         }
         if(recTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2")){      
        	component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_SP2") );
         }
       // var frameSrc = '/apex/UploadFilePage?id=' + component.get('v.recordId') + '&lcHost=' + component.get('v.lcHost');
       // var frameSrc = 'https://edistribuzione--four420--c.cs86.visual.force.com/apex/UploadFilePage?id=a0K7E0000014tj5'+ '&lcHost=' + component.get('v.lcHost');
       //WORKING var frameSrc = 'https://edistribuzione--four420--c.cs86.visual.force.com/apex/UploadFilePage?'+ '&lcHost=' + component.get('v.lcHost'); //
        var frameSrc = 'https://edistribuzione--four420--c.cs86.visual.force.com/apex/UploadFilePage?recType='+ component.get("v.recordTypeName") + '&lcHost=' + component.get('v.lcHost');
        console.log('frameSrc:' , frameSrc);
        component.set('v.frameSrc', frameSrc);
	// var testStr = "https://www.web2pdfconvert.com/download?path=0e929557-eff8-4c4d-9cd9-639ae3334617www-google-com.pdf";
      //  component.set('v.myURL',testStr);	
        //	alert('vfhost'+event.data.vfHost);
        //Add message listener
       // var uploadTxt = component.find('uploadFileButton').get('v.label');
		//component.set('v.testStr',uploadTxt);
            ///////////message listener
           
            var recordTypeName = {
                    
                    "recordTypeName" : recTypeName
                };
   /*         alert('Record Type Name variable'+recordTypeName);
        var vframename = component.get('v.vfFrame');
		//var iframeval = document.getElementById(vframename);
        var iframe = document.getElementById(vframename);
        alert('iframe@@@@@@'+vframename);
 		var vfWindow = iframe.contentWindow; 
         alert('after vfWindow');
		vfWindow.postMessage(recordTypeName, component.get("v.vfHost")); */
            
        window.addEventListener("message", function(event) {
    //        alert('inside listener at line 22'+event.data.state);

            console.log('event.data:', event.data);

            // Handle the message
            if(event.data.state == 'LOADED'){
                //Set vfHost which will be used later to send message
   //               alert('inside LOADED');
                           component.set('v.vfHost', event.data.vfHost);
                   var vframename = component.get('v.vfFrame');
		//var iframeval = document.getElementById(vframename);
        var iframe = document.getElementById(vframename);
  //      alert('iframe@@@@@@'+vframename);
 		var vfWindow = iframe.contentWindow; 
   //      alert('after vfWindow'+ component.get("v.vfHost"));
		vfWindow.postMessage(recordTypeName, component.get("v.vfHost")); 
     
                //component.set('v.vfHost', event.data.vfHost);
                
            }

            if(event.data.state == 'uploadFileSelected'){
//                alert('inside upload selected');
            /*    alert('uploadFileSelected name'+event.data.fname);
                if(event.data.fname != "")
                {
                    var messageFile = {
            			"fileselected" : true,
           			};
                    alert('messageFile'+messageFile);
                     messageFile.origin = window.location.hostname;
        			 var vfWindow = component.find("vfFrame").getElement().contentWindow;
       				 vfWindow.postMessage(messageFile, component.get("v.vfHost"));
                }
           /*     component.find('uploadFileButton').set('v.label','Upload');
                component.set('v.testStr','Upload');
                component.find('uploadFileButton').set('v.disabled', false);
                component.find('uploadicon').set('v.disabled', true);*/
                //var frameSrc = 'https://edistribuzione--four420--c.cs86.visual.force.com/apex/UploadFilePage?id=a0K7E0000014qTD'+ '&lcHost=' + component.get('v.lcHost');
        		//alert('frameSrc:' , frameSrc);
        		//component.set('v.frameSrc', frameSrc);

            }
            
            
            if(event.data.state == 'formatmismatched'){
                
                alert('inside LC format mismatched -------------------->');
                component.set('v.showMessage',true);
                $A.createComponents([
                        ["markup://ui:message",{
                            "body" : event.data.message,
                            "severity" : event.data.messageType,
                        }]
                    ],
                    function(components, status, errorMessage){
                        if (status === "SUCCESS") {
                            var message = components[0];
                            alert('messageinside LC ++++++++++++++'+message);
                            // set the body of the ui:message to be the ui:outputText
  
                            component.find('uiMessage').set("v.body", message);
             //               component.find('uploadFileButton').set('v.disabled', false);
                           // component.set('v.showMessage',true);
                        }
                       
                    }
                );
                
            }
            
            if(event.data.state == 'formatmatched'){
                $A.createComponents([
                        ["markup://ui:message",{
                            "body" : event.data.message,
                            "severity" : event.data.messageType,
                        }]
                    ],
                    function(components, status, errorMessage){
                        if (status === "SUCCESS") {
                            var message = components[0];
                            alert('messageinside LC ++++++++++++++ MATCHED'+message);
                            // set the body of the ui:message to be the ui:outputText
                            component.find('uiMessage').set("v.body", message);
                            component.set('v.showMessage',false);
                //            component.set('v.showMessage',false);
             //               component.find('uploadFileButton').set('v.disabled', false);
                        }
                       
                    }
                );
                
            }

            if(event.data.state == 'fileUploadprocessed'){
                var uiMessage = component.find('uiMessage');

                //Disable Upload button until file is selected again
  //              alert('inside file processed');

       //         component.find('uploadFileButton').set('v.disabled', true);
       //         component.find('uploadicon').set('v.disabled', true);
        //        component.find('uploadFileButton').set('v.label','Or drop files');
                $A.createComponents([
                        ["markup://ui:message",{
                            "body" : event.data.message,
                            "severity" : event.data.messageType,
                        }]
                    ],
                    function(components, status, errorMessage){
                        if (status === "SUCCESS") {
                            var message = components[0];
                            // set the body of the ui:message to be the ui:outputText
                            component.find('uiMessage').set("v.body", message);
             //               component.find('uploadFileButton').set('v.disabled', false);
              				component.set('v.testStr','Or drop files');
                        }
                        else if (status === "INCOMPLETE") {
                            console.log("No response from server or client is offline.")
                            // Show offline error
                        }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                    }
                );
            }
        }, false);
    },
   
    
   Message: function(component, event, helper) {
        var recordId = event.getParam("responseid");
       alert('inside message upload component'); 
       component.set('v.recordId',recordId);
        //Clear UI message before trying for file upload again
    //    alert('inside message'+recordId);
        
      
  //      var strUpload = component.find('uploadFileButton').get('v.label');
   //     alert('strUpload'+strUpload);
      /*  if(strUpload != 'Upload'){
            return;
        }*/
  //      component.find('uploadFileButton').set('v.label','Uploading...');
        component.set('v.testStr','Uploading...');
   //     component.find('uploadFileButton').set('v.disabled', true);
    //    component.find('uploadicon').set('v.disabled', true);
       // component.set('v.showSpinner',true);
        component.find('uiMessage').set("v.body",[]);
		alert('ui Message body'+component.find('uiMessage'));
        //Prepare message in the format required in VF page
        var message = {
            "uploadFile" : true,
            "recordId" : recordId
        } ;
        
  //  alert("message"+message);
        //Send message to VF
   //    alert("inside Send message"+ JSON.stringify(message));
        helper.sendMessage(component, helper, message);
    }
    
})