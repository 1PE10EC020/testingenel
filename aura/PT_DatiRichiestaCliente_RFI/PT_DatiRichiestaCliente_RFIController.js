({
    doinit: function(component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName', recType.Name);
        var recTypeName = component.get("v.recordTypeName");
        helper.filterPicklistValues(component, event, helper, recTypeName, 'TipologiaRichiesta__c', 'v.tipoRich', '', '', '', '');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'CorrettCompl_Dati__c', 'v.corr', '', '', '', '');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'RichiestaNuovoTentativo__c', 'v.richnew', '', '', '', '');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'Appuntamento_cl__c', 'v.Appc', '', '', '', '');
         if(recTypeName==='PT_M01'){
        	component.find("TipologiaRichiesta__c").set("v.value","M01");
             component.set("v.check","true");
        }
        if(recTypeName==='PT_M02' ){
        	component.find("TipologiaRichiesta__c").set("v.value","M02");
             component.set("v.check","true");
        }
        if(recTypeName==='PT_M03' ){
            
        	component.find("TipologiaRichiesta__c").set("v.value","M03");
            var test3=component.find("TipologiaRichiesta__c").get("v.value");
            alert(test3);
            component.set("v.check","true");
        }
        var tipolop = component.find("TipologiaRichiesta__c").get("v.value");
        console.log('tipolop==>'+tipolop);
    },

    onCorrRadio: function(component, event, helper) {
        //var selected = event.target.value;
        var selected = event.getSource().get("v.value");
        //alert(selected);
        component.set("v.requestObj.CorrettCompl_Dati__c", selected);
        if (selected === "S") {
            component.set("v.flagElencoDati", "true");
        }
        if (selected === "N") {
            component.set("v.flagElencoDati", "false");
        }

    },

    onRichRadio: function(component, event, helper) {
       //var selected = event.target.value;
       var selected = event.getSource().get("v.value");
        if (selected !== "") {
            if (selected === "S") {
                component.set("v.requestObj.RichiestaNuovoTentativo__c", "S");
            }
            if (selected === "N") {
                component.set("v.requestObj.RichiestaNuovoTentativo__c", "N");
            }
        }

    },

    onAppRadio: function(component, event, helper) {
        //var selected = event.target.value;
        var selected = event.getSource().get("v.value");
        if (selected !== "") {
            if (selected === "S") {
                component.set("v.requestObj.Appuntamento_cl__c", "S");
            }
            if (selected === "N") {
                component.set("v.requestObj.Appuntamento_cl__c", "N");
            }
        }

    }

})