({
 getIntialization : function(component, event, helper) {
     
     var cod = component.get("v.recordId");
     console.log('codmain==>'+cod);
     
     var recordType = component.get("v.recordType");
     console.log('recordTypeMain--==>'+recordType);
    /*var controllerValues = component.get("c.Costiobjectquery");
     controllerValues.setParams({
                  objid :component.get("v.recordId")
          });
     controllerValues.setCallback(this, function(response){
					var state = response.getState();
         			if (state === "SUCCESS"){
                         var responseData = response.getReturnValue(); 
                        	if(responseData != null){
                               component.set("v.RecId", responseData);
                        }
                    }    
                });
      
        $A.enqueueAction(controllerValues);	*/
 },
     createRequest : function(component,helper) {
        var request = component.get("v.request");
        var action = component.get("c.updatefields");
         
         
     },
    
    
    
    getSessionId : function(component) {
        var action = component.get("c.getSessionInfo");
        //setting method parameters 
        action.setParams
         ({ 
         });
        //setting callback function
        action.setCallback(this,function(response){
            // get the request status
            var status = response.getState();
            // get the response
            var result = response.getReturnValue();
            
                var session_id ;
            //checking the request status
            if(status==='SUCCESS')
            {
            	if (result == null||typeof(result)=='undefined')
            	{

                	//component.set("v.contact_user",false);
                	this.getContactAccountId(component);
            	}
            	else
                {
                	session_id= result;
                    component.set("v.sessionId ",session_id);
                    this.getContactAccountId(component);
                }
            }
            });
        	$A.enqueueAction(action);
    },   
    /*Author : Gopinath Perumal
     * Method Name : getContactAccountId
     * Parameter : component
     * Return type : NA
     * Description : This method is used to call Apex server method 
     * getContactId and gets the currently logged in users detail
     * Version : 1.0
     */
    getContactAccountId : function(component) {
        var btntype = component.get("v.btnType");
        var btnurl = $A.get("$Label.c.Form_Assembly_URL");
        var btnid = component.get("v.btnURL"); 
        var session_id = component.get("v.sessionId");
        var api_url=$A.get("$Label.c.API_URL");
        //getContactId controller method
        var action = component.get("c.getContactId");
        //setting callback function
        action.setCallback(this,function(response){
            // get the request status
            var status = response.getState();
            
            //checking the request status
            if(status==='SUCCESS')
            {
                // get the response
                var result = response.getReturnValue();
                var contact_id = result.ContactId;
                var AccntId = result.AccountId;
                var user_id=result.Id;
                var form_link = btnurl + "/" + session_id + "/" + user_id + "/" + btnid +
                        "/" + api_url;
              
                if(btntype === 'OwnershipChange' && contact_id !== undefined)
                {
                    form_link += '?cont_id='+contact_id;
                    component.set("v.btnURL",form_link);
                }
                else if(btntype === 'OwnershipChange' && contact_id === undefined)
                {
                    component.set("v.showBtn",false);
                }
            }});
        $A.enqueueAction(action);
    },
    
    /*Author : Gopinath Perumal
     * Method Name : getCompanyAdminDetails
     * Parameter : component
     * Return type : NA
     * Description : This method is used to call Apex server method 
     * getContactId and gets the currently logged in users detail
     * Version : 1.0
     */
    
    getCompanyAdminDetails : function(component) {
        var caseItemId = component.get("v.caseItemId");
        //getContactId controller method
        var action = component.get("c.checkCompanyOwner");
        
        action.setParams({
            "caseId": caseItemId    
        })
        
        //setting callback function
        action.setCallback(this,function(response){
            // get the request status
            var status = response.getState();
            
            //checking the request status
            if(status==='SUCCESS')
            {
                // get the response
                var result = response.getReturnValue();
                if(result !== undefined)
                {
                    component.set("v.showBtn" ,result); 
                }
                else
                {
                    component.set("v.showBtn" ,false); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    /*Author : Gopinath Perumal
     * Method Name : getCompanyAdminAccntId
     * Parameter : component
     * Return type : NA
     * Description : This method is used to call Apex server method 
     * to check if the currently logged in user is company admin or not
     * Version : 1.0
     */
    getCompanyAdminAccntId : function(component)
    {
    //checkCompanyAdminUIVisibility controller method
        var action = component.get("c.checkCompanyAdminUIVisibility");
        
        // this method requires no parameters
        action.setParams({
        })
        
        //setting callback function
        action.setCallback(this,function(response){
            // get the request status
            var status = response.getState();
            
            //checking the request status
            if(status==='SUCCESS')
            {
                // get the response
                var result = response.getReturnValue();
                if(typeof(result) !== undefined && result !== null)
                {
                    var adminAccntId = result;
                    component.set("v.showBtn",true);  
                    var manageContactUrl = component.get("v.btnURL");
                    //manageContactUrl = manageContactUrl+'?accntId='+adminAccntId;
                    //Updated as part of NCC-2123
                    manageContactUrl = manageContactUrl;
                    component.set("v.btnURL",manageContactUrl);
                }
                else
                {
                    component.set("v.showBtn" ,false); 
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    validateFinalize:function(component){
        component.set("v.showBtn" ,false); 
        var action = component.get("c.renderFinalizeSubmissionBtn");
        action.setParams({ caseRecordId : component.get("v.caseItemId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.showBtn",response.getReturnValue());  
            }
        });
        $A.enqueueAction(action);
    },
    

})