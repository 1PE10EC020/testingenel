({
    doInit : function(component, event, helper) {

        var SObject = component.get('v.SObject');
        var FieldName = component.get('v.fieldName');
        
        var output=SObject[FieldName];
        console.log('**output: ' +output);
        component.set('v.fileContent', output);

        var SObjectName = component.get('v.SObjectName');
        helper.prepareData(component,SObjectName);
        
    },

    SaveIntoFolder : function(component, event, helper){
        

        var fileContent = component.get('v.fileContent');
        console.log('***fileContent: ' +fileContent);

        if(fileContent == undefined || fileContent.length == 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                    "message" : "Nessun file da scaricare"
            });
            toastEvent.fire();
        }
        else{

            var fileName = component.get('v.FileName');
            console.log('**NOME FILE: ' +fileName);
        
            //this will remove the blank-spaces from the title and replace it with an underscore 
           
            //Initialize file format
            var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(fileContent);

            var link = document.createElement("a");

            link.setAttribute('download',fileName);
            //To set the content of the file
            link.href = uri;
            link.click();

        }

    }
})