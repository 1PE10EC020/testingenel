({
	prepareData : function(component, SObjectName) {
        
        var SObject = component.get('v.SObject');
        var FieldName = component.get('v.fieldName');
        /*** - Apex CallBack - start
                *** This callback method is set to retrieve sobject field Four ID  ***/    
        var action = component.get("c.getFourId");
        action.setParams({ 
            "sObjectName": SObjectName
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var stringItems = response.getReturnValue();
                component.set('v.fourid', stringItems);
                console.log('**CAMPO FOUR ID: ' +component.get('v.fourid'));
                
                var idFour = component.get('v.fourid');
                console.log('**idFour:' +idFour);
                component.set('v.FileName', SObject[idFour]);
                var fileName = component.get('v.FileName');
                console.log('**fileName: ' +fileName);
                
                if(FieldName.match(/csv/i)){
                    fileName += ".csv";
                    component.set('v.FileName' , fileName);
                    console.log('**fileName: ' +fileName);
                }
                else if(FieldName.match(/xml/i)){
                    fileName += ".xml";
                    component.set('v.FileName' , fileName);
                    console.log('**fileName: ' +fileName);
                }
                
            } 
            
        });
        
        $A.enqueueAction(action);
        /*** - Apex CallBack - end ***/
	}
})