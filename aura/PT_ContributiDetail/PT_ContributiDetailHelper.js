({
 getIntialization : function(component, event, helper) {
     var cod = component.get("v.recordId");
     var controllerValues = component.get("c.costiobjectquery");
     controllerValues.setParams({
                  objid :component.get("v.recordId")
          });
     controllerValues.setCallback(this, function(response){
					var state = response.getState();
         			if (state === "SUCCESS"){
                         var responseData = response.getReturnValue(); 
                        	if(responseData != null){
                                var costiRecord = responseData.costiObj;
                               component.set("v.RecId", costiRecord.Id);
                               component.set("v.contributi",costiRecord); 
                               component.set("v.recordType", costiRecord.RecordType.Name);
                               component.set("v.QuotaDistanzatotal",responseData.Quota_Distanza_Totale);
                               component.set("v.QuotaPotenzatotal",responseData.Quota_Potenza_Totale);
                               component.set("v.OneriAmministrativitotal",responseData.Oneri_Amministrativi_Totale);
                                
            		debugger;
                    if((costiRecord.RecordType.Name === 'PT_A_Preventivo')
                            &&(costiRecord.Sottostato__c === 'INL.052')
                            &&($A.util.isEmpty(costiRecord.DataAccettazione__c))){
            		         component.set("v.Accettabttnvisible","true");
        			  }
        		   else if((costiRecord.RecordType.Name === 'PT_Predeterminabile')
                             &&(costiRecord.Sottostato__c === 'SSP.006')
                             &&($A.util.isEmpty(costiRecord.DataAccettazione__c))
                             &&($A.util.isEmpty(costiRecord.FlagAccettazione__c))){
                       				component.set("v.Rifiutabttnvisible","true"); 
                        		}
                    	     } 
                         }
                    });
        $A.enqueueAction(controllerValues);	
 }
})