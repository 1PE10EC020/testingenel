({
	doinit : function(component, event, helper) {
        debugger;
    	 var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
           recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO")))
    	{
           component.set("v.DisAttMonora","false");
       	   component.set("v.DisReattMonorario","false");
           component.set("v.DisPotenzaMonorario","false");
                
    	}         		
	},
    checkEnergiaAttivaMonorarioFormat : function(component, event, helper) {
     	debugger;
     	 var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
     	var fieldAuraId = event.getSource().getLocalId();
        var fieldValue = component.find(fieldAuraId).get("v.value");
        //Added logic for 218
        var label = component.find(fieldAuraId).get("v.label");
        
        
            
            // Display specific error message Recordtype PT_AUT & PT_ARS
            if((!$A.util.isEmpty(fieldValue)) &&
              (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))||
               recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS")))){
        	/*	var AttmonoFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_DatiMisuraRFI_REGEX"));
                var validateAttmonoFormat = AttmonoFormat.test(fieldValue);
                var inputCmp = component.find(fieldAuraId);
                if(validateAttmonoFormat){
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Controllare la validità del campo  '+ component.find(fieldAuraId).get("v.label");
                    inputCmp.set("v.errors", [{message:errMsg}]);
                }*/
                debugger;
                 var inputCmp = component.find(fieldAuraId);
                 var strVal = fieldValue.toString();
        		 var lettFormat = /^\d{0,12}(\,\d{0,3})?$/g;
        	     var validateAttmonoFormat = strVal.match(lettFormat); 
                if(validateAttmonoFormat){
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Controllare la validità del campo  '+ component.find(fieldAuraId).get("v.label");
                    inputCmp.set("v.errors", [{message:errMsg}]);
                }
                 
            }
        else{
			inputCmp.set("v.errors", null);
		}
                // Display specific error message Recordtype PT_ACV & PT_AVO
                 
     if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
        recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))){
        
		if(!$A.util.isEmpty(fieldValue))
		{
           if(validateAttmonoFormat){
            inputCmp.set("v.errors", null);
                }else{
                    var errMsg2 = component.find(fieldAuraId).get("v.label") +' non presente ' ;
                    inputCmp.set("v.errors", [{message:errMsg2}]);
                }
        }
		else{
			inputCmp.set("v.errors", null);
		}
     }		          
        },
   
    hideMisuraFields : function(component, event, helper){
        //  If the user chooses Trattamento = "M / Monorario".
        var trattamento = event.getParam("trattamento");
        if(trattamento === "M")
        {
            component.set("v.DisAttMonora","false");
       	    component.set("v.DisReattMonorario","false");
            component.set("v.DisPotenzaMonorario","false");
            component.set("v.DisEnergiaAttF1","true");
            component.set("v.DisEnergiaReattF1","true");
            component.set("v.DisPotenzaF1","true");
            component.set("v.DisEnergiaAttF2","true");
            component.set("v.DisEnergiaReattF2","true");
            component.set("v.DisPotenzaF2","true");
            component.set("v.DisEnergiaAttF3","true");
            component.set("v.DisEnergiaReattF3","true");
            component.set("v.DisPotenzaF3","true");
            component.set("v.DisEnergiaAttF4","true");
            component.set("v.DisEnergiaReattF4","true");
            component.set("v.DisPotenzaF4","true");
            component.find("AttivaFascia1").set("v.value", "");
            component.find("ReattivaFascia1").set("v.value", "");
            component.find("PotenzaFascia1").set("v.value", "");
            component.find("AttivaFascia1").set("v.errors", null);
            component.find("ReattivaFascia1").set("v.errors", null);
            component.find("PotenzaFascia1").set("v.errors", null);
            component.find("AttivaFascia2").set("v.value", "");
            component.find("ReattivaFascia2").set("v.value", "");
            component.find("PotenzaFascia2").set("v.value", "");
            component.find("AttivaFascia2").set("v.errors", null);
            component.find("ReattivaFascia2").set("v.errors", null);
            component.find("PotenzaFascia2").set("v.errors", null);
             component.find("AttivaFascia3").set("v.value", "");
            component.find("ReattivaFascia3").set("v.value", "");
            component.find("PotenzaFascia3").set("v.value", "");
            component.find("AttivaFascia3").set("v.errors", null);
            component.find("ReattivaFascia3").set("v.errors", null);
            component.find("PotenzaFascia3").set("v.errors", null);
            component.find("AttivaFascia4").set("v.value", "");
            component.find("ReattivaFascia4").set("v.value", "");
            component.find("PotenzaFascia4").set("v.value", "");
            component.find("AttivaFascia4").set("v.errors", null);
            component.find("ReattivaFascia4").set("v.errors", null);
            component.find("PotenzaFascia4").set("v.errors", null);  
           }
        // if the user choose Trattamento ="F / A Fasce"
         else if(trattamento === "F")
        {
            component.set("v.DisAttMonora","true");
       	    component.set("v.DisReattMonorario","true");
            component.set("v.DisPotenzaMonorario","true");
            component.set("v.DisEnergiaAttF1","false");
            component.set("v.DisEnergiaReattF1","false");
            component.set("v.DisPotenzaF1","false");
            component.set("v.DisEnergiaAttF2","false");
            component.set("v.DisEnergiaReattF2","false");
            component.set("v.DisPotenzaF2","false");
            component.set("v.DisEnergiaAttF3","false");
            component.set("v.DisEnergiaReattF3","false");
            component.set("v.DisPotenzaF3","false");
            component.set("v.DisEnergiaAttF4","false");
            component.set("v.DisEnergiaReattF4","false");
            component.set("v.DisPotenzaF4","false");
            component.find("AttMonorario").set("v.value", "");
            component.find("ReattMonorario").set("v.value", "");
            component.find("PotenzaMonora").set("v.value", "");
            component.find("AttMonorario").set("v.errors", null);
            component.find("ReattMonorario").set("v.errors", null);
            component.find("PotenzaMonora").set("v.errors", null);
        }
        // if the user choose Trattamento = "null"
             else if(trattamento === null||trattamento ===""){
            component.set("v.DisAttMonora","true");
       	    component.set("v.DisReattMonorario","true");
            component.set("v.DisPotenzaMonorario","true");
            component.set("v.DisEnergiaAttF1","true");
            component.set("v.DisEnergiaReattF1","true");
            component.set("v.DisPotenzaF1","true");
            component.set("v.DisEnergiaAttF2","true");
            component.set("v.DisEnergiaReattF2","true");
            component.set("v.DisPotenzaF2","true");
            component.set("v.DisEnergiaAttF3","true");
            component.set("v.DisEnergiaReattF3","true");
            component.set("v.DisPotenzaF3","true");
            component.set("v.DisEnergiaAttF4","true");
            component.set("v.DisEnergiaReattF4","true");
            component.set("v.DisPotenzaF4","true");
            component.find("AttMonorario").set("v.value", "");
            component.find("ReattMonorario").set("v.value", "");
            component.find("PotenzaMonora").set("v.value", "");
            component.find("AttMonorario").set("v.errors", null);
            component.find("ReattMonorario").set("v.errors", null);
            component.find("PotenzaMonora").set("v.errors", null);
            component.find("AttivaFascia1").set("v.value", "");
            component.find("ReattivaFascia1").set("v.value", "");
            component.find("PotenzaFascia1").set("v.value", "");
            component.find("AttivaFascia1").set("v.errors", null);
            component.find("ReattivaFascia1").set("v.errors", null);
            component.find("PotenzaFascia1").set("v.errors", null);
            component.find("AttivaFascia2").set("v.value", "");
            component.find("ReattivaFascia2").set("v.value", "");
            component.find("PotenzaFascia2").set("v.value", "");
            component.find("AttivaFascia2").set("v.errors", null);
            component.find("ReattivaFascia2").set("v.errors", null);
            component.find("PotenzaFascia2").set("v.errors", null);
            component.find("AttivaFascia3").set("v.value", "");
            component.find("ReattivaFascia3").set("v.value", "");
            component.find("PotenzaFascia3").set("v.value", "");
            component.find("AttivaFascia3").set("v.errors", null);
            component.find("ReattivaFascia3").set("v.errors", null);
            component.find("PotenzaFascia3").set("v.errors", null);
            component.find("AttivaFascia4").set("v.value", "");
            component.find("ReattivaFascia4").set("v.value", "");
            component.find("PotenzaFascia4").set("v.value", "");
            component.find("AttivaFascia4").set("v.errors", null);
            component.find("ReattivaFascia4").set("v.errors", null);
            component.find("PotenzaFascia4").set("v.errors", null); 
            }
          },
})