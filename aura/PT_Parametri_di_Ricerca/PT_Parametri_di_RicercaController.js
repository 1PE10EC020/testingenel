({
    doinit : function(component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName', recType.Name);
        var recTypeName = component.get("v.recordTypeName");        
    },
    
    checkpod : function(component, event, helper) {
        var pod = component.find("PodRicerche__c").get("v.value");        
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        if(recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4"))&&pod!==null){        	
                
            var podFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_POD_REGEX"));       
            var validatepodFormat = podFormat.test(pod);
            if(validatepodFormat){
                var inputCmp = component.find("PodRicerche__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Il campo POD inserito non è conforme alla specifiche richieste' ;
                var inputCmp1 = component.find("PodRicerche__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
        else{
             var inputCmp = component.find("PodRicerche__c");
             inputCmp.set("v.errors", null);
        }
 	},
	
	checkeneltel : function(component, event, helper) {
     	
        var eneltel = component.find("EneltelRicerche__c").get("v.value");  
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        if((recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))) && eneltel!==null)
        {        
            var eneltelFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_ENELTEL_REGEX"));
            var validateeneltelFormat = eneltelFormat.test(eneltel);
            
            if(validateeneltelFormat){
                var inputCmp = component.find("EneltelRicerche__c");
                inputCmp.set("v.errors", null);
            }
            else{
                var errMsg = 'Il campo ENELTEL inserito non è conforme alla specifiche richieste' ;
                var inputCmp1 = component.find("EneltelRicerche__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
        else{
            var inputCmp = component.find("EneltelRicerche__c");
            inputCmp.set("v.errors", null);
        }
 	},
	
   
})