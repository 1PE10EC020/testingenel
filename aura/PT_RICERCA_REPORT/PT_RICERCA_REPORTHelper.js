({
 getIntialization : function(component) {
        
   var controllerValues = component.get("c.getWrapper");
     
     controllerValues.setCallback(this, function(response){
					var state = response.getState();
         			var optionsList = [];
         			var optionsList1 = [];
         			var optionsList2 = [];
             		if (state === "SUCCESS"){
                         var responseData = response.getReturnValue(); 
                           component.set("v.wrapperList",responseData)
                           component.find("Venditore").set("v.value", responseData.Venditore);
                        	component.set("v.VendDetails",responseData.Venditore);
                           //component.set("v.lsttipoFlusso", responseData.tipofl);
                           //component.set("v.lstofCategoriaFlusso", responseData.categ);
                           //component.set("v.lstOftiporich", responseData.tipric);
                           component.set("v.lstofprovincia", responseData.province);
                           component.set("v.isVenditore",true); 
                      // alert('responseData---->'+JSON.stringify(responseData));
                        /* var allvalues = responseData.categ;
                         var allvalues1 = responseData.tipofl;
                          var allvalues2 = responseData.tipric;
                        
                        // alert('responseData tiporich---->'+JSON.stringify(allvalues));
                         for (var key in allvalues) {
                            if (allvalues.hasOwnProperty(key)) {
                                optionsList.push({value: key, label: response[key]});
                            }
                        };
                         for (var key in allvalues1) {
                            if (allvalues1.hasOwnProperty(key)) {
                                optionsList1.push({value: key, label: response[key]});
                            }
                        };
                         for (var key in allvalues2) {
                            if (allvalues2.hasOwnProperty(key)) {
                                optionsList2.push({value: key, label: response[key]});                               
                            }
                        };*/
                        //alert('optionsList2'+JSON.stringify(optionsList2));
                        //component.set('v.lstofCategoriaFlusso', optionsList);
                       //alert("options list"+JSON.stringify(component.get('v.lstofCategoriaFlusso')));
                        //component.set('v.lsttipoFlusso', optionsList1);
                        //component.set('v.lstOftiporich', optionsList2);
                        //alert("LIST OF TIPO RICH ::::: "+JSON.stringify(optionsList[0]));
                    }    
                });
      
        $A.enqueueAction(controllerValues);	
	},
    
    
    initilizePicklistValues :function(component, event, helper)
    {
       // debugger;
        
        var requestObj = component.get('v.request'); 
        var action = component.get("c.getAllRichiestaPicklistValuesCntr");
       
        //alert('inside');		
        
        action.setCallback(this,function(response){
            var State = response.getState();
           // debugger;
           // alert(State);
            var allpicklistValues = response.getReturnValue().picklistObjectMap;
            //var picklistCustomSettingList = response.getReturnValue().reqPicklistCustomSetting;
            var picklistCustomSettingMap = response.getReturnValue().reqPicklistCustomSettingMap;
            // alert('repomse'+JSON.stringify(response.getReturnValue().picklistObjectMap));
            component.set('v.picklistValues', allpicklistValues);  
            //component.set('v.picklistCustomSetting', picklistCustomSettingList);
            component.set('v.picklistCustomSettingMap', picklistCustomSettingMap);

            //var desc = ' - ' + recTypeDetail1.Description;
            //component.set("v.rtDesc",desc);
            //helper.setTextDisplay(component,event,helper,RecTypeName);
            //component.set("v.recordTypeName",RecTypeName);
  ///////////          var requestRec = component.get("v.request");
            //requestRec.RecordTypeId = recTypeDetail1.Id;
  ///////////          component.set("v.request",requestRec);
   //////////         component.set("v.Spinner",false);
       helper.filterPicklistValues(component, event, helper,'','IdTipoReport__c','v.lstOftiporich','','','','');
       //helper.filterPicklistValues(component, event, helper,'','CategFlusso__c','v.lstofCategoriaFlusso','IdTipoReport__c','','','');     
       //helper.filterPicklistValues(component, event, helper,'','TipoFlusso__c','v.lsttipoFlusso','CategFlusso__c','','','');     
        });
        $A.enqueueAction(action);
        
    },
    
    
     getDisp : function(component) {
       var action = component.get("c.getDisp");
        action.setCallback(this, function(response) {
            component.set("v.lstOfDisp", response.getReturnValue());
			//alert('getIntialization2'+response.getReturnValue());
        });
    $A.enqueueAction(action);
	},
    getListOfRecords: function(component, event, helper) {
       
        var CMsearch = component.get("v.wrapperList");
       // debugger;
       		CMsearch.TipoRich=component.find("TipoRich").get("v.value");
            CMsearch.Venditore=component.find("Venditore").get("v.value");
            CMsearch.Dispacciamento=component.find("Dispacciamento").get("v.value");
            CMsearch.DataPubblicazioneda=component.find("DataPubblicazioneda").get("v.value");
        	CMsearch.DataPubblicazioneA=component.find("DataPubblicazioneA").get("v.value");
            CMsearch.DataEstrazioneda=component.find("DataEstrazioneda").get("v.value");
            CMsearch.DataEstrazioneA=component.find("DataEstrazioneA").get("v.value");
        	CMsearch.Provincia=component.find("Provincia").get("v.value");
            CMsearch.CategoriaFlusso=component.find("CategoriaFlusso").get("v.value");
            CMsearch.TipoFlusso=component.find("TipoFlusso").get("v.value");
            CMsearch.NomeFile=component.find("NomeFile").get("v.value");
            CMsearch.CodiceRichiesta=component.find("CodiceRichiesta").get("v.value");
            component.set("v.wrapperList",CMsearch);


       var controllerValues = component.get("c.getQueryall");
       controllerValues.setParams({wrapperobj : JSON.stringify(CMsearch),
                                   strObjectName : component.get("v.objectName"),
                                   strFields : component.get("v.FieldAPIs"),
                                   RecordNum : component.get("v.RecordLimit") 
                                    });
       controllerValues.setCallback(this, function(response){
           //debugger;
					var state = response.getState();
          
             		if (state === "SUCCESS"){
                         var responseData = response.getReturnValue(); 
                           if(responseData != null){
                               component.set("v.RecordsList", responseData.dataList);
                               component.set("v.TestRecord", responseData.dataList);
                              // alert('DATALIST'+JSON.stringify(component.get("v.RecordsList")));
                               component.set("v.LabelList", responseData.labelList); 
                              // alert('LABELLIST'+JSON.stringify(responseData.labelList));
                               
                               component.set("v.apiList",responseData.apiList);
                               //alert(component.get("v.apiList"));
                              
                               if(responseData.dataList.length===0){
                                    component.set("v.NoRecords",true);
                                    component.set("v.Spinner",false);
                                    component.set("v.displayRecordList",false);
                                    component.set("v.LimitRec",False);
								 }
                               if(responseData.dataList.length>0){
                                   debugger;
                                    component.set("v.displayRecordList",true);
                                   //alert('displayRecordList'+component.get('v.displayRecordList'));
                                   debugger;
                                  //  component.set("v.NoRecords",false);
                                       //alert('displayRecordList'+component.get('v.displayRecordList'));

                               }
                               var Recordlimit = component.get("v.RecordLimit");
                               if(responseData.dataList.length > Recordlimit ){
                                    component.set("v.LimitRec",true);
									component.set("v.NoRecords",false);	 
                                    component.set("v.Spinner",false);
                               }                         
                             }
                                            
       
                    }
                       component.set("v.Spinner",false); 
                });
        $A.enqueueAction(controllerValues);  
           component.set("v.Spinner",true); 
    },
        //PREV_REP_01 Validation
    validatePREV_REP_01 : function(component,helper) {
        var ErrorMsgLable=$A.get("$Label.c.PT_PREV_REP_01");
        var errMsg = null;
        var Tipo = component.find("TipoRich").get("v.value");
		if($A.util.isEmpty(Tipo)){
           return ErrorMsgLable;
        }
        
        return errMsg;
    },
        
     
    throwErrorMsg : function(component,errMsg,helper) 
    { 
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
			resultsToast.fire();
        }            
    },
    
   filterPicklistValues : function(component, event, helper, RecordTypeName,FieldAPINAME,atbName,ContFieldAPI,ContFieldVal,ContField2API,ContField2Val){
       debugger; 
       var picklistValueSet = new Set();
        var picklistCustomSettingMapTemp = component.get('v.picklistCustomSettingMap');
        var picklistCustomSettingListTemp = picklistCustomSettingMapTemp[FieldAPINAME];
        var allpicklistValuesTemp = component.get('v.picklistValues').pickListValues[FieldAPINAME];             
        
        var x;    
        if(picklistCustomSettingListTemp!==undefined){   
            for(var i = 0; i < picklistCustomSettingListTemp.length; i++) {
                if(ContFieldAPI!=='' && ContFieldVal!==''){
                    if(ContField2API!=='' && ContField2Val!==''){
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal &&
                           picklistCustomSettingListTemp[i].Controlling_Field_2_API__c === ContField2API && picklistCustomSettingListTemp[i].Controlling_Field_2_Value__c === ContField2Val)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }    
                    else{
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }  
                }
                else{    
                    picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c);
                } 
            }
        }
        var picklistValueListFinal = allpicklistValuesTemp.slice(0);
        var j = allpicklistValuesTemp.length;
        var picklistKeyValue = new Array(); //This array is being used to render picklist values & labels on component UI
        var counter = 0;
        for(var k = 0; k < allpicklistValuesTemp.length; k++) {
          //  alert('allpicklistValuesTemp[k] '+allpicklistValuesTemp[k]);
            var picklistvaluesplit = allpicklistValuesTemp[k].split("~",1); // Splits the Picklist string into Value & Label
          //  alert('splitvalue'+splitvalue);
            if(picklistValueSet.has(picklistvaluesplit[0]))//If picklist value is present in the set then add the same in a new array
            {
              //  alert ('inside if');
                picklistKeyValue[counter] = new Array(2);
                picklistKeyValue[counter] = allpicklistValuesTemp[k].split("~");
                counter = counter +1;
            }
            else{
                picklistValueListFinal.splice(k-j,1);
            }
        }
        //alert('array'+picklistKeyValue);
        component.set(atbName, picklistKeyValue);
    }  
    
})