({
    doInit: function(component, event, helper) {
       
      
                
       helper.initilizePicklistValues(component, event, helper);
       helper.getIntialization(component);
       helper.getDisp(component);
        
      // alert('DOINIT PICKLIST VALUES'+ component.get("v.picklistValues"));
       // alert( component.get("v.picklistCustomSettingMap"));
       		component.set("v.istipo","true");
            //component.set("v.iscateflusso","true");            
            //component.set("v.istipoflusso","true"); 
        
  		//helper.filterPicklistValues(component, event, helper,'','IdTipoReport__c','v.lstOftiporich','','','','');
      // helper.filterPicklistValues(component, event, helper, recTypeNAME,'TipoFlusso__c','v.lsttipoFlusso','','','','');
       
},
   
   
    search: function(component, event, helper) {
        //PREV_REP_01 Validation
        var ERR_PREV_REP_01 = helper.validatePREV_REP_01(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_REP_01)) {
            helper.throwErrorMsg(component, ERR_PREV_REP_01, helper);
            return;
        }
         //Display Search records if no Validation
        if ($A.util.isEmpty(ERR_PREV_REP_01)){
            helper.getListOfRecords(component);
           // alert("component"+component.get("v.RecordsList"));
            return;
        }
          
    },
    //For the cancel button
    cancel: function(component, event, helper) {
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");
    },
   
    //For the navigating into Record
    /*navigateToRecord: function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: event.target.id,
            slideDevName: "detail"
        });
        navEvent.fire();
    },*/
    navigateToRecord: function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
       // alert(event.target.id);
        var action = component.get("c.getResponse");
        action.setParams({
           RepId : event.target.id
         });
        action.setCallback(this, function(response) {
            component.set("v.urlLink", response.getReturnValue());

        });
        $A.enqueueAction(action);*/
       /* var key='KeyFile Does Not exist.Select the Record which has Keyfile';
          helper.throwErrorMsg(component, key,helper);
        }*/
    }, 
    onTipologiaChange: function(component, event, helper) {
	var selected = event.getSource().get("v.value");
    var selectedlabel = event.getSource().get("v.label");    
      // alert('selected'+selected);
         if(selected==="B03"||selected==="B01"|| selected==="O09"
            ||selected==="R10"||selected==="U01" ||selected==="BR3" 
            || selected==="BR1"){
             component.find('Dispacciamento').set("v.value","");
              component.set("v.isDispable","true");
             
         }
        else{
           component.set("v.isDispable","false");
         }
        if(selected==="R10"){
             component.set("v.isprovincia","false");            
        }
        else{
          component.set("v.isprovincia","true"); 
          component.find('Provincia').set("v.value","");
        }
        
        
        if(selected==="R10"|| selected==="O09"|| selected==="U01"){
            alert(component.get("v.VendDetails"));
            component.set("v.isRagione","true");            
        }
        else{

          component.set("v.isRagione","false"); 
          //component.find("Venditore").set("v.value",component.get("v.VendDetails"));
         // component.find('Venditore').set("v.value","");
        }
        
        
        
        //alert(selected);
        debugger;
        helper.filterPicklistValues(component, event, helper,'','CategFlusso__c','v.lstofCategoriaFlusso','IdTipoReport__c',selected,'','');     
       if(selected==="X45"||selected==="X53"){
             component.set("v.iscateflusso","false");
            component.set("v.istipoflusso","false");

        }else{
             component.set("v.iscateflusso","true");
             component.find('CategoriaFlusso').set("v.value",""); 
             component.set("v.istipoflusso","true");
             component.find('TipoFlusso').set("v.value","");
            //alert(component.find('TipoFlusso').get("v.value"));
            //alert(component.find('CategoriaFlusso').get("v.value"));
        }
    },
        
        onCatFlussoChange: function(component, event, helper) {
		var selected = event.getSource().get("v.value");
    	var selectedlabel = event.getSource().get("v.label");    
      	//alert('selected'+selected);
        
        helper.filterPicklistValues(component, event, helper,'','TipoFlusso__c','v.lsttipoFlusso','CategFlusso__c',selected,'',''); 
           
       
    }
})