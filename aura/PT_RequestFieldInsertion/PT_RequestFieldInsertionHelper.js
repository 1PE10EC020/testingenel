({
	createRecord: function(component, event, helper) {
        console.log(">>>>> Helper Called");
      component.set("v.isOpen", true);
 
      var action = component.get("c.getRecTypeIdReq");
          console.log(">>>>> action : "+ action);
      var recordTypeLabel = component.find("r0").get("v.value");
      console.log(">>>>> recordTypeLabel : "+ recordTypeLabel);
      action.setParams({
         "recordTypeLabel": recordTypeLabel
  		});
      action.setCallback(this, function(response) {
         var state = response.getState();
         if (state === "SUCCESS") {
            var createRecordEvent = $A.get("e.force:createRecord");
            var RecTypeID  = response.getReturnValue();
            createRecordEvent.setParams({
               "entityApiName": 'PT_Richiesta__c',
               "recordTypeId": RecTypeID
            });
            createRecordEvent.fire();
             
         } else if (state == "INCOMPLETE") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
               "title": "Oops!",
               "message": "No Internet Connection"
            });
            toastEvent.fire();
             
         } else if (state == "ERROR") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
               "title": "Error!",
               "message": "Please contact your administrator"
            });
            toastEvent.fire();
         }
      });
      $A.enqueueAction(action);
   },
})