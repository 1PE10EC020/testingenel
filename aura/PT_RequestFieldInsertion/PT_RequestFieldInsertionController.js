({
      fetchListOfRecordTypes: function(component, event, helper) {
      var action = component.get("c.fetchRecordTypeValuesReq");
          console.log(">>>>>> Record Type : "+ action);
      action.setCallback(this, function(response) {
         component.set("v.lstOfRecordType", response.getReturnValue());
      });
      $A.enqueueAction(action);
   },
    
    textClicked : function(component, event, helper) {
        console.log(">>>>> Component Called");
        helper.createRecord(component, event.target.id);
    },
    
     onGroup: function(cmp, evt) {
         var selected = evt.getSource().get("v.label");
         console.log(">>>>> Selected Record Type : "+ selected);
         var resultCmp = cmp.find("selectId");
         console.log(">>>>> resultCmp : "+ resultCmp);
         resultCmp.set("v.value", selected);
     },
   
     Navigate : function(component, event, helper) {
         $A.createComponent(
            "c:C1",
            {},
            function(newCmp){
                if (component.isValid()) {
                    var body = component.get("v.body");
                    body.push(newCmp);
                    component.set("v.body", body);
                }
            }
        );         
    },
    closeModal: function(component, event, helper) {
      // set "isOpen" attribute to false for hide/close model box 
      component.set("v.isOpen", false);
   },
 
   openModal: function(component, event, helper) {
      // set "isOpen" attribute to true to show model box
      component.set("v.isOpen", true);
   },
})