({
	doinit : function(component, event, helper) {
		var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
    },
    
    onChange : function(component,event,helper){
         var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
     	var fieldAuraId = event.getSource().getLocalId();
        var fieldValue = component.find(fieldAuraId).get("v.value");
            // Display specific error message Recordtype PT_AUT & PT_ARS
            if(!$A.util.isEmpty(fieldValue)){
                var orreFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Ore_Utilizzo_Richiesta_REGEX"));
                var validateOrreFormat = orreFormat.test(fieldValue);
                var inputCmp = component.find(fieldAuraId);
                if(validateOrreFormat){
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Controllare la validità del campo  '+ component.find(fieldAuraId).get("v.label");
                    inputCmp.set("v.errors", [{message:errMsg}]);
                }
    		}
        else{
		    var inputCmp = component.find(fieldAuraId);
			inputCmp.set("v.errors", null);
			}
 	}
})