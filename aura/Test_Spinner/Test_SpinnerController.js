({
	doInit : function(component, event, helper) {
	// work on component init..
       
	},
 
    showSpinner : function(component,event,helper){
      // display spinner when aura:waiting (server waiting)
        component.set("v.toggleSpinner", true);  
      },
    hideSpinner : function(component,event,helper){
   // hide when aura:downwaiting
        component.set("v.toggleSpinner", false);
        
    }
})