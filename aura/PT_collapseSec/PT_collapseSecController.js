({
	toggleCollapse : function(component, event, helper) 
    { 
		helper.toggleCollapseHandler(component, event);
	},
    doInit: function(component) 
    {
       	var existingText = component.get("v.collpaseText"); 
        var container = component.find("containerCollapsable") ;
        var closeIcon = component.find("closeIcon") ;
        var openIcon = component.find("openIcon") ;
        var expandSec = component.get("v.expandSec");
        //debugger;
        if(expandSec)
        {
            $A.util.addClass(container, 'slds-hide');  
			$A.util.addClass(closeIcon, 'slds-hide');    
            component.set("v.collpaseText",'chevrondown');
            
        }
        else
        {
			 $A.util.addClass(openIcon, 'slds-hide');   
            component.set("v.collpaseText",'chevronright'); 
        }    
    }    
})