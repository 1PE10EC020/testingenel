({
	doinit : function(component, event, helper) {
    	 var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
     
    },
    
     checkAttivaCliente1 : function(component, event, helper) {
         helper.checkformat(component, event, helper,"Att_F1__c");
    },
    
    checkAttivaCliente2 : function(component, event, helper) {
        helper.checkformat(component, event, helper,"Att_F2__c");
    },
    
    checkAttivaCliente3 : function(component, event, helper) {
        helper.checkformat(component, event, helper,"Att_F3__c");
    },
    
    checkReattivaCliente1 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F1__c");
    },
    
      checkReattivaCliente2 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F2__c");
    },
    
    checkReattivaCliente3 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F3__c");
    },
    
    checkPotCliente1 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F1__c");
    },
    checkPotCliente2 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F2__c");
    },
    checkPotCliente3 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F3__c");
    },
    
})