({
	doInit : function(component, event, helper) {
    
        helper.renderfields(component, event, helper);

        helper.initializePagesection(component, event, helper);
          
	},
  toggleCollapse : function(component, event, helper) 
  { 
    helper.toggleCollapseHandler(component, event);
  }
})