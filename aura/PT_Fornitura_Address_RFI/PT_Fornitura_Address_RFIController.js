({
	fireComponentEvent : function(component, event, helper) {
		var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            //"message" : component.get("v.request.Cap_pod__c") 
            "requestEventObj" : component.get("v.request") 
        });
        debugger;
        cmpEvent.fire();
	},
    showModal : function(component, event, helper) {
        debugger;
        var addt= component.get("v.addressType");
        document.getElementById("backGroundSectionId").style.display = "block";
		document.getElementById(addt).style.display = "block";
    },
    hideModal : function(component, event, helper) {
        debugger;
        var errorMsg = null;
        if(((component.get("v.addressType")==="Esazione") || (component.get("v.addressType")==="SedeLegale"))){
            var copyFromAddress = component.find("copyFromForniture").get("v.value");
            if($A.util.isEmpty(copyFromAddress)){
                    errorMsg = 'Seleziona un valore.';
                    var inputCmp = component.find("copyFromForniture");
                    inputCmp.set("v.errors", [{message:errorMsg}]);
                    
             }
             else if(copyFromAddress==="si"){
                    var inputCmp = component.find("copyFromForniture");
                    inputCmp.set("v.errors", null);            
             }
             else if(copyFromAddress==="no"){
             	errorMsg = helper.validateAddressFields(component, event, helper);    
             }
        }
        else{
            component.set("v.bypassCodiceViaCheck","false");
            errorMsg = helper.validateAddressFields(component, event, helper);
        }
        console.log('errorMsg' +errorMsg);
        if(!$A.util.isEmpty(errorMsg))
        {
            console.log('errorMsg' +errorMsg);
            $A.enqueueAction(action); 
        }
        
        var requestObj = component.get("v.requestObj");
        var addt= component.get("v.addressType");
        if(addt==="Fornitura"){
            helper.setFornituraFields(component, event, helper);
        }
        else if(addt==="Esazione"){
            helper.setEsazioneFields(component, event, helper);
        }
        else if(addt==="SedeLegale"){
            helper.setSedeLegaleFields(component, event, helper);
        }
        
        component.set("v.displayAddress",false);
        
		document.getElementById("backGroundSectionId").style.display = "none";
		document.getElementById(addt).style.display = "none";
    },
    hideAddressModal : function(component, event, helper) {
		document.getElementById("backGroundAddressSectionId").style.display = "none";
		document.getElementById("newAddressSectionId").style.display = "none";
        document.getElementById("backGroundSectionId").style.display = "block";
		document.getElementById(component.get("v.addressType")).style.display = "block";
    },
    verifyAddress : function(component, event, helper){
        debugger;
        var errorMsg = null;
        component.set("v.bypassCodiceViaCheck","true");
        errorMsg = helper.validateAddressFields(component, event, helper); 
        console.log('errorMsg' +errorMsg);
        if(!$A.util.isEmpty(errorMsg))
        {
            console.log('errorMsg' +errorMsg);
            //helper.throwErrorMsg(component,errorMsg,helper) ;
            //return;
            $A.enqueueAction(action); 
        }
        else
        {
            var action = component.get("c.retriveAddress");
        	debugger;
        	action.setParams({
                comune : component.find("comunePod").get("v.value"), 
                cap : component.find("capPod").get("v.value"), 
                via : component.find("viaPod").get("v.value"), 
                codistat : component.find("codIstatPod").get("v.value")
        	});
        	action.setCallback(this,function(a){
            var state = a.getState();
            debugger;
            if(state == "SUCCESS"){
                var retValue = a.getReturnValue();
                console.log('retValue' +retValue);
                if(!$A.util.isEmpty(retValue)){
                	component.set("v.addressList",retValue);
                    component.set("v.displayAddress",true);
                    document.getElementById("backGroundAddressSectionId").style.display = "block";
					document.getElementById("newAddressSectionId").style.display = "block";
                    document.getElementById("backGroundSectionId").style.display = "none";
					document.getElementById(component.get("v.addressType")).style.display = "none";
                }
                
            }    
        	});
        	$A.enqueueAction(action);  
                    
        }   
        
    },
    selectAddress : function(component, event, helper){
        debugger;
        var requestObj = component.get("v.requestObj");
        var selected = event.target.getAttribute("data-recId");
        
        component.find("PPAAAVVVPod").set("v.value",selected);
        requestObj.Toponimo_pod__c = component.find("toponimoPod").get("v.value");
        requestObj.Via_pod__c = component.find("viaPod").get("v.value");
        requestObj.Numero_Civico_pod__c = component.find("numeroCivicoPod").get("v.value");
        requestObj.Scala_pod__c = component.find("scalaPod").get("v.value");
        requestObj.Piano_pod__c = component.find("pianoPod").get("v.value");
        requestObj.Interno_pod__c = component.find("internoPod").get("v.value");
        requestObj.Provincia_pod__c = component.find("provinciaPod").get("v.value");
        requestObj.Comune_pod__c = component.find("comunePod").get("v.value");
        requestObj.Localita_pod__c = component.find("localitaPod").get("v.value");
        requestObj.Cap_pod__c = component.find("capPod").get("v.value");
        requestObj.CodIstat_pod__c = component.find("codIstatPod").get("v.value");
        requestObj.PPAAAVVV_pod__c = component.find("PPAAAVVVPod").get("v.value");
        
        component.set("v.requestObj",requestObj);
        
        var requestObj = component.get("v.requestObj");
        var toponimo = requestObj.Toponimo_pod__c;
		var via = requestObj.Via_pod__c;        
        var numeroCivico = requestObj.Numero_Civico_pod__c;
        var CAP = requestObj.Cap_pod__c;
        var Comune = requestObj.Comune_pod__c;
        var Provincia = requestObj.Provincia_pod__c;
        var finalAddress = toponimo + " " + via + ", " + numeroCivico + ", " + CAP + ", " + Comune + ", " + Provincia;
        
        
        component.set("v.addressSummary",finalAddress);
        component.set("v.displayAddress",false);
        document.getElementById("backGroundSectionId").style.display = "block";
		document.getElementById(component.get("v.addressType")).style.display = "block";
        document.getElementById("backGroundAddressSectionId").style.display = "none";
		document.getElementById("newAddressSectionId").style.display = "none";
    },
    doinit : function(component, event, helper) {
        debugger;  
        var addt= component.get("v.addressType");
        console.log('addt' +addt);
        if(addt=="Fornitura"){
            component.set("v.isFornituraAddress",true);
        }
        else if(addt=="Esazione"){
            debugger;
            component.set("v.isEsazioneAddress",true);
        	//component.find("nazioneEsaz").set("v.value","IL");    
            
        }
        else if(addt=="SedeLegale"){
            component.set("v.isSedeLegaleAddress",true);
            //component.find("nazioneEsaz").set("v.value","IL");   
        }
        helper.initilizePicklistValues(component, event, helper);
        helper.assignFieldValues(component, event, helper); 
        
        
    },
    renderComuneCodIstat : function(component, event, helper){
    	debugger;
        if(!$A.util.isEmpty(component.find("provinciaPod").get("v.value"))){
        	var action = component.get("c.getComumneCodIstatPicklistValues");
        	debugger;
            
        	action.setParams({
                provinciaPod : component.find("provinciaPod").get("v.value")
        	});
            
        	action.setCallback(this,function(a){
            var state = a.getState();
            debugger;
            
            if(state == "SUCCESS"){
                var retValue = a.getReturnValue();
                console.log('retValue' +retValue);
                if(!$A.util.isEmpty(retValue)){
                	var pickListValue = a.getReturnValue().pickListValues;
                    component.set('v.Comune', pickListValue['Comune_pod__c']); 
                    component.set('v.CodIstat', pickListValue['CodIstat_pod__c']); 
                    component.find("capPod").set("v.value","");
                    component.set("v.comuneReadOnly",false);
        			component.set("v.codiceISTATReadOnly",false);
                }
                
            }    
        	});
        	$A.enqueueAction(action);  
            
            
        }
        else{
            component.set("v.comuneReadOnly",true);
        	component.set("v.codiceISTATReadOnly",true);
        }
	},
    setCAPCodIstat : function(component, event, helper){
        debugger;
        if(!$A.util.isEmpty(component.find("comunePod").get("v.value"))){
        	component.set("v.codiceISTATReadOnly",true);
            var action = component.get("c.setCAPCodIstatFields");
        	debugger;
            
        	action.setParams({
                comunePodVal : component.find("comunePod").get("v.value")
        	});
            
        	action.setCallback(this,function(a){
            var state = a.getState();
            debugger;
            
            if(state == "SUCCESS"){
                var retValue = a.getReturnValue();
                console.log('retValue' +retValue);
                if(!$A.util.isEmpty(retValue)){
                	component.find("capPod").set("v.value",retValue.Name);
                    component.find("codIstatPod").set("v.value",retValue.COD_ISTAT__c);
                    
                   
                }
                
            }    
        	});
        	$A.enqueueAction(action);  
            
            
        }
        else{
            //component.set("v.capReadOnly",true);
        	component.set("v.codiceISTATReadOnly",false);
        }

    },
    setComuneCodIstat : function(component, event, helper){
        debugger;
        if(!$A.util.isEmpty(component.find("codIstatPod").get("v.value"))){
        	component.set("v.comuneReadOnly",true);
            var action = component.get("c.setComuneCodIstatFields");
        	debugger;
            
        	action.setParams({
                codIstatPodVal : component.find("codIstatPod").get("v.value")
        	});
            
        	action.setCallback(this,function(a){
            var state = a.getState();
            debugger;
            
            if(state == "SUCCESS"){
                var retValue = a.getReturnValue();
                console.log('retValue' +retValue);
                if(!$A.util.isEmpty(retValue)){
                	component.find("capPod").set("v.value",retValue.Name);
                    component.find("comunePod").set("v.value",retValue.COMUNE__c);
                }
                
            }    
        	});
        	$A.enqueueAction(action);  
            
            
        }
        else{
            //component.set("v.capReadOnly",true);
        	component.set("v.comuneReadOnly",false);
        }

    },
    renderEsazioneAddressFields : function(component, event, helper){
    	debugger;
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("copyFromForniture").get("v.value")==="no"){
           		component.set("v.displayEsazioneAddressFields","true");
                component.find("nazioneEsaz").set("v.value","IL");    
            }
            else{
                component.set("v.displayEsazioneAddressFields","false");
            }
        }
        else{
                component.set("v.displayEsazioneAddressFields","false");
        }
	},
    checkNazione : function(component, event, helper){
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("copyFromForniture").get("v.value")!=="IL"){
                component.set("v.disableEsazioneAddressFields",true);
                component.set("v.capReadOnly",false);                
            }
            else{
                component.set("v.disableEsazioneAddressFields",false);
            }
        }
    }    
})