({
	getSobjectRecord : function(component, sobjOffset, isnext, updateOffset) {
		var objOffset = sobjOffset; 
        var maxNoOfRecords = component.get('v.maxNoRecords');     
        var sObjFieldApiName = component.get('v.FieldApiList'); 
        var Profiles = component.get('v.Profiles');
        var action = component.get("c.getRecords"); 
        
        /***Apex CallBack - start***/
        action.setParams({
            "sObjOffset": objOffset,
            "fieldApiName" :sObjFieldApiName,
            "maxNoOfRecord" :maxNoOfRecords, 
            "profiles": Profiles

        }); 

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var stringItems = response.getReturnValue();
                component.set("v.sObjectRecords", stringItems.sobjLst); 
                component.set("v.hasNext", stringItems.hasNext);
                var list = component.get('v.sObjectRecords');
                if(list.length == 0){
                    component.set('v.ListIsEmpty', true);
                }
                /*component.set("v.errorMessage", stringItems.errorMessage);
                console.log('**errorMessage: ' +stringItems.errorMessage);
                component.set('v.parentID', stringItems.parentID);*/
            
                if(updateOffset)
                {    
                    if(isnext)
                    {    
                        component.set("v.sObjOffset", (component.get("v.sObjOffset") + maxNoOfRecords));
                    }
                    else
                    {
                        var offObj =  component.get("v.sObjOffset") - maxNoOfRecords;                   
                        offObj =  (offObj <= 0) ? 0 : offObj;
                        component.set("v.sObjOffset",offObj);
                    }   
                }
                //EOF
            }
        });
        $A.enqueueAction(action); 
        /***Apex CallBack - end***/
	}
    
})