({
	doInit : function(component, event, helper) {

		var Profiles = component.get("v.Profiles"); 
        var Fields = component.get("v.Fields");
        var Labels = component.get("v.Labels");
        
        //For table pagination
       /* var firstCol = Labels.slice(0, Labels.indexOf(','));
        var LastCol = Labels.slice(Labels.indexOf(',')+1);
        component.set('v.FirstCol', firstCol);
        component.set('v.FirstRow', firstRow); */
        
        //Replace comma with ;
        var regex = new RegExp(',', 'g');
        //Profiles = Profiles.replace(regex, ';');
        component.set('v.Profiles', Profiles);
        console.log('**profiles: ' +Profiles);
        component.set('v.FieldList', Fields.split(','));
        component.set('v.LabelList', Labels.split(','));
        //end
        component.set('v.FieldApiList', Fields.split(','));
        console.log('*** LabelList:' + component.get('v.LabelList'));
        console.log('*** FieldList:' + component.get('v.FieldList'));
        
        //Call helper method
        var offObj = component.get('v.sObjOffset');         
        helper.getSobjectRecord(component, offObj, true, false);
        
	},
    
    nextRec : function(component, event, helper) 
    {        
        var maxNoOfRecords = component.get('v.maxNoRecords');   
        var offObj =  component.get("v.sObjOffset") + maxNoOfRecords;                           
        
        helper.getSobjectRecord(component, offObj, true,true);         
          
    },
    
    previous  : function(component, event, helper) 
    {
        var maxNoOfRecords = component.get('v.maxNoRecords');   
        var offObj =  component.get("v.sObjOffset") - maxNoOfRecords;                   
        offObj =  (offObj <= 0) ? 0 : offObj;      
       
        
        helper.getSobjectRecord(component, offObj, false,true);         
         
    }
})