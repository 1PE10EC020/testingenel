({
 Accettaclickperform: function(component, event, helper){
       var contributi = component.get("v.contributi");
        var action = component.get("c.updateACCETTAZIONECONTRIBUTI");
        action.setParams({
            contributi : contributi,
        });

        action.setCallback(this,function(response){
            var state = response.getState(); 
            if(state === "SUCCESS"){
                var retValue = response.getReturnValue();
                if(retValue !== null){
                    component.set("v.contributi", retValue);
                     var pageurl = window.location.href;
                     var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                         "url": pageurl
                      });
                    urlEvent.fire();
                    }
                 }
            });
          $A.enqueueAction(action);
     },
 Rifiutaclickperform: function(component, event, helper){
         var contributi = component.get("v.contributi");
        var action = component.get("c.presetRifiutaperform");
        action.setParams({
            contributi : contributi,
        });
        action.setCallback(this,function(response){
            var state = response.getState(); 
            if(state === "SUCCESS"){
                var retValue = response.getReturnValue();
                if(retValue !== null){
                    component.set("v.contributi", retValue); 
                   var pageurl = window.location.href;
                   var urlEvent = $A.get("e.force:navigateToURL");
                         urlEvent.setParams({
                          "url": pageurl
                       });
                     urlEvent.fire();
                 }
            }
        });
        $A.enqueueAction(action);  
    },
})