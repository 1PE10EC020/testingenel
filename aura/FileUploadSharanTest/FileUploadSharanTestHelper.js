({
     /* 1 000 000 * 3/4 to account for base64 */

    save : function(component) {
        var fileInput = component.find("file").getElement();
    	var file = fileInput.files[0];
     //   alert('File ===>'+ file);
     //   alert('Fileinput ===>'+ fileInput);

	//	var MAX_FILE_SIZE = 750 000;   
      //  if (file.size > MAX_FILE_SIZE) {
            alert('File size cannot exceed 5MB' + ' bytes.\n' +
    		  'Selected file size: ' + file.size);
    	    //return;
     //   }
    
        var fr = new FileReader();
      //  alert('1');
	var self = this;
       	fr.onload = function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
        //	alert('2');

            fileContents = fileContents.substring(dataStart);
        
    	    self.upload(component, file, fileContents);
        };
	//	alert('3');
        fr.readAsDataURL(file);
    },
        
    upload: function(component, file, fileContents) {
        var action = component.get("c.saveTheFile"); 
     //   alert('inside upload');
        var parentIdTest = component.find("v.parentId");
	//	alert('v.parentIdValue'+parentIdTest);
        action.setParams({
            parentId: component.get("v.parentId"),
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type
        });

        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            console.log(attachId);
        });
            $A.enqueueAction(action); 
        }
})