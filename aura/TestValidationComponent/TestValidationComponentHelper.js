({
	initilizePicklistValues :function(component, event, helper)
    {
        // get all the picklist from  the fields.
		var action = component.get("c.getRichiestaPicklistValues");   
        debugger;
        action.setCallback(this, function(a) 
		{     
			var returnValue = a.getReturnValue();   
            // Check if server response is null.
            if(!$A.util.isEmpty(a.getReturnValue()))
            {
                // initlize Richiesta picklist values
                var pickListValue = a.getReturnValue().pickListValues;
                console.log(">>>> Picklist Value :" + pickListValue);
                debugger;
                component.set('v.tipologia', pickListValue['IdConnessione__c']); 
                component.set('v.contratto', pickListValue['IdTipoContr__c']);
                component.set('v.tensione', pickListValue['TensioneRichiesta__c']);
                component.set('v.energia', pickListValue['IdUsoEnergia__c']);
                component.set('v.stagionale', pickListValue['FlagStagRic__c']);
                component.set('v.impegnata', pickListValue['PotImpRichiesta__c']);
                component.set('v.settore', pickListValue['IdSettore__c']);
                component.set('v.autocert', pickListValue['AutocertContrConn__c']);
                component.set('v.trattamento', pickListValue['CarDetConRichiesta__c']);
                component.set('v.persone', pickListValue['FlagSollevamentoPers__c']);
                component.set('v.sollevamento', pickListValue['FlagAutocertSollev__c']);
                component.set('v.gestito', pickListValue['FlagForfait__c']);
               	debugger;
            }    
        });
        $A.enqueueAction(action);
    }
})