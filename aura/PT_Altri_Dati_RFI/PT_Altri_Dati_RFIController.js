({
	doinit : function(component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeNAME=component.get("v.recordTypeName");
        
        if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagCurveCarico__c','v.servizio','','','','');
         }
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))){ 
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAttFuoriOrario__c','v.attivazione','','','','');
       }
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_D01"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagCessFuoriOrario__c','v.cessazione','','','','');
       }
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))|| 
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertIstanza__c','v.flagAutocert','','','','');
       } 
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_D01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))|| 
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PT1"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagPresenzaCliente__c','v.presenza','','','','');
      
       }
        if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
           recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))){
      helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlgGestAnticipo__c','v.picFlgGestAnticipo','','','','');     
       
            component.find("Gestione").set("v.value",'S');
        }
        
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
          recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'CategoriaCliente__c','v.categoriaCl','','','','');
       }
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'DistSpostamento__c','v.distSpostamen','','','','');
        }
       if(recTypeNAME ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlgModificaOccasionale__c','v.modificaOccasionale','','','','');
       }
    },
 
    hideFieldsTEM : function(component, event, helper) {
    	//debugger;
    	var idConnessione = event.getParam("idConnessione");
        if(idConnessione==="TEM"){
            component.set("v.displayOrarioFields","true");
        }
        else{
            component.set("v.displayOrarioFields","false");
        }     
	},
    
    checkNoteFormat : function(component, event, helper) {
     	//debugger;
        var note = component.find("Note__c").get("v.value");
        if(!$A.util.isEmpty(note)){
            var noteFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Note_REGEX"));
        
            var validatenoteFormat = noteFormat.test(note);
            if(validatenoteFormat){
                var inputCmp = component.find("Note__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: Note' ;
                var inputCmp1 = component.find("Note__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
    },
        
    checkPhoneFormat : function(component, event, helper) {
     	//debugger;
        var phone = component.find("phone").get("v.value");
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
          if((!$A.util.isEmpty(phone))&& 
            (recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_D01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_I01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R03"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PT1"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
             recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01")))){
            var phoneFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_NumeroTelefonoRichiedente_REGEX"));
            var validatephoneFormat = phoneFormat.test(phone);
            var inputCmp = component.find("phone")
            if(validatephoneFormat){
                  inputCmp.set("v.errors", null);
            }
            else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: Numero telefono Richiedente ' ;
                inputCmp.set("v.errors", [{message:errMsg}]);
            }   
          }
          else{
			inputCmp.set("v.errors", null);
		}        
    },
})