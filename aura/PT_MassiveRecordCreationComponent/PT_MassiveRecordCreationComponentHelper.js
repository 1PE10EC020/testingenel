({
    validateDatiCarricamentoRFI : function(component,helper) {
        
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        var recType = component.get("v.recordType"); 
        component.set('v.recordTypeName',recType.Name); 
        var recTypeName=component.get("v.recordTypeName");
        var strRagionesociale = requestObj.DescrTraderEstesa__c; 
        var strContrattodispacciamento = requestObj.DescrDispaccEstesa__c;      
      //    component.set("v.Spinner",true);
   //     alert('spinner true'+component.get('v.Spinner'));
       
     //   alert('strRagionesociale :: ' + strRagionesociale);
     //   alert('strContrattodispacciamento :: ' + strContrattodispacciamento);
        if($A.util.isEmpty(strRagionesociale)){
            return 'Il campo Ragione sociale - Cod. è obbligatorio';
        }
        if($A.util.isEmpty(strContrattodispacciamento)){
            return 'Il campo Contratto di dispacciamento è obbligatorio';
        } 
        return errMsg;
    },
    
    validateFileUpload : function(component,helper){
        var errMsg = null;
        //alert('inside validate file upload');
        var formatCheck = component.get('v.formatError');
        alert("format check value"+formatCheck);
        var fileSelected = component.get('v.file1selected');
        if(formatCheck===true){
            var errMsg1 = $A.get("$Label.c.PT_ErrMsg_Contributi_Massivi");
            return errMsg1;
        }
        if(fileSelected === "false"){
            return 'Il campo File è obbligatorio';   
        }
        return errMsg;
    },
    
    initilizePicklistValues :function(component, event, helper)
    {
        var recTypeId = component.get("v.recordId");
        var requestObj = component.get('v.request'); 
        var action = component.get("c.getAllRichiestaPicklistValuesCntr");
        //alert('inside');		
        action.setParams({
            'RecordTypeId': recTypeId
            
        });
        action.setCallback(this,function(response){
            var State = response.getState();
            //alert(State);
            //var allpicklistValues = response.getReturnValue().picklistObjectMap;
            //var picklistCustomSettingList = response.getReturnValue().reqPicklistCustomSetting;
            //var picklistCustomSettingMap = response.getReturnValue().reqPicklistCustomSettingMap;
            //alert('before recTypeDetail---'+response.getReturnValue());
            var recTypeDetail = response.getReturnValue().reqrecordTypeDetail;
            //alert('recTypeDetail--'+recTypeDetail);
            //var rfiComponentSettings = response.getReturnValue().requestRfiComponentSettings;
            var micrfiComponentSettings = response.getReturnValue().requestMICRfiComponentSettings;
            // alert('1');
            //component.set('v.picklistValues', allpicklistValues);  
            //component.set('v.picklistCustomSetting', picklistCustomSettingList);
            //component.set('v.picklistCustomSettingMap', picklistCustomSettingMap);
            component.set("v.recordType", recTypeDetail);
            //component.set("v.checkRFIVisibility",rfiComponentSettings);
            component.set("v.checkMICRFIVisibility",micrfiComponentSettings);
            var test=component.get("v.checkMICRFIVisibility");
     //       alert('test---'+test.PT_DatiCaricamentoMassivoRFI__c);
            
            var recTypeDetail1 = component.get('v.recordType');  
            var desc = ' - ' + recTypeDetail1.Description;
            component.set("v.rtDesc",desc);
            var RecTypeName = recTypeDetail1.Name;
            helper.setTextDisplay(component,event,helper,RecTypeName);
            component.set("v.recordTypeName",RecTypeName);
            var requestRec = component.get("v.request");
            requestRec.RecordTypeId = recTypeDetail1.Id;
        //    alert('requestRec.RecordTypeId--'+requestRec.RecordTypeId);
            component.set("v.request",requestRec);
            component.set("v.Spinner",false);
            
        });
        $A.enqueueAction(action);
        
        
    },
    
    // Throws the error as toast message as part of request field validation checks.
    throwErrorMsg : function(component,errMsg,helper) 
    { 
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
            resultsToast.fire();
        }            
    },
    
    setTextDisplay : function(component,event,helper,RecTypeName){
        if(RecTypeName === "PT_Contributi_Massivi" || RecTypeName === "PT_Richieste_Massive"){      
            var textStr = $A.get("$Label.c.PT_Upload_File_Display_Massiv");
            component.set('v.textDisplay',textStr);
        }
    },
    
    //This method invokes Apex controller method which inserts request object record post all the validation checks.
    createRequest : function(component,helper) {
        // var ammissibilita = null;
        // code by Priyanka
        var tipoRic=component.get("v.tipoRic");
        if(tipoRic==="E01"){
             component.set("v.request.TipoRich__c","E010050");
        }
        else if(tipoRic==="Spostamenti_impianti_presa_GDM"){
            component.set("v.request.TipoRich__c","SP1");
        }
        else if(tipoRic==="Conferma_A03_N01_N02_S02_VL2_MC1"){
                component.set("v.request.TipoRich__c","XXX0051");
            }
        else{
        var tipoRicSplit = tipoRic.split("PT_");
        component.set("v.request.TipoRich__c",tipoRicSplit[1]);
        }
        /////
        var request = component.get("v.request");
       // alert('request'+JSON.stringify(request));
        var action = component.get("c.createMasRequestRecord");
        
        action.setParams({
            request : request,
            // requestTwo:requestEventParam              
        });
        action.setCallback(this,function(a){
            var state = a.getState();
        //    alert('state'+state);
            if(state === "SUCCESS"){
                var response = a.getReturnValue();
                /*  if(response !== null){
                    var responseIdfile =response.Id;
                    alert('responseID'+responseIdfile);
                 }*/
          //      alert('response'+JSON.stringify(response));
                //debugger;
                var responseId = response.Id;
                component.set('v.requestRecId',responseId);
         //       alert("at 573"+responseId);
                component.set("v.request",response);
                var appEvent = $A.get("e.c:PT_Upload_File_Container_Event");
                appEvent.setParams({
                    "responseid" : responseId });
                appEvent.fire(); 
        //        alert('event fired');
            }    
        });
        $A.enqueueAction(action);
        component.set('v.Spinner',true);
        },
})