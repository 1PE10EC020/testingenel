({
    afterSVGScriptsLoad: function(component, event, helper) 
    {
        svg4everybody();
	},
	myAction : function(component, event, helper) 
	{
		var action = component.get("c.findAll");
            action.setCallback(this, function(data) {
            component.set("v.moduliList", data.getReturnValue());
            });
            $A.enqueueAction(action);

	},
	onSelectChange : function(component, event, helper) 
	{
		var selected = component.find("levels").get("v.value");
		console.log(selected);
		var moduli =  component.get("v.moduliList");
        var ordering;
        var field;
        switch (selected) {
            case "Data di pubblicazione: Più Recenti":
                field = "LastPublishedDate";
                ordering = "DESC";
                break; 
            case "Data di pubblicazione: Meno Recenti":
                field = "LastPublishedDate";
                ordering = "ASC";               
                break;
            
            case "Titolo Ordinamento  Z- A":
                field = "Title";
                ordering = "DESC";
                break;
            case "Titolo Ordinamento  A- Z":
                field = "Title";
                ordering = "ASC";
                break;
           
        }
        helper.sorting(moduli, field ,ordering);
        
       component.set("v.moduliList", moduli);
	}

})