({
    doinit : function(component, event, helper) {
        //debugger;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName', recType.Name);
        var recTypeName = component.get("v.recordTypeName");
        
        helper.filterPicklistValues(component, event, helper, recTypeName, 'FlagDisalimentabile__c','v.disalimentabile','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'CodCategoriaDisalim__c','v.categoria','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'FlagAutocertASL__c','v.flagAutocertASL','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeName, 'FlagAutocertPESSE__c','v.flagAutocertPESSE','','','','');
        //$A.enqueueAction(action); 
     
    },
   
})