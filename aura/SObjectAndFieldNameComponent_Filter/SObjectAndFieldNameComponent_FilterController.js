({
    doInit : function(component, event, helper) {
        var SObject = component.get('v.SObject');
        var FieldName = component.get('v.fieldName');
        var outputText = component.find("outputTextId");    
        
        var output=SObject[FieldName]; 
        //var fieldValue = output;
        component.set('v.output', output);
        console.log('**mannaggialamiseria: ' +output);
       
        //if fieldName is a picklist call Apex for translation
        if(FieldName == 'Direzione__c'){
            var recordId = SObject['Id'];
            console.log('**recordIdSO: ' +recordId);
            helper.setOutput(component,recordId,FieldName);
        }
        //if fieldName is a date must be formatted in italian format
        else if(FieldName.includes('data')|| FieldName.includes('Data')){
            if(output==undefined){
                outputText.set("v.value", '');
            }else{
                var date = new Date(output);
                var newdate = new Date();
                newdate = $A.localizationService.formatDate(date, "DD"+"/"+"MM"+"/"+"YYYY, kk:mm");
                //console.log('**format date: '+$A.localizationService.formatDate(date, "DD"+"/"+"MM"+"/"+"YYYY, hh:mm:ss a"));
                outputText.set("v.value", newdate);
            }
        }
        else{
            outputText.set("v.value", SObject[FieldName]);
        }
                
        var isHyperlink = component.get("v.isHyperlink");
        console.log('***isHyperlink: ' +isHyperlink);

       

       
    },



    gotoURL : function (component, event, helper) {

        var drill = component.get('v.drilldown');
        var SObject = component.get('v.SObject');
        console.log('** drilldown: ' +component.get('v.drilldown'));
        console.log('** parent: ' +component.get('v.recordParent'));
        var parent = component.get('v.recordParent');
        var recID = '';
        
        if(drill && parent!=undefined){
            recID = SObject[parent];
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": "/detail/"+recID
            });
            urlEvent.fire();
        
        }
        else if(drill && parent==undefined){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                    "message" : "Nessun parent configurato"
            });
            toastEvent.fire();
        }
        else if(!drill){
            recID =  component.get('{!v.SObject.Id}');
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": "/detail/"+recID
            });
            urlEvent.fire();
        }
        
        console.log('** recID: ' +recID);
        

    }




    
})