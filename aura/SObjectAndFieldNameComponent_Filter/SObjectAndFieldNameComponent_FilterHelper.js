({
	
	
	setOutput : function(component,recordId,FieldName){

		console.log('**recordId SOBJ: '+recordId);
		var outputText = component.find("outputTextId");
		var action = component.get("c.getTranslationPicklist");
		var SObject = component.get('v.SObject');

		action.setParams({

            "recordId" : recordId,
            "objectt" : component.get('v.SObject'),
            "fieldName" : FieldName
            
        }); 

        action.setCallback(this, function(response){
		    var state = response.getState();

		    if (state === "SUCCESS"){

		    	var translationWrapper = response.getReturnValue();
		    	console.log('**translation: ' +translationWrapper.translation);
		    	component.set('v.translation', translationWrapper.translation);
		    	
		    	console.log('**isPicklistWR: ' +translationWrapper.isPicklist);
		    	if(translationWrapper.isPicklist == true){
					component.set('v.isPicklist', translationWrapper.isPicklist);		    	
				}

				var isPick = component.get('v.isPicklist');
		        var translation = component.get('v.translation');
		        console.log('**isPicklistSO' + isPick);
		        if(isPick == true){
		            outputText.set("v.value", translation);
		        }
		       /* else{
		            outputText.set("v.value", SObject[FieldName]);
		        }*/


		    }

        });
        $A.enqueueAction(action); 


	}

})