({
    /**
    * This method is called to fetch the SObject details for the API Name of SObject Passed
    **/
    getSobjectRecord : function(component, sobjOffset, isnext, updateOffset) 
    { 
        var objOffset = sobjOffset;
        var maxNoOfRecords = component.get('v.maxNoRecords');     
        var sObjName  = component.get('v.sObjectName');
        var sObjFieldApiName = component.get('v.sObjectFieldApiName');          
        var action = component.get("c.getRecords");        
        var ooo =  component.get('v.otherSoql');
        var sourceField = component.get('v.sourceField');
        if(sourceField != ''){
            component.set('v.DownloadFields', sourceField.split(','));
            console.log('*** DownloadFields: ' +component.get('v.DownloadFields'));
            var DownloadFields = component.get('v.DownloadFields');
        }
        else {
            var DownloadFields = null;
        }
        
        var isDownloadable = false;

        if (DownloadFields!=null){
            isDownloadable=true;
            console.log('*** isDownloadable è vero: ' +isDownloadable);
        }

        var where = component.get('v.whereClause');
        

        action.setParams({
            "otherSoql" :  ooo, 
            "sObjOffset": objOffset,
            "fieldApiName" :sObjFieldApiName,
            "maxNoOfRecord" :maxNoOfRecords, 
            "sobjApiName"  :sObjName,
            "createdByMe" : component.get('v.createdByMe'),
            "recordId": component.get("v.recordId"),
            "isDownloadable" : isDownloadable,
            "downloadFields" : DownloadFields,
            "whereClause" : where,
            "drilldown": component.get('v.drilldown')
        }); 

        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               if (state === "SUCCESS") 
                               {
                                    var stringItems = response.getReturnValue();
                                    component.set("v.sObjectRecords", stringItems.sobjLst); 
                                    component.set("v.hasNext", stringItems.hasNext);
                                    var list = component.get('v.sObjectRecords');
                                    if(list.length == 0){
                                      component.set('v.ListIsEmpty', true);
                                    }
                                    component.set("v.errorMessage", stringItems.errorMessage);
                                    console.log('**errorMessage: ' +stringItems.errorMessage);
                                    component.set('v.parentID', stringItems.parentID);
                                    

                                   if(updateOffset)
                                   {    
                                       if(isnext )
                                       {    
                                           component.set("v.sObjOffset", (component.get("v.sObjOffset") + maxNoOfRecords));
                                       }
                                       else
                                       {
                                           var offObj =  component.get("v.sObjOffset") - maxNoOfRecords;                   
                                           offObj =  (offObj <= 0) ? 0 : offObj;
                                           component.set("v.sObjOffset",offObj);
                                       }   
                                   }                    
                               }
                           });
        $A.enqueueAction(action);       
    },
    
    /**
    * This method is called to fetch the Records which match the Filter Criteria
    **/
    getSobjectFilteredRecords : function(component, sobjOffset, isnext) 
    {

        var objOffset = sobjOffset;
        var maxNoOfRecords = component.get('v.maxNoRecords');     
        var sObjName  = component.get('v.sObjectName');
        var sObjFieldApiName = component.get('v.sObjectFieldApiName');          
        var action = component.get("c.getRecordsForSearch");        
        var ooo =  component.get('v.otherSoql');  
        var searchField = component.get('v.sObjectSearchFieldApiName');  
        //var searchtxt = component.find("searchTxt").get("v.value");     
        
        
        action.setParams({
            "otherSoql" :  ooo, 
            "sObjOffset": objOffset,
            "fieldApiName" :sObjFieldApiName,
            "maxNoOfRecord" :maxNoOfRecords, 
            "sobjApiName"  :sObjName,
            "searchField" : searchField,
            "createdByMe" : component.get('v.createdByMe')
        });      
        
        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               if (state === "SUCCESS") 
                               {
                                   var stringItems = response.getReturnValue();
                                   component.set("v.sObjectRecords", stringItems.sobjLst); 
                                   component.set("v.hasNext", stringItems.hasNext);                                
                                   if(isnext)
                                   {    
                                       component.set("v.sObjOffset", (component.get("v.sObjOffset") + maxNoOfRecords));
                                   }
                                   else
                                   {
                                       var offObj =  component.get("v.sObjOffset") - maxNoOfRecords;                   
                                       offObj =  (offObj <= 0) ? 0 : offObj;
                                       component.set("v.sObjOffset",offObj);
                                   }   
                                   
                               }
                           });
        $A.enqueueAction(action);       
    }


   

    

    
})