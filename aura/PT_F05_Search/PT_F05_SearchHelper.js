({
 getIntialization : function(component) {

   var controllerValues = component.get("c.getWrapper");
   
   controllerValues.setCallback(this, function(response){
     var state = response.getState();
     if (state === "SUCCESS"){
       var responseData = response.getReturnValue(); 
       component.set("v.wrapperList",responseData)
       component.find("Venditore").set("v.value", responseData.Venditore);
       component.set("v.isVenditore",true); 
     }    
   });
   
   $A.enqueueAction(controllerValues);	
 },
 getstatoRichValue : function(component) {
   var action = component.get("c.StaRichValues");
   action.setCallback(this, function(response) {
    component.set("v.lstOfstatrich", response.getReturnValue());
  });
   
   $A.enqueueAction(controllerValues);	
 },
 
 helpersearch: function(component,helper,key) {
        //PREV_F05_01 Validation
        var ERR_PREV_F05_01 = helper.validateF05_One(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_01)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_01, helper);
          return;
        }
        //PREV_F05_02 Validation
        var ERR_PREV_F05_02 = helper.validateF05_two(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_02)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_02, helper);
          return;
        }
        //PREV_F05_03 Validation
        var ERR_PREV_F05_03 = helper.validateF05_three(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_03)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_03, helper);
          return;
        }
        //PREV_F05_04 Validation
        var ERR_PREV_F05_04 = helper.validateF05_four(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_04)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_04, helper);
          return;
        }
        //PREV_F05_05 Validation
        var ERR_PREV_F05_05 = helper.validateF05_five(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_05)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_05, helper);
          return;
        }
        //PREV_F05_06 Validation
        var ERR_PREV_F05_06 = helper.validateF05_six(component, helper);
        if (!$A.util.isEmpty(ERR_PREV_F05_06)) {
          helper.throwErrorMsg(component, ERR_PREV_F05_06, helper);
          return;
        }
        //Display Search records if no Validation
        if ($A.util.isEmpty(ERR_PREV_F05_01) && $A.util.isEmpty(ERR_PREV_F05_02)&& $A.util.isEmpty(ERR_PREV_F05_03) && $A.util.isEmpty(ERR_PREV_F05_04) && $A.util.isEmpty(ERR_PREV_F05_05) && $A.util.isEmpty(ERR_PREV_F05_06)) {
         if(key==='Search'){   
          helper.getListOfRecords(component);
        }
        if(key==='Report'){
         helper.runreport(component); 
       }
       return;
     }
   }, 
   getListOfRecords: function(component) {

     var F05search = component.get("v.wrapperList");
     F05search.TipoRich=component.find("TipoRich").get("v.value");
     F05search.CodRichDistribDa=component.find("CodRichDistribDa").get("v.value");
     F05search.CodRichDistribA=component.find("CodRichDistribA").get("v.value");
     F05search.CodRichVend=component.find("CodRichVend").get("v.value");
     F05search.StatoRich=component.find("StatoRich").get("v.value");
     F05search.SottostatoRich=component.find("SottostatoRich").get("v.value");
     F05search.DataDecorrenzaDa=component.find("DataDecorrenzaDa").get("v.value");
     F05search.DataDecorrenzaA=component.find("DataDecorrenzaA").get("v.value");
     F05search.DataEvasioneDa=component.find("DataEvasioneDa").get("v.value");
     F05search.DataEvasioneA=component.find("DataEvasioneA").get("v.value");
     F05search.POD=component.find("POD").get("v.value");
     F05search.NumPresa=component.find("NumPresa").get("v.value");
     F05search.Eneltel=component.find("Eneltel").get("v.value");
     F05search.Venditore=component.find("Venditore").get("v.value");
     F05search.ContrattodiDisp=component.find("ContrattodiDisp").get("v.value");
     F05search.CFRich=component.find("CFRich").get("v.value");
     F05search.PIVARich=component.find("PIVARich").get("v.value");
     F05search.Nome =component.find("Nome").get("v.value");
     F05search.Ordinaper =component.find("Ordinaper").get("v.value");
     F05search.Cognome =component.find("Cognome").get("v.value");
     F05search.RagSociale =component.find("RagSociale").get("v.value");
     F05search.ProvinciaPOD =component.find("ProvinciaPOD").get("v.value");
     component.set("v.wrapperList",F05search);
     var controllerValues = component.get("c.getQueryreqrecord");
     controllerValues.setParams({wrapperobj : JSON.stringify(F05search),
       strObjectName : component.get("v.objectName"),
       strFields : component.get("v.FieldAPIs"),
       RecordNum : component.get("v.RecordLimit") ,

     });
     controllerValues.setCallback(this, function(response){
       var state = response.getState();
       if (state === "SUCCESS"){
         var responseData = response.getReturnValue(); 
         if(responseData != null){
           component.set("v.recordsList", responseData.dataList);
           component.set("v.labelList", responseData.labelList);                
           component.set("v.apiList",responseData.apiList);
           component.set("v.csvrecList",responseData.csvList);
           if(responseData.dataList.length===0){
            component.set("v.noRecords",true);
            component.set("v.Spinner",false);
            component.set("v.displayRecordList",false);
            component.set("v.limitRec",False);
          }
          if(responseData.dataList.length>0){
            component.set("v.displayRecordList",true);
            component.set("v.noRecords",false);	
            component.set("v.limitRec",false);
          }
          if(responseData.dataList.length>100){
            component.set("v.LimitRec",true);
            component.set("v.NoRecords",false);	 
            component.set("v.Spinner",false);
          } 
        }
      }
      component.set("v.Spinner",false); 
    });
     $A.enqueueAction(controllerValues);  
     component.set("v.Spinner",true); 
   },


        //PREV_F05_01
        validateF05_One : function(component,helper) {
          var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_01");
          var errMsg = null;
          var Cognome  = component.find("Cognome").get("v.value");
          var RagSociale   = component.find("RagSociale").get("v.value");
          if(!$A.util.isEmpty(Cognome) && !$A.util.isEmpty(RagSociale)){
            return ErrorMsgLable;
          }
          return errMsg;
        },
         //PREV_F05_02 Validation
         validateF05_two : function(component,helper) {
          var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_02");
          var errMsg = null;
          var Cognome = component.find("Cognome").get("v.value");
          var Nome = component.find("Nome").get("v.value");
          if(!$A.util.isEmpty(Nome) && ($A.util.isEmpty(Cognome)) ){
           return ErrorMsgLable;
         }
         
         return errMsg;
       },
       
          //PREV_F05_03 Validation
          validateF05_three : function(component,helper) {
            var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_03");
            var errMsg = null;
            var CodRichDistribDa = component.find("CodRichDistribDa").get("v.value");
            var Eneltel = component.find("Eneltel").get("v.value");
            var POD = component.find("POD").get("v.value");
            var NumPresa = component.find("NumPresa").get("v.value");
            var CFRich = component.find("CFRich").get("v.value");
            var CodRichVend = component.find("CodRichVend").get("v.value");
            
            if(!(!$A.util.isEmpty(CodRichDistribDa) || !$A.util.isEmpty(Eneltel) || 
             !$A.util.isEmpty(POD) || !$A.util.isEmpty(NumPresa) || !$A.util.isEmpty(CFRich) || !$A.util.isEmpty(CodRichVend) )){
             return ErrorMsgLable;
         }
         return errMsg;
       },
    //PREV_F05_04 Validation
    validateF05_four : function(component,helper) {
      var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_04");
      var errMsg = null;
      var DataDecorrenzaDa  = component.find("DataDecorrenzaDa").get("v.value");
      var DataDecorrenzaA  = component.find("DataDecorrenzaA").get("v.value");
      
      if(($A.util.isEmpty(DataDecorrenzaDa) && !$A.util.isEmpty(DataDecorrenzaA) || !$A.util.isEmpty(DataDecorrenzaDa) && $A.util.isEmpty(DataDecorrenzaA) )){
        return ErrorMsgLable; 
      }
      return errMsg;
    },  
          //PREV_F05_05 Validation
          validateF05_five : function(component,helper) {
            var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_05");
            var errMsg = null;
            var DataDecorrenzaDa = component.find("DataDecorrenzaDa").get("v.value");
            var DataDecorrenzaA = component.find("DataDecorrenzaA").get("v.value");
            var dateFormat = "MM-dd-yyyy";
            var dt1 = $A.localizationService.formatDateTime(DataDecorrenzaDa, dateFormat);
            var dt2 = $A.localizationService.formatDateTime(DataDecorrenzaA, dateFormat);
            
            var timeDiff = Date.parse(dt2)-Date.parse(dt1);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            
            if((!$A.util.isEmpty(DataDecorrenzaDa) && !$A.util.isEmpty(DataDecorrenzaA))){
              if(diffDays>90){
                return ErrorMsgLable;
              }
            }
            return errMsg;
          }, 
     //PREV_F05_06 Validation
     validateF05_six : function(component,helper) {
      var ErrorMsgLable=$A.get("$Label.c.PT_PREV_F05_06");
      var errMsg = null;
      var CodRichDistribA = component.find("CodRichDistribA").get("v.value");
      var CodRichDistribDa = component.find("CodRichDistribDa").get("v.value");
      
      if(!($A.util.isEmpty(CodRichDistribA) && $A.util.isEmpty(CodRichDistribDa))){
        if (isNaN(CodRichDistribA) && !($A.util.isEmpty(CodRichDistribA) )){
          return ErrorMsgLable;
        }
        if (isNaN(CodRichDistribDa) && !($A.util.isEmpty(CodRichDistribDa))){
          return ErrorMsgLable;
        }
      }
      return errMsg;
    },
    throwErrorMsg : function(component,errMsg,helper) 
    { 
      var resultsToast = $A.get("e.force:showToast");
      if(resultsToast)
      {    
        resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
        resultsToast.fire();
      }            
    },
    convertArrayOfObjectsToCSV : function(component,objectRecords,headerlabel,headerApilist){
      var csvStringResult, counter, keys, columnDivider, lineDivider,keylabel;
      if (objectRecords == null || !objectRecords.length) {
        return null;
      }
      var headcsvrecList = component.get("v.csvrecList");
      console.log('headcsvrecList==>'+headcsvrecList);
      columnDivider = ',';
      lineDivider =  '\n';
      //header values for csv file
      keylabel=headerlabel;
       //api values for csv file
       keys=headerApilist;
       csvStringResult = '';
       csvStringResult += keylabel.join(columnDivider);
       csvStringResult += lineDivider;
       for(var i=0; i < objectRecords.length; i++){   
        counter = 0;
        for(var sTempkey in keys) {
          var skey = keys[sTempkey] ;  
          if(counter > 0){ 
            csvStringResult += columnDivider;
          } 
          if(objectRecords[i][skey] !== undefined){
           csvStringResult += '"'+ objectRecords[i][skey]+'"';
         }
         else{
           csvStringResult += '"'+ '' +'"';
         }
         counter++;
       } 
       csvStringResult += lineDivider;
     }
     return csvStringResult;        
   },
   
   runreport : function(component,errMsg,helper) {
        var F05search = component.get("v.wrapperList");
        F05search.TipoRich=component.find("TipoRich").get("v.value");
        F05search.CodRichDistribDa=component.find("CodRichDistribDa").get("v.value");
        F05search.CodRichDistribA=component.find("CodRichDistribA").get("v.value");
        F05search.CodRichVend=component.find("CodRichVend").get("v.value");
        F05search.StatoRich=component.find("StatoRich").get("v.value");
        F05search.SottostatoRich=component.find("SottostatoRich").get("v.value");
        F05search.DataDecorrenzaDa=component.find("DataDecorrenzaDa").get("v.value");
        F05search.DataDecorrenzaA=component.find("DataDecorrenzaA").get("v.value");
        F05search.DataEvasioneDa=component.find("DataEvasioneDa").get("v.value");
        F05search.DataEvasioneA=component.find("DataEvasioneA").get("v.value");
        F05search.POD=component.find("POD").get("v.value");
        F05search.NumPresa=component.find("NumPresa").get("v.value");
        F05search.Eneltel=component.find("Eneltel").get("v.value");
        F05search.Venditore=component.find("Venditore").get("v.value");
        F05search.ContrattodiDisp=component.find("ContrattodiDisp").get("v.value");
        F05search.CFRich=component.find("CFRich").get("v.value");
        F05search.PIVARich=component.find("PIVARich").get("v.value");
        F05search.Nome =component.find("Nome").get("v.value");
        F05search.Ordinaper =component.find("Ordinaper").get("v.value");
        F05search.Cognome =component.find("Cognome").get("v.value");
        F05search.RagSociale =component.find("RagSociale").get("v.value");
        F05search.ProvinciaPOD =component.find("ProvinciaPOD").get("v.value");
        var action = component.get("c.createF05Report");
        action.setParams({wrapperobj : JSON.stringify(F05search)
        });
        action.setCallback(this, function(response) {
          var state = response.getState();
          var responseData = response.getReturnValue();
          var RecName;
          debugger;
          if(responseData != null){
            var F05Record = responseData;
            component.set("v.f05Report",F05Record); 
            component.set("v.recordName", F05Record.Name);
            component.set("v.recname",F05Record.Name);
            if(F05Record.Name!=null){
             component.set("v.popupcall",true);    
           }
         }

       });
        $A.enqueueAction(action);
        
      }
      /* setEndDate : function(component,errMsg,helper) {
        debugger;
        
        var action = component.get("c.PT_getEndDate");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state==="SUCCESS"){
                var startEndDate = response.getReturnValue();
                component.find("DataEvasioneDa").set("v.value",startEndDate[0]);
                component.find("DataEvasioneA").set("v.value",startEndDate[1]);
                
                component.find("DataDecorrenzaDa").set("v.value",startEndDate[0]);
                component.find("DataDecorrenzaA").set("v.value",startEndDate[1]);
                
            }    
        });
        $A.enqueueAction(action);
      }*/
      
      
    })