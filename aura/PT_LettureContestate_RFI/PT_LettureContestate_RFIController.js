({
	doinit : function(component, event, helper) {
    	var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        helper.filterPicklistValues(component, event, helper, recTypeName, 'TipologiaLettura__c','v.tipoLett','','','','');
    },
         
/************************************************************************************************
@Author:Sharan Sukesh
@Description:These methods are used to call helper method to validate the regular expression by 
 passing the auraId as a parameter.
*************************************************************************************************/
    checkAttiva1 : function(component, event, helper) {
         helper.checkformat(component, event, helper,"Att_F1_LetCon__c");
    },
    
    checkAttiva2 : function(component, event, helper) {
        helper.checkformat(component, event, helper,"Att_F2_LetCon__c");
    },
    
    checkAttiva3 : function(component, event, helper) {
        helper.checkformat(component, event, helper,"Att_F3_LetCon__c");
    },
    
    checkReattiva1 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F1_LetCon__c");
    },
    
    checkReattiva2 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F2_LetCon__c");
    },
    
    checkReattiva3 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Reatt_F3_LetCon__c");
    },
    
    checkPot1 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F1_LetCon__c");
    },
    checkPot2 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F2_LetCon__c");
    },
    checkPot3 : function(component,event,helper){
         helper.checkformat(component, event, helper,"Pot_F3_LetCon__c");
    },
    
})