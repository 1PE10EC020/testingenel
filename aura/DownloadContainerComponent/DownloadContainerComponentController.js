({
    doInit : function(component, event, helper) {

        var SObject = component.get('v.SObject');
        var FieldName = component.get('v.fieldName');
        console.log('**SObject download: ' +SObject); 
        console.log('**Field to download: ' +FieldName);

        //var output=SObject[FieldName];
        
        //console.log('**output: ' +output);
        //component.set('v.fileContent', output);
 
        var filter = component.get('v.Filter');
        console.log('**TEST filter :' +filter);

        var SObjectName = component.get('v.SObjectApi');
        var id = SObject['Id'];
        component.set('v.recordToSend', id);
        helper.checkDownloadType(component,helper,SObjectName,id);
        console.log('**recordToSend contr: '+component.get('v.recordToSend'));
        //helper.pollApex(component,helper);


        
        
    },

    SaveIntoFolder : function(component, event, helper){

        var FieldName = component.get('v.fieldName');
        var SObject = component.get('v.SObject');

        helper.getContent(component,SObject,FieldName);


    },

    sendRequestDownload : function(component,event,helper){
        var attachmentId = component.get('v.attachmentId');
        var SObject = component.get('v.SObject');
        component.set('v.enableDownload',false);
        //component.set('v.DMSMessage', true);
        
        helper.updateAllegato(component,SObject);
        //helper.refresh(component,event,helper);
        var id=component.get('v.recordToSend');
        var interval = window.setInterval(
            $A.getCallback(function() { 
               helper.refresh(component,event,interval,id,attachmentId);
               var doRefresh = component.get('v.doRefresh');
               if(doRefresh){
                    var attId = component.get('v.attId');
                    helper.AutoDownload(component,event,attId);
               }
               
            }), 2000
        ); 

      
        window.setTimeout(function() {

            window.clearInterval(interval); 
            //helper.updateFileRichiesto(component,event,id);
            console.log('**Polling stopped after 15 seconds!');

        }, 15000);

      
        helper.updateFileRichiesto(component,event,id);
      
    },

    downloadAttachment : function(component,event,helper){

        var attachmentId = component.get('v.attachmentId');
        console.log('**attachmentId c: ' +attachmentId);    
       
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: attachmentId['Id'],
            slideDevName: "detail"
        });
        navEvent.fire(); 
                  

    }
   

})