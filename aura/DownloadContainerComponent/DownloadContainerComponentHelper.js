({
    


    getContent : function(component,SObject,FieldName){

        var action = component.get("c.getFileConfiguration");
        var recordId = SObject['Id'];

        action.setParams({

            "sObjectt" : SObject,
            "fieldName" : FieldName,
            "sObjectApi": component.get('v.SObjectApi'),
            "recordId" : recordId

        });

        /*** - Apex CallBack - start
            *** This callback method is set to retrieve sobject field Four ID  ***/
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
               
                var stringItems = response.getReturnValue();
                //****IF NOT EXTERNAL
                component.set('v.fourid', stringItems.fourID);
                console.log('**CAMPO FOUR ID: ' +component.get('v.fourid'));
                /**** CONFIGURATION OF FILE *****/
                //Nome File 
                var fourID = component.get('v.fourid');
                console.log('**fourID: ' +fourID);
                var output=SObject[fourID];
                console.log('**outputNomeFile: ' +output);
                component.set('v.FileName', output);

                //Estensione File            
                component.set('v.extension', stringItems.extension);
                //console.log('**extension: ' +component.get('v.extension'));
                var ext = component.get('v.extension');

                var fileName = component.get('v.FileName');
                fileName += '.' + ext;
                component.set('v.FileName' , fileName);
                console.log('**fileName: ' +component.get('v.FileName'));
                //Content
                component.set('v.fileContent', stringItems.content);
                //****CONFIGURATION - END ********

                //ONCLICK
                var fileContent = component.get('v.fileContent');
                console.log('***fileContent: ' +fileContent);

                if(fileContent == undefined || fileContent.length == 0){
                    component.set('v.noDownload', true);
                }
                
                else{
                    component.set('v.noDownload', false);
                    var fileName = component.get('v.FileName');
                    console.log('**NOME FILE: ' +fileName);
                   
                    //Initialize file format
                    var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(fileContent);

                    var link = document.createElement("a");

                    link.setAttribute('download',fileName);
                    //To set the content of the file
                    link.href = uri;
                    link.click();

                }
                //END CLICK
                //****IF NOT EXTERNAL - END
                

                }
        
                
           
            
        });
        
        $A.enqueueAction(action);


    },

    checkDownloadType : function(component, helper, SObjectName, id) {
        
        var fileDisponibile = component.get('v.disponibile');
           
        var action = component.get("c.getStatus");
        action.setParams({ 
            "sObjectName": SObjectName,
            "recId" : id,
            "disponibile" : fileDisponibile
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var items = response.getReturnValue();
                component.set('v.DownloadDMS', items.isDMS);
                console.log('**DownloadDMS: ' +component.get('v.DownloadDMS'));
                //component.set('v.StandardDownload', pageRender.standardDownload);
                //console.log('**DownloadDMS: ' +component.get('v.StandardDownload'));
                /*var dms = component.get('v.DownloadDMS');*/
                component.set('v.DMSMessage', items.showMessage);
                component.set('v.disponibile', items.disponibile);
                component.set('v.enableDownload', items.enableDownload);
                fileDisponibile = component.get('v.disponibile');

                if(fileDisponibile){
                    component.set('v.DMSMessage', false);
                    component.set('v.enableDownload', false);
                    component.set('v.attachmentId', items.attachment);
                    console.log('**AttachmentID: ' +component.get('v.attachmentId'));
                    component.set("v.url", "/servlet/servlet.FileDownload?file=" + items.attachment);
                    
                }
        
                console.log('**DMSMessage: ' +items.showMessage);
                if(!items.isDMS){
                
                    component.set('v.StandardDownload',true);
                }

            } 
            
        });
        
        $A.enqueueAction(action);
        // - Apex CallBack - end 
    },

    updateAllegato : function(component,SObject){

        var record = SObject['Id'];
        var action = component.get("c.updateAllegato");
        action.setParams({
            "recId" : record
        })


        action.setCallback(this, function(response) {
            var state = response.getState();
            //var items = response.getReturnValue();
            
            if (state === "SUCCESS") {     
                
                component.set('v.DMSMessage', response.getReturnValue());     
                
            } 
            
        });
        
        $A.enqueueAction(action);
        // - Apex CallBack - end 

    },


    
    refresh : function(component, event, interval,id,attachmentId) {

        var action = component.get('c.refreshAttachment');

        action.setParams({
            "recId" : id
        })

        action.setCallback(component,
            function(response) {
                var state = response.getState();
                if (state === 'SUCCESS'){

                    var risposta = response.getReturnValue();
                    var attachmentEx=risposta.exists;
                    if(attachmentEx){
                        component.set('v.doRefresh', true);
                        console.log('**doRefresh in refresh:' +component.get('v.doRefresh'));
                        console.log('**attachment in refresh:' +attachmentEx);
                        component.set('v.attId',risposta.attachmentid);
                    }
                    
                } 
            }
        );
        $A.enqueueAction(action);

       

        //If Attachment exists clear polling
        var doRefresh = component.get('v.doRefresh');
        if(doRefresh){
            component.set('v.doneRefresh',true);
            window.clearInterval(interval);
            $A.get('e.force:refreshView').fire();

            
        }
        
   
    },

    updateFileRichiesto : function(component,event,id){

        var action = component.get('c.updateFileRichiesto');
        action.setParams({
            "recId" : id
        })


        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {          
                //component.set('v.enableDownload',false);
                console.log('** sto chiamando updateFileRichiesto Apex');
            } 
            
        });
        
        $A.enqueueAction(action);

         window.setTimeout(function() {

            var refreshed=component.get('v.doneRefresh');
            if(!refreshed){
                console.log('**non è stato fatto refresh');
                //component.set('v.DMSMessage', false);
                //component.set('v.enableDownload', true);
                $A.get('e.force:refreshView').fire();
            }
            

        }, 15000);


    },

    AutoDownload : function(component,event,attId){

        console.log('**AutoDownload must start!');
        console.log('**idatt: ' +attId);

        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: attId,
            slideDevName: "detail"
        });
        navEvent.fire(); 

    }


   

    
})