({
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodal: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    editRecord : function(component, event, helper) {
    var editRecordEvent = $A.get("e.force:editRecord");
        var id='a0K7E000001CZUv';
    editRecordEvent.setParams({
         "recordId": 'a0K7E000001CZUv'

   });
    editRecordEvent.fire();
}

})