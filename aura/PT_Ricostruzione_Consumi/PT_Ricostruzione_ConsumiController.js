({
	doinit : function(component, event, helper) {
        
    helper.getPicklistTrClValues(component);
		
	},
    
    
    onSelectChange : function(component, event, helper) {
        component.set("v.ricostruzione_consumi_lst", null);
        component.set("v.anni", null);
        var selected = component.find("cppClTr").get("v.value");
        console.log('PT_Ricostruzione_Consumi selected: '+selected);  
        if (selected ===""){
             component.set("v.secondSection", false); 
                }
        else{
             component.set("v.secondSection", true); 
             helper.getListRecords(component, selected);

            }
         },
    
    
   
})