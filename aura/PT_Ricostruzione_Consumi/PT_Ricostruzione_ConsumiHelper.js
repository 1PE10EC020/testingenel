({
	  getPicklistTrClValues : function(cmp)  {
          var action = cmp.get("c.getPicklistTrCl"); 
          
          action.setParams({
            "recordId": cmp.get("v.recordId")
        });
          
           action.setCallback(this, function(response) {
               var state=response.getState();
               if (state==="SUCCESS"){
                   var stringItems = response.getReturnValue();
                   cmp.set("v.coppiaTrCl_set", stringItems.coppiaTrCl_set); 
                   console.log('PT_Ricostruzione_Consumi stringItems: '+stringItems);
                   console.log('PT_Ricostruzione_Consumi stringItems: '+stringItems.coppiaTrCl_set);
               }
                                    
                           });
        $A.enqueueAction(action);       
    },
    
    
    getListRecords : function(cmp, coppiaTrCl)  {
          var trClSelected=coppiaTrCl;
          var action = cmp.get("c.getListRicConsumi"); 
          
          action.setParams({
            "recordId": cmp.get("v.recordId"),
            "coppiaTrCl": trClSelected
        });
          
           action.setCallback(this, function(response) {
               var state=response.getState();
               if (state==="SUCCESS"){
                   var consItems = response.getReturnValue();
                   cmp.set("v.ricostruzione_consumi_lst", consItems.ricConsumi_Lst);
                   console.log('PT_Ricostruzione_Consumi ricConsumi_Lst: '+consItems.ricConsumi_Lst);
                   cmp.set("v.anni", consItems.anni_set);
                   console.log('PT_Ricostruzione_Consumi anni_set: '+consItems.anni_set);
                   cmp.set("v.mapRicFromDate", consItems.mapRecordForMonths);
                   //console.log('PT_Ricostruzione_Consumi mapRecordForMonths: '+consItems.mapRecordForMonths['201701']['Name']);
                   
                   cmp.set("v.mapAMesi", consItems.mapMonthsForYear);
                  
                   console.log('PT_Ricostruzione_Consumi*******END');
                   //console.log('PT_Ricostruzione_Consumi mapMonthsForYear: '+consItems.mapMonthsForYear['2016']);
                   
               }
                                    
                           });
        $A.enqueueAction(action);       
    },
    
})