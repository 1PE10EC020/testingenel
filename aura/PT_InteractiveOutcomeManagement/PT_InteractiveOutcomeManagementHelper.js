({
    assignFieldValues :function(component, event, helper){       
        //debugger;
        var reqobj = component.get("v.requestObj");
        component.set('v.requestId', reqobj.Id);
        var reqId = component.get("v.requestId");
        
        //debugger;
        if(reqobj !== null){        
            component.find("richiestafour").set("v.value",reqobj.IdRichiestaFour__c);
            //component.find("ammissibilita").set("v.value",reqobj.AmmissCodCausale__c);
            var dateFormat = "yyyy-MM-ddTHH:mm:ss.000Z";
            var DataAcquisizione = new Date();
            DataAcquisizione.setHours(DataAcquisizione.getHours());        
            var dateString = $A.localizationService.formatDateTime(DataAcquisizione, dateFormat); 
            component.find("dataacquisizione").set("v.value",dateString);
            if(component.get("v.reqRefused")){
                component.find("descammissibilita").set("v.value", 'Richiesta Rifiutata per POD not attivo all\'interno dell\'intervello temporale richiesto');
            }
        }
    },
})