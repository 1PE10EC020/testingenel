({
    doInit : function(component, event, helper) {
      	
        //debugger;
        var reqobj = component.get("v.requestObj");
        component.set('v.ammissCodCausale', reqobj.AmmissCodCausale__c);
        
        if(!$A.util.isEmpty(component.get("v.ammissCodCausale")) && !$A.util.isUndefined(component.get("v.ammissCodCausale"))){     
            //debugger;
             if(component.get("v.ammissCodCausale") === '000'){
                 component.set("v.reqAccepted","true");
                 helper.assignFieldValues(component, event, helper);
             }
             if(component.get("v.ammissCodCausale") !== '000'){
                 component.set("v.reqRefused","true");
                 helper.assignFieldValues(component, event, helper);
             }
         }
         else{
             //debugger;
             component.set("v.reqNotReceived","true");
         }
        
    },	
    
    continueToDetailPage : function(component,helper) {
        
        //debugger;
        var reqobj = component.get("v.requestObj");
        component.set('v.requestId', reqobj.Id);
        var reqId = component.get("v.requestId");
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": reqId,
            "slideDevName": "detail"
        });
        navEvt.fire();
        
    },
})