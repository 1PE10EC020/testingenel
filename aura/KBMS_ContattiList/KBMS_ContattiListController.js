({
	ToggleCollapse : function(component, event, helper) { 
		helper.ToggleCollapseHandler(component, event);
        var scId = event.target.id;
        var action1 = component.get("c.findContact");
        action1.setParams
        ({
            "scId" : scId
        })
            action1.setCallback(this, function(a) 
            {
                console.log(a.getReturnValue());
                component.set("v.contacts", a.getReturnValue());
                component.set("v.contactSize", a.getReturnValue().length);
            });
         $A.enqueueAction(action1); 
        
	}
})