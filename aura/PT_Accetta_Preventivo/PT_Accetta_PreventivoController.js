({
	doinit : function(component, event, helper) {
        //debugger;
        var contributi = component.get("v.contributi");
        contributi.Id = component.get("v.recordId");
        component.set("v.contributi",contributi); 
	},
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodal: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    Accettaperform : function(component, event, helper) {
       // debugger;
      var errorMsg_ric = helper.validateAccettaPreventivo(component,helper);
        if(!$A.util.isEmpty(errorMsg_ric))
        {
            helper.throwErrorMsg(component,errorMsg_ric,helper);
            return;
        }
        if($A.util.isEmpty(errorMsg_ric)){
            
         helper.AccettaNoErrorperform(component,event,helper);
            
        }
       },
})