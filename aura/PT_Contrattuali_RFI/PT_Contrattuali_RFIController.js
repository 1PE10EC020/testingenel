({
	doinit : function(component, event, helper) {
        //debugger;
        
        var recType = component.get("v.recordType");
       	var myJSON = JSON.stringify(recType);
        //alert('recType'+myJSON);
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        var recTypename = component.get("v.recTypeName");
        component.set('v.recTypeName', recType.Name);
		var recTypeNAME = component.get("v.recTypeName");
       // alert('recordTypename->'+recTypeNAME);
        
        if(recTypeNAME==='PT_N02'){          
            component.set("v.isIdConnessione","true");
            component.set("v.isIdTipoCont","true");            
            component.set("v.isTensione","true");
            component.set("v.isUsoEnergia","true");
            component.set("v.isFlagStagRicAtto","true");
            component.set("v.isPotImp","true");
            component.set("v.isPotDisp","true");
            component.set("v.isIdSettore","true");
            component.set("v.isAutocertContrConn","true");
            component.set("v.isCarDetCon","true");
			component.set("v.isSollevamentoDisable","false"); 
            
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdConnessione__c','v.tipologia','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'PotImp__c','v.impegnata','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'AutocertContrConn__c','v.autocert','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdConnessione__c','v.tipologia','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');       
        }
        else if(recTypeNAME==='PT_N01'){			 
             component.set("v.isIdConnessione","true");
             component.set("v.isIdTipoCont","true");         
             component.set("v.isTensione","true");
             component.set("v.isUsoEnergia","true");
             component.set("v.isPotImp","true");
             component.set("v.isPotDisp","true");
             component.set("v.isCarDetCon","true");
			 component.set("v.isTipologiaDisable","false"); 
             component.set("v.inN01","false");
             component.find('IdConnessione__c').set("v.disabled","true");  
             component.find('IdConnessione__c').set("v.value","PER");
           
         helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdConnessione__c','v.tipologia','','','','');    
	     helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');
		 helper.filterPicklistValues(component, event, helper, recTypeNAME,'PotImp__c','v.impegnata','','','','');            
         helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');
        }
       else if(recTypeNAME==='PT_A01'){
             component.set("v.isIdTipoCont","true");
			 component.set("v.isTipologiaDisable","false");            
			 component.set("v.Tipologiarequired","true");
			 component.set("v.isFlagStagRicAtto","true");
             component.set("v.isFlagStagRicAttoDisable","false");            
             component.set("v.isIdSettore","true");
             component.set("v.isAutocertContrConn","true");
             component.set("v.isCarDetCon","true");
             component.set("v.isSollevamentoDisable","true");
           
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'AutocertContrConn__c','v.autocert','','','','');            
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');            
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');                                 
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');             
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','','','','');
             helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');                 			      
               
        }
        else if(recTypeNAME==='PT_A03'){
             component.set("v.isIdTipoCont","true");
			 component.set("v.isTipologiaDisable","false");            
			 component.set("v.Tipologiarequired","true");
             component.set("v.isTensione","true");
             component.set("v.isTensioneDisable","true"); 
             component.set("v.isTensionerequired",false); 
             component.set("v.isFlagStagRicAtto","true");
             component.set("v.isFlagStagRicAttoDisable","false");
             component.set("v.isPotImp","true");
             component.set("v.isPotDisp","true");
             component.set("v.isIdSettore","true");
             component.set("v.isSettoreDisable","true");
             component.set("v.isAutocertContrConn","true");
             component.set("v.isCarDetCon","true");
             component.set("v.isSollevamentoDisable","true"); 
              
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');         
   			//helper.filterPicklistValues(component, event, helper, recTypeNAME,'Tensione__c','v.tensione','','','','');         
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');                      
			helper.filterPicklistValues(component, event, helper, recTypeNAME,'PotImp__c','v.impegnata','','','','');        
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');            
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'AutocertContrConn__c','v.autocert','','','','');
			helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');              
        }
        else if(recTypeNAME==='PT_S01'){
            component.set("v.isIdTipoCont","true");
            component.set("v.Tipologiarequired",false);
            component.set("v.isTipologiaDisable","false");
            component.set("v.isFlagStagRicAtto","true");
            component.set("v.isFlagStagRicAttoDisable","false");
            component.set("v.isIdSettore","true");
            component.set("v.isSettoreDisable","true");
            component.set("v.isAutocertContrConn","true");
            component.set("v.isCarDetCon","true");
            component.set("v.isSollevamentoDisable","true");
            component.set("v.dispAutoSol","true");
         
         
	        helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');                 			      
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');             
           // helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','','','','');         			       
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');            
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'AutocertContrConn__c','v.autocert','','','','');            
                          
            //component.set("v.displayDataFields","true");
         
        }
        else if(recTypeNAME==='PT_S02'){
             component.set("v.isIdTipoCont","true");
			 component.set("v.isTipologiaDisable","false");            
			 component.set("v.Tipologiarequired","true"); 
             component.set("v.isTensione","true");
             component.set("v.isTensioneDisable","false"); 
             component.set("v.isTensionerequired",false); 
             component.set("v.isFlagStagRicAtto","true");
             component.set("v.isFlagStagRicAttoDisable","false");
             component.set("v.isPotImp","true");
             component.set("v.isPotDisp","true");
             component.set("v.isIdSettore","true");
             component.set("v.isSettoreDisable","true"); 
             component.set("v.isAutocertContrConn","true");
             component.set("v.isCarDetCon","true");
             component.set("v.isSollevamentoDisable","true");
             component.set("v.dispAutoSol","true");  
            
			helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');         
   			helper.filterPicklistValues(component, event, helper, recTypeNAME,'Tensione__c','v.tensione','','','','');         
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');             
           // helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','','','','');         
			helper.filterPicklistValues(component, event, helper, recTypeNAME,'PotImp__c','v.impegnata','','','','');        
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'CarDetCon__c','v.trattamento','','','','');            
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'AutocertContrConn__c','v.autocert','','','','');            
                       
            //component.set("v.displayDataFields","true");
            
        }
        else if(recTypeNAME==='PT_MC1'){
            component.set("v.isIdTipoCont","true");
            component.set("v.isTipologiaDisable","false");
            component.set("v.Tipologiarequired",false);
            component.set("v.isTensione","true");
            component.set("v.isTensioneDisable","false"); 
            component.set("v.isTensionerequired",false);
            component.set("v.isFlagStagRicAtto","true");
            component.set("v.isFlagStagRicAttoDisable","false");
            component.set("v.isPotImp","true");
            component.set("v.isPotDisp","true");
			component.set("v.isIdSettore","true");
            component.set("v.isSettoreDisable","false"); 
			component.set("v.Settorerequired",false);             
            component.set("v.isSollevamentoDisable","true");  
			component.set("v.dispAutoSol","true");
            
    		helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','','','','');         
   			helper.filterPicklistValues(component, event, helper, recTypeNAME,'Tensione__c','v.tensione','','','','');         
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','','','','');         
			helper.filterPicklistValues(component, event, helper, recTypeNAME,'PotImp__c','v.impegnata','','','','');        
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagSollevamentoPers__c','v.persone','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagAutocertSollev__c','v.sollevamento','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','','','','');             
        }
        else{}
        
        if(recTypeNAME==='PT_PC1'||
            recTypeNAME==='PT_PT1'){
            component.set("v.isDataFine","true");            
        }
        
        //helper.initilizePicklistValues(component, event, helper);    
       
    },
    
    fireComponentEvent : function(component, event, helper) {
		var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "requestEventObj" : component.get("v.request") 
        });
        //debugger;
        cmpEvent.fire();
	},
    
   
    onConnessioneFieldChange: function(component, event, helper) {
		var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        var recTypename = component.get("v.recTypeName");
        component.set('v.recTypeName', recType.Name);
		var recTypeNAME = component.get("v.recTypeName");
        
      var selected = event.getSource().get("v.value");
   //  if(selected==="TEM")   
      helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdTipoContr__c','v.contratto','IdConnessione__c',selected,'','');
          	
        //alert('second-->'+selected);
        //debugger;        
        if(selected==="TEM"){
            component.set("v.displayDataFields","true");
            component.set("v.isTipologiaDisable", "false"); 
            component.set("v.isTensioneDisable","true");
            component.set("v.isUsoDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true");
            component.set("v.isSettoreDisable","true");
            component.set('v.isILLMT', "true");
            component.set('v.isILLMTstagionale', "true");  
            component.find('Tensione__c').set("v.value","");  
			component.find('IdTipoContr__c').set("v.value","");                     
           	component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","");
            component.find('IdSettore__c').set("v.value","");
             
         }        
        else{
            component.set("v.displayDataFields","false");
            component.set("v.isTipologiaDisable", "false"); 
            component.set("v.isUsoDisable","true");
            component.set("v.isTensioneDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true");
            component.set("v.isSettoreDisable","true");
            component.set('v.isILLMT', "true");
            component.set('v.isILLMTstagionale', "true");  
            component.find('Tensione__c').set("v.value","");
            component.find('IdTipoContr__c').set("v.value","");
            component.find('IdSettore__c').set("v.value","");
            component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","");
        }
		if(selected==="")
        {
         component.set("v.isTipologiaDisable","true");   
        }
         if(selected==="TEM" && recTypeNAME=="PT_N02"){
            component.set("v.displayDataFields","true");
            component.set("v.isTipologiaDisable", "false"); 
            component.set("v.isTensioneDisable","true");
            component.set("v.isUsoDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true");
            component.set("v.isSettoreDisable","true");
            component.set('v.isILLMT', "true");
            component.set('v.isILLMTstagionale', "true");  
            component.find('Tensione__c').set("v.value","");  
			component.find('IdTipoContr__c').set("v.value","");                     
           	component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","N");
            component.find('IdSettore__c').set("v.value","");
             
         }        
        
        
        //Fire Application event to hide fields in the PT_AltriDati_RFI Component.
            var appEvent = $A.get("e.c:PT_AltriDati_RFI_Event");
        	appEvent.setParams({
            	"idConnessione" : event.getSource().get("v.value") });
        	appEvent.fire();
               
   },
 
   // function call on change tha Dependent field    
   onTipologiaFieldChange: function(component, event, helper) {
		//alert('insidetipo');
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        var recTypename = component.get("v.recTypeName");
        component.set('v.recTypeName', recType.Name);
		var recTypeNAME = component.get("v.recTypeName");
        var selected = event.getSource().get("v.value");
        
       
    
       if(recTypeNAME!=='PT_S01'&& recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1' && recTypeNAME!=='PT_A01'&& recTypeNAME!=='PT_A03'){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'Tensione__c','v.tensione','IdTipoContr__c',selected,'',''); 
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','IdTipoContr__c',selected,'','');
       }
       
       if(selected!=="" &&(recTypeNAME==='PT_A01'|| recTypeNAME==='PT_S01' || recTypeNAME==='PT_S02')){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','IdTipoContr__c',selected,'','');
       component.set("v.isSettoreDisable","false");
       component.find('IdSettore__c').set("v.value","");
       }
       if(selected==="" && (recTypeNAME==='PT_A01'|| recTypeNAME==='PT_S01' || recTypeNAME==='PT_S02')){
       component.set("v.isSettoreDisable","true");
       component.find('IdSettore__c').set("v.value","");
       }
       if(selected!=="" && recTypeNAME==='PT_A03' ){
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'Tensione__c','v.tensione','IdTipoContr__c',selected,'','');     
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdSettore__c','v.settore','IdTipoContr__c',selected,'','');   
       component.set("v.isSettoreDisable","false");
       component.find('IdSettore__c').set("v.value","");
       component.set("v.isTensioneDisable","false");
       component.find('Tensione__c').set("v.value","");
       }
    	if(selected==="" && recTypeNAME==='PT_A03'){
       component.set("v.isSettoreDisable","true");
       component.set("v.isTensioneDisable","true");
       component.find('IdSettore__c').set("v.value","");
       component.find('Tensione__c').set("v.value","");
       }
       if(selected!=="" && (recTypeNAME!=='PT_S01'&& recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1'&& recTypeNAME!=='PT_A03' && recTypeNAME!=='PT_A01')){                    
            component.set("v.isTensioneDisable","false");
            component.set("v.isUsoDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true");
            component.set("v.isSettoreDisable","false");
            component.find('Tensione__c').set("v.value","");
            component.set('v.isILLMT', "true");
            component.set('v.isILLMTstagionale', "true");  
           	component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","");
            component.find('IdSettore__c').set("v.value",""); 
            component.set("v.isSollevamentoDisable","false");
        }
       if(selected==="" && (recTypeNAME!=='PT_S01'&& recTypeNAME!=='PT_MC1'&& recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_A03'&& recTypeNAME!=='PT_A01')){
            component.set("v.isTensioneDisable","true");
            component.set("v.isUsoDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true");
            component.set("v.isSettoreDisable","true");
            component.set('v.isILLMT', "true");
            component.set('v.isILLMTstagionale', "true");  
            component.find('Tensione__c').set("v.value","");                     
           	component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","");
            component.find('IdSettore__c').set("v.value","");
            component.set("v.isSollevamentoDisable","false");
       }
       if(recTypeNAME=="PT_N02"){
        var connessione= component.find('IdConnessione__c').get("v.value","");
           if(connessione==="TEM"){
           component.find('FlagStagRicAttoo__c').set("v.value","N");    
           } 
       }
   },

   // function call on change tha Dependent field    
   onTensioneFieldChange: function(component, event, helper) {
	   var selected = event.getSource().get("v.value");
       var recType = component.get("v.recordType");
       component.set('v.recTypeName', recType.Name);
	   var recTypeNAME = component.get("v.recTypeName");                    
       var cont2 = component.find("IdTipoContr__c").get("v.value");
       
       if((recTypeNAME==='PT_N01' && selected!=="")){
            helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdUsoEnergia__c','v.energia','Tensione__c',selected,'','');    
       		component.set("v.isUsoDisable","false");
            component.find("IdUsoEnergia__c").set("v.value","");
       }  else if(recTypeNAME==='PT_N01' && selected!=="")  {
           component.set("v.isUsoDisable","true");       
           component.find('IdUsoEnergia__c').set("v.value","");
       }
       
       if(recTypeNAME==='PT_N02'){
       if (selected==="MT" && cont2==="ILL" ){
       component.find("IdUsoEnergia__c").set("v.disabled","true");
       component.find("FlagStagRicAttoo__c").set("v.disabled","true");
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdUsoEnergia__c','v.energia','Tensione__c',selected,'IdTipoContr__c',cont2);            
       helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','Tensione__c',selected,'IdTipoContr__c',cont2);  
         var energia = component.get("v.energia"); 
         var stagionale = component.get("v.stagionale");
         var v = JSON.stringify(energia);
         //alert(v);
          
          component.set('v.isILLMT', "false");
          component.set('v.isILLMTstagionale', "false");  
          component.find("IdUsoEnergia__c").set("v.value",energia);
          component.find("FlagStagRicAttoo__c").set("v.value",stagionale);
          component.set("v.isSollevamentoDisable","true"); 
                    
       }
       else
       {         
           component.set('v.isILLMT', "true");
           component.set('v.isILLMTstagionale', "true");  
           component.find("IdUsoEnergia__c").set("v.value","");
           component.find("FlagStagRicAttoo__c").set("v.value","");
           component.set("v.isSollevamentoDisable","false");
           helper.filterPicklistValues(component, event, helper, recTypeNAME,'IdUsoEnergia__c','v.energia','Tensione__c',selected,'','');  
       }
       }
      if(selected==="AT" && (recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1'&& recTypeNAME!=='PT_A03' )){
            component.find("FlagStagRicAttoo__c").set("v.value","N");  
            component.find("IdUsoEnergia__c").set("v.disabled","true");
            component.find("IdUsoEnergia__c").set("v.value","001");                      
            component.find("FlagStagRicAttoo__c").set("v.disabled","true");
   		    component.set("v.isSollevamentoDisable","true");          
         }
           if((selected!=="AT" && selected!=="" && selected!=="MT") && (recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1'&& recTypeNAME!=='PT_A03') ) {	
            component.find("IdUsoEnergia__c").set("v.value","");
            component.set("v.isUsoDisable","false");
            component.find("FlagStagRicAttoo__c").set("v.disabled","true");
            component.find("FlagStagRicAttoo__c").set("v.value","");
            
            //helper.filterPicklistValues(component, event, helper, 'N02','IdUsoEnergia__c','v.energia','Tensione__c',selected,'',''); 
            //helper.fetchPicklistValues1(component, 'Tensione__c', 'IdUsoEnergia__c'); 
            component.set("v.isSollevamentoDisable","false");
               
    	} 
       if((selected==="MT" && cont2!=="ILL") && (recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1' && recTypeNAME!=='PT_A03' )){
            component.set("v.isUsoDisable","false");
           component.set('v.isILLMT', "true");
           component.set('v.isILLMTstagionale', "true"); 
            component.find("FlagStagRicAttoo__c").set("v.disabled","true");
            component.find("FlagStagRicAttoo__c").set("v.value",""); 
            component.find("IdUsoEnergia__c").set("v.value","");
            component.set("v.isSollevamentoDisable","false");   
       }
      if(selected==="" &&(recTypeNAME!=='PT_S02'&& recTypeNAME!=='PT_MC1' && recTypeNAME!=='PT_A03' )){  
            component.set("v.isUsoDisable","true");
            component.set("v.isFlagStagRicAttoDisable","true"); 
          
            component.find('IdUsoEnergia__c').set("v.value","");
            component.find('FlagStagRicAttoo__c').set("v.value","");
            component.set("v.isSollevamentoDisable","false");
                       
        }
        if(recTypeNAME=="PT_N02"){
        var connessione= component.find('IdConnessione__c').get("v.value","");
           if(connessione==="TEM"){
           component.find('FlagStagRicAttoo__c').set("v.value","N");    
           } 
       }
                          
   },
   // function call on change tha Dependent field    
   onUsoFieldChange: function(component, event, helper) {

        var recType = component.get("v.recordType");
        component.set('v.recTypeName', recType.Name);
	    var recTypeNAME = component.get("v.recTypeName");
        var selected = event.getSource().get("v.value");
       var idCons = component.find("IdConnessione__c").get("v.value");
       var usoenergiaval = component.find("IdUsoEnergia__c").get("v.value");
      
       if(idCons==="PER" && (selected==="001" || usoenergiaval==="001")  && recTypeNAME==='PT_N02'){
         component.set("v.isSollevamentoDisable","true");  
       }
       if(idCons==="PER" && (selected!=="001" || usoenergiaval!=="001") && recTypeNAME==='PT_N02'){
         component.set("v.isSollevamentoDisable","false");  
       }
       if(idCons==="TEM" && (selected!=="" ||  usoenergiaval!=="") && recTypeNAME==='PT_N02'){
         component.set("v.isSollevamentoDisable","true");  
       }
      
       if(selected!=="" ){
        helper.filterPicklistValues(component, event, helper, recTypeNAME,'FlagStagRicAtto__c','v.stagionale','IdUsoEnergia__c',selected,'','');       
        component.set("v.isFlagStagRicAttoDisable","false");
        component.find('FlagStagRicAttoo__c').set("v.value","");
        }
        if(selected===""){
        component.set("v.isFlagStagRicAttoDisable","true"); 
        component.find('FlagStagRicAttoo__c').set("v.value",""); 
        component.set("v.isSollevamentoDisable","false");   
        }
        if(recTypeNAME=="PT_N02"){
        var connessione= component.find('IdConnessione__c').get("v.value","");
           if(connessione==="TEM"){
           component.find('FlagStagRicAttoo__c').set("v.value","N"); 
           component.set("v.isFlagStagRicAttoDisable","true"); 
           } 
       }
         
       },      
 
        onSolChange: function (component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        var recTypename = component.get("v.recTypeName");
        component.set('v.recTypeName', recType.Name);
		var recTypeNAME = component.get("v.recTypeName");

       var selected = event.getSource().get("v.value");
;
        if((selected === "S") && (recTypeNAME==='PT_A01'||recTypeNAME==='PT_N02')){
            component.set("v.dispAutoSol","true");
        }    
        if((selected === "N" || selected === "" )&& (recTypeNAME==='PT_A01'||recTypeNAME==='PT_N02')){
            component.set("v.dispAutoSol","false");
        }
     },
    
    onFlagStagRicAttoChange: function (component, event, helper) {
    var selected = event.getSource().get("v.value");
        
    if(selected==='S'){
    component.set("v.displayDataFields","true");  
		}
    if(selected!=='S'){
    component.set("v.displayDataFields","false");  
		}    
		},
    
    
 checkPotDispFormat : function(component, event, helper){
        var	fieldValue = component.find("PotDisp__c").get("v.value");
        var label = component.find("PotDisp__c").get("v.label");
    	var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recTypeName");
         if((!$A.util.isEmpty(fieldValue))&&
           (recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02")))){
        var strVal = fieldValue.toString();
          var lettFormat=  /^([+])?\d{1,5}$/g; //new RegExp($A.get("^\d{0,12}(\.\d{0,3})?$"));
        var validatelettFormat = strVal.match(lettFormat);   //lettFormat.test(nCAP);
         if(validatelettFormat){
                var inputCmp = component.find("PotDisp__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: '+label;
                var inputCmp1 = component.find("PotDisp__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }
         
         }
         else{
                var inputCmp2 = component.find("PotDisp__c");
                inputCmp2.set("v.errors", null);
         }
    },
    
  
})