@RestResource(urlMapping='/getCategoryList/*')

global with sharing class KBMS_DataCategoryListRestFul{
    
    @HttpGet
    global static List<KBMS_Category> doGet() 
    {
		String topCatString = 'Tutto';
		
        System.debug(topCatString);
    	List<KBMS_Category> cat =    KBMS_DataCategoryUtil.getCategoryGroupStructure('KBMS_Operai_Tecnici',topCatString);
     	return cat;
   }    
}