/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is a batch class which takes records from loginHistory of specific profiles & insert data into KBMS_UserLogin__c.
*/
global class KBMS_UserLoginBatchable implements  Database.Batchable<sObject>, Database.Stateful
{
    global Set<String> uniqueValues = new Set<String>();
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        Integer loginHistoryPeriod = Integer.valueOf(Label.KBMS_LoginHistoryPeriod);
        
        String query = 'SELECT ApiType,Application,Browser,Id,LoginTime,LoginType,LoginUrl,NetworkId,Platform,UserId FROM LoginHistory WHERE LoginTime=LAST_N_DAYS:' +loginHistoryPeriod ;
        
        if (Test.isRunningTest())
        {
            query += ' Limit 200';
        }
        return Database.getQueryLocator(query);

    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<LoginHistory> loginHistoryList) 
    {    
        List<String> profileList = new List<String>();
        profileList = Label.KBMS_Profile.split(',');
        
        List<User> userList = new List<User>();
        
        if (Test.isRunningTest())
        {
            userList = [SELECT Id,Profile.Name,FederationIdentifier FROM User];
        }
        else
        {
            userList = [SELECT Id,Profile.Name,FederationIdentifier FROM User where Profile.Name In: profileList];
        }
        
                
        Map<ID,User> userMap=new Map<ID,User>();    //it contains userid and User details 
        
        for (User u: userList)
        {
            userMap.put(u.id,u);   
        } 
        
        List<KBMS_UserLogin__c> toBeInsrtedUserLogin = new List<KBMS_UserLogin__c>();
        
        for (LoginHistory lh: loginHistoryList)
        {
        
            if (userMap.containsKey(lh.UserId))
            {
                Date loginDate = date.newinstance(lh.LoginTime.year(), lh.LoginTime.month(), lh.LoginTime.day());
                String loginDateStr = String.valueOf(loginDate );
                String currentValue ;
                
                KBMS_UserLogin__c ul = new KBMS_UserLogin__c();
                ul.ApiType__c = lh.ApiType;
                ul.Application__c = lh.Application;
                ul.Browser__c = lh.Browser;
                ul.LoginTime__c = lh.LoginTime;
                ul.LoginType__c = lh.LoginType;
                ul.LoginUrl__c = lh.LoginUrl;
                ul.NetworkId__c = lh.NetworkId;
                ul.Platform__c = lh.Platform;
                ul.FederationIdentifier__c=userMap.get(lh.UserId).FederationIdentifier;
                ul.Profile__c = userMap.get(lh.UserId).Profile.Name;                    
                ul.User_Name__c = lh.UserId;
                ul.Interface__c = (ul.Application__c == 'EnelOpenKnowledge' || (ul.Application__c.contains('Browser') && ul.Platform__c.contains('Windows'))) ? true : false;
                
                currentValue = (ul.Application__c == 'EnelOpenKnowledge') ? (lh.UserId + loginDateStr + lh.Application) : (lh.UserId + loginDateStr + lh.Application + lh.Platform);
                
                if (!uniqueValues.contains(currentValue) && ul.Interface__c  == true)
                {
                    ul.PrimoAccessoGiornaliero__c = true;
                    uniqueValues.add(currentValue);
                }

                toBeInsrtedUserLogin.add(ul);
            
            }
        }            
        
        insert toBeInsrtedUserLogin;   
    }

    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        
    }
}