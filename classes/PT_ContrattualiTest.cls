/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for PT_Contrattuali.
**********************************************************************************************/

@isTest(SeeAllData = false)
/********************/
private class PT_ContrattualiTest{
    private static final String ALIAS = 'unEx1';
/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description :method to dependant picklist values in PT_Contrattuali.
**********************************************************************************************/
    private static testMethod void dependent(){
         User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);
        
        String req='PT_Richiesta__c';
        String tensione='Tensione__c';
        String usoEnergia='IdUsoEnergia__c';
        PT_Contrattuali.getDependentOptionsImpl(req,tensione,usoEnergia);
        system.assertNotEquals(null, PT_Contrattuali.getDependentOptionsImpl(req,tensione,usoEnergia));
        // System.runAs(runningUser){
              PT_Contrattuali.getDependentOptionsImpl(req,tensione,usoEnergia);  
         //   }
    }
}