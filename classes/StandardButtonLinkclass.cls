/* Author:@Accenture
*/

public class StandardButtonLinkclass {

   public static Integer indexGen {get; set;}
    public List<SBLwrapper> SBLlist;
    public Integer numRows {get; set;}
    

     public class SBLwrapper {
    
        private     Standard_Buttons_and_Links__c SBLsetting;
        private Integer index;  

        public SBLwrapper() {
            Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            System.Debug('Key is ' + key);
            String strParentObjectID = lstParams.get(key);
            this.SBLsetting = new   Standard_Buttons_and_Links__c (Custom_Object__c = strParentObjectID);

            //this.SBLsetting = new   Standard_Buttons_and_Links__c (Custom_Object__c = ApexPages.currentPage().getParameters().get(label.CL00041));
            //String coId = ApexPages.currentPage().getParameters().get(label.CL00042);
            //co = [SELECT Enable_History__c FROM CustomObjects__c WHERE Id = :coId];);
            this.index = indexGen++;
        }
        
        public  Standard_Buttons_and_Links__c getSBLsetting() {
            return SBLsetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 
     
     public StandardButtonLinkclass(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;

        SBLlist = new List<SBLwrapper>();
        numRows = 1;
    }
    
    public List<SBLwrapper> getSBLlist() {
        return SBLlist;
}
 

public PageReference save() {
        try {
            List<   Standard_Buttons_and_Links__c> tempList = new List< Standard_Buttons_and_Links__c>();
            for(Integer i=0; i<SBLlist.size(); ++i)
                tempList.add(SBLlist[i].getSBLsetting());
            upsert(tempList);
            return new PageReference ('/' + ApexPages.currentPage().getParameters().get('retURL'));
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }


        


    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    SBLlist.add(new SBLwrapper());
        } 
        catch (Exception ex) {
        ApexPages.addMessages(ex);
        
        }
    }
    
    public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                SBLwrapper sblWrap = new SBLwrapper();
                sblWrap.SBLsetting = [SELECT Comment__c,Content_Name__c,    Content_Type__c,Custom_Object__c,Label__c,Name__c,Overridden__c,Requirement_no__c FROM Standard_Buttons_and_Links__c WHERE ID = :CurrentRecordID];
                SBLList.add(sblWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    public void clear() {
        SBLlist.clear();
        numRows = 1;
    }
    
  
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<SBLlist.size(); ++i)
                if(SBLlist[i].index == delIndex) {
                    SBLlist.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}