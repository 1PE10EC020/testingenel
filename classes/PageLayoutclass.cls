/*
 * @author : Accenture
 * @date : 2011-09-28
 */

public class PageLayoutclass {

   public static Integer indexGen {get; set;}
    public List<PLwrapper> PLlist;
    public Integer numRows {get; set;}
    

     public class PLwrapper {
    
        private     Page_Layout__c PLsetting;
        private Integer index;  

        public PLwrapper() {
            Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            System.Debug('Key is ' + key);
            String strParentObjectID = lstParams.get(key);
            this.PLsetting = new   Page_Layout__c (Custom_Object__c = strParentObjectID );
            
            //this.PLsetting = new   Page_Layout__c (Custom_Object__c = ApexPages.currentPage().getParameters().get(label.CL00046));
            this.index = indexGen++;
        }
        
        public  Page_Layout__c getPLsetting() {
            return PLsetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 
     
     public PageLayoutclass(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;

        PLlist = new List<PLwrapper>();
        numRows = 1;
    }
    
    public List<PLwrapper> getPLlist() {
        return PLlist;
}
 

public PageReference save() {
        try {
            List<   Page_Layout__c> tempList = new List< Page_Layout__c>();
            for(Integer i=0; i<PLlist.size(); ++i)
                tempList.add(PLlist[i].getPLsetting());
            upsert(tempList);
            return new PageReference ('/' + ApexPages.currentPage().getParameters().get('retURL'));
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }


        


    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    PLlist.add(new PLwrapper());
        } 
        catch (Exception ex) {
        ApexPages.addMessages(ex);
        
        }
    }
    public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                PLwrapper PLWrap = new PLwrapper();
                PLWrap.PLsetting = [SELECT Active__c,Custom_Object__c,Description__c,Layout_Name__c, Requirement_no__c FROM Page_Layout__c WHERE ID = :CurrentRecordID];
                PLList.add(PLWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    
    public void clear() {
        PLlist.clear();
        numRows = 1;
    }
    
  
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<PLlist.size(); ++i)
                if(PLlist[i].index == delIndex) {
                    PLlist.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}