/*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search.
**********************************************************************************************/
@isTest
public class PT_Ricerca_Report_Test {
/*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search.
**********************************************************************************************/
	PRIVATE STATIC FINAL STRING TESTS='test23';
		PRIVATE STATIC FINAL STRING TESTS1='test231';
    private static final String TEST = 'test';
    private static final String B01 = 'B01';
    private static final String C1 = 'C1';
    private static final String M03 = 'M03';
     PRIVATE STATIC FINAL INTEGER LMT=1; 
  PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING REPONAME='ReportPrenotatoRichieste_100_10:49:00.csv';
    
     @testSetup static void createTestRecords() {
    list<PT_Report__c > lstInsertService = new list<PT_Report__c >();
    PT_Report__c cmaer= new PT_Report__c();
    	 cmaer.IdTipoReport__c= M03;
         cmaer.DataEstraz__c=system.today();
         cmaer.DataPub__c=system.today();
         cmaer.IdReportFour__c=12;
         cmaer.IdTrader__c=TEST;
         cmaer.IdSocieta__c=TEST;
         cmaer.CategFlusso__c= C1;
         cmaer.NomeFileReport__c=TEST;
         cmaer.Note_Report__c=TEST;
         cmaer.IdTipoReport__c=B01;
         lstInsertService.add(cmaer);
        
        database.insert(lstInsertService);
        // system.debug('lstInsertService==>'+lstInsertService);
    }
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
    static testMethod void pt_Riciera_Test () {
        //List < PT_F05_Search.wrapperClass > wraplist = new List < PT_F05_Search.wrapperClass > ();
        //Test.startTest();
        //User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        list<PT_Report__c> lstInsertService = new list<PT_Report__c>();
        PT_Report__c objService =[Select Id,Name,Note_Report__c,DataPub__c,DataEstraz__c,NomeFileReport__c From PT_Report__c];
       	// system.debug('objService==>'+objService);
      	// system.debug('lstInsertService2==>'+lstInsertService);
         PT_Ricerca_Report_Controller.getWrapper();            
            PT_Ricerca_Report_Controller.tiporichValues();
        	PT_Ricerca_Report_Controller.getDisp();
            PT_Ricerca_Report_Controller.wrapperClass wrap = new PT_Ricerca_Report_Controller.wrapperClass();
            wrap.DataPubblicazioneda = System.today();
            wrap.DataPubblicazioneA = System.today()+1;
            wrap.TipoRich = 'M03';
            wrap.Provincia='test';
            wrap.NomeFile='test';
       		wrap.CodiceRichiesta='test';
            wrap.CategoriaFlusso='C1';
        	wrap.TipoFlusso='F1';
 			wrap.DataEstrazioneda = System.today();
            wrap.DataEstrazioneA = System.today()+1;    
        
          PT_Ricerca_Report_Controller.wrapperClass wrap1 = new PT_Ricerca_Report_Controller.wrapperClass();
            wrap1.DataPubblicazioneda = System.today();
            wrap1.DataPubblicazioneA = System.today()+1;
            wrap1.TipoRich = 'M03';
            wrap1.Provincia=null;
            wrap1.NomeFile='test';
       		wrap1.CodiceRichiesta='test';
            wrap1.CategoriaFlusso='C1';
        	wrap1.TipoFlusso='F1';
 			wrap1.DataEstrazioneda = System.today();
            wrap1.DataEstrazioneA = System.today()+1; 
        
            String myJSON = JSON.serialize(wrap);
        	String myJSON1 = JSON.serialize(wrap1);
            string strfields = 'Note_Report__c,DataPub__c,DataEstraz__c,NomeFileReport__c';
            string objname = 'PT_Report__c ';
            //Test.startTest();
            Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
  User usr = new User(Alias = ALIAS, Email=EMAIL,
    EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
    LocaleSidKey=ENUS, ProfileId = pfl.Id,
    TimeZoneSidKey=TZK,
    UserName=UUNAME);
  database.insert(usr);
          System.runAs(usr) {
        	List<PT_Ricerca_Report_Controller.WrapperListView> cslist = new List<PT_Ricerca_Report_Controller.WrapperListView>();
            PT_Ricerca_Report_Controller.getQueryall(myJSON, 'PT_Report__c', 'Nome,Note_Report__c,DataPub__c,DataEstraz__c,NomeFileReport__c,DescrDispaccEstesa__c,wrap.CategFlusso__c', 100);
         PT_Ricerca_Report_Controller.getQueryall(myJSON1, 'PT_Report__c', 'Nome,Note_Report__c,DataPub__c,DataEstraz__c,NomeFileReport__c,DescrDispaccEstesa__c,wrap.CategFlusso__c', 100);     
      //  Test.stopTest();
             system.assertEquals(wrap.TipoRich , 'M03');
      }
    }
     
 }