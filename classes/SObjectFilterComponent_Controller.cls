/*******************
 * Author : Giulia Tedesco
 * Apex Class  :  SObjectFilterComponent_Controller
 * CreatedDate :  20/09/2017
 * Description :  This class is SObjectAndFieldName component controller.
 				  It is used to translate Picklist Values of the SmartListView Component
****************/


public class SObjectFilterComponent_Controller {


	@AuraEnabled
	public static PaginationWrapper getTranslationPicklist(String recordId, SObject objectt, String fieldName){

		PaginationWrapper wrapObj = new PaginationWrapper();

		wrapObj = checkMetadata(recordId,objectt,fieldName,wrapObj);
		System.debug('**translation Wrapper: ' +wrapObj);
		

		return wrapObj;
	}


	// Wrapper Class for the pagination
    public class PaginationWrapper{
        @AuraEnabled
        public Boolean isPicklist {get;set;}
        @AuraEnabled
        public String translation {get;set;}

       
        public PaginationWrapper(){
            isPicklist = false;
            translation = '';
        }
        
    }


    public static PaginationWrapper checkMetadata(String recordId, SObject objectt, String fieldName, PaginationWrapper wrapObj){

    	//**************GET SObject and field API NAME**********************
		Schema.SObjectType sobjectApiName = objectt.getsObjectType();
		system.debug('***Sobject API Name: ' + sobjectApiName);
		String picklistValue = '';
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(String.valueOf(sobjectApiName));
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

        
        //It provides to get the object fields data type.
        Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
	        
        if(fielddataType == Schema.DisplayType.Picklist){
        	wrapObj.isPicklist = true;
        	//It provides to get the object fields label.
        	String fieldLabel = fieldMap.get(fieldName).getDescribe().getName();
        	System.debug('***fieldLabel: ' + fieldLabel);
        	//Generate dynamic DML
        	String query = 'SELECT ' +  fieldLabel + ' ' + 'FROM ' +String.valueOf(sobjectApiName)+ ' WHERE '
						+ 'Id' + ' = ' +'\''+recordId+'\''+' LIMIT 1';
			System.debug('***query: ' + query);

			SObject s = (Database.query(query));
			Object o = s.get(fieldLabel);
			
			picklistValue = String.valueOf(o);
			System.debug('***picklistValue: '+picklistValue);

			if(picklistValue!=null || !String.isBlank(picklistValue)){
				wrapObj.translation = getPicklistTranslation(fieldMap,sobjectApiName,fieldName,wrapObj,picklistValue);
			}
        }
		return wrapObj;

    }

    
    public static String getPicklistTranslation(Map<String, Schema.SObjectField> fieldMap, Schema.SObjectType sobjectApiName, 
    											String fieldName,PaginationWrapper wrapObj, String picklistValue){

    	List<SelectOption> options = new List<SelectOption>();
    	Map<String,String> cmMap = new Map<String,String>();

		if(sobjectApiName != null){

			List<Schema.PicklistEntry> ple = fieldMap.get(fieldName).getDescribe().getpicklistValues();
			System.debug('*** PicklistEntry: '+ple);
			//List<SelectOption> options2 = new List<SelectOption>();
			for(Schema.PicklistEntry f : ple)
		    {
		      options.add(new SelectOption(f.getLabel(), f.getValue()));
		    }
		    System.debug('*** options: '+options);

		    for (SelectOption so : options){
		    	cmMap.put(so.getLabel(), so.getValue());
		    } 

		    wrapObj.translation = cmMap.get(picklistValue);
		    System.debug('*** mappa: '+cmMap);
		    System.debug('*** wrapObj.translation: '+wrapObj.translation);

		}
		return wrapObj.translation;

    }

}