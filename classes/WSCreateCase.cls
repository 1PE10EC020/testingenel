Global class WSCreateCase {

 /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 8-10-2015
    * Description : Webservice class used as imput for the webservice  
    * Output :  
    *			CONNID						String				conversation ID ( used by IVR system)
    * 			PHONE						String				inbound phone number
    *			PHONE_CB					String				phone number used to recall the customer
    *			CF							String				customer fiscal code
    *			POD							String				pod related to the case
    *			CASE_NUM					String				related case number
    *			ID_PR						String				ID pratica
    *			COD_IVR						String				Navigation's Code
    *			FASCIA_ORARIA				String				callback preferred time    	
    */
	global class CallbackCase{
    webservice String CONNID;
    webservice String PHONE;
    webservice String PHONE_CB;
    webservice String CF;
    webservice String POD;
    webservice String CASE_NUM;
    webservice String ID_PR;
    webservice String COD_IVR;
    webservice String FASCIA_ORARIA;
}

  
        /* 
        * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
        * Createddate : 8-10-2015
        * Description : Webservice class used to retunr the result on external service       
        * Output :  
        *			codice						String				error Code ( 0 --> no error found)
        * 			descrizione					String				error description
        */
	global class CallbackCaseResponse {
		webservice String codice;
		webservice String descrizione;
		
		public CallbackCaseResponse(string cod, string descr){
			codice = cod;
			descrizione = descr;
		}
		
	}

	/* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 8-10-2015
    * Description : Webservice used by IVR to create a callback case
    * input :  
	*			inCase			CallbackCase		input data for case's creation
    *output :
	*			    			CallbackCaseResponse		response for the operation	
    *		
    */
	webservice static CallbackCaseResponse CreateCallBackCase (CallbackCase inCase){
		try{
			if (String.isNotBlank(inCase.COD_IVR) )
			 {
			 	// search for the IVR PATH selected
			 	PercorsoTelefonico__c ivrPathSelected = IVR_Helper.findIVRPath(inCase.COD_IVR);
			 	Account accFound;
			 	// if a CF is present , search for the related account
			 	if(String.isNotBlank(inCase.CF))
			 	       accFound= Account_Helper.retriveAccountByFiscalCode(inCase.CF);
			 	// call the helper class for case creation
				Case newCase = Case_Helper.createNewCase(accFound,ivrPathSelected,inCase.CONNID,'','',incase.POD,incase.ID_PR,Constants.ORIGIN_TELEFONO,incase.PHONE,Constants.CASE_STATUS_NUOVO );
			 	// decode the callback time
			 	if(String.isNotBlank(inCase.FASCIA_ORARIA))
			 	{
			 		if ( Constants.FASCIA_ORARIA_1.equals(inCase.FASCIA_ORARIA))
			 			newCase.Fascia_Oraria_Callback__c = System.Label.CASE_FASCIA_ORARIA_1;
				 	if ( Constants.FASCIA_ORARIA_2.equals(inCase.FASCIA_ORARIA))
				 		newCase.Fascia_Oraria_Callback__c = System.Label.CASE_FASCIA_ORARIA_2;
				 	if ( Constants.FASCIA_ORARIA_3.equals(inCase.FASCIA_ORARIA))
				 		newCase.Fascia_Oraria_Callback__c = System.Label.CASE_FASCIA_ORARIA_3;
			 	}
			 	else
			 	{
			 		return new CallbackCaseResponse('101','FASCIA_ORARIA is a required field');
			 	}
			 	
			 	// assigh the case to the queue 'Callback'	
			 	 newcase.ownerid = [select id from Group where type='Queue' and  name  = 'CallBack' limit 1].id;
			 	 // set the callback number
			 	 if(String.isNotBlank(inCase.PHONE_CB))
			 		 newcase.TelefonoDaRichiamare__c = inCase.PHONE_CB;
			     else
			     	return new CallbackCaseResponse('101','PHONE_CB is a required field');
			     
			     	
			 	//search for the related case if present	 
			 	 if (String.isNotBlank(inCase.CASE_NUM ) ){
			 	 	Case parentCase  = Case_Helper.retriveCaseFromCaseNum (inCase.CASE_NUM);
			 	 	if (parentCase != null)
			 	 		newCase.parentid = parentCase.id;
			 	 }
			 	 //inset the case
			 	 insert newCase;
			 	 //return an ok response
			 	 return  new CallbackCaseResponse('000','');
			 	 
			 }
			 else
			 {
			 	return new CallbackCaseResponse('101','COD_IVR is a required field');
			 }
		}
		catch(Exception e){
			 //return an generic error response
			system.debug(logginglevel.error, 'Generic Exception in WSCreateCase - CreateCallBackCase : '+ e.getmessage());
			return new CallbackCaseResponse('100','Generic Error');
		}
		return new CallbackCaseResponse('100','Generic Error');
	}

}