public class Registrazione_Helper {

	public class RecordingResult {
		public Boolean result; // result of WS operation
		public String errorMessage ;  //errormessage is the result is false
		public String recordingId ;  // recording id- saved into case in case of recording start
		
		public RecordingResult(boolean es, string err ){
			result = es;
			errorMessage = err;
			recordingId = '' ; 
		}
		public RecordingResult(boolean es, string err, string recId ){
			result = es;
			errorMessage = err;
			recordingId = recId ; 
		}		
	}
		/*
	*@Author : Claudio Quaranta
	*@Date : 07/10/2015
	*@Description : static method used to start the recording process
	*Input :   Name 						type 				Description
	*			ConnID						String				unique call's identify
	*			DN							String				parameter used by end recording's webservice
	*			TENANT						String				identify the callcenter
	*			PHONE						String				phone number used by caller
	*			CF							String				client's fiscal code
	*			POD							String				client's POD
	*			caseNum					String				case number
	*			id_Pr						String				ID Pratica 
	*			COD_IVR						String				path used by the IVR
							String				parameter used by end recording's webservice
	*Output : 
	*		RecordingResult.result			boolean				true --> recording successfull ended   false--> an error as occurred
	*		RecordingResult.errorMessage	string				populated only if result = False, contains the errormessage
	*/
	public static RecordingResult StartRecording (string connID, string DN, String tenant, String phone , string cf,string pod,  string caseNum, string id_Pr, string cod_ivr) {
		//return new RecordingResult (true,'', 'qwe123qwe'); // STUB DA ELIMINARE 
		try{
				// DN parameter is mandatory
				system.debug('*** StartRecording DN: ' +DN);
				if ( String.isBlank(DN))
					return new RecordingResult (false,System.Label.DN_NOT_DEFINED);
				else{
					// connID parameter is mandatory
					if(String.isBlank(connID)){
						return new RecordingResult (false,System.Label.CONNID_NOT_DEFINED);
					}
					else {
						// WS should be called 
						RegistrazioneService.WsCallRec service = new RegistrazioneService.WsCallRec();
						RegistrazioneService.StartRegistrazioneResponse_element wsResponse =  service.StartRegistrazione(ConnID,DN,tenant,phone,cf,pod,caseNum,id_Pr,cod_ivr);
						system.debug(logginglevel.debug, 'Registrazione_Helper - endRecording - wsResponse : '+ wsResponse);
							if (Constants.RESULT_OK.equals(wsResponse.CodiceEsito))
						return new RecordingResult (true,'', wsResponse.IDRegistrazione);
						else
							return new RecordingResult (false,wsResponse.DescrizioneEsito);
					}
				}			
		}
		catch(Exception e){
			return new RecordingResult (false,e.getMessage());
		}
		
	}
	/*
	*@Author : Claudio Quaranta
	*@Date : 07/10/2015
	*@Description : static method used to end the recording process
	*Input :   Name 						type 				Description
	*			DN							String				parameter used by end recording's webservice
	*Output : 
	*		RecordingResult.result			boolean				true --> recording successfull ended   false--> an error as occurred
	*		RecordingResult.errorMessage	string				populated only if result = False, contains the errormessage
	*/
	
	public static RecordingResult endRecording (string DN) {
		//return new RecordingResult (true,''); // STUB DA ELIMINARE 
		//RecordingResult result;
		try{
				// DN parameter is mandatory for the webservice
				if ( String.isNotBlank(DN)){
					RegistrazioneService.WsCallRec service = new RegistrazioneService.WsCallRec();
					// Call the WS
					RegistrazioneService.StopRegistrazioneResponse_element wsResponse = service.StopRegistrazione(DN);
					// manage The WS Resposnse
					system.debug(logginglevel.debug, 'Registrazione_Helper - endRecording - wsResponse : '+ wsResponse);
					if (Constants.RESULT_OK.equals(wsResponse.CodiceEsito))
						return new RecordingResult (true,'');
					else
						return new RecordingResult (false,wsResponse.DescrizioneEsito);
					
				}
				else
					return new RecordingResult (false,System.Label.DN_NOT_DEFINED);
		}
		catch(CalloutException ce){
			system.debug(logginglevel.error, 'Registrazione_Helper - endRecording - callout exception rised : '+ ce.getMessage());
			// return an error message for timeout
			return new RecordingResult (false,System.label.EXC_Missing_WS_RESP);
		}
		catch(Exception e ){
			system.debug(logginglevel.error, 'Registrazione_Helper - endRecording - generic exception rised : '+ e.getMessage());
			return new RecordingResult (false,e.getMessage());
		}
		
		//return result;
	
	}

}