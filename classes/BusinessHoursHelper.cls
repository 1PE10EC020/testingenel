public class BusinessHoursHelper{






// public members, contains a map of all business hours present on the system.


/*
* Author : Claudio Quaranta
* Date : 18/06/2015
* Description : method that adds an 'hours' using the standard features 'BusinessHours'
* Input :  Name          Type          Description
*          listOfCase    List<Case>    list of all case to modify
*          hours         integer       number to hours to add
*output :
*           listOfCase   List<Case>    list of all case modified
*/

public static list<Datetime> addHours(list<Case> listOfCase){
    //retriving the business hours id
    ID busnessHoursId = Constants.mapBusinessHours.get(Constants.DEFAULT_BUSINESS_HOURS);// retrive the businesshour id from developername
    system.debug(logginglevel.debug,'Businesshours find : '+busnessHoursId);
    long millisec16 = 16*3600*1000; // calculate the inteval to add in millisec
    long millisec40 = 40*3600*1000; // calculate the inteval to add in millisec
    list<Datetime> listDate = new list<Datetime>();
    try{
        //cycling on all case to modify
        for(case c : listOfCase){
            system.debug(logginglevel.debug,'case processing : '+c);
            //system.debug(logginglevel.debug,'AssegnatoIILivello__c : '+c.AssegnatoIILivello__c);
            // check , if  AssegnatoIILivello__c is null --> the case is new, so we had to check te createddate
            // ( first scenario , 16 hours before te createddate). Otherwise we are on second scenario (40 hours before II level's assignment)
           system.debug(logginglevel.debug,' ***  c.Secondo_Livello_Manuale__c'+ c.Secondo_Livello_Manuale__c);
           system.debug(logginglevel.debug,' ***  c.tech_case_aperto_manualmente__c'+ c.tech_case_aperto_manualmente__c); 
            if ( !c.tech_IsStatusIILivello__c){ // if false, the case is assigned to II liv
                system.debug(logginglevel.debug,'!c.tech_IsStatusIILivello__c : '+c.tech_IsStatusIILivello__c);
                listDate.add(BusinessHours.add(busnessHoursId,c.lastModifiedDate,millisec40));}
            else
            /*Giorgio risolto bug sul ricalcolo datatime per l'escalation al BO*/
                //listDate.add(BusinessHours.add(busnessHoursId,c.createddate,millisec16));
            {
                if (c.tech_case_aperto_manualmente__c)//E' aperto manualmente
                {
                    system.debug(logginglevel.debug,' *** E aperto manualmente c.tech_case_aperto_manualmente__c'+ c.tech_case_aperto_manualmente__c+
                    ' **** c.Secondo_Livello_Manuale__c: '+c.Secondo_Livello_Manuale__c); 
                    if(c.Secondo_Livello_Manuale__c)

                    {
                        listDate.add(BusinessHours.add(busnessHoursId,c.createddate,millisec40));
                        
                       // c.tech_IsStatusIILivello__c = false;
                    }
                    
                    else
                    {                     
                        listDate.add(BusinessHours.add(busnessHoursId,c.createddate,millisec16));
                    }
                    
                }
                else{//E' aperto in automativo e ricalcolo il tempo di escalation al BO
                    if(!c.Secondo_Livello_Manuale__c)
                    {
                        if (Constants.CASE_STATUS_NUOVO.equalsIgnoreCase(c.status))
                        {
                            system.debug(logginglevel.debug,' *** IF (Constants.CASE_STATUS_NUOVO.equalsIgnoreCase(c.status))');
                            listDate.add(BusinessHours.add(busnessHoursId,c.createddate,millisec16));
                            
                        }   
                        else
                        {
                            system.debug(logginglevel.debug,' *** ELSE if (Constants.CASE_STATUS_NUOVO.equalsIgnoreCase(c.status))');
                            listDate.add(BusinessHours.add(busnessHoursId,c.lastModifiedDate,millisec16));
                        }
                    }
                    else
                    {
                        system.debug(logginglevel.debug,' *** ELSE if(!c.Secondo_Livello_Manuale__c)');
                        listDate.add(BusinessHours.add(busnessHoursId,c.lastModifiedDate,millisec40));
                    }
                    
                
                }    
                
                
            }
                
        }

    }
    catch(Exception e){
        system.debug(logginglevel.error, 'Exception raised on BusinessHoursHelper :'+e.getMessage());
    }
    return listDate;
}
}