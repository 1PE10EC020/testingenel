public class Prova_DownloadContainer_controller{

  @AuraEnabled
  public static String getFourId(String sObjectName){

    //Custom Setting to get FourID Field Name in order to create the correct File Name
    SetFourID__c customSetting = SetFourID__c.getValues(sObjectName);
    String fourID = '';
    fourID = customSetting.FourID__c;
    System.debug('***objectName: ' +sObjectName);
    System.debug('***Id Four: ' +fourID);

    return fourID;
  }

}