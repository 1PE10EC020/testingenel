/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is a batch class
*/
global class KBMS_ArticleFAQViewBatchable implements  Database.Batchable<sObject>
{
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        String language = Label.KBMS_Language;
        String publishedStatus = Label.KBMS_PublishStatus_Online;
        String defaultDataCategoryGroup = Label.KBMS_AppDataCategory;
        String defaultdataCategory = 'Tutto__c';
        
        String query = 'SELECT Id,FirstPublishedDate,KnowledgeArticleId,LastModifiedDate,Summary,Title,UrlName,CreatedDate FROM FAQ__kav ' +
                        ' WHERE PublishStatus =:publishedStatus AND Language =:language ' +
                        ' WITH DATA CATEGORY '+defaultDataCategoryGroup+ ' BELOW '+defaultdataCategory +' AND  KBMS__c AT (OT__c )';
        
        return Database.getQueryLocator(query);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<FAQ__kav> articleList) 
    {    
        system.debug('## article details'+articleList.size());     
         
        List<KBMS_ArticleHistory__c> toBeInsertedArticleHistory = new List<KBMS_ArticleHistory__c>();

        Set<Id> parentArticleId = new Set<Id>();
        Map<String,FAQ__kav> FaqDetailMap = new Map<String,FAQ__kav>();
         
        for (FAQ__kav fq : articleList)
        {
            parentArticleId.add(String.valueOf(fq.KnowledgeArticleId));
        }
         
        List<FAQ__ViewStat> FQAViewList = new  List<FAQ__ViewStat>();
        
        FQAViewList =[SELECT ParentId, ViewCount, Channel,NormalizedScore 
                        FROM FAQ__ViewStat 
                        WHERE IsDeleted = false and ParentId In : parentArticleId];
        
        Map<Id, FAQ__ViewStat> FAQViewStatMap = new Map<Id, FAQ__ViewStat>();
        
        for (FAQ__ViewStat fvs : FQAViewList)
        {
            FAQViewStatMap.put(fvs.ParentId, fvs);
        }
        
        for (FAQ__kav fq : articleList)
        {
            KBMS_ArticleHistory__c ar=new KBMS_ArticleHistory__c();
            ar.Article_Id__c= fq.KnowledgeArticleId;
            ar.Article_Score__c = FAQViewStatMap.get(fq.KnowledgeArticleId) == null ? 0 : FAQViewStatMap.get(fq.KnowledgeArticleId).NormalizedScore;
            ar.Article_Summary__c = fq.Summary;
            ar.Article_Title__c = fq.Title;
            ar.Article_URL__c = fq.UrlName;
            ar.Channel__c = FAQViewStatMap.get(fq.KnowledgeArticleId) == null ? '' : FAQViewStatMap.get(fq.KnowledgeArticleId).Channel;
            ar.Article_Total_Views__c = FAQViewStatMap.get(fq.KnowledgeArticleId) == null ? 0 : FAQViewStatMap.get(fq.KnowledgeArticleId).ViewCount;
            ar.Create_Date__c = fq.CreatedDate;
            ar.Publish_Date__c = fq.FirstPublishedDate; 
            ar.Article_Type__c = 'FAQ';
            toBeInsertedArticleHistory.add(ar);
        }
            
        System.debug('testing list--'+toBeInsertedArticleHistory);
        insert toBeInsertedArticleHistory;
    }
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        KBMS_ArticleProcessiViewBatchable b = new KBMS_ArticleProcessiViewBatchable();
        database.executebatch(b);
        
    }
    
    
}