@IsTest
public class ED_FourDashbord_Extension_test {
	@testSetup
	private static void setup() {
		Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :Constants.CASE_DA_DEFINIRE ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.accountid = acc.id;
        insert c;
	}
	
	
	@isTest
	static void test(){
		Case c = [select id,caseNumber,Account.RecordTypeId,RecordType.developerName,pod__c,Account.codicefiscale__c,Account.PartitaIVA__c,IdPratica__c,CF_Search_DS__c,PIVA_Search_DS__c from case limit 1];			
		Test.startTest();
		
		PageReference myVfPage = Page.ED_FourDashboard;
		Test.setCurrentPage(myVfPage);
		// test 1 - pod popolato
		ApexPAges.StandardController sc = new ApexPages.StandardController(c);
		ED_FourDashbord_Extension testController = new ED_FourDashbord_Extension(sc);
		system.debug('testController '+testController.endURL);
		c.pod__c=null;
		update c;
		// test 2 - pod non popolato - richiedente popolato
		sc = new ApexPages.StandardController(c);
		testController = new ED_FourDashbord_Extension(sc);
		
		system.debug('testController '+testController.endURL);
		
		c.accountid=null;
		update c;
		// test 3 - pod non popolato - richiedente non popolato
		sc = new ApexPages.StandardController(c);
		testController = new ED_FourDashbord_Extension(sc);
		
		system.debug('testController '+testController.endURL);
		
		c.CF_Search_DS__c = 'test';
		update c;
		// test 4 - pod non popolato - richiedente non popolato - popolato cf ricerca
		sc = new ApexPages.StandardController(c);
		testController = new ED_FourDashbord_Extension(sc);
		
		system.debug('testController '+testController.endURL);
		
		c.PIVA_Search_DS__c = 'test';
		update c; 
		// test 4 - pod non popolato - richiedente non popolato - popolato piva ricerca
		sc = new ApexPages.StandardController(c);
		testController = new ED_FourDashbord_Extension(sc);
		
		system.debug('testController '+testController.endURL);
		
		Test.stopTest();
		
		  // Assertions to verify behaviour
	}

}