/*******************
 * Author : Akansha Lakhchaura
 * Apex Class  :  PT_TestDataFactory
 * CreatedDate :  04/07/2017
 * Description :  This class is used to create test data to be used for all test classes.
****************/
public class PT_TestDataFactory{
    
/**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for defining User object record to be created to use in Test Class for setting the community User. 

      */
    
    public static final String ALIAS = 'unEx1';
    public static final String PROF_SYS_ADMIN = 'System Administrator'; 
        private static final String SVALUE='S';
   
    public static User userDataFetch(String userAlias){
        //Id profileId = [SELECT Id FROM Profile WHERE Name=:PROF_SYS_ADMIN].Id;
        Contact con = null;
        Id p = [select id from profile where name='PT_V01_Partner'].id;  
        //Id uRole = [Select Id From UserRole Where PortalType ='Partner' Limit 1].Id;
        RecordType rt=[select id, name from recordtype where sobjecttype='Account' and name='Trader'];
        RecordType rectype=[select id, name from recordtype where sobjecttype='Contact' and name='Richiedente'];
        //User u1 = [select id from user where Profile.Name ='System Administrator' AND IsActive = True Limit 1];
        
            Account ac = new Account(name ='TestNewAccount',IdTrader__c='1456',Nazionalita__c = 'Italiana',phone='9999999999',recordtypeid=rt.id);
            insert ac;    
            con = new Contact(LastName ='testCon',AccountId = ac.Id,Email = 'test@testcontact.com',recordtypeid=rectype.id);
            insert con; 
        
         //UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        //insert r;
        //string ur =[select id,name from UserRole where id='00E24000000Hx7jEAC' ].name;
        //userRoleId = uRole,
        User userData = new User(FirstName=userAlias, LastName=userAlias, Alias=userAlias , IsActive=True, Email=userAlias+'@exceptionHandle.com',
                                 ProfileId=p, Username=userAlias+'@exceptionHandle.com', EmailEncodingKey='UTF-8',
                                 LanguageLocaleKey='en_US', LocaleSidKey='en_US', TimeZoneSidKey='GMT',
                                 ContactId = con.Id);        
        
        return userData;
    }
    
    
    
    /* 
    Author : Akansha Lakhchaura
    Apex Method : createRequest
    Return Type : request
    CreatedDate : 04/07/2017
    Description : createRequest is a method used to create test request data
    */
    
    public static PT_Richiesta__c createRequest()
    {        
        PT_Richiesta__c request = new PT_Richiesta__c();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_N02').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
        request.AccountDisId__c=null;
    	request.AccountTrId__c=null;
        request.Cognome_cl__c ='ROSSI';
        request.CodFisc_cl__c ='RSSMRA92A01H501Y';
        //request.RecordTypeId='0127E0000009134';
        request.Piva_cl__c ='12345678903';
        //request.Email__c='Test@test.com';
        request.NumTelRichiedente__c='9999999999';
        request.MandConnessione__c='N';
       // request.RagSoc_cl__c ='Test';
        request.Nazione_pod__c ='IT';
         request.PotDisp__c=19;
        request.PotImp__c='1';
        request.Nazione_cl__c ='IT';
        request.Nome_cl__c='MARIO';
        request.PPAAAVVV_pod__c ='ABCXYZ';
        request.IdConnessione__c = 'PER';
        request.IdTipoContr__c = 'ALT';
        request.Tensione__c = 'BM';
        request.IdUsoEnergia__c = '001';
        request.FlagStagRicAtto__c = 'S';
        request.DataInizio__c = null;
        request.FlagDisalimentabile__c='S';
        request.CodCategoriaDisalim__c='01';
        request.FlagAutocertASL__c='S';
        request.CellPreavvPESSE__c='test';
        request.FlagAutocertPESSE__c='S';       
        request.FlagSollevamentoPers__c = 'S';
        request.FlagAutocertSollev__c='';
        request.MandConnessione__c = 'N';
        request.FlagAttFuoriOrario__c = '';
        request.FlagPresenzaCliente__c='S';
       // request.FlagCessFuoriOrario__c = '';
       
        User u1 = [select id from user where Profile.Name = 'System Administrator' AND IsActive = True Limit 1];
        System.runAs(u1){
            PT_RFI_Component_Visibility__c css = new PT_RFI_Component_Visibility__c();
            css.Name='PT_N02';
            css.IdTipoRichiesta__c = 'NCA';
            //database.insert(css);
        }
        return request;
    }
       
/**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateAltri_Dati. 

      */
 public static PT_Richiesta__c createRequestvalidateAltri_Dati()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
          request.RecordTypeId='0127E0000009134';
        request.FlagPresenzaCliente__c ='S';
        request.NumTelRichiedente__c=null;
        return request;
    }
 /**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateDisalimentabilita. 

      */   
    public static PT_Richiesta__c createRequestvalidateDisalimentabilita()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
          request.RecordTypeId='0127E0000009134';
        request.FlagDisalimentabile__c ='S';
        request.CodCategoriaDisalim__c='01';
        return request;
    }
    
    
 /**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateDatiVenditore. 

      */   
    public static PT_Richiesta__c createRequestvalidateDatiVenditore()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_GD1').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
        
        request.TipoRichiedente__c ='T';
        request.RagSoc_cl__c=null;
        return request;
    }
    
/**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateLetture_Effettuate. 

      */    
     public static PT_Richiesta__c createRequestvalidateLetture_Effettuate()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
        RecordType RT = [Select id,name,DeveloperName,Sobjecttype From RecordType where SobjectType='PT_Richiesta__c'AND Name='PT_M01'];
        request.DataLettura_cl__c= null;
        request.Att_F1__c=123;
        request.Reatt_F1__c=123;
        request.Pot_F1__c=123;
        request.Att_F2__c=123;
        request.Reatt_F2__c=123;
        request.Pot_F2__c=123;
        request.Att_F3__c=123;
        request.Reatt_F3__c=123;
        request.Pot_F3__c=123;
        return request;
    }
    
    /**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateDatiRichiesta. 

      */    
     public static PT_Richiesta__c createRequestvalidateDatiRichiesta()
    {
        PT_Richiesta__c request = new PT_Richiesta__c();
        RecordType RT = [Select id,name,DeveloperName,Sobjecttype From RecordType where SobjectType='PT_Richiesta__c'AND Name='PT_M02'];
        request.CategReclamo_cl__c='A02';
        request.TipoRichiesta_cl__c='1';
        request.RichiestaUtente__c='';
        return request;
    }
    
  /**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateAdress. 

      */    
     public static PT_Richiesta__c createRequestvalidateAddress()
    {
        PT_Richiesta__c request = new PT_Richiesta__c();
        RecordType RT = [Select id,name,DeveloperName,Sobjecttype From RecordType where SobjectType='PT_Richiesta__c'AND Name='PT_M02'];
        request.CategReclamo_cl__c='A02';
        request.TipoRichiesta_cl__c='1';
        request.RichiestaUtente__c='';
        request.POD__c='IT001E49559154';
        request.Via_esaz__c='Text';
        request.NumCivico_esaz__c='Text';
        request.Via_cl__c='Text';
        request.NumCivico_cl__c='Text';
        request.Trattamento__c='M';
        request.EnergiaAttMonorario__c = 'TExt';
         request.TipologiaRichiesta__c = 'M02';
         request.ElencoDatiTecniciRichiesti__c = null;
       
        return request;
    }  
    
/**
        Author : Akansha Lakhchaura
        CreatedDate : 04/07/2017
        @description : Method Used for creating test data for validateDati_Contrattuali. 

      */    
     public static PT_Richiesta__c createRequestvalidateAVO()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
        RecordType RT = [Select id,name,DeveloperName,Sobjecttype From RecordType where SobjectType='PT_Richiesta__c'AND Name='PT_AVO'];
        request.DataMisura__c = System.today();
        request.DataVoltura__c=System.today();
        request.EnergiaAttMonorario__c='1234';
        request.POD__c='IT001E58293725';
        return request;
    }    
    
    public static PT_Richiesta__c createRequestvalidateDati_Contrattuali()
    {
         PT_Richiesta__c request = new PT_Richiesta__c();
        request.IdConnessione__c = 'PER';
        request.RecordTypeId='0127E0000009134';
        request.IdTipoContr__c = 'ALT';
        request.Tensione__c = 'BM';
        request.IdUsoEnergia__c='001';
        request.FlagStagRicAtto__c ='S';
        request.DataInizio__c=null;
        request.PotImp__c='1';
        request.PotDisp__c=11;
        return request;
    }
    
    

public static PT_Richiesta__c createRequestvalidateNCA()
    {
       PT_Richiesta__c request = new PT_Richiesta__c();
          request.RecordTypeId='0127E0000009134';
        request.PPAAAVVV_pod__c = null;
        return request;
    }
    
   
/*******************
 * Author : Akansha Lakhchaura
 * Method name : fetchAddress
 * CreatedDate :  07/07/2017
 * Description :  This method is used to generate test Address data for PT_Toponimo__c object.
****************/
    
     public static PT_Toponimo__c fetchAddress()
    {
            PT_Toponimo__c address = new PT_Toponimo__c();
        
            address.Comune__c = 'test';
            address.CodCap__c = 'test';
            address.DescrVia1__c = 'test';
            address.CodIstat__c = 'test';
        
            return address;
    }
    
/*******************
 * Author : Akansha Lakhchaura
 * Method name : retrievRecordCodInstat
 * CreatedDate :  07/07/2017
 * Description :  This method is used to generate test Address data for Anag_Cap_Istat__c object on Cod Istat value.
****************/
    
     public static Anag_Cap_Istat__c retrievRecordCodInstat()
    {
            Anag_Cap_Istat__c addressRecordCodInstat = new Anag_Cap_Istat__c();
            addressRecordCodInstat.Comune__c = 'test';
            addressRecordCodInstat.Name = 'test';
            addressRecordCodInstat.COD_ISTAT__c = 'test';
            addressRecordCodInstat.FLG_COMPETENZA_ENEL__c='S';
            return addressRecordCodInstat;
  }
  
/*******************
 * Author : Akansha Lakhchaura
 * Method name : retrieveRecordComunePod
 * CreatedDate :  07/07/2017
 * Description :  This method is used to generate test Address data for Anag_Cap_Istat__c object on ComunePod value.
****************/ 
  
  public static Anag_Cap_Istat__c retrieveRecordComunePod()
    {
            Anag_Cap_Istat__c addressRecordComunePod = new Anag_Cap_Istat__c();
            addressRecordComunePod.Comune__c = 'test';
            addressRecordComunePod.Name = 'test';
            addressRecordComunePod.COD_ISTAT__c = 'test';
            addressRecordComunePod.FLG_COMPETENZA_ENEL__c='S';
            return addressRecordComunePod;
  }
  
/*******************
 * Author : Akansha Lakhchaura
 * Method name : retrievePicklist
 * CreatedDate :  07/07/2017
 * Description :  This method is used to generate test data for Comune & CodIstat picklist field value.
****************/   
  
  public static Anag_Cap_Istat__c retrievePicklist()
    {
            Anag_Cap_Istat__c addressPicklist = new Anag_Cap_Istat__c();
            addressPicklist.Comune__c = 'test';
            addressPicklist.Name = 'test';
            addressPicklist.COD_ISTAT__c ='test';
            return addressPicklist;
  }
  
/*******************
 * Author : Akansha Lakhchaura
 * Method name : retrieveList
 * CreatedDate :  10/07/2017
 * Description :  This method is used to generate test data for distinct CAP values from ANAG_CAP_ISTAT__C oobject.
****************/
  
    public static Anag_Cap_Istat__c retrieveList()
    {
          Anag_Cap_Istat__c addressList = new Anag_Cap_Istat__c();
            addressList.Comune__c = 'test';
            addressList.Name = 'test';
            addressList.COD_ISTAT__c ='test';
            addressList.FLG_COMPETENZA_ENEL__c ='S';
            return addressList;
    }
      
/**
    *    Author          : Bhrath Sharma
    *    Date Created    : 21-aug-2017
    *    Description     :  This method is used for stato richesta custom setting Creation.
    **/    
    public static ANAG_STATO_RICHIESTA__c insertstatoric(string name,string DESCR_STATO,string ID_STATO){
       
            ANAG_STATO_RICHIESTA__c objstato = new ANAG_STATO_RICHIESTA__c (name=name,DESCR_STATO_RICH_TR__c=DESCR_STATO,
                                      ID_STATO_RICHIESTA__c=ID_STATO);
            return objstato;
        
    } 
          
    /**
    *    Author          : Bhrath Sharma
    *    Date Created    : 21-aug-2017
    *    Description     :  This method is used for Richieste__c custom setting Creation.
    **/    
    public static Richieste_Picklist_Values__c insertrichesta(string name,string RT,string controlling,string Confield_Api,
    string Confield_val,string Confield_2_Api,string Confield_2_val,string Dependent,String Picklist_Api_Name,String Picklist_Value){
       
            Richieste_Picklist_Values__c objstato = new Richieste_Picklist_Values__c (Name=name,Record_Type__c=RT,Controlling__c=controlling,
                                      Controlling_Field_API__c=Confield_Api,Controlling_Field_Value__c=Confield_val,Controlling_Field_2_API__c=Confield_2_Api,
                                      Controlling_Field_2_Value__c=Confield_2_val,Picklist_Api_Name__c=Picklist_Api_Name,Picklist_Value__c=Picklist_Value);
            return objstato;
        
    } 
      /* 
    Author : Bharath Sharma
    Apex Method : createRequest
    Return Type : request
    CreatedDate : 11/10/2017
    Description : createRequest is a method used to create test request data
    */
    
    public static PT_Richiesta__c createRequestF05()
    {        
        PT_Richiesta__c request = new PT_Richiesta__c();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_N02').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
        request.Cognome_cl__c ='ROSSI';
        request.CodFisc_cl__c ='RSSMRA92A01H501Y';
        //request.RecordTypeId='0127E0000009134';
        request.Piva_cl__c ='12345678903';
        request.NumTelRichiedente__c='9999999999';
        request.Nazione_pod__c ='IT';
        request.Nazione_cl__c ='IT';
        request.Nome_cl__c='MARIO';
        request.PPAAAVVV_pod__c ='ABCXYZ';
        request.IdConnessione__c = 'PER';
        request.IdTipoContr__c = 'ALT';
        request.Tensione__c = 'BM';
        request.IdUsoEnergia__c = '001';
        request.FlagStagRicAtto__c = 'S';
        request.DataInizio__c = null;
        request.FlagDisalimentabile__c='S';
        request.CodCategoriaDisalim__c='01';
        request.FlagAutocertASL__c='S';
        request.CellPreavvPESSE__c='test';
        request.FlagAutocertPESSE__c='S';       
        request.FlagSollevamentoPers__c = 'S';
        request.FlagAutocertSollev__c='';
        request.MandConnessione__c = 'N';
        request.FlagAttFuoriOrario__c = '';
        request.FlagPresenzaCliente__c='S';
        
        request.IdRichTrader__c=SVALUE;
        request.AmmissDescrizione__c=null;
        request.IdMessaggioPT__c='0';
        request.PodRicerche__c ='IT001E07262433';
        request.Eneltel__c='Test';
        request.EneltelRicerche__c = '123456789';
        request.MandConnessione__c='S';
        request.Stato__c='RES';
        request.PotImp__c='1';
        request.Tensione__c='AT';
        request.IdRichiestaFour__c='1';
        request.NumPresa__c='test';
        request.DescrDispaccEstesa__c='test12345';
        request.FlagCurveCarico__c='S';
        request.POD__c='IT001E00000677';
        request.Intestatario_pr__c='IT';
        request.CodFisc_cl_pr__c='IT';
        request.RagSoc_cl__c='IT';
        request.Via_pod__c='IT';
        request.Numero_Civico_pod__c='IT';
        request.Cap_pod__c='12345';
        request.Localita_pod__c='12345';
        request.Provincia_pod__c='NA';
        request.DataNonEsegPrimaDel__c=system.today();
        request.MatricolaMisAtt__c='test';
        request.Att_F1_Mis__c=12345;
        return request;
    }
       /* 
    Author : Bharath Sharma
    Apex Method : createRequest
    Return Type : request
    CreatedDate : 11/10/2017
    Description : createRequest is a method used to create test request data
    */
    
    public static PT_Richiesta__c createRequestF05_one()
    {        
        PT_Richiesta__c request = new PT_Richiesta__c();
       // Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_N02').getRecordTypeId();
        //request.RecordTypeId=recordtypeid;
        request.Cognome_cl__c ='ROSSI';
        request.CodFisc_cl__c ='RSSMRA92A01H501Y';
        //request.RecordTypeId='0127E0000009134';
        request.Piva_cl__c ='12345678903';
        request.NumTelRichiedente__c='9999999999';
        request.Nazione_pod__c ='IT';
        request.Nazione_cl__c ='IT';
        request.Nome_cl__c='MARIO';
        request.PPAAAVVV_pod__c ='ABCXYZ';
        request.IdConnessione__c = 'PER';
        request.IdTipoContr__c = 'ALT';
        request.Tensione__c = 'BM';
        request.IdUsoEnergia__c = '001';
        request.FlagStagRicAtto__c = 'S';
        request.DataInizio__c = null;
        request.FlagDisalimentabile__c='S';
        request.CodCategoriaDisalim__c='01';
        request.FlagAutocertASL__c='S';
        request.CellPreavvPESSE__c='test';
        request.FlagAutocertPESSE__c='S';       
        request.FlagSollevamentoPers__c = 'S';
        request.FlagAutocertSollev__c='';
        request.MandConnessione__c = 'N';
        request.FlagAttFuoriOrario__c = '';
        request.FlagPresenzaCliente__c='S';
        
        request.IdRichTrader__c=SVALUE;
        request.AmmissDescrizione__c=null;
        request.IdMessaggioPT__c='0';
        request.PodRicerche__c ='IT001E07262433';
        request.Eneltel__c='Test';
        request.EneltelRicerche__c = '123456789';
        request.MandConnessione__c='S';
        request.Stato__c='RES';
        request.PotImp__c='1';
        request.Tensione__c='AT';
        request.IdRichiestaFour__c='1';
        request.NumPresa__c='test';
        request.DescrDispaccEstesa__c='test12345';
        request.FlagCurveCarico__c='S';
        request.POD__c='IT001E00000677';
        request.Intestatario_pr__c='IT';
        request.CodFisc_cl_pr__c='IT';
        request.Via_pod__c='IT';
        request.Numero_Civico_pod__c='IT';
        request.Via_esaz__c ='test';
        request.NumCivico_esaz__c='test';
        request.Via_cl__c ='test';
        request.NumCivico_cl__c ='test';
        request.IdTipoRichAEEG__c='N01';
        request.Via_pod__c='IT';
        request.Numero_Civico_pod__c='IT';
        request.Cap_pod__c='12345';
        request.Localita_pod__c='12345';
        request.Provincia_pod__c='NA';
        request.Cap_esaz__c ='test';
        request.Localita_esaz__c  ='test';
        request.Provincia_esaz__c  ='NA';
        request.IdUsoEnergia__c   ='001';
        return request;
    }
       /* 
    Author : Priyanka Sathyamurthy
    Apex Method : createFornitRecord
    Return Type : rFour_Comp_Fornit
    CreatedDate : 12/10/2017
    Description : createFornitRecord is a method used to create test fornit data
    */
    
    public static Four_Comp_Fornit__c createFornitRecord()
    {
    	Four_Comp_Fornit__c fornit = new Four_Comp_Fornit__c();
        fornit.ID_CONNESSIONE__c='PER';
        fornit.ID_TIPO_CONTR__c='DRE';
        fornit.ID_LIV_TENSIONE__c='BM';
        fornit.ID_USO_ENERGIA__c='007';
        fornit.POTENZA_MAX__c=3;
        fornit.POTENZA_MIN__c=0.5;
        fornit.ID_TIPO_FORNIT__c='01';
        fornit.FLG_POTENZA__c='N';
        return fornit;
    }
       /* 
    Author : Priyanka Sathyamurthy
    Apex Method : createMassiveRecord
    Return Type : PT_Caricamento_Massivo
    CreatedDate : 16/10/2017
    Description : createMassiveRecord is a method used to create test massive data
    */
    
    public static PT_Caricamento_Massivo__c createMassiveRecord()
    {
    	PT_Caricamento_Massivo__c massive = new PT_Caricamento_Massivo__c();
        massive.DescrTraderEstesa__c='TestData';
        massive.TipoRich__c='N02';
        massive.DescrDispaccEstesa__c='test';
        return massive;
    }
     /* 
    Author : Priyanka Sathyamurthy
    Return Type : PT_Trasporto__c
    CreatedDate : 26/10/2017
    Description : createTransportoRecord is a method used to create test transporto data
    */
    
    public static PT_Trasporto__c createTransportoRecord()
    {
    	PT_Trasporto__c trans=new PT_Trasporto__c();
    	trans.Trader__c=null;
    	trans.Distributore__c=null;
    	trans.FlgMoroso__c=SVALUE;
    	trans.DataInizioValid__c=system.now()-20;
    	trans.DataFineValid__c=system.now()+20;
        return trans;
    }
  }