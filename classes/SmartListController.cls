public class SmartListController {
   

    @AuraEnabled
    public static String recordId{get;set;}


  /*
  Description: This method is returning records to the lightning Component.
  */
    @AuraEnabled
    public static PaginationWrapper getRecords(String otherSoql,Integer sObjOffset, 
                                               List<String> fieldApiName, Integer maxNoOfRecord, 
                                               String sobjApiName, 
                                               Boolean createdByMe, String recordId,Boolean isDownloadable,
                                               List<String> downloadFields, String whereClause, Boolean drilldown)
    {        

        // System is throwing internal salesforce error so converting to string to add 1 
        String maxNoOfRecordStr =  maxNoOfRecord+'';                        
        Integer maxRec = (Integer.valueOf(maxNoOfRecordStr)+ 1);    
        
        List<Sobject> sobjLst = null;
        PaginationWrapper wrapObj = new PaginationWrapper();
        //Pass parameters to create query String and fetch the records
        sobjLst  = Database.query(generateSoql(sObjOffset,fieldApiName,
                                               maxRec,sobjApiName,createdByMe,recordId, isDownloadable, 
                                               downloadFields, whereClause, drilldown
                                               )
                                 );
        
        if(sobjLst.size() == maxRec ){
            for(integer i =0; i< maxRec-1;i++){
                wrapObj.sObjLst.add(sobjLst.get(i));                    
            }    
        }else{
            wrapObj.sObjLst.addALl(sobjLst);    
            wrapObj.hasNext = true;        
        }
        
        //[G.T.] - CAN BE REMOVED - Only for Object Contributi
        if(sObjLst.size()==0){
            getErrorMessage(sObjLst,sobjApiName,wrapObj,recordId);
        }
        

        if(drilldown){
            SmartList_Setup__c cs = SmartList_Setup__c.getValues(sobjApiName);
            if(cs.Drilldown_to_Parent__c != '' || cs.Drilldown_to_Parent__c != null){
                 wrapObj.parentID = cs.Drilldown_to_Parent__c;
            }
        }
      
        System.debug('**recordId: ' +recordId);
        System.debug('**sObjLst: ' +sObjLst);

        return wrapObj;
    }
    
    
    /*
  Description: This method will generate query String
  */
    @TestVisible
    private  static String generateSoql(Integer sObjOffset,
                                        List<String> fieldApiName, Integer maxNoOfRecord,
                                        String sobjApiName, Boolean createdByMe, String recordId, Boolean isDownloadable,
                                        List<String> downloadFields, String whereClause, Boolean drilldown
                                       ) 
    {
        String queryStr = 'Select ';
        String fields  = ' ';
        String fileNameField = ' ';
        SmartList_Setup__c cs = SmartList_Setup__c.getValues(sobjApiName);
        // Add 1 just to Check if it has more number of records.
        //   maxNoOfRecord = maxNoOfRecord + 1;                
        if(fieldApiName != null){
            for(String str : fieldApiName){
                fields +=  str + ',';
            }
        }

        

        System.debug('**Drilldown è: ' +drilldown);
        System.debug('**Before drilldown: ' +fields);
        
        if(drilldown){
            String str3 = '';
            if(cs.Drilldown_to_Parent__c != '' || cs.Drilldown_to_Parent__c!= null){
                System.debug('**sono qui: ' +fields);
                str3 = cs.Drilldown_to_Parent__c;
                fields +=  str3 + ',';
                System.debug('**sono qui: ' +str3);
            }
           System.debug('**After drilldown: ' +fields);
        }
           
        // remove last , from field string         
        fields  = fields.substring(0, fields.length()-1); 
         
        queryStr = queryStr + fields + ',ownerId from ' + sobjApiName;
    
        
        if(createdByMe){
            Id runninguserId = UserInfo.getUserId();
            queryStr = queryStr  +' where createdById='+'\''+runninguserId+'\'';
        }
        
        //***[G.Tedesco] - Filter to get context page
        System.debug('**recordId: ' +recordId);
        
        String filter = '';
        filter = setQueryFilter(sobjApiName,recordId);
        

        if(filter <> null && filter.trim()<>''){
            if(createdByMe){
                queryStr +=' AND '+filter;
            }
            else{
                queryStr += ' where '+filter;
            }
        }

        if(filter == '' && whereClause != ''){
            queryStr += ' where '+whereClause;
            System.debug('***queryStr+whereClause: ' +queryStr);
            System.debug('***whereClause: ' +whereClause);
        }
        else if(filter != '' && whereClause != ''){
            queryStr += ' AND '+whereClause;
            System.debug('***queryStr+whereClause: ' +queryStr);
            System.debug('***whereClause: ' +whereClause);
        }

        System.debug('***whereClause: ' +whereClause);

        queryStr = queryStr + ' LIMIT ' + maxNoOfRecord + ' OFFSET '   + sObjOffset;

        system.debug('*** queryStr: '+queryStr);
        return queryStr;


    }    
    
    // Wrapper Class for the pagination
    public class PaginationWrapper{
        @AuraEnabled
        public Boolean hasNext {get;set;}
        @AuraEnabled
        public List<Sobject> sobjLst {get;set;}
        @AuraEnabled
        public List<Sobject> sobjLst2 {get;set;}
        @AuraEnabled
        public String errorMessage {get;set;}
        @AuraEnabled
        public String parentID {get;set;}
       

       
        public PaginationWrapper(){
            hasNext = false;
            sobjLst  = new List<Sobject>();
            sobjLst2  = new List<Sobject>();
            errorMessage = '';
            parentID = '';
         
        }
        
    }


    /*** [G.Tedesco] - Method to get Custom Setting Configuration of Lookup Context field***/
    @AuraEnabled
    public static String setQueryFilter(String sObjectName, String recordId){

        String queryFilter = '';
        
        //if Drilldown
        SmartList_Setup__c customSetting = SmartList_Setup__c.getValues(sObjectName);
        queryFilter = customSetting.Lookup_Filter__c;

        if(!String.isBlank(queryFilter)){
            System.debug('*** queryFilter: ' +queryFilter);
            queryFilter += ' = ' + '\''+recordId+'\'';
            System.debug('*** queryFilter con ID: ' +queryFilter);
        }
        else{
            queryFilter = '';
        }
        
        return queryFilter;

    }

    /*** [G.Tedesco] - Method to get Custom Setting Configuration of Error Message***/
   @AuraEnabled
   public static void getErrorMessage(List<Sobject> sObjLst, String sobjApiName, PaginationWrapper wrapObj, String recordId){

        
        if(sobjApiName == 'PT_Componente_Costo__c'){
           
            String messaggio1 = 'PT_Costi_Mess1';
            String messaggio2 = 'PT_Costi_Mess2';
            String messaggio3 = 'PT_Costi_Mess3';
            String messaggio4 = 'PT_Costi_Mess4';
            System.debug('**Ric ID : ' +recordId);
            try{
                PT_Richiesta__c recordRichiesta = [SELECT Name,Sottostato__c,TipologiaContributi__c FROM PT_Richiesta__c WHERE Id =: recordId];
                System.debug('**record richiesta: ' +recordRichiesta);

            /* M.T. 27/10/2017- Start
                if(recordRichiesta.Sottostato__c=='INL.070' && String.isBlank(recordRichiesta.TipologiaContributi__c)){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio1);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio1');
                }
                else if(!String.isBlank(recordRichiesta.Sottostato__c) && (recordRichiesta.TipologiaContributi__c!=null || !String.isBlank(recordRichiesta.TipologiaContributi__c))){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio2);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio2');
                }
                else if(String.isBlank(recordRichiesta.Sottostato__c) && !String.isBlank(recordRichiesta.TipologiaContributi__c)){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio2);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio2');
                }
                else{
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio3);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio3');
                }*/
                
              
                List<PT_Costi__c> recordCosti=[select Id,name,protocolloric__c from PT_Costi__c WHERE protocolloric__c =: recordRichiesta.Id];
                
               
                List<PT_Componente_Costo__c> recordCompCosto=[select name,protocolloric__c,CostiComponente__c 
                                                                   from PT_Componente_Costo__c 
                                                                   WHERE protocolloric__c =: recordRichiesta.Id];
              

                if(recordCosti.isEmpty() && recordRichiesta.Sottostato__c=='INL.070'){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio1);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio1');
                }
                 else if(recordCosti.isEmpty() && (recordRichiesta.TipologiaContributi__c=='A Preventivo')){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio2);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio2');
                }
                else if(recordCosti.isEmpty()){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio4);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio4');
                }
                 else if(recordCompCosto.isEmpty()){
                    SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio3);
                    wrapObj.errorMessage = csetting.Error_Message__c;
                    System.debug('**messaggio3');
                }
                
                //M.T. END
            }
            catch(Exception e){
                //SmartList_Setup__c csetting = SmartList_Setup__c.getValues(messaggio3);
                //wrapObj.errorMessage = csetting.Error_Message__c;
                System.debug('Error in : '+e.getMessage()+ ' at line: '+e.getLineNumber()+' stack trace: '+e.getStackTraceString());
            }

        }
        else{
            SmartList_Setup__c csetting = SmartList_Setup__c.getValues(sobjApiName);
            wrapObj.errorMessage = csetting.Error_Message__c;
        }
       
        
    }


    
}