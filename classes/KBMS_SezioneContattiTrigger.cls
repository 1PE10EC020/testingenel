public class KBMS_SezioneContattiTrigger {
    public static boolean flag = true;
    integer maxPrior;
    list < Sezione_Contatti__c > SezioneContattiId = new list < Sezione_Contatti__c > ();
    List < Sezione_Contatti__c > SezioneContattiIdUpdate = new List < Sezione_Contatti__c > ();
    Set < id > idContattiToUpdate = new Set < Id > ();
    Set < Integer > updatedPriorities = new Set < Integer > ();
    
    List < Sezione_Contatti__c > toBeDeletedContatti = new List < Sezione_Contatti__c > ();
    
    List < Sezione_Contatti__c > toBeUnDeletedContatti = new List < Sezione_Contatti__c > ();
    
    public KBMS_SezioneContattiTrigger() {
        SezioneContattiId = [select Priority__c, Name, ID from Sezione_Contatti__c where Priority__c != Null ORDER BY Priority__c ASC];
        if (!SezioneContattiId.isEmpty()) {
            maxPrior = Integer.valueOf(SezioneContattiId[SezioneContattiId.size() - 1].Priority__c);
        } else
            maxPrior = 0;
        
    }
    
    
    public List < Sezione_Contatti__c > insertMethod(List < Sezione_Contatti__c > toInsertList) {
        for (Sezione_Contatti__c obj: toInsertList) {
            obj.Priority__c = maxPrior + 1;
            maxPrior++;
        }
        return toInsertList;
        
    }
    
    public void updateMethod(List < Sezione_Contatti__c > tempList, Map < Id, Sezione_Contatti__c > temMap) {
        
        for (Sezione_Contatti__c obj: tempList) {
            if (obj.Priority__c != temMap.get(obj.Id).Priority__c) {
                updatedPriorities.add(Integer.valueof(obj.Priority__c));
                idContattiToUpdate.add(obj.Id);
            }
        }
        
        integer i= 1;
        List < Sezione_Contatti__c > toBeUpdatedSezioneContatti = [Select Id, Priority__c, Name FROM Sezione_Contatti__c
                                                                   WHERE Id not In: idContattiToUpdate ORDER BY Priority__c ASC ];
        for (Sezione_Contatti__c  sz : toBeUpdatedSezioneContatti ){
            while (updatedPriorities.contains(i)){
                i++;
            }
            sz.Priority__c = i;
            i++;
            SezioneContattiIdUpdate.add(sz);
        }
        Update SezioneContattiIdUpdate;
    }
    
    public void deleteMethod(List < Sezione_Contatti__c > tempList) {
        toBeDeletedContatti = tempList;
        List<Sezione_Contatti__c> updateSZbeforeDel = new  List<Sezione_Contatti__c>();
        Set < Id > deletedSezioneContattiIds = new Set < Id > ();
        Set < Decimal > deletedPriorityValues = new Set < Decimal > ();
        for (Sezione_Contatti__c sc: toBeDeletedContatti) {
            deletedSezioneContattiIds.add(sc.Id);
            deletedPriorityValues.add(sc.Priority__c);
        }
        
        List < Sezione_Contatti__c > toBeUpdatedSezioneContatti = [Select Id, Priority__c, Name FROM Sezione_Contatti__c 
                                                                    WHERE Id not In: deletedSezioneContattiIds 
                                                                    ORDER BY Priority__c ASC];
        
        integer min = 1;
        for (Sezione_Contatti__c sz: toBeUpdatedSezioneContatti) {
            sz.Priority__c = min;
            min++;
            updateSZbeforeDel.add(sz);
        }
        Update updateSZbeforeDel ;
        
    }
    
    public void undeleteMethod(List < Sezione_Contatti__c > tempList) {
        
        
        List < Sezione_Contatti__c > toBeUpdatedAfterUndelete = new List < Sezione_Contatti__c > ();
        Set <Id> unDeletedSezioneContattiIds = new Set <Id>();
        Set <integer> unDeletedPriorityValues = new Set <integer>();
        
        for (Sezione_Contatti__c sc: tempList) {
            unDeletedSezioneContattiIds.add(sc.Id);
            unDeletedPriorityValues.add(integer.valueOf(sc.Priority__c));
            
        }
        integer i= 1;
        List < Sezione_Contatti__c > toBeUpdatedSezioneContatti = [Select Id, Priority__c, Name, Nome_Sezione__c 
                                                                   FROM Sezione_Contatti__c WHERE Id not In: unDeletedSezioneContattiIds
                                                                   ORDER BY Priority__c ASC];
        for (Sezione_Contatti__c  sz : toBeUpdatedSezioneContatti ){
            while (unDeletedPriorityValues.contains(i)){
                i++;
            }
            sz.Priority__c = i;
            i++;
            toBeUpdatedAfterUndelete.add(sz);
        }
        Update toBeUpdatedAfterUndelete;
    }
    
    public static boolean runonce() {
        if (flag) {
            flag = false;
        } else {
            return flag;
        }
        return true;
    }
    
}