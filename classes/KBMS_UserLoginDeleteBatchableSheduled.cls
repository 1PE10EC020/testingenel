/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is a batch class which first deletes all preexisting data in KBMS_UserLogin__c object.then in execute method,it takes data from loginHistory & insert data into KBMS_UserLogin__c.

*/
global class KBMS_UserLoginDeleteBatchableSheduled implements Schedulable,Database.Batchable<sObject>
{
    //Execute menthod of Schedulable interface
    global void execute(SchedulableContext scMain) 
    {
         //deleting existed records from userlogin custom object
         
         KBMS_UserLoginDeleteBatchableSheduled db = new KBMS_UserLoginDeleteBatchableSheduled();
         database.executebatch(db);        
    }
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        String query='SELECT Id FROM  KBMS_UserLogin__c';        
        return Database.getQueryLocator(query);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<KBMS_UserLogin__c> KBMSUserLoginList) 
    {
        Delete KBMSUserLoginList;
    }
       
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        //inserting new records         
       
        KBMS_UserLoginBatchable b = new KBMS_UserLoginBatchable();
        if(!test.isrunningtest())
        database.executebatch(b);
    }
}