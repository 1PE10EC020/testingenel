@IsTest
public class EmailHelper_Test {
        
        /*
        @isTest
        static void sendEmailNoErr (){  
            User u = CreateTestUser(Constants.PROFILE_II_LIV , 'Piemonte Liguria');    
            insert u;
            System.runAs(u){
                 u = [select id from user where username = 'classTestEmailHelper@enel.test.com'];
                 u.email = 'userTestEmail@enel.test.com';
                Case c = GenerateCase(Constants.CASE_DA_DEFINIRE);
                //c.ownerid = [select id from group where name ='Piemonte Liguria' limit 1 ].id;
                c.ownerid = u.id;
                insert c;
                list<String> listOFCaseid = new list<String>();
                listOFCaseid.add(c.id);
                list<Case> listOFCase = new list<Case>();
                listOFCase.add(c);
                EmailHelper.sendEmailToRole(listOFCaseid);
                EmailHelper.sendEmail(listOFCase, u);
            }
        }
        */
        @isTest
        static void sendEmailwithErr (){    
            // into this thest the case has no group as owner , so the email_helper should manage the exception
            User u = CreateTestUser(Constants.PROFILE_II_LIV , 'Piemonte Liguria');    
            insert u;
            System.runAs(u){
                Case c = GenerateCase(Constants.CASE_DA_DEFINIRE);
                //c.ownerid = [select id from group where name ='Piemonte Liguria' limit 1 ].id; 
                insert c;
                list<String> listOFCaseid = new list<String>();
                listOFCaseid.add(c.id);
                EmailHelper.sendEmailToRole(listOFCaseid);
            }
        }
        
        
        private static User CreateTestUser(string profileName, string rolename){
            User u = new User();
            u.username = 'classTestEmailHelper@enel.test.com';
            u.firstname = 'User';
            u.lastName = 'test';
            u.email = 'userTestEmailHelper@enel.test.com';
            u.alias ='alias';
            u.TimeZoneSidKey = 'GMT';
            u.LocaleSidKey = 'it_IT'; 
            u.EmailEncodingKey='ISO-8859-1';
            u.profileid =  [Select id from profile where name = :profileName limit 1].id;
            u.UserRoleId = [Select id from Userrole where name = : rolename limit 1 ].id;
            u.LanguageLocaleKey ='it';
            return u;
        }
        
        /*
        * @author : Claudio Quaranta
        * @Date : 2015-06-30
        * @Description : generate new case for test purpose
        */ 
        private static case GenerateCase(string RT_DEVELOPER_NAME) {
            case c = new case();
            c.recordTypeId = [select id from recordtype where developername = :RT_DEVELOPER_NAME ].id ;
            c.TipologiaCase__c ='Da Definire';
            c.origin = 'Email';
            c.status='In carico al II Livello';
            c.DescrizioneStato__c='In Lavorazione';
            c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
            c.pod__C = 'testPod';
            c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
            c.UnitaDiCompetenza__c ='Piemonte Liguria';
            c.LivelloTensioneDiFornitura__c ='BT';
            c.livello1__C ='In prelievo';
            c.livello2__C = 'Già Connesso';
            c.livello3__C ='MT/AT' ;
            return c;
        }
}