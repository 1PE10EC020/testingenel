/**
* @author Atos India Pvt Ltd.
* @date  Sept,2016
* @description This class is to create Test data in test classes.
*/

public class KBMS_TestDataFactory {  
    
    
    public static List<FAQ__kav>  createFAQTestRecords(integer faqno) {
        
        String lang= Label.KBMS_Language;
        List<FAQ__kav> lstFaq = new List<FAQ__kav>();
        
        for(Integer i=0;i<faqno;i++) {
            FAQ__kav faqtst = new FAQ__kav();
            
            faqtst.Title = 'TestFAQ'+i;
            faqtst.Summary = 'KB Summary ';
            faqtst.URLName = 'TestFAQ'+i;                
            faqtst.QUESITO__c='testno';
            faqtst.RISPOSTA__c='testno';
            faqtst.Language=lang;
            lstFaq.add(faqtst);
        }
        insert lstFaq;
        return lstFaq;
        
    }
    
     public static List<Moduli__kav>  createModuliTestRecords(integer moduliNo) {
        
        String lang= Label.KBMS_Language;
        List<Moduli__kav> lstModuli = new List<Moduli__kav>();
        
        for(Integer i=0;i<moduliNo;i++) {
            Moduli__kav moduli= new Moduli__kav();
            moduli.Title = 'TestModuli'+i;
            moduli.UrlName = 'TestModuli'+i;
            moduli.Summary = 'KB Summary ';
            moduli.Language=lang;
            lstModuli.add(moduli);
        }
        insert lstModuli;
        return lstModuli;
        
    }
   /* public static List<Procedure__kav> createProcedureTestRecords(integer procno) {
        
        String lang= Label.KBMS_Language; 
        List<Procedure__kav> lstProc = new List<Procedure__kav>();
        for(Integer i=0;i<procno;i++) {
            
            Procedure__kav testpro = new Procedure__kav();
            testpro.Title = 'Test procedure'+i;
            testpro.Summary = 'KB Summary';
            testpro.URLName = 'Test-procedure'+i;
            lstProc.add(testpro);
            
        }
        
        insert lstProc ;
        return lstProc;
        
    }*/
    public static List<Processi__kav> createProcessTestRecords(integer processno) {            
        
        
        List<Processi__kav> lstProcess = new List<Processi__kav>();
        for(Integer i=0;i<processno;i++) {
            
            Processi__kav testprocess = new Processi__kav();
            testprocess.Title = 'Test proces'+i;
            testprocess.Summary = 'KB Summary';
            testprocess.URLName = 'Test-proces'+i;
            lstProcess.add(testprocess);  
        }
        insert lstProcess;
        return lstProcess;
    }
   
    public static List<Sezione_Contatti__c>  createSezioneTestRecords(integer no) {
        
        List<Sezione_Contatti__c> lstSezione = new List<Sezione_Contatti__c>();
        
        for(Integer i=0;i<no;i++) {
            Sezione_Contatti__c testSezione = new Sezione_Contatti__c();            
            
            testSezione.Nome_Sezione__c='Test'+i;
            testSezione.Tipologia_Richiedente__c = ( i == 0 ? 'Clienti' : 'Produttori');          
            lstSezione.add(testSezione);
        }
        insert lstSezione;
        return lstSezione;
        
    }
    public static List<Contact>  createContactTestRecords(integer no) {
        
        List<Contact> lstcontact = new List<Contact>();
        
        for(Integer i=0;i<no;i++) {
            
                Contact con = new Contact();        
                con.FirstName = 'test';
                con.LastName = 'Test Contact';
                con.Email = 'test'+i+'@test.net';            
                con.Phone  = '983215789';
                con.MobilePhone = '9865897';
                con.Fax = '12345789';
                lstcontact.add(con);
            
        }
       
        insert lstcontact;
        return lstcontact;
        
    }/*
    public static User createUserRecord() {
        
         Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];        
      User usr = new User(LastName = 'test',
                           FirstName='testf',
                           Alias = 'test',
                           Email = 'test123@gmail.com',
                           Username = 'testryz@gmail.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        return usr;
    }*/
    public static User createUser() {
    Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];        
    User usr = new User(LastName = 'test',
                           FirstName='testf',
                           Alias = 'test',
                           Email = 'test123@gmail.com',
                           Username = 'testryz@gmail.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        return usr;
        }
    public static Vote createVoteTestRecords(String id,String Type) {      
        
            Vote vo = new Vote();
            vo.ParentId=id;        
            vo.Type =Type;        
            return vo;       
    }
    public static FAQ__DataCategorySelection createFAQDataCategoryTestRecords(String id,String dataCateGroupName,String dataCateName) {      
        
            
            FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();      
            datacatTest.ParentId= id;     
            datacatTest.DataCategoryName=dataCateName;
            datacatTest.DataCategoryGroupName=dataCateGroupName; 
            return datacatTest;
    }
       public static Processi__DataCategorySelection createProcessiDataCategoryTestRecords(String id,String dataCateGroupName,String dataCateName) {      
        
            
            Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();      
            datacatTest.ParentId= id;     
            datacatTest.DataCategoryName=dataCateName;
            datacatTest.DataCategoryGroupName=dataCateGroupName; 
            return datacatTest;
    }
}