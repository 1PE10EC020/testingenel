/*******************************************************************************
* @author       Cloud Sherpas
* @date         5.19.2014
* @description  Class that is used to retrieve the LogConfig custom settings of 
*               the current user profile. 
*******************************************************************************/
public class LogConfigSettings{
    
    private static LogConfig__c theSetting = LogConfig__c.getInstance(UserInfo.getUserId());
    private static List<RecordType> recordTypes = [SELECT Id, Name from RecordType WHERE SobjectType =: 'apexLog__c'];
    
    public static Id getDebugRecordType(){
        for(RecordType rt : recordTypes){
            if(rt.Id == theSetting.LOGRecordTypeIDDebug__c){
                return rt.Id;
            }
        }
        return null;       
    }
    
    
    public static Id getExceptionRecordType(){
        for(RecordType rt : recordTypes){
            if(rt.Id == theSetting.LOGRecordTypeIDException__c){
                return rt.Id;
            }
        }
        return null;       
    }
    
    public static  Boolean loggingEnabled(){

        return theSetting.DebugsEnabled__c;
    }
    public static  Boolean exceptionEnabled(){

        return theSetting.ExceptionsEnabled__c;
    }
}