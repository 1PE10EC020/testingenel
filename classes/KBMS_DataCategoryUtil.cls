global class KBMS_DataCategoryUtil
{
    public static Integer getCategoryListCountByType(String categoryId,String articleType)
    {
        Integer count=0;
        String language = Label.KBMS_Language;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String appArticleTypes = Label.KBMS_AppArticle;
        
        
        if (articleType != null && categoryId != null && appArticleTypes.contains(articleType))
        {   
            String firstPartQuery = 'SELECT count() FROM ' + articleType+'__kav ';
            String secondPartQuery = ' WHERE ( PublishStatus = \'online\' AND Language =:language AND IsDeleted = false) '+ 
                ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW ' + categoryId + '__c AND KBMS__c BELOW OT__c';
           
            String query = firstPartQuery + secondPartQuery;
            
           
            count = Database.countQuery(query); 
            System.debug('Final Count '+count);
        }
        return count;
    }
    
    public static List<String> getParentCategory(String child)
    {
        return getMapParentCategoryList().get(child);
    }
    
    public static Map<String, List<String>> getMapParentCategoryList() 
    {
        Map<String, List<String>> result = new Map<String, List<String>>();
        List<DescribeDataCategoryGroupResult> describeCategoryResult;

        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        System.debug('@@ into datacategory Function--');
        try 
        {
            List<String> objType = new List<String>();

            objType.add('KnowledgeArticleVersion');
            describeCategoryResult = Schema.describeDataCategoryGroups(objType);
            
            //Creating a list of pair objects to use as a parameter
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

            //Looping throught the first describe result to create
            //the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult)
            {
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }

            //describeDataCategoryGroupStructures()
            describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);

            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult)
            {
                singleResult.getSobject();
                singleResult.getName();
                singleResult.getLabel();
                singleResult.getDescription();
                
                //Get the top level categories
                List<DataCategory> toplevelCategories = singleResult.getTopCategories();

                //Recursively get all the categories
                List<DataCategory> allCategories =  getAllCategories(toplevelCategories);

                for(DataCategory category : allCategories) 
                {
                    category.getName();
                    category.getLabel();
                    
                    //Get the list of sub categories in the category
                    DataCategory [] childCategories = category.getChildCategories();
                    for(DataCategory childCategory : childCategories) 
                    {
                        List<String> parentAttr = new List<String>();
                        parentAttr.add(childCategory.getLabel());
                        parentAttr.add(category.getName());
                        parentAttr.add(category.getLabel());
                        result.put(childCategory.getName(),parentAttr);
                    }
                }

            }

        } catch (Exception e)
        {

        }
        return result;
    }
    public Static Integer getArticleCountByCategory(String categoryId){
        Integer count =0;
        String language = Label.KBMS_Language;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String appArticleTypes = Label.KBMS_AppArticle;
        List<String> articleTypes = appArticleTypes.split(',');
        System.debug('articleTypes   '+articleTypes);
        if(categoryId != NULL){
            String firstPartQuery = 'SELECT count() FROM KnowledgeArticleVersion ';
            String secondPartQuery = ' WHERE ( PublishStatus = \'online\' AND Language =: language AND IsDeleted = false) AND ArticleType IN: articleTypes'+ 
                ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW ' + categoryId + '__c AND KBMS__c BELOW OT__c';
           
            String query = firstPartQuery + secondPartQuery;
            System.debug('QUERYY '+query);
           count = Database.countQuery(query); 
            System.debug('Final Count 2 '+count);
        }
        return count;
    }
    
    public Static List<KBMS_Category> getCategoryGroupStructure(String categoryGroup,String topCategory)
    {
   
        
        Integer subcategoriesCount = 0;
        Integer articlesCount = 0;
        Integer subCategoryArticleCount = 0;
        String appArticleTypes = Label.KBMS_AppArticle;
         System.debug(appArticleTypes);
        System.debug('****'+appArticleTypes.split(','));
        String[] articleTypes = appArticleTypes.split(',');
        String language = Label.KBMS_Language;
        List<String> DataCategoryNames = New List<String>();
        
        Map<String, Set<Id>> dataCategoryNameArticleMap = New Map<String, Set<Id>>();
        
        System.debug(articleTypes);
        for (String art : articleTypes)
        {
            System.debug(art);
            System.debug('ENETERED');
           // To get all the Articles
            String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + art+'__kav';
            String secondPartQuery;
            secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:language WITH DATA CATEGORY ' + categoryGroup +'__c BELOW '+topCategory+'__c AND KBMS__c AT OT__c';
            String query = firstPartQuery + secondPartQuery;
            
            List<sObject> knwoledgesObject = Database.query(query);
            System.debug(knwoledgesObject.size());
            Set<String> kavIds = new SET<String>();
            for (sObject sObjKAV  : knwoledgesObject)
            {
                kavIds.add(String.valueOf(sObjKAV.get('Id')));
            }
            System.debug('kavIds'+kavIds.size());
            // To get Data Categories associated to above Articles
            firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ art+'__DataCategorySelection ';
            secondPartQuery = ' WHERE ParentId In: kavIds';
            query = firstPartQuery+secondPartQuery ;
            List<sObject> dataCategoryObject = Database.query(query);
            
            for (sObject sObjDatCat  : dataCategoryObject)
            {
                DataCategoryNames.add(String.valueOf(sObjDatCat.get('DataCategoryName')));
                
                
                Set<Id> articleIds = new Set<Id>();
                
                if (dataCategoryNameArticleMap.get(String.valueOf(sObjDatCat.get('DataCategoryName'))) != null)
                {
                    articleIds = dataCategoryNameArticleMap.get(String.valueOf(sObjDatCat.get('DataCategoryName')));
                    
                }
                
                articleIds.add(String.valueOf(sObjDatCat.get('ParentId')));
                dataCategoryNameArticleMap.put(String.valueOf(sObjDatCat.get('DataCategoryName')), articleIds);
            } 
            
        }
        
        
        // To Store Count of a specific Data Category Occurance
        Map<String, Integer> dataCategoryCount = new  Map<String, Integer>();
        for (String key : dataCategoryNameArticleMap.keySet())
        {
            /*
            if(!dataCategoryCount.containsKey(key))
            {
                dataCategoryCount.put(key, 0);                
            }
            Integer currentInt=dataCategoryCount.get(key)+1;
            
            */
            Set<Id> articleIds = dataCategoryNameArticleMap.get(key);
            List<Id> uniquArticleIds = new List<Id>();
            uniquArticleIds.addAll(articleIds);
            
            Integer currentInt = uniquArticleIds.size();
            dataCategoryCount.put(key, currentInt);
            
            System.debug('*************'+key);
            System.debug('*************'+currentInt);
        }
        
        //return null;
        
        List<KBMS_Category> categories = new List<KBMS_Category>();              
        Set<String> insertedCategories = new Set<String>();
        
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
    
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
    
        try 
        {
            //get the list of category groups associated
    
            List<String> objType = new List<String>();
    
            objType.add('KnowledgeArticleVersion');
    
            describeCategoryResult = Schema.describeDataCategoryGroups(objType);
   
            //Creating a list of pair objects to use as a parameter
            //for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
    
            //Looping throught the first describe result to create
            //the list of pairs for the second describe call
            for (DescribeDataCategoryGroupResult singleResult : describeCategoryResult)
            {
                if (singleResult.getName()==categoryGroup) 
                {
                    DataCategoryGroupSobjectTypePair p =  new DataCategoryGroupSobjectTypePair();
                    p.setSobject(singleResult.getSobject());
                    p.setDataCategoryGroupName(singleResult.getName());
                    pairs.add(p);
                }   
            }
            
            //describeDataCategoryGroupStructures()
            describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);
            
            for (DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult)
            {
                singleResult.getSobject();
                singleResult.getName();
                singleResult.getLabel();
                singleResult.getDescription();
                
                //Get the top level categories
                DataCategory [] toplevelCategories =  singleResult.getTopCategories();
                
                //Recursively get all the categories
                List<DataCategory> allCategories = getAllCategories(toplevelCategories);
    
                Boolean skipTheBrothers =false;
                for (DataCategory dataCategory : allCategories) 
                {
                    if ((topCategory==dataCategory.getName()&& insertedCategories.isEmpty()) || insertedCategories.contains(dataCategory.getName()) )
                    {
                        continue;
                    }
                
                    String categoryUniqueName = dataCategory.getName();
                    insertedCategories.add(dataCategory.getName());
    
                    
                    //Addition of article count at Parent Data Category 
                    Set<Id> articleCatIds = new Set<Id>();
                    if (dataCategoryCount.get(dataCategory.getName()) != NULL)
                    {
                        articlesCount = articlesCount+dataCategoryCount.get(dataCategory.getName());
                        
                        articleCatIds.addAll(dataCategoryNameArticleMap.get(dataCategory.getName()));
                    }
                    String categoryLabel =dataCategory.getLabel();
                    
                    //Get the list of sub categories in the category
                    DataCategory [] childCategories = dataCategory.getChildCategories();
                 
                    List<KBMS_SubCategory> subcategoriesTmp = new List<KBMS_SubCategory>();
                 
                    for (DataCategory childCategory : childCategories) 
                    {
                        if (topCategory==dataCategory.getName() )
                        {
                            String childCategoryUniqueName =childCategory.getName(); 
                            String childCategoryLabel = childCategory.getLabel();
                            
                            KBMS_Category categoryChild = new KBMS_Category(childCategoryUniqueName,childCategoryLabel, null, null,null);
                            categories.add(categoryChild);
                            
                            if (dataCategoryCount.get(childCategoryUniqueName) != NULL)
                            {
                                articlesCount = articlesCount + dataCategoryCount.get(childCategoryUniqueName); 
                                
                                articleCatIds.addAll(dataCategoryNameArticleMap.get(childCategoryUniqueName));
                            }
                        }
                        else
                        {
                            String SubCategoryUniqueName = childCategory.getName();
                            String subCategoryLabel = childCategory.getLabel();
                            
                            Integer subCatArticleCount =0;
                            if((dataCategoryCount.get(SubCategoryUniqueName) != NULL))
                            {
                                subCatArticleCount = dataCategoryCount.get(SubCategoryUniqueName);
                            }
                            
                            KBMS_SubCategory subcategory = new KBMS_SubCategory(SubCategoryUniqueName,subCategoryLabel, subCatArticleCount);
                            subcategoriesTmp.add(subcategory);
                            
                            //Addition of article count at Sub Category
                            if (dataCategoryCount.get(SubCategoryUniqueName) != NULL)
                            {
                                articlesCount = articlesCount + dataCategoryCount.get(SubCategoryUniqueName); 
                                
                                articleCatIds.addAll(dataCategoryNameArticleMap.get(SubCategoryUniqueName));
                            }
                            
                            //Subcategory Count is being incremented for each sub category under a parent category
                            subcategoriesCount++;
                        }
                        
                        insertedCategories.add(childCategory.getName());
                        List<Id> uniqueArticleCatIds = new List<Id>();
                        uniqueArticleCatIds.addAll(articleCatIds);
                        articlesCount = uniqueArticleCatIds.size();
                        System.debug('articlesCount'+articlesCount);
                    }
             
                    if (topCategory!=dataCategory.getName())
                    {
                        //Sort subcategories according to Article count in descending order
                        subcategoriesTmp.sort();
                   
                        KBMS_Category category = new KBMS_Category(categoryUniqueName,categoryLabel,subcategoriesTmp,articlesCount,subcategoriesCount);
                        categories.add(category);  
                        subcategoriesCount =0;
                        articlesCount =0;
                    }
                    else
                    {
                        skipTheBrothers = true;
                    }
                }
            }
        
        } catch (Exception e)
        {
            System.debug('Error: ' + e);               
        }
        //Sort Categories according to Article count in descending order
        categories.sort();
        return categories;
    }
    private static List<DataCategory> getAllCategories(List<DataCategory> categories)
    {
        if(categories.isEmpty())
        {
            return new DataCategory[]{};
        } 
        else 
        {
            List<DataCategory> categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];

            List<DataCategory> allCategories = new List<DataCategory>{category};

            categoriesClone.remove(0);
    
            categoriesClone.addAll(category.getChildCategories());

            allCategories.addAll(getAllCategories(categoriesClone));

            return allCategories;
        }
    }
    
    //For KBMS
    public static List<DataCategory> getDescribeDataCategoryGroupStructureResults(){
    
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        List<String> objType = new List<String>();
        List<DataCategory> lstKBMSDataCategory=new List<DataCategory>();

        objType.add('KnowledgeArticleVersion');

        describeCategoryResult = Schema.describeDataCategoryGroups(objType);
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
        
        for(DescribeDataCategoryGroupResult r:describeCategoryResult){

            if(r.getName()=='KBMS'){       

                        DataCategoryGroupSobjectTypePair p =new DataCategoryGroupSobjectTypePair();
                        p.setSobject(r.getSobject());
                        p.setDataCategoryGroupName(r.getName());
                        pairs.add(p);
            }
         }
         describeCategoryStructureResult =Schema.describeDataCategoryGroupStructures(pairs, false);
         
         for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
        
                //Get the name of the data category group
                
                //singleResult.getName());
                //singleResult.getLabel());
                
                
                //Get the top level categories
                DataCategory [] toplevelCategories = singleResult.getTopCategories();
                List<DataCategory> allCategories = getAllCategories(toplevelCategories);
                for(DataCategory category : allCategories) {
                     
                     category.getName();
                     category.getLabel();
                     lstKBMSDataCategory.add(category);
        
                 }
                 
       }     
       return lstKBMSDataCategory;
}
}