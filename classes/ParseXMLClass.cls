public class ParseXMLClass{

public static wwwExampleOrgRecuperofile.BODY_element parseResponse(wwwExampleOrgRecuperofile.BODY_element response){
    wwwExampleOrgRecuperofile.BODY_element resp = new wwwExampleOrgRecuperofile.BODY_element();
    Dom.document doc = new Dom.Document();
    //doc.load(response);
    Dom.XmlNode envelope = doc.getRootElement();
    if (envelope != null && envelope.getChildElements()!=null)
        {
            for(Dom.XmlNode envelopeChild : envelope.getChildElements())
                {
                    if(envelopeChild.getName().equalsIgnoreCase('Body'))
                    {
                        if(envelopeChild.getChildElements()!=null)
                        {
                            for(Dom.XmlNode responseElement: envelopeChild.getChildElements())
                            {
                                if(responseElement.getName().equalsIgnoreCase('RecuperoFileReply'))
                                {
                                    
                                    for(Dom.XmlNode element: responseElement.getChildElements())
                                    {
                                        if(element.getName().equalsIgnoreCase('BODY'))
                                        {
                                            for(Dom.XMLNode responseNode: element.getChildElements())
                                            {
                                                if(responseNode.getName() == 'CodEsito') 
                                                resp.CodEsito = responseNode.getText();
                                                
                                                else if(responseNode.getName() == 'DescrEsito')
                                                resp.DescrEsito= responseNode.getText();
                                                
                                                else if(responseNode.getName() == 'Url')
                                                resp.Url= responseNode.getText();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        }
        return resp;
}
}