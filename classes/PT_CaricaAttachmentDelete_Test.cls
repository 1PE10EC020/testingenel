/*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search.
**********************************************************************************************/
@isTest
private class PT_CaricaAttachmentDelete_Test {
  PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING TESTS='test23';
  PRIVATE STATIC FINAL INTEGER LMT=1; 

   /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/
@testSetup static void createTestRecords() {
 Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
 User usr = new User(Alias = ALIAS, Email=EMAIL,
  EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
  LocaleSidKey=ENUS, ProfileId = pfl.Id,
  TimeZoneSidKey=TZK,
  UserName=UUNAME);
 database.insert(usr);
}
  /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_CaricaAttachmentDeleteTest() {
  User usr=[Select id,name from User LIMIT: lmt];
  System.runAs(usr) {
    
    list<PT_Caricamento_Massivo__c > lstInsertService = new list<PT_Caricamento_Massivo__c >();
    PT_Caricamento_Massivo__c cmaer= new PT_Caricamento_Massivo__c();
    cmaer.TipoRich__c='M03';
    cmaer.DataDecorr__c=system.today();
    cmaer.CreatedDate__c=system.today();
    lstInsertService.add(cmaer);
    
        //Datetime yesterday = Datetime.now().addDays(-3);
        //Test.setCreatedDate(lstInsertService[0].Id, yesterday);
        
        database.insert(lstInsertService);
        Attachment attach = new Attachment();
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = lstInsertService[0].id;
        database.insert(attach);
        Test.startTest();
        PT_CaricaAttachmentDelete.attachementdelete_OnCaricomento(lstInsertService);
        Test.stopTest();
        system.assertEquals(attach.Name, 'Unit Test Attachment');
      }
    }
  }