@isTest
public class CaseMgmtListController_Test {


   
    @isTest
    static void test() {
    	case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :Constants.CASE_DA_DEFINIRE ].id ;
        c.TipologiaCase__c ='Misura';
        c.origin = 'Email';
        c.status='In carico al FO';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'IT001E020070030';
        c.DirezioneDTR__c = 'Toscana Umbria' ;
        c.UnitaDiCompetenza__c ='FIRENZE';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.Famiglia__c = '';
        insert c;
        test.startTest();
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Operatore CM I Livello' limit 1]; 
        UserRole ur = [select developername, name, id from userRole where developername = 'Back_Office' limit 1];
      	
      	String selection = 'Toscana Umbria';
      	String queueId = [SELECT id,Name, type FROM Group where name  =:selection and type = 'Queue' limit 1].id;
        User u = new User(UserRoleId = ur.Id,Alias = 'pliv', Email='primoliv@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='primoliv@testorg.com');
        
             c = [select id,TipologiaCase__c, Famiglia__c, CodiceFiscaleTitolarePOD__c,pod__C, status,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,Oraria__c,LivelloTensioneDiFornitura__c from case limit 1 ];
            
			PageReference casePage = new PageReference('/' + c.id);
			Test.setCurrentPage(casePage);
           apexpages.currentpage().getparameters().put('id' , c.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            CaseMgmtListController controller = new CaseMgmtListController(sc);
            
            
            controller.setassignSelected(queueId);
            
            controller.getcaseAssignments();
        	controller.getassignSelected();
           controller.assignOk();
		
		boolean errorfound = false;
		try{
			controller.assignOk();
		}
		catch(Exception e){
			errorfound = true;
		}
		system.assertEquals(false,errorfound);
		
		controller.assignOk();
            
            test.stopTest();
            
    }
    
    @isTest
    static void test2() {
    	case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :Constants.CASE_DA_DEFINIRE ].id ;
        c.TipologiaCase__c ='Misura';
        c.origin = 'Email';
        c.status='In carico al FO';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'IT001E020070030';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.Famiglia__c = '';
        insert c;
        test.startTest();
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Operatore CM I Livello' limit 1]; 
        UserRole ur = [select developername, name, id from userRole where developername = 'Back_Office' limit 1];
      
        User u = new User(UserRoleId = ur.Id,Alias = 'pliv', Email='primoliv@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='primoliv@testorg.com');
        
        
             c = [select id,TipologiaCase__c, Famiglia__c, CodiceFiscaleTitolarePOD__c,pod__C, status,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,Oraria__c,LivelloTensioneDiFornitura__c from case limit 1 ];
           
           PageReference casePage = new PageReference('/' + c.id);
			Test.setCurrentPage(casePage);
           apexpages.currentpage().getparameters().put('id' , c.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            CaseMgmtListController controller = new CaseMgmtListController(sc);
          
            
            controller.getcaseAssignments();
        	controller.getassignSelected();
           controller.CancelOk();
		
		boolean errorfound = false;
		try{
			controller.CancelOk();
		}
		catch(Exception e){
			errorfound = true;
		}
		system.assertEquals(false,errorfound);
		
		controller.CancelOk();
            
            test.stopTest();
            
    }
}