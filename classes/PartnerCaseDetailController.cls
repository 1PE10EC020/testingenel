public with Sharing class PartnerCaseDetailController {

    @auraenabled
    public static Case getCase(String caseId){
       system.debug('!!caseid'+caseId);   
      return [select id , ownerid, contactPhone,CaseNumber,ContactEmail,
                Subject,Description,Status,Priority
                from case where id =: caseId][0];    
      
        
    }
    
     @auraenabled
    public static List<Attachment> getAttachment( String  caseId){
       system.debug('@@@');  
        
      return [SELECT BodyLength,LastModifiedById,CreatedById,Name,ParentId FROM Attachment where Parentid= : caseId ];      
        
    }
    
    @auraenabled
    public static List<Solution> getSolution(String caseID) {
         List<CaseSolution>  caseSolList = [Select id,SolutionId from caseSolution]; 
     List<Solution> solList = [Select OwnerId,SolutionName,SolutionNumber,Status from Solution ];
          system.debug('@@@'+solList);  
        return solList;      
    }
}