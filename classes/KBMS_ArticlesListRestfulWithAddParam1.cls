@RestResource(urlMapping='/getArticlesListWithAddParam1/*')
global with sharing class KBMS_ArticlesListRestfulWithAddParam1
{
    @HttpGet
    global static List<KBMS_ArticleWrapperComparable> getArticlesList()
    {
        String articleType = RestContext.request.params.get('articleType');
        String limits = RestContext.request.params.get('limit');
        String froms = RestContext.request.params.get('from');
        
        //limit, from, listCategory, articleType, orderBy, textSearch
        //Additional Parameters
        String listCategory= RestContext.request.params.get('listCategory');
        system.debug('@@---listCategory'+listCategory);
        String orderBy =RestContext.request.params.get('orderBy');
        String strIsDesc = RestContext.request.params.get('isDESC');
        String searchstring =RestContext.request.params.get('textSearch'); 
		
		Map<String, List<String>> categoryParentChildMap = KBMS_DataCategoryUtil.getMapParentCategoryList();
        
        searchstring = searchstring == null?'':searchstring;
        Boolean isDataCategoryUnselected = FALSE;
        List<String> searchStringList = new List<String>();
        Set<String> setSearchString=new Set<String>(); 
        
        if(searchstring != NULL && searchstring != '')
        {
            searchStringList = searchstring.split(' ');      
        }
        setSearchString.addAll(searchStringList);       
        
        //preposition word remove       
             
        String prePositionWords = System.Label.KBMS_prepositionWords;
        String[] splitPrepositionWords = new String[] {};       
        splitPrepositionWords = prePositionWords.split(',');
        System.debug('splitPrepositionWords '+splitPrepositionWords);        
        Set<String> setprepositionWords=new Set<String>();      
        setprepositionWords.addAll(splitPrepositionWords);      
                
        //removeing all preposition words from set      
        setSearchString.removeAll(setprepositionWords);
        System.debug('setSearchString'+setSearchString);     
        searchStringList.Clear();   
        setprepositionWords.Clear();    
        for(String s:setSearchString)
        {      
            searchStringList.add('%'+s+'%');        
        } 
        
        //adding for vote like dislike
        Map<Id,Integer> mapTotalUpVote=new Map<Id,Integer>();
        Map<Id,Integer> mapTotalDownVote=new Map<Id,Integer>(); 
        String articleParentType=articleType+'__ka'; 
        
        List<AggregateResult> groupedResults= [select Type,Count(Id)typecount,ParentId  from vote WHERE Parent.type=:articleParentType  Group By ParentId,Type];
                    
        for (AggregateResult ar : groupedResults)  
        {
            if(ar.get('Type')=='Up')
            {
                mapTotalUpVote.put((Id)ar.get('ParentId'),(Integer)ar.get('typecount')); 
            }
            else if(ar.get('Type')=='Down')
            {
                mapTotalDownVote.put((Id)ar.get('ParentId'),(Integer)ar.get('typecount'));
            }
        } 
        
        //ended
        
        //List<ArtcileWrapper> artcileWrappers = new List<ArtcileWrapper>();
        List<KBMS_ArticleWrapperComparable> artcileWrappers = new List<KBMS_ArticleWrapperComparable>();
        if(orderBy.equals('LastModifiedDate'))
        {
            orderBy = 'LastModifiedDate';
        }else if(orderBy.equals('Alphabetic'))
        {
            orderBy = 'Title';
        }
        String categoryList='';
            if(listCategory != NULL && listCategory !='' && listCategory.length() != 0)
            {
                categoryList = '(';
                //Parsing Data Category JSON String
                JSONParser parser = JSON.createParser(listCategory);
                while (parser.nextToken() != null) 
                {
                    if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
                    { 
                        while (parser.nextToken() != null) 
                        {
                            if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                            {
                                dataCategories le = (dataCategories)parser.readValueAs(dataCategories.class);
                                categoryList = categoryList+le.categoryName+'__c,'; 
                            }
                        }
                        categoryList =categoryList.substring(0, categoryList.length()-1);
                        categoryList = categoryList+')';
                    }
                }
            }else
            {
                categoryList = '(Tutto__c)';
                isDataCategoryUnselected = TRUE;
            }
            
        
        
            
        system.debug('@@-- category list--'+categoryList);
        listCategory = categoryList;
        system.debug('debug--'+listCategory);
        Integer noLimits = limits == null ? 0 : Integer.valueOf(limits);
        Integer offsets = froms == null ? 0 : Integer.valueOf(froms );
        //Organization orgDetails = [SELECT Id, LanguageLocaleKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
        String language = Label.KBMS_Language;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        
       if(orderBy.equals('Title') || orderBy.equals('LastModifiedDate'))
        {
            String secondPartQuery='';
            String thirdPartQuery='';
            String fourthPartQuery='';
            String fifthPartQuery='';
            
           String firstPartQuery = ' SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language ' ;
            
            thirdPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +' BELOW '+listCategory+' AND KBMS__c AT (OT__c) ';          
            
            Integer i = 0;
            if (searchStringList != null  && searchStringList.size() > 0 )
            {
                secondPartQuery += 'AND (Title LIKE :searchStringList  OR Summary LIKE :searchStringList )';
            }
            
            System.debug('@@secondPartQuery '+secondPartQuery);
            //fourthPartQuery += ' ORDER BY ' +orderBy+ ' ASC';
           if(strIsDesc.containsIgnoreCase('FALSE'))
            {
                fourthPartQuery += ' ORDER BY ' +orderBy+ ' ASC';
            }
            else
            {
                fourthPartQuery += ' ORDER BY '+orderBy+' DESC'; 
            }
                
            
            fifthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
            String query = '';
            if(articleType !='Moduli')
            { 
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery;
            }
            else if(isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery +fourthPartQuery+fifthPartQuery ;
            }
            else if(!isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery;
            }
            
            system.debug('@@testing--query2--'+query);
            List<sObject> knwoledgesObject = Database.query(query); 
                
            Set<Id> parentArticleId = new Set<Id>();            
            Set<String> kavIds = new Set<String>();
            
            Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
            
            if(knwoledgesObject.size()>0)
            {
                for (sObject sObjKAV  : knwoledgesObject)
                {
                    kavIds.add(String.valueOf(sObjKAV.get('Id')));
                    parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                }
            }
            
            //VoteStat
            Map<String, Double> NormalizedScore = new  Map<String, Double>();
            firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
            secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId'; 
            //thirdPartQuery = ' ORDER BY NormalizedScore ASC NULLS LAST ';
            if(strIsDesc.containsIgnoreCase('TRUE'))
            {
                thirdPartQuery = ' ORDER BY NormalizedScore DESC NULLS LAST ';
            }
            else
            {
                thirdPartQuery = ' ORDER BY NormalizedScore ASC NULLS LAST ';
            }
            query = firstPartQuery+secondPartQuery+thirdPartQuery;
            
            system.debug('@@testing--query1--'+query);
            List<sObject> voteStatusObject = Database.query(query);
            for(sObject s : voteStatusObject)
            {
                NormalizedScore.put(String.valueOf(s.get('ParentId')), Double.valueOf(s.get('NormalizedScore')));
            }
            
            //ViewStat
            Map<String, Integer> ViewCount = new  Map<String, Integer>();
            firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
            secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId';
            //thirdPartQuery = ' ORDER BY ViewCount ASC NULLS LAST, NormalizedScore ASC NULLS LAST ';
            if(strIsDesc.containsIgnoreCase('TRUE'))
            {
                thirdPartQuery = ' ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
            }
            else
            {
                thirdPartQuery = ' ORDER BY ViewCount ASC NULLS LAST, NormalizedScore ASC NULLS LAST ';
            }
            
            query = firstPartQuery+secondPartQuery+thirdPartQuery;
            
            system.debug('@@testing--query--'+query);
            List<sObject> viewStatusObject = Database.query(query);
            for(sObject s : viewStatusObject)
            {
                ViewCount.put(String.valueOf(s.get('ParentId')), Integer.valueOf(s.get('ViewCount')));
            }
            
            firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
            secondPartQuery = ' WHERE ParentId In: kavIds';
            query = firstPartQuery + secondPartQuery;
            
            List<sObject> dataCategoryObject = Database.query(query);
            
            Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
            for (sObject sObjDatCat  : dataCategoryObject)
            {
                
                if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))!='KBMS' && 
                   String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c'))
                { 
                    if(  sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) == NULL ||
                       (sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) != NULL && 
                        !listCategory.contains( sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))))))
                    {
                        sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
                    }
                }
            }
            
            system.debug('@@--in loop--'+knwoledgesObject.size());
            for (sObject sObjKAV  : knwoledgesObject)
            {
                sObject articles ;
                String parentName;
                String parentLabel;
                String dataCategoryName;
                String dataCategoryLabel;
                Double likeScore; 
                Integer vewCount;      
                Integer totalLikes; 
                Integer totalDislikes;
                                
                // aw.dataCategoryList = categoryList;
                articles = sObjKAV;
               // likeScore = NormalizedScore.get(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                dataCategoryName = sObjectDataCategoryMaps.get(articles.Id);
                
                vewCount = ViewCount.get(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                if(mapTotalUpVote.containskey((Id)sObjKAV.get('KnowledgeArticleId')))
                {
                    totalLikes=mapTotalUpVote.get((Id)sObjKAV.get('KnowledgeArticleId'));
                }else
                {
                    totalLikes=0;
                }
                if(mapTotalDownVote.containskey((Id)sObjKAV.get('KnowledgeArticleId')))
                {
                    totalDislikes=mapTotalDownVote.get((Id)sObjKAV.get('KnowledgeArticleId'));
                }else
                {
                    totalDislikes=0;
                }
                if(totalLikes + totalDislikes != 0){
                    Double dividend = Double.valueOf(totalLikes) - Double.valueOf(totalDislikes);
                        Double divisor = Double.valueOf(totalLikes) + Double.valueOf(totalDislikes);
                        likeScore = dividend/divisor;
                }
                else{
                    likeScore =0;
                }
                likeScore = NormalizedScore.get(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                if(dataCategoryName != null && dataCategoryName != '')
                {
                    List<String> parent = new List<String>();                    
                    parent = categoryParentChildMap.get(dataCategoryName);                 
                    if(dataCategoryName != 'Tutto' && parent != null)
                    {
                        dataCategoryLabel= parent[0];
                        parentName = parent[1];
                        parentLabel = parent[2];
                        KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                        artcileWrappers.add(aw);
                    }else
                    {
                        dataCategoryLabel = dataCategoryName;
                        KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                        artcileWrappers.add(aw);
                    }
                }
                else
                {
                   KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                   artcileWrappers.add(aw);
                }
                

            }
        
            
        }
        else if(orderBy.equals('Like'))
        {
            
            String firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
            String secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In '; 
            String thirdPartQuery='';
            
            String fourthPartQuery = ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) ';
            
            thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language ';
            if (searchStringList != null  && searchStringList.size() > 0 )
            {
                thirdPartQuery += ' AND (Title LIKE :searchStringList  OR Summary LIKE :searchStringList )';
            }
            String fifthPartQuery;
            fifthPartQuery = ' ) ORDER BY NormalizedScore ASC NULLS LAST ';
            if(strIsDesc.containsIgnoreCase('TRUE'))
            fifthPartQuery = ' ) ORDER BY NormalizedScore DESC NULLS LAST ';
            String sixthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
            String query='';
            
            if(articleType !='Moduli')
            { 
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery +sixthPartQuery;
            }
            else if(isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fifthPartQuery+sixthPartQuery;
            }
            else if(!isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery +sixthPartQuery;
            }
            
            List<sObject> voteStatObjects = Database.query(query);
            
            Set<String> artcilesIds = new Set<String>();
            if(voteStatObjects.size()>0)
            {
                for (sObject sObjVS : voteStatObjects)
                {
                    artcilesIds.add(String.valueOf(sObjVS.get('ParentId')));
                }
            }
            firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav ';
            secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language ' ;
            thirdPartQuery ='  WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory;
            
            if(articleType !='Moduli')
            { 
                query = firstPartQuery + secondPartQuery + thirdPartQuery;
            }
            else if(isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery;
            }
            else if(!isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery;
            }
            
            List<sObject> knwoledgesObject = Database.query(query);
            
            Set<Id> parentArticleId = new Set<Id>();
            Set<String> kavIds = new Set<String>();
            
            Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
            if(knwoledgesObject.size()>0)
            {
                for (sObject sObjKAV  : knwoledgesObject)
                {
                    sObjectKAVMaps.put(String.valueOf(sObjKAV.get('KnowledgeArticleId')), sObjKAV);
                    kavIds.add(String.valueOf(sObjKAV.get('Id')));
                    parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                }
            }
                
            //ViewStat
            Map<String, Integer> ViewCount = new  Map<String, Integer>();
            firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
            secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId';
            //thirdPartQuery = ' (SELECT KnowledgeArticleId FROM faq__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+')';
            thirdPartQuery = ' ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
            //String fourthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
            
            query = firstPartQuery+secondPartQuery+thirdPartQuery;
            List<sObject> viewStatusObject = Database.query(query);
            for(sObject s : viewStatusObject){
                ViewCount.put(String.valueOf(s.get('ParentId')), Integer.valueOf(s.get('ViewCount')));
            }
            
            firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
            secondPartQuery = ' WHERE ParentId In: kavIds ';
            query = firstPartQuery + secondPartQuery;
            
            List<sObject> dataCategoryObject = Database.query(query);
            System.debug('-->'+dataCategoryObject );
            
            Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
            for (sObject sObjDatCat  : dataCategoryObject)
            {
                
                if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))!='KBMS' && 
                   String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c'))
                { 
                    if(  sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) == NULL ||
                       (sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) != NULL && 
                        !listCategory.contains( sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))))))
                    {
                        sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
                    }
                }
            }
            
           
            for (sObject sObjVS : voteStatObjects)
            {
                sObject articles ;
                String parentName;
                String parentLabel;
                String dataCategoryName;
                String dataCategoryLabel;
                Double likeScore; 
                Integer vewCount;      
                Integer totalLikes; 
                Integer totalDislikes;
                //Double customLikeScore;
                articles = sObjectKAVMaps.get(String.valueOf(sObjVS.get('ParentId')));
                //likeScore= double.valueOf(sObjVS.get('NormalizedScore'));
                
                if(articles!=null)
                {
                    
                    dataCategoryName = sObjectDataCategoryMaps.get(articles.Id);
                    vewCount = ViewCount.get(String.valueOf(sObjVS.get('ParentId')));
                    if(mapTotalUpVote.containskey((Id)sObjVS.get('ParentId')))
                    {
                         totalLikes=mapTotalUpVote.get((Id)sObjVS.get('ParentId'));
                    }
                    else
                    {
                        totalLikes=0;
                    }
                    
                    if(mapTotalDownVote.containskey((Id)sObjVS.get('ParentId')))
                    {
                        totalDislikes=mapTotalDownVote.get((Id)sObjVS.get('ParentId'));
                    }
                    else
                    {
                        totalDislikes=0;
                    }
                    
                    if(totalLikes + totalDislikes != 0){
                            Double dividend = Double.valueOf(totalLikes) - Double.valueOf(totalDislikes);
                        Double divisor = Double.valueOf(totalLikes) + Double.valueOf(totalDislikes);
                        likeScore = dividend/divisor;
                    }
                    else{
                        likeScore =0;
                    }
                    likeScore= double.valueOf(sObjVS.get('NormalizedScore'));
                    if(dataCategoryName != null && dataCategoryName != '' )
                    {
                        List<String> parent = new List<String>();
                        parent = categoryParentChildMap.get(dataCategoryName);
                        
                        if(dataCategoryName != 'Tutto' && parent != null)
                        {
                            dataCategoryLabel= parent[0];
                            parentName = parent[1];
                            parentLabel = parent[2];
                               
                            KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                            artcileWrappers.add(aw);
                               
                            
                        }else
                        {
                            dataCategoryLabel= dataCategoryName;
                            KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                            artcileWrappers.add(aw);
                        }
                    }
                    else
                    {
                        
                         KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                         artcileWrappers.add(aw);
                    }
                }
                
            }
            //Setting static variable sortBy in wrapper class to likeScore
            KBMS_ArticleWrapperComparable.sortBy='likeScore';
            KBMS_ArticleWrapperComparable.sortByDesc = TRUE;
            if(strIsDesc.containsIgnoreCase('FALSE'))
                KBMS_ArticleWrapperComparable.sortByDesc = FALSE;
            artcileWrappers.sort();
            
            
        }
        else if(orderBy.equals('View'))
        {
            
            String firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
            String secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In ';
            //Now Using 'articleType' Parameter instead of Explicit reference to an article type: "FROM faq__kav"
            String thirdPartQuery='';
            String fourthPartQuery='';
            String fifthPartQuery;
           
            thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language ';
            
            if (searchStringList != null  && searchStringList.size() > 0 )
            {
                thirdPartQuery += 'AND (Title LIKE :searchStringList  OR Summary LIKE :searchStringList )';
            }   
            
            fourthPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) ';
            
            fifthPartQuery= ') ORDER BY ViewCount ASC NULLS LAST, NormalizedScore ASC NULLS LAST ';
            if(strIsDesc.containsIgnoreCase('TRUE'))
            fifthPartQuery= ') ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
            
            String sixthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
            String query='';
            
            if(articleType !='Moduli')
            { 
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery+sixthPartQuery ;
            }
            else if(isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fifthPartQuery +sixthPartQuery;
            }
            else if(!isDataCategoryUnselected && articleType =='Moduli')
            {
                query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery+sixthPartQuery ;
            }
            
            System.debug('@@--view problem--'+query);
            List<sObject> voteStatObjects = Database.query(query);
            
            Set<String> artcilesIds = new Set<String>();
            Set<Id> parentArticleId = new Set<Id>();
            if(voteStatObjects.size()>0)
            {
                for (sObject sObjVS : voteStatObjects)
                {
                    artcilesIds.add(String.valueOf(sObjVS.get('ParentId')));
                }
            
            }
            
            firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav ';
            
            
            if(articleType !='Moduli')
            { 
                secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory;
            }
            else if(isDataCategoryUnselected && articleType =='Moduli')
            {
                secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language';
            }
            else if(!isDataCategoryUnselected && articleType =='Moduli')
            {
                secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory;
            }
            query = firstPartQuery + secondPartQuery;
            
            List<sObject> knwoledgesObject = Database.query(query);
            
            Set<String> kavIds = new Set<String>();
            
            Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
            if(knwoledgesObject.size()>0)
            {
                for (sObject sObjKAV  : knwoledgesObject)
                {
                    sObjectKAVMaps.put(String.valueOf(sObjKAV.get('KnowledgeArticleId')), sObjKAV);
                    kavIds.add(String.valueOf(sObjKAV.get('Id')));
                    parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                }
            }
            //VoteStat
            Map<String, Double> NormalizedScore = new  Map<String, Double>();
            firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
            secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId'; 
            thirdPartQuery = ' ORDER BY NormalizedScore DESC NULLS LAST ';
            
            query = firstPartQuery+secondPartQuery+thirdPartQuery;
            List<sObject> voteStatusObject = Database.query(query);
            for(sObject s : voteStatusObject){
                NormalizedScore.put(String.valueOf(s.get('ParentId')), Double.valueOf(s.get('NormalizedScore')));
            }
            
            
            firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
            secondPartQuery = ' WHERE ParentId In: kavIds ';
            query = firstPartQuery + secondPartQuery;
            
            List<sObject> dataCategoryObject = Database.query(query);
            System.debug('-->'+dataCategoryObject );
            
            Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
            for (sObject sObjDatCat  : dataCategoryObject)
            {
                
                if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))!='KBMS' && 
                   String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c'))
                { 
                    if(  sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) == NULL ||
                       (sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))) != NULL && 
                        !listCategory.contains( sObjectDataCategoryMaps.get(String.valueOf(sObjDatCat.get('ParentId'))))))
                    {
                        sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
                    }
                }
            }
            
            
            
            for (sObject sObjVS : voteStatObjects)
            {
                sObject articles ;
                String parentName;
                String parentLabel;
                String dataCategoryName;
                String dataCategoryLabel;
                Double likeScore; 
                Integer vewCount;      
                Integer totalLikes; 
                Integer totalDislikes;
                //Double customLikeScore;
                
                articles = sObjectKAVMaps.get(String.valueOf(sObjVS.get('ParentId')));
                vewCount = Integer.valueOf(sObjVS.get('ViewCount'));
               // likeScore = NormalizedScore.get(String.valueOf(sObjVS.get('ParentId')));
                if(mapTotalUpVote.containskey((Id)sObjVS.get('ParentId')))
                {
                    totalLikes=mapTotalUpVote.get((Id)sObjVS.get('ParentId'));
                } else   
                {
                    totalLikes=0;
                }
                if(mapTotalDownVote.containskey((Id)sObjVS.get('ParentId'))){
                    totalDislikes=mapTotalDownVote.get((Id)sObjVS.get('ParentId')); 
                }   
                else{
                    totalDislikes=0;
                }
                if(totalLikes + totalDislikes != 0){
                        Double dividend = Double.valueOf(totalLikes) - Double.valueOf(totalDislikes);
                        Double divisor = Double.valueOf(totalLikes) + Double.valueOf(totalDislikes);
                        likeScore = dividend/divisor;
                }
                else{
                    likeScore =0;
                }
                likeScore = NormalizedScore.get(String.valueOf(sObjVS.get('ParentId')));
                if(articles!=null){
                    dataCategoryName = sObjectDataCategoryMaps.get(articles.Id); 
                    if(dataCategoryName != null && dataCategoryName != '' ){
                        List<String> parent = new List<String>();
                        parent = categoryParentChildMap.get(dataCategoryName);
                        if(dataCategoryName != 'Tutto' && parent != null){
                            dataCategoryLabel= parent[0];
                            parentName = parent[1];
                            parentLabel = parent[2]; 
                            
                            KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                            artcileWrappers.add(aw);
                        }else
                        {
                            dataCategoryLabel = dataCategoryName;
                            KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                            artcileWrappers.add(aw);
                        }
                    }else
                    {
                            KBMS_ArticleWrapperComparable aw = new KBMS_ArticleWrapperComparable(articles,parentName,parentLabel,dataCategoryName,dataCategoryLabel,likeScore,vewCount,totalLikes,totalDislikes);
                            artcileWrappers.add(aw);
                    }
                }
            }
            //Setting static variable sortBy in wrapper class to viewCount
            KBMS_ArticleWrapperComparable.sortBy='viewCount';
            KBMS_ArticleWrapperComparable.sortByDesc = TRUE;
            if(strIsDesc.containsIgnoreCase('FALSE'))
                KBMS_ArticleWrapperComparable.sortByDesc = FALSE;
            artcileWrappers.sort();
            
        }
        
        return artcileWrappers;
    }
   
   public class dataCategories
    { 
        public String categoryName{get;set;}
    }
    
}