public with sharing class UploadFile_Controller {
    public transient Attachment objAttachment;
    //Use getter so we can make attachment transient
    public Attachment getObjAttachment(){
        objAttachment = new Attachment();
        return objAttachment;
    }
    public UploadFile_Controller(){
     	String recTypeName = (String) ApexPages.currentPage().getParameters().get('recType');
        system.debug('recTypeName'+recTypeName);
        if(recTypeName == System.Label.PT_RICHIESTE_RecordTypeName_PT_M03){
            //recTypeCon = '.pdf, .xls, .xlsx, .csv, .doc, .msg, .jpg, .jpeg';
            recTypeCon = System.Label.PT_Upload_File_Format_M03;
        }
        if(recTypeName == System.Label.PT_RICHIESTE_RecordTypeName_PT_GD1){
            //recTypeCon = '.pdf';
            recTypeCon = System.Label.PT_Upload_File_Format_GD1;
        }
        if(recTypeName == System.Label.PT_RICHIESTE_RecordTypeName_PT_SP2){
            //recTypeCon = '.pdf, .xls, .xlsx, .csv, .doc, .msg, .jpg, .jpeg';
            recTypeCon = System.Label.PT_Upload_File_Format_SP2;
        }
        
    }

    public Boolean fileUploadProcessed{get;set;}
    public String message{get;set;}
    public String messageType{get;set;}
    public string para {get;set;}
    public string recordId {get;set;}
    public string recTypeCon {set;get;}
    public string fileName {set;get;}
    /*public string getRecType() { 
        return recTypeCon; 
    }*/
    
    Public void returnVals(){
               
    }
    
    
    Public void uploadFile(){
            try {
            
//Use the parent id as the atrribute passed from the RIC and call the upload button on click of confirm
          //objAttachment.ParentId = (Id) ApexPages.currentPage().getParameters().get('id');
          //  objAttachment.ParentId = 'a0K7E0000014tj5'; 
          // String para = Apexpages.currentPage().getParameters().get('selected');  
        
     	String recTypeName = (String) ApexPages.currentPage().getParameters().get('recType');
        system.debug('recTypeName12345678'+recTypeName);
        system.debug('objAttachment.AttachmentName'+ objAttachment.Name );
  		String attachName = objAttachment.Name;
                      
              			
            
        system.debug('objAttachment.Name'+ objAttachment.Name );
        system.debug('objAttachment.ParentId'+ objAttachment.ParentId );
                    
        objAttachment.ParentId = (Id) recordId;
   		insert objAttachment;
   		message = 'File was uploaded successfully';
   		messageType = 'success';
                
        }catch(Exception e){
            message = e.getMessage();
            messageType = 'error';
        }
        fileUploadProcessed = true;
     }
    //}
}