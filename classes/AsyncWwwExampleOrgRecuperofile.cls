//Generated by wsdl2apex

public class AsyncWwwExampleOrgRecuperofile {
    public class RecuperoFileReply_elementFuture extends System.WebServiceCalloutFuture {
        public wwwExampleOrgRecuperofile.BODY_element getValue() {
            wwwExampleOrgRecuperofile.RecuperoFileReply_element response = (wwwExampleOrgRecuperofile.RecuperoFileReply_element)System.WebServiceCallout.endInvoke(this);
            return response.BODY;
        }
    }
}