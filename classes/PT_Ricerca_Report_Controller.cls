/**********************************************************************************************
@author:         Bharath Sharma
@date:           19 sep,2017
@description:    This controller is used for Caricamento_Massivo object search Lightning component to search and display table
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
public without sharing class PT_Ricerca_Report_Controller {
        PRIVATE STATIC FINAL STRING IPHN = '-';
        PRIVATE STATIC FINAL STRING ADDRESSQUERTY = 'Select Id,Name,Note_Report__c,DataPub__c,DataEstraz__c,'+
                                                    'NomeFileReport__c From PT_Report__c where id!=null';
        public static final String PTREQUESTRECORDCREATE='PT_Ricerca_Report_Controller';
        PRIVATE STATIC FINAL STRING QUOTE = '\'';
        PRIVATE STATIC FINAL STRING ANDTXT = ' and ';
        PRIVATE STATIC FINAL STRING LIMT = ' Limit ';
        PRIVATE STATIC FINAL STRING COMMA = ',';
        PRIVATE STATIC FINAL STRING GRT = ' >= ';
        PRIVATE STATIC FINAL STRING LST = ' <= ';
        PRIVATE STATIC FINAL STRING FIELD_ID = 'id';
        PRIVATE STATIC FINAL STRING TIPO = 'IdTipoReport__c=';
        PRIVATE STATIC FINAL STRING DISP =  'DescrDispaccEstesa__c=';
        PRIVATE STATIC FINAL STRING PROV =  'Note_Report__c=';
        PRIVATE STATIC FINAL STRING CATE =  'CategFlusso__c=';
        PRIVATE STATIC FINAL STRING TIPOF =  'TipoFlusso__c=';
        PRIVATE STATIC FINAL STRING NOME =  'NomeFileReport__c=';
        PRIVATE STATIC FINAL STRING CODI =  'CodiceRichiesta__c=';
        PRIVATE STATIC FINAL STRING DESCEST = 'DescrDispaccEstesa__c=';
        PRIVATE STATIC FINAL STRING DATAESTC = 'DataEstraz__c';
        PRIVATE STATIC FINAL STRING DATAPUB = 'DataPub__c';
        PRIVATE STATIC FINAL STRING NAME= 'Name';
        PRIVATE STATIC FINAL STRING SPACE = ' ';
        PRIVATE STATIC FINAL STRING ORDRBY = ' Order By';
        PRIVATE STATIC FINAL STRING DESN = ' DESC ';
        PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
        public static final String GETALLRICHPICKVALS = 'getAllRichiestaPicklistValuesCntr';
        public static final String CATEFLUSSO = 'CategFlusso__c'; 
        public static final String TIPOFLUSSO1='TipoFlusso__c';
        public static final String TIPO1='IdTipoReport__c';
        public static final String PT_REPORT = 'PT_Report__c';

/**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method used to hold data entered in Caricamento_Massivo object Lightning component 
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
    public with sharing class WrapperClass {
         @AuraEnabled
        public string TipoRich {
            get;
            set;
        }
         @AuraEnabled
        public string Venditore {
            get;
            set;
        }

        @AuraEnabled
        public string Dispacciamento {
            get;
            set;
        }
        @AuraEnabled
        public Date DataPubblicazioneda {
            get;
            set;
        }
        @AuraEnabled
        public Date DataPubblicazioneA {
            get;
            set;
        }
        @AuraEnabled
        public Date DataEstrazioneda {
            get;
            set;
        }
        @AuraEnabled
        public Date DataEstrazioneA {
            get;
            set;
        }
        
        @AuraEnabled
        public string Provincia {
            get;
            set;
        }
        @AuraEnabled
        public string CategoriaFlusso {
            get;
            set;
        }
        @AuraEnabled
        public string TipoFlusso {
            get;
            set;
        }
        @AuraEnabled
        public string NomeFile {
            get;
            set;
        }
         @AuraEnabled
        public string CodiceRichiesta {
            get;
            set;
        }
         @AuraEnabled
        public Map<String, String> categ {
            get;
            set;
        }
             @AuraEnabled
         public Map<String, String> tipofl {
            get;
            set;
        }
             @AuraEnabled
        public Map<String, String> tipric {
            get;
            set;
        }
         @AuraEnabled
        public list<string> province {
            get;
            set;
        }
        
    }
    
/**********************************************************************************************
    @author:         Bharath Sharma
    @date:           20 sep,2017
    @description:    This Aura Enabled method to get Tipo richesta values
**********************************************************************************************/
    @AuraEnabled
    public static Map<String, String> tiporichValues() {
        try {
            Schema.DescribeFieldResult fpickList = PT_Report__c.IdTipoReport__c.getDescribe();
            List < Schema.PicklistEntry > pickvalues = fpickList.getPicklistValues();
            Map < String, String > tiporichmap = new Map < String, String > ();
            for (PicklistEntry srt: pickvalues) {
                           system.debug('tiporichmap==>'+srt.getLabel());
           system.debug('tiporichmap==>'+ srt.getValue());     
                tiporichmap.put(srt.getLabel(), srt.getValue());
            }
            system.debug('tiporichmap==>'+tiporichmap);
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            
            return tiporichmap;
            
        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
     /**********************************************************************************************
    @author:         sushant yadav
    @date:           20 sep,2017
    @description:    This Aura Enabled method to get Categ Flusso values
**********************************************************************************************/
     @AuraEnabled
    public static  Map<String, String> categFlussovalues() {
        try {
            Schema.DescribeFieldResult fpickList = PT_Report__c.CategFlusso__c.getDescribe();
            List < Schema.PicklistEntry > pickvalues = fpickList.getPicklistValues();
            Map < String, String > tipocategflussomap = new Map < String, String > ();
            for (PicklistEntry srt: pickvalues) {
                tipocategflussomap.put(srt.getLabel(), srt.getValue());
            }
           // system.debug('tiporichmap==>'+tipocategflussomap);
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            return tipocategflussomap;
        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
    
    

 /**********************************************************************************************
    @author:         sushant yadav
    @date:           20 sep,2017
    @description:    This Aura Enabled method to get Tipo richesta values
**********************************************************************************************/    
     @AuraEnabled
    public static Map<String, String> tipoFlussovalues() {
        try {
            Schema.DescribeFieldResult fpickList = PT_Report__c.TipoFlusso__c.getDescribe();
            List < Schema.PicklistEntry > pickvalues = fpickList.getPicklistValues();
            Map < String, String > tipoflussomap = new Map < String, String > ();
            for (PicklistEntry srt: pickvalues) {

                tipoflussomap.put(srt.getLabel(), srt.getValue());
            }
            //system.debug('tiporichmap==>'+tipoflussomap);
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            return tipoflussomap;
        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
      /**********************************************************************************************
@author:         sushant yadav
@date:           19,sep 2017
@description:    This Aura Enabled method is used to intialize wrapperclass
**********************************************************************************************/
         @AuraEnabled
    public static List < String > provinciavalues() {
        try {
            Schema.DescribeFieldResult fpickList = PT_Richiesta__c.Provincia_pod__c.getDescribe();
            List < Schema.PicklistEntry > pickvalues = fpickList.getPicklistValues();
            Map < String, String > tipoflussomap = new Map < String, String > ();
            for (PicklistEntry srt: pickvalues) {
                tipoflussomap.put(srt.getLabel(), srt.getValue());
            }
            //system.debug('tiporichmap==>'+tipoflussomap);
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            return tipoflussomap.values();
        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
      /**********************************************************************************************
@author:         sushant yadav
@date:           19,sep 2017
@description:    This Aura Enabled method is used to intialize wrapperclass
**********************************************************************************************/
    @AuraEnabled
    public static wrapperClass getWrapper() {

        try {

            User usr = [select Id, username, ContactID, Contact.AccountId, Contact.Account.Name,
                        Contact.Account.IdTrader__c, IdDistributore__c from User
                        where Id =: UserInfo.getUserId() LIMIT: (Limits.getLimitQueryRows() - Limits.getQueryRows())
            ];
            string venditoreValue = usr.Contact.Account.name;
            string venditoreValueId = usr.Contact.Account.IdTrader__c;
            string venditoreFinal = venditoreValue+ ' - ' +venditoreValueId;
            wrapperClass wrapperObj = new wrapperClass();
            wrapperObj.Venditore = venditoreFinal;
            
           Map<String, String> cat = categFlussovalues();
            //system.debug('CAT'+CAT);
            wrapperObj.categ = cat;
            
            Map<String, String> tipoo = tipoFlussovalues();
            //system.debug('CAT'+tipoo);
            wrapperObj.tipofl = tipoo;
            
            Map<String, String> tiprich = tiporichValues();
            wrapperObj.tipric = tiprich;
            
             List < String > prov = provinciavalues();
                wrapperObj.province = prov;
            
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            return wrapperObj;
        } catch (Exception e) {
            logger.debugException(e);
        }
        logger.pop();
        return null;

    }
 /**********************************************************************************************
    @author:         sushant yadav
    @date:           20 sep,2017
    @description:    This Aura Enabled method to get Disp values
**********************************************************************************************/ 
     @AuraEnabled
        public static List<PT_Report__c> getDisp() {
        try {     
     
      User toReturnn = [SELECT  Id, username, ContactID, Contact.AccountId,account.Distributore__c, 
                      LastName FROM User WHERE Id = :UserInfo.getUserId() Limit 1];
      
       List<PT_Report__c> report = [SELECT Id,DescrDispaccEstesa__c,IdDispacc__c,AccountTrId__c,
                                    AccountTrId__r.name FROM PT_Report__c 
                                    where AccountTrId__c=: toReturnn.contact.AccountId LIMIT 500 ];
            
      // system.debug('-->>'+ Report +'--->'); 
         if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
            return report;
        }
             catch(Exception e){
        return null;    
    } 
       
  }
/**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method is used to query data based on input entered in Lightning component
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
    @AuraEnabled
    Public static WrapperListView getQueryall(String wrapperobj, String strObjectName, String strFields, Integer RecordNum) {

        try {
            wrapperClass wrapperObj1 = (wrapperClass) System.JSON.deserialize(wrapperobj, wrapperClass.class);

            string dataPubblicazioneda = JSON.Serialize(wrapperObj1.DataPubblicazioneda);
            dataPubblicazioneda = dataPubblicazioneda.substring(1, dataPubblicazioneda.length() - 1);
           
            
            string dataPubblicazioneA = JSON.Serialize(wrapperObj1.DataPubblicazioneA);
            dataPubblicazioneA = dataPubblicazioneA.substring(1, dataPubblicazioneA.length() - 1);
            
            string dataEstrazioneda = JSON.Serialize(wrapperObj1.DataEstrazioneda);
            dataEstrazioneda = dataEstrazioneda.substring(1, dataEstrazioneda.length() - 1);
            
            string dataEstrazioneA = JSON.Serialize(wrapperObj1.DataEstrazioneA);
            dataEstrazioneA = dataEstrazioneA.substring(1, dataEstrazioneA.length() - 1);

            
            //system.debug('dataDecorrenzaA==>' + DataPubblicazioneA);
            //system.debug('wrapperobj1==>'+wrapperObj1.TipoRich);
            //system.debug('strObjectName==>'+strObjectName);
            //system.debug('strFields==>'+strFields);
            //system.debug('RecordNum==>'+RecordNum);
            User usr = [select Id, username, ContactID, Contact.AccountID, Contact.Account.Name,
                        IdDistributore__c from User 
                        where Id =: UserInfo.getUserId() LIMIT: (Limits.getLimitQueryRows() - Limits.getQueryRows())];
            
            
            
            string userq = usr.IdDistributore__c;
            string venditoreValue = usr.Contact.Account.id;
            String queryAddress = ADDRESSQUERTY;
              
            if (usr.IdDistributore__c != null) {
                //queryAddress = ADDRESSQUERTY + ANDTXT + IDDTRBTR + QUOTE + userq + QUOTE;
            }
            if (wrapperObj1.TipoRich != null && wrapperObj1.TipoRich.length() > 0) {
                queryAddress = queryAddress + ANDTXT + TIPO + QUOTE + wrapperObj1.TipoRich + QUOTE;
               //system.debug('queryAddress1 ==>' + queryAddress);
            }
            if (wrapperObj1.Venditore != null && wrapperObj1.Venditore.length() > 0) {
               // queryAddress = queryAddress + ANDTXT + TIPO + QUOTE + venditoreValue + QUOTE;
                              // system.debug('queryAddress2 ==>' + queryAddress);
            }
             if (wrapperObj1.Dispacciamento != null && wrapperObj1.Dispacciamento.length() > 0) {
                queryAddress = queryAddress + ANDTXT + DISP + QUOTE + wrapperObj1.Dispacciamento + QUOTE;
                               //system.debug('queryAddress3==>' + queryAddress);
            }
         /*      if (wrapperObj1.Provincia != null && wrapperObj1.Provincia.length() > 0) {
                 //List<PT_Report__c> projects = new List<PT_Report__c>();
                 //queryAddress = queryAddress + ANDTXT + PROV + QUOTE + wrapperObj1.Provincia + QUOTE;
                 system.debug('queryAddress3==>' + queryAddress);
            }
          if (wrapperObj1.Provincia != null && wrapperObj1.Provincia.length() > 0) {
               
                 for(PT_Report__c proj :[Select Id,Name,Note_Report__c,DataPub__c,DataEstraz__c,NomeFileReport__c From PT_Report__c where id!=null]){
                      if(proj.Note_Report__c.contains(wrapperObj1.Provincia)) {
                         projects.add(proj);
                        }
                     
                 }
            }*/

             if (wrapperObj1.CategoriaFlusso != null && wrapperObj1.CategoriaFlusso.length() > 0) {
                queryAddress = queryAddress + ANDTXT + CATE + QUOTE + wrapperObj1.CategoriaFlusso + QUOTE;
                              
            }
             if (wrapperObj1.TipoFlusso != null && wrapperObj1.TipoFlusso.length() > 0) {
                queryAddress = queryAddress + ANDTXT + TIPOF + QUOTE + wrapperObj1.TipoFlusso + QUOTE;
                              
            }
             if (wrapperObj1.NomeFile != null && wrapperObj1.NomeFile.length() > 0) {
                queryAddress = queryAddress + ANDTXT + NOME + QUOTE + wrapperObj1.NomeFile + QUOTE;
                          
            }
             if (wrapperObj1.CodiceRichiesta != null && wrapperObj1.CodiceRichiesta.length() > 0) {
                queryAddress = queryAddress + ANDTXT + CODI + QUOTE + wrapperObj1.CodiceRichiesta + QUOTE;
                              
            }
      /*      if (wrapperObj1.ContrattodiDisp != null && wrapperObj1.ContrattodiDisp.length() > 0) {
                queryAddress = queryAddress + ANDTXT + DESCEST + QUOTE + wrapperObj1.ContrattodiDisp + QUOTE;
                               system.debug('queryAddress3==>' + queryAddress);
            } */
            if (wrapperObj1.DataPubblicazioneda != null) {
                queryAddress = queryAddress + ANDTXT + DATAPUB + GRT + dataPubblicazioneda;
                               //system.debug('queryAddress4 ==>' + queryAddress);
            }

            if (wrapperObj1.DataPubblicazioneA != null) {
                queryAddress = queryAddress + ANDTXT + DATAPUB + LST + dataPubblicazioneA;
                 //system.debug('queryAddress5 ==>' + queryAddress);
            }
             if (wrapperObj1.DataEstrazioneda != null) {
                queryAddress = queryAddress + ANDTXT + DATAESTC + GRT + dataEstrazioneda;
                               //system.debug('queryAddress4 ==>' + queryAddress);
            }

            if (wrapperObj1.DataEstrazioneA != null) {
                queryAddress = queryAddress + ANDTXT + DATAESTC + LST + dataEstrazioneA;
                 //system.debug('queryAddress5 ==>' + queryAddress);
            }
            
            queryAddress = queryAddress+ ORDRBY + SPACE + Name+ SPACE  + DESN + SPACE +LIMT + RecordNum;
            //system.debug('queryAddressFINAL ==>' + queryAddress);
            
            List < String > outputstrlist = new List < String > ();
            outputstrlist = strFields.split(COMMA);
            //system.debug('outputstrlist ==>' + outputstrlist);
            
            String stringFields = String.join(outputstrlist, COMMA);
            List < SObject > objectQuery = Database.query(queryAddress);
            //system.debug('objectQuery ==>' + objectQuery);
            
            Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType objSchema = schemaMap.get(strObjectName);
            Map < String, Schema.SObjectField > fieldMap = ObjSchema.getDescribe().fields.getMap();
            List < String > apiList = new List < String > ();
            List < String > allLabels = new List < String > ();
            for (String key: outputstrlist) {
                if (!FIELD_ID.equalsIgnoreCase(key) && fieldMap.containsKey(key)) {
                    allLabels.add(fieldMap.get(key).getDescribe().getLabel());
                    apiList.add(key);
                    //system.debug('fieldMap' + fieldMap);
                    //system.debug('allLabels==>' + allLabels);
                    //system.debug('apiList==>' + apiList);
                }

            }
           
         /*   if(projects!= null){
                
                system.debug('wrapObj=>'+wrapObject);
            } 
            else{*/
         //system.debug('wrapperObj1.Provincia'+wrapperObj1.Provincia);
          WrapperListView wrapObject;       
         if (wrapperObj1.Provincia != null && wrapperObj1.Provincia.length() > 0) {
               List<PT_Report__c> projects = new List<PT_Report__c>();
                 for(PT_Report__c proj :[Select Id,Name,Note_Report__c,DataPub__c,DataEstraz__c,
                                         NomeFileReport__c From PT_Report__c where id!=null]){
                      if(proj.Note_Report__c.contains(wrapperObj1.Provincia)) {
                         projects.add(proj);
                            wrapObject = new WrapperListView(allLabels, APIList, projects);
                            return wrapObject; 
                            //system.debug('wrapObj=>'+wrapObject);
                        }
      
                 }
         }else{
              WrapperListView wrapObj = new WrapperListView(allLabels, APIList, objectQuery);
         //system.debug('wrapObj=>'+wrapObj);
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
             return wrapObj;
         }
         //SOAP WS Call
          //  wwwExampleOrgRecuperofile.BODY_element getURL= getResponse(queryAddress);
        }
        catch (Exception e) {
            logger.debugException(e);
        }
         logger.pop();
        return null;
    }
    /**********************************************************************************************
            @author:         Bharath Sharma
            @date:           19 sep,2017
            @description:    This Aura Enabled method is used to call Web Service
            @reference:    420_Trader_Detail_Design_Wave3 (Section 8.8)
        **********************************************************************************************/
     public static string getResponse(string RepId) {
     PT_Report__c repRec=[Select id,Name,NomeFileReport__c From PT_Report__c where id=:RepId];
     string NomeFielReport=repRec.NomeFileReport__c;
       wwwTibcoComRecuperofile.RecuperoFileSOAP  getDwld= new wwwTibcoComRecuperofile.RecuperoFileSOAP();
        wwwExampleOrgRecuperofile.BODY_element wrapObj = new wwwExampleOrgRecuperofile.BODY_element();
        //keyfile assignment
        wrapObj.keyfile= NomeFielReport;
       //Calling WS
        wwwExampleOrgRecuperofile.BODY_element getURL= getDwld.RecuperoFile(wrapObj);
        return NomeFielReport;
    }
       /**********************************************************************************************
    @author:         Bharath Sharma
    @date:           sep 19,2017
    @description:    This Aura Enabled method is used to send data back to Lightning component
    @reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
    **********************************************************************************************/
    Public with sharing class WrapperListView {
        @AuraEnabled
        public List < String > labelList {
            get;
            set;
        }
        @AuraEnabled
        public List < SObject > dataList {
            get;
            set;
        }
        @AuraEnabled
        public List < String > apiList {
            get;
            set;
        }
        

        /**********************************************************************************************
            @author:         Bharath Sharma
            @date:           19 sep,2017
            @description:    This Aura Enabled method is used to send data back to Lightning component
            @reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
        **********************************************************************************************/
        public wrapperListView(List < String > LabelLst, List < String > APILst, List < Sobject > DataLst) {
            labelList = LabelLst;
            dataList = DataLst;
            apiList = APILst;
            
        }
    }
    //D-360 @AKANSHA
    /**********************************************************************************************
@author:        Karan Kansagra
@date:          24 Jun,2017
@Name:          WrapperPicklist
@description:   This Aura Enabled method is used to get all the Picklist values 
across RFI component fields before request can be inserted.
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.2 to 2.8.1.9)
**********************************************************************************************/
    public static PT_PicklistAuraComponent getRichiestaPicklistValues()
    {
        // List<PT_PicklistAuraComponent.SelectOptionWrapper> recordTypeLst = new List<PT_PicklistAuraComponent.SelectOptionWrapper>();
        try{
            PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
            obj.pickListValues.put(CATEFLUSSO,PT_Utility.getPickListValues(PT_REPORT,CATEFLUSSO)); 
            obj.pickListValues.put(TIPOFLUSSO1,PT_Utility.getPickListValues(PT_REPORT,TIPOFLUSSO1));
            obj.pickListValues.put(TIPO1,PT_Utility.getPickListValues(PT_REPORT,TIPO1));
            
                        if(test.isRunningTest()){ 
                    throw new PT_ManageException (EXCEMES); 
                }
            return obj; 
        }
        catch(Exception e){
            // system.debug('Error Message'+ e.getMessage());  
            return null;
        } 
    }
    
    @AuraEnabled 
    public static WrapperPicklist getAllRichiestaPicklistValuesCntr(){
        
        Logger.push(GETALLRICHPICKVALS,PTREQUESTRECORDCREATE);
        try{
            
            Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
            Schema.SObjectType pType = objGlobalMap.get(PT_REPORT);
            Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
                        PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
            
            obj.pickListValues.put(CATEFLUSSO,PT_Utility.getPickListValues(PT_REPORT,CATEFLUSSO,objFieldMap));  
            obj.pickListValues.put(TIPOFLUSSO1,PT_Utility.getPickListValues(PT_REPORT,TIPOFLUSSO1,objFieldMap));
            obj.pickListValues.put(TIPO1,PT_Utility.getPickListValues(PT_REPORT,TIPO1,objFieldMap)); 
            
            List<Richieste_Picklist_Values__c> reqAllPicklist = [SELECT Id, Name, Record_Type__c, Picklist_Api_Name__c, Picklist_Value__c, Controlling_Field_API__c ,Controlling_Field_Value__c, Controlling_Field_2_API__c, Controlling_Field_2_Value__c 
                                                                 FROM Richieste_Picklist_Values__c 
                                                                 where Picklist_Api_Name__c =:'CategFlusso__c' OR Picklist_Api_Name__c =:'IdTipoReport__c' OR Picklist_Api_Name__c =:'TipoFlusso__c' order by Picklist_Api_Name__c];
            
            Map<String,List<Richieste_Picklist_Values__c>> reqPicklistMap = new Map<String,List<Richieste_Picklist_Values__c>>();
           
            List<Richieste_Picklist_Values__c> reqPicklistList;
            for(Richieste_Picklist_Values__c reqPicklist:reqAllPicklist){
                if(reqPicklistMap.containsKey(reqPicklist.Picklist_Api_Name__c)){
                    reqPicklistList = new List<Richieste_Picklist_Values__c>();
                    reqPicklistList = reqPicklistMap.get(reqPicklist.Picklist_Api_Name__c);
                    reqPicklistList.add(reqPicklist);
                    reqPicklistMap.put(reqPicklist.Picklist_Api_Name__c,reqPicklistList);
                }
                else{
                     reqPicklistList = new List<Richieste_Picklist_Values__c>();
                    reqPicklistList.add(reqPicklist);
                    reqPicklistMap.put(reqPicklist.Picklist_Api_Name__c,reqPicklistList);
                }
            }
            
            //WrapperPicklist wrapperlist= new WrapperPicklist(obj,reqAllPicklist,reqPicklistMap,reqrecordTypeDetail,rfiComponentSettings,micrfiComponentSettings);
            WrapperPicklist wrapperlist= new WrapperPicklist(obj,reqPicklistMap);
            system.debug('Map End');
            if(test.isRunningTest()){ 
                    throw new PT_ManageException (EXCEMES); 
                }
            return wrapperlist;
            
        }
        catch(Exception e){
            return null;
        }
        logger.pop();
        return null;
    }
    /**********************************************************************************************
@author:        Karan Kansagra
@date:          24 Jun,2017
@Name:          WrapperPicklist
@description:   This wrapper class is used to set variables related to picklist fetching.
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.2 to 2.8.1.9)
**********************************************************************************************/
    Public with sharing class WrapperPicklist {
        @AuraEnabled
        public PT_PicklistAuraComponent picklistObjectMap{get;set;}

        @AuraEnabled
        public Map<String,List<Richieste_Picklist_Values__c>> reqPicklistCustomSettingMap{ get;set;}
       
        
        /*********************************************************************
@date:          24 Jun,2017
@Name:   WrapperPicklist
@description:   This is wrapper constructor for the class WrapperPicklist
**********************************************************************/
        public WrapperPicklist(PT_PicklistAuraComponent pickVals, 
                               Map<String,List<Richieste_Picklist_Values__c>> picklistCustomSettingMap) { 
                                   picklistObjectMap=pickVals;
                                   reqPicklistCustomSettingMap=picklistCustomSettingMap;
                                   system.debug(picklistCustomSettingMap);
                               }
    }

}