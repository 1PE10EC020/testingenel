public class customRelatedList_Controller {	
	public String blockTitle {get;set;} // title used into pageblockSection - passed in input by a parameter
	public String  baseObject {get;set;} // base table to query in order to retrive data
	public String whereCondition {get;set;} // optional where condition to filter data
	ApexPages.StandardSetController ssc; // standard set controller - used to manage the component's pagination
	public String  namefields {get;set {  // list of fields to display into the table, the list is a comma-separated string
											namefields = value;
											system.debug('namefields is '+namefields);
											listOfFields = namefields.split(',',-1);
										}
		
	} 
	public list<String> listOfFields {get;set;} // list of string to display into the VF page

// boolean used to display an error message if the table was empty
	public boolean displayTable {	get { 
											try{
											list<Sobject> elemToDisplay = this.elementToDisplay;
											if (elemToDisplay != null && !elemToDisplay.isEmpty() )
												return true;
											else
												return false;
											}
											catch(exception e){
												system.debug(logginglevel.error,'Exception in customRelatedList_Controller - displayTable : '+e.getMessage());
												return false; // if there is an error, display a error message 
											}
						
										}
									set;
								}
	 //execute the query and return the result	
	public list<SObject> elementToDisplay{ 
											get{
													try{
														if ( this.ssc == null){
															String soql = 'Select '+namefields+' FROM '+baseObject;
															if ( String.isNotblank(whereCondition))
															soql += whereCondition;
															system.debug(logginglevel.debug,'customRelatedList_Controller - soql query is '+soql);
															this.ssc = new ApexPages.StandardSetController(Database.query (soql));
															this.ssc.setPageSize(5);
														}
													return	this.ssc.getRecords();
													 
													}
													catch(Exception e){
														system.debug(logginglevel.error,'Exception in customRelatedList_Controller - elementToDisplay : '+e.getMessage());
														// manage the exception retunring a null value
														return null;
													}
												}
											set;
										}
	 // property used to display the "next" button 
	 public boolean hasNext {get  
								{
									if (this.ssc != null)
										return ssc.getHasNext();
									return false;
								} 
							private set;
							}
	 // property used to display the "previous" button 
	public boolean hasPrevious {get 
									{
										if (this.ssc != null)
											return ssc.getHasPrevious();
										return false;
									} 
								private set;
							    }
	// uset to display current page number
	public String currentpageNumber {get { 
											if (this.ssc != null) {
												String retVal =  this.ssc.getPageNumber()+'/'+math.ceil(this.ssc.getResultSize()/5.0);
												return retval;
											}
											return '1/1'; // return a default value
										 }
									 set;
									 } 
	// propertu used to show the total page
	public integer totalPageNumber {get { 
											if (this.ssc != null ) 
												return (integer) this.ssc.getResultSize()/5;
											return 1; // return default value
										}
									set;
									}  
	  
	// Method used to "next" button  
	public void Next(){ 
		if(ssc != null)
			ssc.next();
	}  
	
	// Method used to "previous" button  
	public void previous (){ 
		if(ssc != null)
			ssc.previous();
	}  
	
   
}