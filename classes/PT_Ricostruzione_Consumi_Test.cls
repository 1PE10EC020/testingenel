/**********************************************************************************************
@author:       	Martina Testa
@date:         	06 Oct,2017
@Class Name:  	PT_Ricostruzione_Consumi_Test
@description:  	Test class for PT_Ricostruzione_Consumi_cntrl.

**********************************************************************************************/

@isTest(SeeAllData = true)
public class PT_Ricostruzione_Consumi_Test {
    private static final String ALIAS = 'unEx14';
    
     static testMethod void pt_Ric_Cons_Test() {
         Test.startTest();

                PT_Richiesta__c request = new PT_Richiesta__c();
         
               Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Richiesta__c; 
               Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
               Id n02_id = richiestaRecordTypeInfo .get('PT_N02').getRecordTypeId();
               request.RecordTypeId= n02_id;
         
               
                request.PotImp__c='1.5';
                database.insert(request);
                PT_Ricostruzione_Consumi_cntrl.recordId=request.name;
         
                PT_Ricostruzione_consumi__c recordConsumi= new PT_Ricostruzione_consumi__c();
                recordConsumi.protocolloRic__c=request.name;
                recordConsumi.COPPIA_CLIENTE_TRADER__c='Rossi-Acea';    
                recordConsumi.ANNO_RIFERIMENTO_RIC__c='2017';
                recordConsumi.MESE_RIFERIMENTO__c='01';
                database.insert(recordConsumi);
         
                PT_Ricostruzione_consumi__c recordConsumi1= new PT_Ricostruzione_consumi__c();
                recordConsumi1.protocolloRic__c=request.name;
                recordConsumi1.COPPIA_CLIENTE_TRADER__c='Rossi-Acea';    
                recordConsumi1.ANNO_RIFERIMENTO_RIC__c='2017';
                recordConsumi1.MESE_RIFERIMENTO__c='02';
                database.insert(recordConsumi1);
         
               PT_Ricostruzione_consumi__c recordConsumi2= new PT_Ricostruzione_consumi__c();
                recordConsumi2.protocolloRic__c=request.name;
                recordConsumi2.COPPIA_CLIENTE_TRADER__c='Rossi-Acea';    
                recordConsumi2.ANNO_RIFERIMENTO_RIC__c='2016';
                recordConsumi2.MESE_RIFERIMENTO__c='02';
                database.insert(recordConsumi2);
         
                   
                PT_Ricostruzione_Consumi_cntrl.PaginationWrapper wrap=new  PT_Ricostruzione_Consumi_cntrl.PaginationWrapper();
          
                wrap=PT_Ricostruzione_Consumi_cntrl.getPicklistTrCl(request.name);
                wrap=PT_Ricostruzione_Consumi_cntrl.getListRicConsumi(request.name,  recordConsumi.COPPIA_CLIENTE_TRADER__c);
         
             
        Test.stopTest();
       
        
     }

}