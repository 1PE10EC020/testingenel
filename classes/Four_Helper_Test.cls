@isTest
public class Four_Helper_Test{
    
    @testSetup
	private static void setup() {
		Endpoints__c newend = new Endpoints__c();
		newend.name = 'FOUR_ARRICCHIMENTO';
		insert newEnd;
		
		UnitaDiCompetenza__c unit = new UnitaDiCompetenza__c();
		unit.CodiceDTZOUR__c = 'aaaaa';
		unit.name = 'ROMA ESTERNA-RIETI';
		insert unit;
		UnitaDiCompetenza__c unit2 = new UnitaDiCompetenza__c();
		unit2.CodiceDTZOUR__c = 'bbbbb';
		unit2.name = 'Lazio Abruzzo e Molise';
		insert unit2;
	}
	
    @isTest
    static void Test_BT_CASE_CONNESSIONE_1 (){
        // case RT = Connessione
        // POD NON presente
        //livello tensione BT
        
        Case newCase = GenerateCase(Constants.CASE_CONNESSIONE);
        insert newCase;

        Test.startTest();
    
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
        system.debug('responseCase is '+responseCase);
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_DA_CONNETTERE, responseCase.livello2__C);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.livello3__C);
        system.assertEquals(true, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.LivelloTensioneDiFornitura__c);
        
        
        
        
      
        
    }
    
     @isTest
    static void Test_Header (){
     test.startTest();
    	 EnelServiceHeaderTypes.TechInfo_element t = new EnelServiceHeaderTypes.TechInfo_element();
    	EnelServiceHeaderTypes.AppInfo_element a = new EnelServiceHeaderTypes.AppInfo_element();
    	EnelServiceHeaderTypes.EnelWSMessageSchema_element b = new EnelServiceHeaderTypes.EnelWSMessageSchema_element();
     test.stopTest();
    
    }
    
    
    @isTest
    static void Test_BT_CASE_CONNESSIONE_2 (){
     	Case newCase = GenerateCase(Constants.CASE_CONNESSIONE);
     	insert newCase;
    	test.startTest();
        // case RT = Connessione
        // POD presente
        //livello tensione BT
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();    
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];  
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_GIA_CONNESSO, responseCase.livello2__C);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.livello3__C);
        system.assertEquals(true, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.LivelloTensioneDiFornitura__c);
    }
    
    @isTest
    static void Test_BT_CASE_CONNESSIONE_3 (){
    	 Case newCase = GenerateCase(Constants.CASE_CONNESSIONE);
        insert newCase;
    	 // case RT = Connessione
        // POD presente
        //livello tensione MT
        Test.startTest();
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_MT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();    
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];    
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_GIA_CONNESSO, responseCase.livello2__C);
        system.assertEquals(Constants.LIVELLO_3_ATMT, responseCase.livello3__C);
        system.assertEquals(true, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_MT, responseCase.LivelloTensioneDiFornitura__c);
    }
     
    @isTest
    static void Test_BT_CASE_CONNESSIONE_4(){
     Case newCase = GenerateCase(Constants.CASE_CONNESSIONE);
        insert newCase;
        // case RT = Connessione
        // POD presente
        //livello tensione AT
        test.starttest();
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_AT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_GIA_CONNESSO, responseCase.livello2__C);
        system.assertEquals(Constants.LIVELLO_3_ATMT, responseCase.livello3__C);
        system.assertEquals(true, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_AT, responseCase.LivelloTensioneDiFornitura__c);
    }

    @isTest
    static void Test_BT_CASE_Misura_1 (){
        // case RT = Misura
        // POD NON presente
        //livello tensione BT
        
        Case newCase = GenerateCase(Constants.CASE_MISURA);
        insert newCase;
    
    	Test.startTest();
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
       
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_CONNESSO_BT, responseCase.livello2__C);
        system.assertEquals(Constants.WS_FASCIA_ORARIA, responseCase.livello3__C);
        system.assertEquals(true, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.LivelloTensioneDiFornitura__c);
    
    
        
    }

	 @isTest
    static void Test_BT_CASE_Misura_2 (){
    
        Case newCase = GenerateCase(Constants.CASE_MISURA);
        insert newCase;
    
    // case RT = Misura
        // POD  presente
        //livello tensione AT
        // fascia oraria non specificagta
        Test.startTest();
         FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_AT,'','bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_CONNESSO_ATMT, responseCase.livello2__C);
        system.assertEquals(Constants.WS_FASCIA_ORARIA, responseCase.livello3__C);
        system.assertEquals(false, responseCase.Oraria__c);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_AT, responseCase.LivelloTensioneDiFornitura__c);
    
    }


    @isTest
    static void Test_BT_CASE_Trasporto_1 (){
        
        Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
        insert newCase;
        test.startTest();
    
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();        
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
                
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_CONNESSO_BT, responseCase.livello2__C);
        system.assertEquals('MT/AT', responseCase.livello3__C);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_BT, responseCase.LivelloTensioneDiFornitura__c);
    	
        
      
    }
    
     @isTest
    static void Test_BT_CASE_Trasporto_2 (){
    	Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
        insert newCase;
        test.startTest();
    	
        FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_MT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
        
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_CONNESSO_MT, responseCase.livello2__C);
        system.assertEquals('MT/AT', responseCase.livello3__C);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_MT, responseCase.LivelloTensioneDiFornitura__c);
    }
    
    @isTest
    static void Test_BT_CASE_Trasporto_3 (){
    	Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
        insert newCase;
        test.startTest();
      	
      	FOURTypes.DashboardArrReplay_element expetedResponse = generateFakeResponse ('S',Constants.LIVELLO_TENSIONE_FORNTITURA_AT,Constants.WS_FASCIA_ORARIA,'bbbbb','aaaaa', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
        test.stopTest();
        Case responseCase = [select id, livello1__C,livello2__C,livello3__C,Oraria__c,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,LivelloTensioneDiFornitura__c from case where id = : newcase.id];
                
        system.assertEquals(Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE, responseCase.livello1__C);
        system.assertEquals(Constants.LIVELLO_CONNESSO_AT, responseCase.livello2__C);
        system.assertEquals('MT/AT', responseCase.livello3__C);
        system.assertEquals('Gennaro', responseCase.NomeTitolarePOD__c);
        system.assertEquals('Esposito', responseCase.CognomeTitolarePOD__c);
        system.assertEquals('07643520567', responseCase.PartitaIVATitolarePOD__c);
        system.assertEquals('ROMA ESTERNA-RIETI', responseCase.UnitaDiCompetenza__c);
        system.assertEquals('Lazio Abruzzo e Molise', responseCase.DirezioneDTR__c);
        system.assertEquals(Constants.LIVELLO_TENSIONE_FORNTITURA_AT, responseCase.LivelloTensioneDiFornitura__c);
    
    }
    
    @isTest
    static void Test_NULL (){
        
        Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
        
    
        FOURTypes.DashboardArrReplay_element expetedResponse;// = generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'Lazio Abruzzo e Molise','ROMA ESTERNA-RIETI', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
    }   
    
        @isTest
    static void Test_Blank (){
    Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
        
    
        FOURTypes.DashboardArrReplay_element expetedResponse  = generateFakeResponse ('','','','','', '','','','','','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        Four_Helper.Arricchisci_Case(newCase.id);
    
    }
    
    @isTest
    static void Test_generic ()
    {
     Case newCase = GenerateCase(Constants.CASE_TRASPORTO);
     Test.StartTest();
     Four_Helper controller = new Four_Helper(newCase);
     Four_Helper.FourService_Arricchimento_Response res = new Four_Helper.FourService_Arricchimento_Response();
     FOURTypes.DashboardArrReplay_element inRes = new FOURTypes.DashboardArrReplay_element();
     Four_Helper.FourService_Arricchimento_Response res2 = new Four_Helper.FourService_Arricchimento_Response(inRes,true);
     Test.StopTest();
    }
        
    /*
    * @author : Claudio Quaranta
    * @Date : 2015-06-23
    * @Description : generate new case for test purpose
    */ 
    public static case GenerateCase(string RT_DEVELOPER_NAME) {
        case c = new case();
            c.recordTypeId = [select id from recordtype where developername = :RT_DEVELOPER_NAME ].id ;
            c.TipologiaCase__c ='Da Definire';
            c.origin = 'Email';
            c.status='In carico al II Livello';
            c.DescrizioneStato__c='In Lavorazione';
            c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
            c.pod__C = 'testPod';
            c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
            c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
            c.LivelloTensioneDiFornitura__c ='BT';
            c.livello1__C ='In prelievo';
            c.livello2__C = 'Già Connesso';
            c.livello3__C ='MT/AT' ;
            return c;
    }
    /*
    * @author : Claudio Quaranta
    * @Date : 2015-06-23
    * @Description : generate new fake WS response
    */ 
    public static FOURTypes.DashboardArrReplay_element generateFakeResponse(String PresenzaPod,String LivelloTensioneFornitura,String TrattamentoOrarioPdm,String DirezioneDtr,String UnitaCompetenza,String CodiceFiscaleTitolare,String PartitaIvaTitolare,String ProduttoreSiNo,String Nome,String Cognome,String RagioneSociale){
        FOURTypes.DashboardArrReplay_element newElement = new FOURTypes.DashboardArrReplay_element ();
        newElement.PresenzaPod = PresenzaPod;
        newElement.LivelloTensioneFornitura =LivelloTensioneFornitura ;
        newElement.TrattamentoOrarioPdm =TrattamentoOrarioPdm ;
        newElement.DirezioneDtr = DirezioneDtr;
        newElement.UnitaCompetenza =UnitaCompetenza ;
        newElement.CodiceFiscaleTitolare = CodiceFiscaleTitolare;
        newElement.PartitaIvaTitolare = PartitaIvaTitolare;
        newElement.ProduttoreSiNo = ProduttoreSiNo;
        newElement.Nome = Nome;
        newElement.Cognome =Cognome ;
        newElement.RagioneSociale = RagioneSociale;
        return newElement;
    }
    

}