/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	24 Jun,2017
@description:  	This controller is used for PT_Address_RFI Lightning component to drive address specific logic
@reference:		420_Trader_Detail_Design_Wave0 (Section#2.8.1.9)
**********************************************************************************************/
public without sharing class PT_AddressRFI_Controller {
    
    PRIVATE STATIC FINAL STRING NONE='--None--';
    PRIVATE STATIC FINAL STRING COMUNE='Comune_pod__c';
    PRIVATE STATIC FINAL STRING CODISTAT='CodIstat_pod__c';
    PRIVATE STATIC FINAL STRING CAP='Cap_pod__c';    
    PRIVATE STATIC FINAL STRING YES='S';
	PRIVATE STATIC FINAL STRING QUOTE='\'';  
    PRIVATE STATIC FINAL STRING ADDRESSQUERTY='select CodPPPAAA__c, CodVVV__c, Toponimo__c, Comune__c, CodCap__c, DescrVia1__c' 
        									+ ' from PT_Toponimo__c' + ' where Comune__c=';
    PRIVATE STATIC FINAL STRING QUERYLIMIT=' LIMIT 100';  
    PRIVATE STATIC FINAL STRING ANDTXT=' and ';
    PRIVATE STATIC FINAL STRING CODCAP='CodCap__c=';
    PRIVATE STATIC FINAL STRING DESCVIA='DescrVia1__c=';
    PRIVATE STATIC FINAL STRING CODISTA='CodIstat__c=';
    PRIVATE STATIC FINAL STRING RECNAME='NAME';
    PRIVATE STATIC FINAL STRING COMUNEADD='COMUNE__c';
    PRIVATE STATIC FINAL STRING COD_ISTA='COD_ISTAT__c';
    PRIVATE STATIC FINAL STRING PT_ADDRESS_RFI='PT_AddressRFI_Controller';
    PRIVATE STATIC FINAL STRING GET_ADDRESS='retriveAddress';
    PRIVATE STATIC FINAL STRING GET_COMUMNE='getComumneCodIstatPicklistValues';
    PRIVATE STATIC FINAL STRING SET_CAPCOD='setCAPCodIstatFields';
    PRIVATE STATIC FINAL STRING SET_COMUNECOD='setComuneCodIstatFields';
    PRIVATE STATIC FINAL STRING SET_CAP='setCAPFieldValue';
    PRIVATE STATIC FINAL STRING BLANK_STR= '';
    
/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	24 Jun,2017
@Method Name:  	retriveAddress
@description:  	This Aura Enabled method is used to fecth matching address records from PT_Toponimo__C address master object
				based on User input provided in Comune, CAP, VIA & Codistat fields on Address RFI component.
@reference:		420_PT_Logiche_pagine_Inserimento (Common/VERIND_1)
**********************************************************************************************/
    @AuraEnabled
    public static List<PT_Toponimo__c> retriveAddress(String comune, String cap, String via, String codistat){
        Logger.push(GET_ADDRESS,PT_ADDRESS_RFI);
        List<PT_Toponimo__c> addressList = new List<PT_Toponimo__c>();
        try{
            String queryAddress = ADDRESSQUERTY + QUOTE + comune + QUOTE + ANDTXT + CODCAP + QUOTE + cap + QUOTE;
            
            if(via!=null && via.length()>0){
                queryAddress = queryAddress + ANDTXT + DESCVIA + QUOTE + via + QUOTE;
            }
            if(codistat!=null && codistat.length()>0){
                queryAddress = queryAddress + ANDTXT + CODISTA + QUOTE + codistat + QUOTE;
            }
            queryAddress = queryAddress + QUERYLIMIT;
            addressList = Database.query(queryAddress);
            
        }
        catch(Exception e){
            logger.debugException(e);
        }
		logger.pop();
        return addressList;    
        
    }

/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	24 Jun,2017
@Method Name:  	getComumneCodIstatPicklistValues
@description:  	This Aura Enabled method is used to retrive Comune & CodIstat picklist field values based on the selected Province
			   	Retrived values are rendered as picklist on Address RFI lightning component.
@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
**********************************************************************************************/    
    @AuraEnabled
    public static PT_PicklistAuraComponent getComumneCodIstatPicklistValues(String provinciaPod)
    {
        Logger.push(GET_COMUMNE,PT_ADDRESS_RFI);
        PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();            
        try{
            List<String> retList = new List<String>();
            
            //retList.add(new PT_PicklistAuraComponent.SelectOptionWrapper(NONE,BLANK_STR));
            for( AggregateResult comuneRecord : [SELECT COMUNE__c FROM ANAG_CAP_ISTAT__c 
                                                            WHERE SIGLA_PROVINCIA__c=:provinciaPod AND FLG_COMPETENZA_ENEL__c=:YES 
                                                 			group by COMUNE__c LIMIT 5000])
            {
                   retList.add((string)comuneRecord.get(COMUNEADD));
            }
            
            obj.pickListValues.put(COMUNE,retList); 
            
            retList = new List<String>();
            //retList.add(new PT_PicklistAuraComponent.SelectOptionWrapper(NONE,BLANK_STR));
            for( AggregateResult codIstatRecord : [SELECT COD_ISTAT__c FROM ANAG_CAP_ISTAT__c 
                                                              WHERE SIGLA_PROVINCIA__c=:provinciaPod AND FLG_COMPETENZA_ENEL__c=:YES 
                                                              group by COD_ISTAT__c LIMIT 5000])
            {
                   retList.add((string)codIstatRecord.get(COD_ISTA));
            }
            obj.pickListValues.put(CODISTAT,retList); 
            
            
        }
        catch(Exception e){
            logger.debugException(e);
        }
        logger.pop();
		return obj;        
    }
    
/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	24 Jun,2017
@Method Name:  	setCAPCodIstatFields
@description:  	This Aura Enabled method is used to retrive a record from ANAG_CAP_ISTAT__C object based on the selected ComunePod field value.
@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
**********************************************************************************************/      
	@AuraEnabled
    public static ANAG_CAP_ISTAT__c setCAPCodIstatFields(String comunePodVal)
    {
        Logger.push(SET_CAPCOD,PT_ADDRESS_RFI);
        ANAG_CAP_ISTAT__c capCodIstat=new ANAG_CAP_ISTAT__c();
        try{
        	capCodIstat = [SELECT COD_ISTAT__c FROM ANAG_CAP_ISTAT__c WHERE COMUNE__c =:comunePodVal 
                                             AND FLG_COMPETENZA_ENEL__c=:YES limit 1];
        	
        }
        catch(Exception e){
            logger.debugException(e);
        }
        logger.pop();
        return capCodIstat;
    }

/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	24 Jun,2017
@Method Name:  	setComuneCodIstatFields
@description:  	This Aura Enabled method is used to retrive a record from ANAG_CAP_ISTAT__C object based on the selected Cod Istat field value.
@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
**********************************************************************************************/      
    @AuraEnabled
    public static ANAG_CAP_ISTAT__c setComuneCodIstatFields(String codIstatPodVal)
    {
        Logger.push(SET_COMUNECOD,PT_ADDRESS_RFI);
        ANAG_CAP_ISTAT__c capCodIstat = new ANAG_CAP_ISTAT__c();
        try{
        	capCodIstat = [SELECT COMUNE__c  FROM ANAG_CAP_ISTAT__c WHERE COD_ISTAT__c =:codIstatPodVal 
                                             AND FLG_COMPETENZA_ENEL__c=:YES limit 1];
        }
        catch(Exception e){
            logger.debugException(e);
        }
        logger.pop();
        return capCodIstat;
    }
   
/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	9th July,2017
@Method Name:  	setCAPFieldValue
@description:  	This Aura Enabled method is used to retrive list of distinct CAP values from ANAG_CAP_ISTAT__C object based on the selected Comune or Cod Istat.
@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
**********************************************************************************************/      
    @AuraEnabled
    public static PT_PicklistAuraComponent setCAPFieldValue(String comunePodVal, String codIstatPodVal)
    {
        Logger.push(SET_CAP,PT_ADDRESS_RFI);
        PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        try{
            List<String> retList = new List<String>();
            List<AggregateResult> listOfDistinctCAP=new List<AggregateResult>();
            if(comunePodVal!=null & !string.isBlank(comunePodVal)){
            	listOfDistinctCAP = [SELECT NAME FROM ANAG_CAP_ISTAT__c WHERE COMUNE__c  =:comunePodVal 
                                                       AND FLG_COMPETENZA_ENEL__c=:YES GROUP BY Name limit 5000];
            }
            else if(codIstatPodVal!=null & !string.isBlank(codIstatPodVal)){
            	listOfDistinctCAP = [SELECT NAME FROM ANAG_CAP_ISTAT__c WHERE COD_ISTAT__c  =:codIstatPodVal 
                                                       AND FLG_COMPETENZA_ENEL__c=:YES GROUP BY Name limit 5000];    
            }
            else{
                listOfDistinctCAP = null;
            }
            List<String> listCAP = new List<String>();
            for( AggregateResult capPodValue : listOfDistinctCAP)
            {
                retList.add((string)capPodValue.get(RECNAME));
            }
            obj.pickListValues.put(CAP,retList);
        }
        catch(Exception e){
            logger.debugException(e);
        }
        logger.pop();
        return obj;    
        
    }
    
}