public class KBMS_ReassignTopics 
{
    public static void reassignTopics(List<Processi__DataCategorySelection> dataCategorySelections)
    {  
        String communityName = Label.KBMS_CommunityName;
        Network myCommunity = [SELECT Id FROM Network WHERE Name =: communityName ];
        String otherTopic = Label.KBMS_ParentOfAdditionalTopics;
        String childOfOtherTopics = Label.KBMS_AdditionalTopic;
        List<String> childOfOtherTopicList = childOfOtherTopics.split(';');
        Set<String> childOfOtherTopicSet = new  Set<String>();
        childOfOtherTopicSet.addAll(childOfOtherTopicList);
       
        Map<Id, Set<String>> articleDataCategoryMap = new Map<Id, Set<String>>();
        System.debug('dataCategorySelections' + dataCategorySelections);
        
        for(Processi__DataCategorySelection dataCat: dataCategorySelections)
        {
            if(dataCat.DataCategoryGroupName == Label.KBMS_GroupDataCategory)
            {
                Set<String> categoriesName = new Set<String> ();
                if (articleDataCategoryMap.get(dataCat.ParentId) != null)
                {
                    categoriesName = articleDataCategoryMap.get(dataCat.ParentId);
                }
               // String dataCatLabel = dataCat.DataCategoryName.replace('_', ' ');
                String dataCatLabel = getLabel(String.valueOf(dataCat.DataCategoryName));
                categoriesName.add(dataCatLabel);
                
                List<String> parentDetails =KBMS_DataCategoryUtil.getParentCategory(String.valueOf(dataCat.DataCategoryName));
                
                
                if(parentDetails != NULL && parentDetails.size() > 0 && parentDetails[1] != NULL && parentDetails[1] != '' )
                {
                    String parentLabel = getLabel(parentDetails[1]);
                    categoriesName.add(parentLabel);    
                }
                // if(childOfOtherTopicSet.contains(dataCatLabel)){
               //     categoriesName.add(otherTopic);
               // }
                for(String childTopics : childOfOtherTopicList){
            		if(childTopics.equalsIgnoreCase(dataCatLabel))
                    {
                		 categoriesName.add(otherTopic);
            		}
        		}
                
                articleDataCategoryMap.put(dataCat.ParentId, categoriesName);
            }
        }
        System.debug('articleDataCategoryMap>>>' + articleDataCategoryMap);
        
        List<Topic> topicList = [SELECT Id, Name, NetworkId FROM Topic WHERE NetworkId =: myCommunity.Id];
        Set<String> topicsNames = new Set<String>();
        for (Topic topic : topicList)
        {
            topicsNames.add(topic.Name);
        }
        
        System.debug('topicList ' + topicList );
        for (Id articleId : articleDataCategoryMap.keySet())
        {
            ConnectApi.TopicNamesInput tpi = new ConnectApi.TopicNamesInput();
            tpi.topicNames = new List<String>();
            
            for (String categoryName: articleDataCategoryMap.get(articleId))
            {
                for(String topicsName : topicsNames)
                {
                    if(topicsName.equalsIgnoreCase(categoryName))
                    {
                        tpi.topicNames.add(categoryName);
                    }
                    
                }
            }
           
            System.debug('articleId Name : '+articleId);
            System.debug('tpi Name : '+tpi);
            ConnectApi.Topics.reassignTopicsByName(myCommunity.Id, articleId,tpi);
        }
    }
    
    public static void reassignTopics(List<FAQ__DataCategorySelection> dataCategorySelections)
    {  
        String communityName = Label.KBMS_CommunityName;
        Network myCommunity = [SELECT Id FROM Network WHERE Name =: communityName ];
        String otherTopic = Label.KBMS_ParentOfAdditionalTopics;
        String childOfOtherTopics = Label.KBMS_AdditionalTopic;
        List<String> childOfOtherTopicList = childOfOtherTopics.split(';');
        Set<String> childOfOtherTopicSet = new  Set<String>();
        childOfOtherTopicSet.addAll(childOfOtherTopicList);
       	
        Map<Id, Set<String>> articleDataCategoryMap = new Map<Id, Set<String>>();
        System.debug('dataCategorySelections' + dataCategorySelections);
        
        for(FAQ__DataCategorySelection dataCat: dataCategorySelections)
        {
            if(dataCat.DataCategoryGroupName == Label.KBMS_GroupDataCategory)
            {
                Set<String> categoriesName = new Set<String> ();
                if (articleDataCategoryMap.get(dataCat.ParentId) != null)
                {
                    categoriesName = articleDataCategoryMap.get(dataCat.ParentId);
                }
               // String dataCatLabel = dataCat.DataCategoryName.replace('_', ' ');
                String dataCatLabel = getLabel(String.valueOf(dataCat.DataCategoryName));
                categoriesName.add(dataCatLabel);
                
                List<String> parentDetails =KBMS_DataCategoryUtil.getParentCategory(String.valueOf(dataCat.DataCategoryName));
                
                
                if(parentDetails != NULL && parentDetails.size() > 0 && parentDetails[1] != NULL && parentDetails[1] != '' )
                {
                    String parentLabel = getLabel(parentDetails[1]);
                    categoriesName.add(parentLabel);    
                }
                // if(childOfOtherTopicSet.contains(dataCatLabel)){
               //     categoriesName.add(otherTopic);
               // }
                for(String childTopics : childOfOtherTopicList){
            		if(childTopics.equalsIgnoreCase(dataCatLabel))
                    {
                		 categoriesName.add(otherTopic);
            		}
        		}
                articleDataCategoryMap.put(dataCat.ParentId, categoriesName);
            }
        }
        System.debug('articleDataCategoryMap>>>' + articleDataCategoryMap);
        
        List<Topic> topicList = [SELECT Id, Name, NetworkId FROM Topic WHERE NetworkId =: myCommunity.Id];
        Set<String> topicsNames = new Set<String>();
        for (Topic topic : topicList)
        {
            topicsNames.add(topic.Name);
        }
        
        System.debug('topicList ' + topicList );
        for (Id articleId : articleDataCategoryMap.keySet())
        {
            ConnectApi.TopicNamesInput tpi = new ConnectApi.TopicNamesInput();
            tpi.topicNames = new List<String>();
            
            for (String categoryName: articleDataCategoryMap.get(articleId))
            {
                for(String topicsName : topicsNames)
                {
                    if(topicsName.equalsIgnoreCase(categoryName))
                    {
                        tpi.topicNames.add(categoryName);
                    }
                    
                }
            }
           
            System.debug('articleId Name : '+articleId);
            System.debug('tpi Name : '+tpi);
            ConnectApi.Topics.reassignTopicsByName(myCommunity.Id, articleId,tpi);
        }
    }
    
    public static String getLabel(String dataCategoryUniqueName)
    {
        String dataCategoryLabel = dataCategoryUniqueName;
        dataCategoryLabel = dataCategoryLabel.replace('_', ' ');
        dataCategoryLabel = dataCategoryLabel.replace('1', 'à');
        dataCategoryLabel = dataCategoryLabel.replace('2', '-');
        dataCategoryLabel = dataCategoryLabel.replace('3', '(');
        dataCategoryLabel = dataCategoryLabel.replace('4',')');
        dataCategoryLabel = dataCategoryLabel.replace('5', 'ù');
        dataCategoryLabel = dataCategoryLabel.replace('6', '/');
        dataCategoryLabel = dataCategoryLabel.replace('7',',');
        dataCategoryLabel = dataCategoryLabel.replace('8', ':');
        dataCategoryLabel = dataCategoryLabel.replace('9', '\'');
        dataCategoryLabel = dataCategoryLabel.replace('0', '.');
        return dataCategoryLabel;
    }
    
    public static void DeleteTopics(Set<Id> articlewithNoDataCategory)
    {
        List<TopicAssignment> toDeleteTopicRecords = new List<TopicAssignment>();
        toDeleteTopicRecords = [SELECT CreatedById,CreatedDate,EntityId,EntityKeyPrefix,EntityType,Id,IsDeleted,NetworkId,SystemModstamp,TopicId FROM TopicAssignment Where EntityId In : articlewithNoDataCategory];
        if(toDeleteTopicRecords!= null && !toDeleteTopicRecords.isEmpty())
        {
            Delete toDeleteTopicRecords;
        }
    }
}