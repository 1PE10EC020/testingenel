public with sharing class KBMS_CustomArticleSearch
{
    public List<KnowledgeArticleVersion> articleList {get;set;}
    public List<Contact>  contactList {get;set;}
    public List<Sezione_Contatti__c> sezioneContattiList {get;set;}
    public String dataCategory {get; set;}
    public String searchstring {get;set;}
    public String selectedArticle{get;set;}
    public Integer totalSearchResult{get;set;}
    public Boolean renderScontacts{set;get;}
    
    List<String> contactFields = new String [] {'App__c','Email','Fax','FirstName','LastName','MailingStreet','MailingCity','MailingState','MailingPostalCode','MailingCountry','Name','Phone','Sezione_Contatti__c','Sito__c','Zona__c','MobilePhone'}; 
    List<String> sezioneContactFields = new String [] {'Nome_Sezione__c','Tipologia_Richiedente__c'};
    List<String> articleFields = new String [] {'Title','UrlName','Summary','KnowledgeArticleId','firstPublishedDate','ArticleType','ArticleNumber'};  
    
    String defaultDataCategoryGroup = 'KBMS_Operai_Tecnici__c';
    String defaultdataCategory = 'KBMS_Operai_Tecnici__c';
    
    String language = System.Label.KBMS_Language;
    String publishStatus = System.Label.KBMS_PublishStatus_Online;
    String recordtype = System.Label.KBMS_RecordTypeInSearch;
    
    public KBMS_CustomArticleSearch()
    {
        articleList = new List<KnowledgeArticleVersion>();
        contactList = new List<Contact>();
        sezioneContattiList = new List<Sezione_Contatti__c>();
    }
    
    public void SearchResult() 
    {
        renderScontacts=false; 
        articleList.clear();
        contactList.clear();
        sezioneContattiList.clear();
        
        List<String> searchStringList = new List<String>();
        Set<String> setSearchString= new Set<String>();   
        
        if (searchString != NULL && searchString != '')     
        {
            searchStringList = searchString.split(' ');      
        }
        setSearchString.addAll(searchStringList);       
        
        //preposition word remove       
        String prePositionWords = System.Label.KBMS_prepositionWords;
        List<String> splitPrepositionWords = new List<String>();       
        splitPrepositionWords = prePositionWords.split(','); 
        Set<String> setprepositionWords=new Set<String>();      
        setprepositionWords.addAll(splitPrepositionWords);      
                
        //removeing all preposition words from set      
        setSearchString.removeAll(setprepositionWords);     
        searchStringList.Clear();      
        for(String s:setSearchString)
        {      
            searchStringList.add('%'+s+'%');        
        }
        
        String currentDataCategory = 'KBMS_Operai_Tecnici__c';
        Boolean isDataCategoryUnselected = FALSE;
        if ((dataCategory == '' || dataCategory == NULL) || dataCategory=='Tutto__c')
        {
            isDataCategoryUnselected = TRUE;
            dataCategory = 'Tutto';
        }
        if ((dataCategory != '' && dataCategory != NULL) && dataCategory.equals('KBMS_Operai_Tecnici')) 
        {
            dataCategory = 'Tutto';
            
        }
        
        
        if (selectedArticle !='Contact' )
        { 
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('FAQ__kav', 'isAccessible') ||
                !(KBMS_UtilityToCheckFLS.checkFLSPermission('FAQ__kav', articleFields, 'isAccessible'))) 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Insufficient access to FAQ Article'));
            }
            else 
            {
                KBMS_CustomSearch__c customSearchFaq = KBMS_CustomSearch__c.getValues('FAQ');
                List<String> faqSearchFields = new List<String>();
                if (customSearchFaq.Fields__c  != null || customSearchFaq.Fields__c != '')
                {
                    faqSearchFields = customSearchFaq.Fields__c.split(',');
                }
                
                if ((dataCategory != ''&& dataCategory != NULL) && dataCategory.indexOf('__c') == -1) 
                {
                    dataCategory = dataCategory + '__c';
                }
                                
                String firstPartQuery = 'SELECT Id,Title,UrlName,Summary,KnowledgeArticleId,firstPublishedDate,ArticleType,ArticleNumber '+
                                        'FROM KnowledgeArticleVersion ';
                String secondPartQuery = ' WHERE (PublishStatus =: PublishStatus AND Language =:language '; 
                String thirdPartQuery = '';  
                if (searchStringList != null  && searchStringList.size() > 0 )
                {
                    thirdPartQuery =' ( ';  
                    Integer k = 0;
                    for (String s: faqSearchFields)
                    {
                        thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                        if (k != faqSearchFields.size())
                        {
                            thirdPartQuery =  thirdPartQuery + ' OR ';
                        }
                        k++;
                    }
                    thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                
                    thirdPartQuery = ' AND '+ thirdPartQuery + ' ) AND ';
                }
                
                String fourthPartQuery = '  ArticleType =: selectedArticle  )';
                
                String fifthPartQuery = '  WITH DATA CATEGORY ' + currentDataCategory +'  BELOW '+ dataCategory  +' AND KBMS__c AT (OT__c) ';
                
                String query;
                
                List<KnowledgeArticleVersion> moduli = new List<KnowledgeArticleVersion> ();
                List<KnowledgeArticleVersion> other = new List<KnowledgeArticleVersion> ();
                
                 
                if (selectedArticle == 'Moduli__kav' || selectedArticle == 'Tutto')
                {
                    String currentArticle = 'Moduli__kav';
                    fourthPartQuery = '  ArticleType =: currentArticle  )';
                    
                    if (searchString == NULL || searchString == '' ||  searchStringList.size() == 0)
                    { System.debug('Entered');
                        if(isDataCategoryUnselected)
                        {
                            query = firstPartQuery + secondPartQuery + ' AND ' +  fourthPartQuery ;
                            
                        }else{
                            query = firstPartQuery + secondPartQuery + ' AND ' +  fourthPartQuery + fifthPartQuery ;
                        }
                        
                    }
                    else
                    {
                        KBMS_CustomSearch__c customSearchModuli = KBMS_CustomSearch__c.getValues('Moduli');
                        List<String> moduliSearchFields = new List<String>();
                        if (customSearchModuli.Fields__c  != null || customSearchModuli.Fields__c != '')
                        {
                            moduliSearchFields = customSearchModuli.Fields__c.split(',');
                        }
                        thirdPartQuery =' ( ';  
                        Integer k = 0;
                        for (String s: moduliSearchFields)
                        {
                            thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                            if (k != moduliSearchFields.size())
                            {
                                thirdPartQuery =  thirdPartQuery + ' OR ';
                            }
                            k++;
                        }
                        thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                    
                        thirdPartQuery = ' AND '+ thirdPartQuery + ' ) AND ';
                        
                        if(isDataCategoryUnselected)
                        {
                             query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery ;
                            
                        }else{
                             query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery + fifthPartQuery ;
                        }
                       
                    }
                    System.debug(query);
                    moduli = Database.query(query); 
                }
                
                
                if(selectedArticle == 'Tutto')
                {
                    String currentArticleType = 'Moduli__kav';
                    fourthPartQuery = '  ArticleType =: currentArticleType  )';
                    
                    if (searchString == NULL || searchString == '' ||  searchStringList.size() == 0)
                    { 
                        if(isDataCategoryUnselected)
                        {
                            query = firstPartQuery + secondPartQuery + ' AND ' +  fourthPartQuery ;
                            
                        }else{
                            query = firstPartQuery + secondPartQuery + ' AND ' +  fourthPartQuery + fifthPartQuery ;
                        }
                        
                    }
                    else
                    {
                        KBMS_CustomSearch__c customSearchModuli = KBMS_CustomSearch__c.getValues('Moduli');
                        List<String> moduliSearchFields = new List<String>();
                        if (customSearchModuli.Fields__c  != null || customSearchModuli.Fields__c != '')
                        {
                            moduliSearchFields = customSearchModuli.Fields__c.split(',');
                        }
                        thirdPartQuery =' ( ';  
                        Integer k = 0;
                        for (String s: moduliSearchFields)
                        {
                            thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                            if (k != moduliSearchFields.size())
                            {
                                thirdPartQuery =  thirdPartQuery + ' OR ';
                            }
                            k++;
                        }
                        thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                    
                        thirdPartQuery = ' AND '+ thirdPartQuery + ' ) AND ';
                        
                        if(isDataCategoryUnselected)
                        {
                             query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery ;
                            
                        }else{
                             query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery + fifthPartQuery ;
                        }
                       
                    }
                    System.debug(query);
                    moduli = Database.query(query);
                    
                    List<String> currentArticle = new List<String>{'Processi__kav', 'FAQ__kav'};
                    if (searchString != NULL && searchString != '' && searchStringList.size() > 0)
                    {
                        fourthPartQuery = ' ArticleType In: currentArticle  )';
                    }
                    else
                    {
                        fourthPartQuery = ' AND   ArticleType In: currentArticle  )';
                    }
                    query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery +fifthPartQuery ;
                    System.debug(query);
                    other = Database.query(query);
                }
                else if(selectedArticle != 'Moduli__kav')
                {
                    if (!(searchString != NULL && searchString != '' && searchStringList.size() > 0))
                    {
                        fourthPartQuery = ' AND  ArticleType =: selectedArticle  )';
                    }
                    
                    query = firstPartQuery + secondPartQuery + thirdPartQuery + fourthPartQuery + fifthPartQuery ;
                    System.debug(query);
                    other = Database.query(query);
                }
                
                
                articleList.addAll(moduli);
                 
                articleList.addAll(other);
                
            }
        }
        if(selectedArticle =='Contact'  || selectedArticle =='Tutto' )
        {
        
            RecordType rcrdtyp = [SELECT Id, Name FROM RecordType WHERE Name = :recordtype LIMIT 1];
            Id RecordTypeId = rcrdtyp.Id;
           
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Contact', 'isAccessible') ||
                ! (KBMS_UtilityToCheckFLS.checkFLSPermission('Contact', contactFields, 'isAccessible'))) 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Insufficient access to Contact'));
            }
            else
            {
                String firstPartQueryCon = 'SELECT App__c, Email, Fax, FirstName, Id, LastName, MailingStreet, MailingCity, MailingState, MailingPostalCode, '+ 
                ' MailingCountry, Name, Phone, Sezione_Contatti__c, Sito__c, Zona__c, MobilePhone,Sezione_Contatti__r.Tipologia_Richiedente__c FROM Contact '+
                'WHERE  RecordTypeId=:RecordTypeId AND Sezione_Contatti__c!= null ';
                String secondPartQueryCon = '';
                
                KBMS_CustomSearch__c customSearchContact = KBMS_CustomSearch__c.getValues('Contact');
                List<String> contactSearchFields = new List<String>();
                if (customSearchContact.Fields__c  != null || customSearchContact.Fields__c != '')
                {
                    contactSearchFields = customSearchContact.Fields__c.split(',');
                }
                
                Integer i = 0;
                if (searchString != NULL && searchString != '' && searchStringList.size() > 0)
                {
                    for (String s: contactSearchFields)
                    {
                        secondPartQueryCon  = secondPartQueryCon + s +' LIKE :searchStringList';
                        if (i != contactSearchFields.size())
                        {
                            secondPartQueryCon = secondPartQueryCon + ' OR ';
                        }
                        
                        i++;
                    }
                    secondPartQueryCon = secondPartQueryCon.removeEnd(' OR ');
                    
                    secondPartQueryCon = ' AND ( ' + secondPartQueryCon + ' )';
                }
                         
                String contactQuery =  firstPartQueryCon + secondPartQueryCon;
                
                System.debug(contactQuery);
                contactList = Database.query(contactQuery);

            }
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isAccessible') ||
                !(KBMS_UtilityToCheckFLS.checkFLSPermission('Sezione_Contatti__c', sezioneContactFields, 'isAccessible'))) 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Insufficient access to Sezione Contact'));
            }
            else 
            {
                
                String firstPartQuerySCon = 'SELECT Nome_Sezione__c,Tipologia_Richiedente__c, ' + 
                '(SELECT App__c,Email,Fax,FirstName,Id,LastName,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Name, '+
                ' Phone,Sezione_Contatti__c,Sito__c,Zona__c,MobilePhone FROM Contacts_del__r) FROM Sezione_Contatti__c ';
                String secondPartQuerySCon = '';
                
                KBMS_CustomSearch__c customSearchContact = KBMS_CustomSearch__c.getValues('Sezione_Contatti__c');
                List<String> contactSearchFields = new List<String>();
                if (customSearchContact.Fields__c  != null || customSearchContact.Fields__c != '')
                {
                    contactSearchFields = customSearchContact.Fields__c.split(',');
                }
                
                if (searchString != NULL && searchString != '' && searchStringList.size() > 0)
                {
                    Integer i = 0;
                    for (String s: contactSearchFields)
                    {
                        secondPartQuerySCon  = secondPartQuerySCon + s +' LIKE :searchStringList';
                        if (i != contactSearchFields.size())
                        {
                            secondPartQuerySCon = secondPartQuerySCon + ' OR ';
                        }
                        i++;
                    }
                    secondPartQuerySCon = secondPartQuerySCon.removeEnd(' OR ');
                    
                    secondPartQuerySCon = ' WHERE ( ' + secondPartQuerySCon + ' )';
                }
                
                String sezioneContactQuery =  firstPartQuerySCon  + secondPartQuerySCon;
                
                System.debug(sezioneContactQuery);
                sezioneContattiList = Database.query(sezioneContactQuery);
                system.debug('===================='+sezioneContattiList);
            }
        }
        totalSearchResult = articleList.size() + ContactList.size() + SezioneContattiList.size();
    }

}