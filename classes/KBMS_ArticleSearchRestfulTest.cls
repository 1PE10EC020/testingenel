/**
* @author Atos
* @date  Sept,2016
* @description This class is test class of KBMS_ArticleSearchRestful
*/
@isTest()
public class KBMS_ArticleSearchRestfulTest {
     static testMethod void testMethod1() {
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);        
        System.assertEquals(faqtest.size(), 2);
        system.debug('faqtest::'+faqtest);
        
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;  
        
         FAQ__DataCategorySelection datacatTest1=new FAQ__DataCategorySelection();
        datacatTest1.ParentId= faqtest[0].Id;     
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 
         
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','test'); 
        
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        
        List<sObject> knwoledgesObject = Database.query('SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM FAQ__kav WHERE PublishStatus =:draft AND Language =:Language');
   
        Test.startTest();      
        
        KBMS_ArticleSearchRestful.ArticleWrapper art=new KBMS_ArticleSearchRestful.ArticleWrapper();
        KBMS_ArticleSearchRestful.getArticleByCategoryIdArticleType();
         
        Test.stopTest();
      
    }
    static testMethod void testMethod2() {
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);        
        System.assertEquals(faqtest.size(), 2);
        system.debug('faqtest::'+faqtest);
        
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;  
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','test test'); 
        
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        
        List<sObject> knwoledgesObject = Database.query('SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM FAQ__kav WHERE PublishStatus =:draft AND Language =:Language');
   
        Test.startTest();      
        
        KBMS_ArticleSearchRestful.ArticleWrapper art=new KBMS_ArticleSearchRestful.ArticleWrapper();
        KBMS_ArticleSearchRestful.getArticleByCategoryIdArticleType();
         
        Test.stopTest();
      
    }
}