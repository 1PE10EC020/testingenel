@IsTest
public class RiconoscimentoRic_Test {

/*
     @testSetup
  	private static void setup() {		
  		Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		Account acc2 = new Account();
		acc2.CodiceFiscale__c =  'DOEJNA80A41H501Z';
		acc2.name = 'Doe';
		Acc2.nome__c ='Jane';
		acc2.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc2.phone = '3333111';
		insert acc2;

  	}
  	*/
  	@isTest
    static void test (){
    	
    	String phone = '23455';
    	// create case with minimal information 
    
		
		RiconoscimentoRic.Esito esito;
		esito = RiconoscimentoRic.RiconoscimentoRicService(phone);
		System.assertEquals ('001', esito.codice);
    }
 
 	  	@isTest
    static void test2 (){
    	Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
    	String phone = '3333111';
    	// create case with minimal information 
    
		
		RiconoscimentoRic.Esito esito;
		esito = RiconoscimentoRic.RiconoscimentoRicService(phone);
		System.assertEquals ('001', esito.codice);
    }
    
     	  	@isTest
    static void test3 (){
    	Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.Email__c = 'testaccount@accenture.it';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
    	String phone = '3333111';
    	// create case with minimal information 
    
		
		RiconoscimentoRic.Esito esito;
		esito = RiconoscimentoRic.RiconoscimentoRicService(phone);
		System.assertEquals ('000', esito.codice);
    }
    
         	  	@isTest
    static void test4 (){
    	Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.Email__c = 'testaccount@accenture.it';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		Account acc2 = new Account();
		acc2.CodiceFiscale__c =  'DOEJNA80A41H501Z';
		acc2.name = 'Doe';
		Acc2.nome__c ='Jane';
		acc2.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc2.phone = '3333111';
		insert acc2;
		
    	String phone = '3333111';
    	// create case with minimal information 
    
		
		RiconoscimentoRic.Esito esito;
		esito = RiconoscimentoRic.RiconoscimentoRicService(phone);
		System.assertEquals ('001', esito.codice);
    }
    
     	  	@isTest
    static void test5 (){
    	Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
    	String phone = '23456';
    	// create case with minimal information 
    
		
		RiconoscimentoRic.Esito esito;
		esito = RiconoscimentoRic.RiconoscimentoRicService(phone);
		System.assertEquals ('001', esito.codice);
    }
 
}