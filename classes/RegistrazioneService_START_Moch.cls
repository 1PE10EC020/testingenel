@isTest
global class RegistrazioneService_START_Moch implements WebServiceMock {
	private RegistrazioneService.StartRegistrazioneResponse_element reqElement{get;Set;} // input from the webservice, need to test;
	 
	public RegistrazioneService_START_Moch(RegistrazioneService.StartRegistrazioneResponse_element inElement){
  		reqElement = inElement;
	}
	
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

	
     RegistrazioneService.StartRegistrazioneResponse_element respElement ;
     respElement = reqElement;
     response.put('response_x', respElement);

   }
	

}