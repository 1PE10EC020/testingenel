/*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description : Test class for PT_MasRequestRecordCreator.
**********************************************************************************************/

@isTest(SeeAllData = false)
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description : Test class for PT_MasRequestRecordCreator.
**********************************************************************************************/
private class PT_MasRequestRecordCreator_Test{
    
    private static final String ALIAS = 'unEx14';
  	private static final String PTCONTRIBUTI = System.Label.PT_CARICAMENTO_RecordTypeName_PT_ConMas;
    
    /* 
    Author : Sharan Sukesh
    Apex Method : createMasRequestRecord
    CreatedDate : 17 Oct 2017
    Description : createMasRequestRecord is a method to invoke methods from PT_MasRequestRecordCreator.
    */
    private static testMethod void createMasRequestRecord(){
        
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Caricamento_Massivo__c; 
      	Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id ptpc1_id = richiestaRecordTypeInfo.get('PT_Contributi_Massivi').getRecordTypeId();
        PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //PT_Caricamento_Massivo__c request = new PT_Caricamento_Massivo__c(); 
		request.IdCanale__c = 'PTR';
        request.RecordTypeId= ptpc1_id;
       // database.insert(request);
        System.assertEquals(ptpc1_id,request.RecordTypeId);
        system.runAs(runningUser){
        PT_MasRequestRecordCreator.createMasRequestRecord(request);
        }
    }
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description : Test method for createCaricamentoMassivoRecord.
**********************************************************************************************/    
	private static testMethod void createCaricamentoMassivoRecord(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Caricamento_Massivo__c; 
      	Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id ptpc1_id = richiestaRecordTypeInfo.get('PT_Contributi_Massivi').getRecordTypeId();
       PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //PT_Caricamento_Massivo__c request = new PT_Caricamento_Massivo__c(); 
		request.IdCanale__c = 'PTR';
        request.RecordTypeId= ptpc1_id;
        //database.insert(request);
        System.assertEquals(ptpc1_id,request.RecordTypeId);
       system.runAs(runningUser){
        PT_MasRequestRecordCreator.createCaricamentoMassivoRecord(request);
        }    
    }
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description : Test method for PT_GetRecTypeDetail.
**********************************************************************************************/    
    private static testMethod void PT_GetRecTypeDetail(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Caricamento_Massivo__c; 
      	Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id ptpc1_id = richiestaRecordTypeInfo.get('PT_Contributi_Massivi').getRecordTypeId();
       PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //PT_Caricamento_Massivo__c request = new PT_Caricamento_Massivo__c(); 
		request.IdCanale__c = 'PTR';
        request.RecordTypeId= ptpc1_id;
        database.insert(request);
        System.assertEquals(ptpc1_id,request.RecordTypeId);
        system.runAs(runningUser){
        PT_MasRequestRecordCreator.PT_GetRecTypeDetail(request.RecordTypeId);
        }
    }
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description :  Test method for getRichiestaPicklistValues.
**********************************************************************************************/
	 private static testMethod void getRichiestaPicklistValues(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Caricamento_Massivo__c; 
      	Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id ptpc1_id = richiestaRecordTypeInfo.get('PT_Contributi_Massivi').getRecordTypeId();
         PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //PT_Caricamento_Massivo__c request = new PT_Caricamento_Massivo__c(); 
		request.IdCanale__c = 'PTR';
        request.RecordTypeId= ptpc1_id;
        database.insert(request);
        System.assertEquals(ptpc1_id,request.RecordTypeId);
        system.runAs(runningUser){
        PT_MasRequestRecordCreator.getRichiestaPicklistValues();
        }
     }
    /*********************************************************************************************
@Author : Sharan Sukesh
@date : 17 Oct 2017
@description :  Test method for getAllRichiestaPicklistValuesCntr.
**********************************************************************************************/
    private static testMethod void getAllRichiestaPicklistValuesCntr(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
         Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Caricamento_Massivo__c; 
      	Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id ptpc1_id = richiestaRecordTypeInfo.get('PT_Contributi_Massivi').getRecordTypeId();
        PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //PT_Caricamento_Massivo__c request = new PT_Caricamento_Massivo__c(); 
		request.IdCanale__c = 'PTR';
        request.RecordTypeId= ptpc1_id;
        database.insert(request);
        System.assertEquals(ptpc1_id,request.RecordTypeId);
        system.runAs(runningUser){
        PT_MasRequestRecordCreator.getAllRichiestaPicklistValuesCntr(request.RecordTypeId);
        }
     }
   }