@isTest
public class KBMS_ArticleProcessiViewBatchableTest
{
    public Static testMethod void testArticleHistoryBatch()
    {
        List<Processi__kav> articles = KBMS_TestDataFactory.createProcessTestRecords(10);  
        
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        
        List<Processi__DataCategorySelection> datacatSections = new List<Processi__DataCategorySelection>();
        
        for (Processi__kav f: articles)
        {
            Processi__DataCategorySelection datacatTest = new Processi__DataCategorySelection();
            datacatTest.ParentId= f.Id;     
            datacatTest.DataCategoryName=dataCategory;
            datacatTest.DataCategoryGroupName=GroupDataCategory;
            datacatSections.add(datacatTest); 
            
            Processi__DataCategorySelection datacatTest1 = new Processi__DataCategorySelection();
            datacatTest1.ParentId= f.Id;     
            datacatTest1.DataCategoryName='OT';
            datacatTest1.DataCategoryGroupName='KBMS';
            datacatSections.add(datacatTest1); 
            
            
        }
        insert datacatSections;
        
        List<Processi__kav> insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID In: articles ]; 
        
        for (Processi__kav f: insertedTestArticle )
        {
            KbManagement.PublishingService.publishArticle(f.KnowledgeArticleId,true);      
        }
        Test.startTest();
        KBMS_ArticleProcessiViewBatchable b = new KBMS_ArticleProcessiViewBatchable();
        database.executebatch(b);
        Test.stopTest();
    }
}