public class Utility 
{
    //String Constant for Success Message 
    public static final string  SUCCESS_MESSAGE = 'Record created successfully';
    
    //String Constant for Error Message
    public static final string  ERROR_MESSAGE = 'Error Message : ';
    
    /**
     * Retervie Picklist values from the object 
     * @param String objectName
     * @param String fieldName
     *
     */     
    public static List<InitlizeAuraComp.SelectOptionWrapper> getPickListValues(String objectName,String fieldName)
    {
        List<InitlizeAuraComp.SelectOptionWrapper> retList = new List<InitlizeAuraComp.SelectOptionWrapper>();
        try
        {
            Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
            Schema.SObjectType pType = objGlobalMap.get(objectName);
            Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
            List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(fieldName).getDescribe().getPicklistValues();
            retList.add(new InitlizeAuraComp.SelectOptionWrapper('--None--',''));
            for( Schema.PicklistEntry f : ctrl_ple)
            {
                retList.add(new InitlizeAuraComp.SelectOptionWrapper(f.getLabel(), f.getValue()));
            }
        }
        catch(Exception ex)
        {
            ex.getMessage();
        }
      

        return retList;
    } 
}