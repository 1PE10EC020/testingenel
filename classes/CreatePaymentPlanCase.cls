public class CreatePaymentPlanCase 
{  
/**
     * Utility method to get all the picklist values of the Richiesta objects
     *
     */
    @AuraEnabled
    public static InitlizeAuraComp getCasePicklistValues()
    {
        List<InitlizeAuraComp.SelectOptionWrapper> recordTypeLst = new List<InitlizeAuraComp.SelectOptionWrapper>();
        InitlizeAuraComp  obj = new InitlizeAuraComp();
        obj.pickListValues.put('FlagAttFuoriOrario__c',Utility.getPickListValues('PT_Richiesta__c','FlagAttFuoriOrario__c'));
        
        obj.pickListValues.put('AutocertContrConn__c',Utility.getPickListValues('PT_Richiesta__c','AutocertContrConn__c')); 
        obj.pickListValues.put('FlagAutocertIstanza__c',Utility.getPickListValues('PT_Richiesta__c','FlagAutocertIstanza__c')); 
        obj.pickListValues.put('FlagAutocertSollev__c',Utility.getPickListValues('PT_Richiesta__c','FlagAutocertSollev__c')); 
        obj.pickListValues.put('CodCategoriaDisalim__c',Utility.getPickListValues('PT_Richiesta__c','CodCategoriaDisalim__c')); 
        obj.pickListValues.put('CategoriaCliente__c',Utility.getPickListValues('PT_Richiesta__c','  CategoriaCliente__c')); 
        obj.pickListValues.put('FlagCessFuoriOrario__c',Utility.getPickListValues('PT_Richiesta__c','FlagCessFuoriOrario__c'));                 
        obj.pickListValues.put('FlagDisalimentabile__c',Utility.getPickListValues('PT_Richiesta__c','FlagDisalimentabile__c'));  
        obj.pickListValues.put('DistSpostamento__c',Utility.getPickListValues('PT_Richiesta__c','DistSpostamento__c')); 
        obj.pickListValues.put('FlgModificaOccasionale__c',Utility.getPickListValues('PT_Richiesta__c','    FlgModificaOccasionale__c')); 
        obj.pickListValues.put('FlagForfait__c',Utility.getPickListValues('PT_Richiesta__c','   FlagForfait__c')); 
        obj.pickListValues.put('Nazione_esaz__c',Utility.getPickListValues('PT_Richiesta__c','Nazione_esaz__c')); 
        obj.pickListValues.put('Nazione_cl__c',Utility.getPickListValues('PT_Richiesta__c','Nazione_cl__c')); 
        obj.pickListValues.put('PotImpRichiesta__c',Utility.getPickListValues('PT_Richiesta__c','PotImpRichiesta__c')); 
        obj.pickListValues.put('FlagPresenzaCliente__c',Utility.getPickListValues('PT_Richiesta__c','   FlagPresenzaCliente__c'));                 
        obj.pickListValues.put('Provincia_pod__c',Utility.getPickListValues('PT_Richiesta__c',' Provincia_pod__c'));
        obj.pickListValues.put('Provincia_esaz__c',Utility.getPickListValues('PT_Richiesta__c','    Provincia_esaz__c')); 
        obj.pickListValues.put('Provincia_cl__c',Utility.getPickListValues('PT_Richiesta__c','  Provincia_cl__c')); 
        obj.pickListValues.put('FlagServAggCurveCar__c',Utility.getPickListValues('PT_Richiesta__c','FlagServAggCurveCar__c')); 
        obj.pickListValues.put('FlagCurveCarico__c',Utility.getPickListValues('PT_Richiesta__c','FlagCurveCarico__c')); 
        obj.pickListValues.put('IdSettore__c',Utility.getPickListValues('PT_Richiesta__c','IdSettore__c')); 
        obj.pickListValues.put('FlagSollevamentoPers__c',Utility.getPickListValues('PT_Richiesta__c','FlagSollevamentoPers__c')); 
        obj.pickListValues.put('FlagStagRic__c',Utility.getPickListValues('PT_Richiesta__c','   FlagStagRic__c'));                 
        obj.pickListValues.put('TensioneRichiesta__c',Utility.getPickListValues('PT_Richiesta__c','TensioneRichiesta__c'));
        obj.pickListValues.put('IdTipoContr__c',Utility.getPickListValues('PT_Richiesta__c','   IdTipoContr__c')); 
        obj.pickListValues.put('IdConnessione__c',Utility.getPickListValues('PT_Richiesta__c',' IdConnessione__c')); 
        obj.pickListValues.put('Toponimo_pod__c',Utility.getPickListValues('PT_Richiesta__c','  Toponimo_pod__c')); 
        obj.pickListValues.put('Toponimo_esaz__c',Utility.getPickListValues('PT_Richiesta__c',' Toponimo_esaz__c')); 
        obj.pickListValues.put('Toponimo_cl__c',Utility.getPickListValues('PT_Richiesta__c','   Toponimo_cl__c')); 
        obj.pickListValues.put('CarDetConRichiesta__c',Utility.getPickListValues('PT_Richiesta__c','    CarDetConRichiesta__c')); 
        obj.pickListValues.put('IdUsoEnergia__c',Utility.getPickListValues('PT_Richiesta__c','IdUsoEnergia__c'));                 
      
        
        return obj;                    
    }
}