/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is a batch class
*/
global class KBMS_ArticleProcessiViewBatchable implements  Database.Batchable<sObject>
{
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        String language = Label.KBMS_Language;
        String publishedStatus = Label.KBMS_PublishStatus_Online;
        String defaultDataCategoryGroup = Label.KBMS_AppDataCategory;
        String defaultdataCategory = 'Tutto__c';
        
        String query = 'SELECT Id,FirstPublishedDate,KnowledgeArticleId,LastModifiedDate,Summary,Title,UrlName,CreatedDate FROM Processi__kav ' +
                        ' WHERE PublishStatus =:publishedStatus AND Language =:language ' +
                        ' WITH DATA CATEGORY '+defaultDataCategoryGroup+ ' BELOW '+defaultdataCategory +' AND  KBMS__c AT (OT__c )';
        
        return Database.getQueryLocator(query);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<Processi__kav> articleList) 
    {    
        system.debug('## article details'+articleList.size());     
         
        List<KBMS_ArticleHistory__c> toBeInsertedArticleHistory = new List<KBMS_ArticleHistory__c>();

        Set<Id> parentArticleId = new Set<Id>();
        Map<String,Processi__kav> ProcessiDetailMap = new Map<String,Processi__kav>();
         
        for (Processi__kav fq : articleList)
        {
            parentArticleId.add(String.valueOf(fq.KnowledgeArticleId));
        }
         
        List<Processi__ViewStat> ProcessiViewList = new  List<Processi__ViewStat>();
        
        ProcessiViewList  =[SELECT ParentId, ViewCount, Channel,NormalizedScore 
                        FROM Processi__ViewStat
                        WHERE IsDeleted = false and ParentId In : parentArticleId];
        
        Map<Id, Processi__ViewStat> ProcessiViewStatMap = new Map<Id, Processi__ViewStat>();
        
        for (Processi__ViewStat pvs : ProcessiViewList)
        {
            ProcessiViewStatMap.put(pvs.ParentId, pvs);
        }
        
        for (Processi__kav  fq : articleList)
        {
            KBMS_ArticleHistory__c ar=new KBMS_ArticleHistory__c();
            ar.Article_Id__c= fq.KnowledgeArticleId;
            ar.Article_Score__c = ProcessiViewStatMap.get(fq.KnowledgeArticleId) == null ? 0 : ProcessiViewStatMap.get(fq.KnowledgeArticleId).NormalizedScore;
            ar.Article_Summary__c = fq.Summary;
            ar.Article_Title__c = fq.Title;
            ar.Article_URL__c = fq.UrlName;
            ar.Channel__c = ProcessiViewStatMap.get(fq.KnowledgeArticleId) == null ? '' : ProcessiViewStatMap.get(fq.KnowledgeArticleId).Channel;
            ar.Article_Total_Views__c = ProcessiViewStatMap.get(fq.KnowledgeArticleId) == null ? 0 : ProcessiViewStatMap.get(fq.KnowledgeArticleId).ViewCount;
            ar.Create_Date__c = fq.CreatedDate;
            ar.Publish_Date__c = fq.FirstPublishedDate; 
            ar.Article_Type__c = 'Processi';
            toBeInsertedArticleHistory.add(ar);
        }
            
        System.debug('testing list--'+toBeInsertedArticleHistory);
        insert toBeInsertedArticleHistory;
    }
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    
}