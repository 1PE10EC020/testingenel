/**********************************************************************************************
@author:       	sushant yadav
@date:         	24 Jun,2017
@Class Name:  	PT_Contrattuali
@description:  	This class is used Get the dependent Picklist values for contract details section 
				before request can be inserted.
@reference:		420_Trader_Detail_Design_Wave0
**********************************************************************************************/ 
public without sharing class PT_Contrattuali {
   
/**********************************************************************************************
@author:       	sushant yadav
@date:         	24 Jun,2017
@Method Name:  	getDependentOptionsImpl
@description:  	This Aura Enabled method is used Get the dependent Picklist values for 
				contract details section before request can be inserted.
@reference:		420_Trader_Detail_Design_Wave0
**********************************************************************************************/   
    @AuraEnabled  
    public static Map<String,List<PT_PicklistAuraComponent.SelectOptionWrapper>> 
        getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
            system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
           
         String objectName = objApiName.toLowerCase();
         String controllingField = contrfieldApiName.toLowerCase();
         String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<PT_PicklistAuraComponent.SelectOptionWrapper>> objResults = 
            new Map<String,List<PT_PicklistAuraComponent.SelectOptionWrapper>>();
            //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
         
        if (!Schema.getGlobalDescribe().containsKey(objectName)){
            System.debug('OBJNAME NOT FOUND --.> ' + objectName);
            return null;
         }
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType==null){
            return objResults;
        }
        PT_BitSet bitSetObj = new PT_BitSet();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
            return objResults;     
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
         objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getValue();
            objResults.put(label,new List<PT_PicklistAuraComponent.SelectOptionWrapper>());
            controllingIndexes.add(contrIndex);
        }
        system.debug('controllingIndexes'+controllingIndexes);
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<PT_PicklistEntryWrapper> objJsonEntries = new List<PT_PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
               Schema.PicklistEntry depentry = depEntries[dependentIndex];
               objEntries.add(depentry);
           system.debug('objEntries->>>>' + objEntries); 
        } 
         
        objJsonEntries = (List<PT_PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), 
                                                               List<PT_PicklistEntryWrapper>.class);
        system.debug('objJsonEntries--->' + objJsonEntries);
        List<Integer> indexes;
        for (PT_PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            system.debug('index'+indexes);
            for (Integer idx : indexes){             
                system.debug('contrEntries[idx]'+contrEntries[idx]);
                String contrLabel = contrEntries[idx].getValue();
                system.debug('objResults'+objResults);
                system.debug('objJson'+objJson);
                
                objResults.get(contrLabel).add
                    (new PT_PicklistAuraComponent.SelectOptionWrapper(objJson.label, objJson.value));
                //objJson.label);
            }
        }
        objEntries = null;
        objJsonEntries = null;
        system.debug('objResults--->' + objResults);
        return objResults;
    }

    
}