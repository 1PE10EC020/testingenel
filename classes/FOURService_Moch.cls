@isTest
global class FOURService_Moch implements WebServiceMock {
 private FOURTypes.DashboardArrReplay_element reqElement{get;Set;} // input from the webservice, need to test;
 
  public FOURService_Moch(FOURTypes.DashboardArrReplay_element inElement){
  	reqElement = inElement;
  }
 
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

	
     FOURTypes.DashboardArrReplay_element respElement ;
     respElement = reqElement;
     response.put('response_x', respElement);

   }



}