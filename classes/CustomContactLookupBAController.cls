global class CustomContactLookupBAController {
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  public static string ixnId {get;set;} // interaction id
  public string inServiceCloudConsole {get;set;}     
  
  public CustomContactLookupBAController() {
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    ixnId = System.currentPageReference().getParameters().get('ixnId');
    inServiceCloudConsole = System.currentPageReference().getParameters().get('inServiceCloudConsole');
    system.debug('*** CustomContactLookupBAController ixnId ' + ixnId);
    system.debug('*** CustomContactLookupBAController inServiceCloudConsole '+inServiceCloudConsole);
    system.debug('*** CustomContactLookupBAController search for '+searchString);      
      
    if(searchString != '' && searchString != null) 
        runSearch();  
  }
    
    public void checkForStrays(){
        system.debug('*** checkForStrays');
        if(inServiceCloudConsole == 'false')
        {    
            String myId = UserInfo.getUserId();
            myId = 'GenesysConnector' + myId;
            try{
                List <WorkspaceConnectorInfo__c> connectors = [SELECT objectId__c, interactionId__c From WorkspaceConnectorInfo__c WHERE genesysId__c = :myId];
                Integer listSize = connectors.size();
                if(listSize > 1)
                {
                    system.debug('*** checkForStrays ERROR found multiple connectors - ' + listSize);
                    delete connectors; 
                }
            }
            catch(QueryException e){
                system.debug('*** checkForStrays - no WorkspaceConnectorInfo__c');
            }    
        }
        else
        {
                system.debug('*** checkForStrays not needed inServiceCloudConsole');
        }
    }    
    
  @RemoteAction
  global static string setWorkspaceConnectorInfo(String objectId, String interactionId)
  {
      system.debug('*** setWorkspaceConnectorInfo objectId = '+objectId+' ,interactionId = '+interactionId);
      try{
        //create new object
        String myId = UserInfo.getUserId();
        myId = 'GenesysConnector' + myId;

        WorkspaceConnectorInfo__c myCustomObject = new WorkspaceConnectorInfo__c (
            name = 'GenesysConnector', interactionId__c = interactionId, objectId__c = objectId,genesysId__c = myId);
        //update/insert myCustomObject; 
        upsert myCustomObject genesysId__c;
        system.debug('*** myCustomObject = ' + myCustomObject );
        String urlForDetailPage = new PageReference('/' + myCustomObject.id).getUrl();
        system.debug('*** url = '+ urlForDetailPage );
        return 'success';
      }
      catch(QueryException e){
            return 'error';
      }
  }
   
  // performs the keyword search
  global PageReference search() {
    system.debug('*** search');
    runSearch();
    return null;
  }
    
  
  // prepare the query and issue the search command
  private void runSearch() {
    system.debug('*** runSearch');
    results = performSearch(searchString);
    if(results != null)
    system.debug('*** runSearch results = '+results);         
  } 
  
  // run the search and return the records found. 
  private List<Contact> performSearch(string searchString) {
    system.debug('*** performSearch for '+searchString);
    String soql = 'select id, name from contact';
    if(searchString != '' && searchString != null)
      //soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
      soql = soql +  ' where phone LIKE \'%' + searchString +'%\'';      
    soql = soql + ' limit 25';
    System.debug(soql);
      //return database.query(soql);
      
    List<List<SObject>> cobjects = [FIND :searchString IN PHONE FIELDS RETURNING Contact(id,name)];
    List<Contact> contacts = null;
    if (!cobjects.isEmpty()){
             contacts = ((List<Contact>)cobjects[0]);
    }
    
    return contacts;  
 

  }
  
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}