public class IntegrationUtils {


    
    /*
    * Author : Claudio Quaranta
    * Date : 24/06/2015
    * Description : retrive the timout setted into the related's input custom setting
    * Input :  Name          Type          Description
    *          serviceName   String         Custom Setting's Name
    *output :
    *               -        Integer        Timout setted into the custom setting.
    */
    public Static Integer getTimeout(String serviceName){
        try{
            // retrive Custom setting
            Endpoints__C currentEndpoint  = Endpoints__C.getValues(serviceName);
            // check if retrive the Production parameter or test parameter
            if (currentEndpoint.IsProd__c)
                return Integer.valueOf(currentEndpoint.Timeout_Prod__c);
            else
                return Integer.valueOf(currentEndpoint.Timeout_Test__c);
        }
        catch(Exception e){
            // return default Timeout;
            system.debug(logginglevel.error, 'Exception Raised into integrationUtils.getTimeout : '+ e.getmessage());
            return Constants.DEFAULT_TIMEOUT;
        }
    
    }
    
    
    /*
    * Author : Claudio Quaranta
    * Date : 24/06/2015
    * Description : retrive the Full endpoint ( concatenation between baseUrl and endpointURL)
    * Input :  Name          Type          Description
    *          serviceName   String         Custom Setting's Name
    *output :
    *               -        String         Full Endpoint
    */
    public Static String getFullEndpoint(string serviceName){
        try{
            // retrive Custom setting
            Endpoints__C currentEndpoint  = Endpoints__C.getValues(serviceName);
            // check if retrive the Production parameter or test parameter
            if (currentEndpoint.IsProd__c)
                return currentEndpoint.BaseURL_Prod__c + currentEndpoint.Endpoint_Prod__c;
            else
                return currentEndpoint.BaseURL_Test__c + currentEndpoint.Endpoint_Test__c;
            
        }
        catch(Exception e){
            // return default Endpoint;
            system.debug(logginglevel.error, 'Exception Raised into integrationUtils.getFullEndpoint : '+ e.getmessage());
            return Constants.DEFAULT_ENDPOINT;
        }
    
    
    }
    
    
     
    /*
    * Author : Claudio Quaranta
    * Date : 27/10/2015
    * Description : retrive service username
    * Input :  Name          Type          Description
    *          serviceName   String         Custom Setting's Name
    *output :
    *               -        String         Username
    */
      public Static String getServiceUsername(string serviceName){
      		try{
            // retrive Custom setting
            Endpoints__C currentEndpoint  = Endpoints__C.getValues(serviceName);
            // check if retrive the Production parameter or test parameter
            if (currentEndpoint.IsProd__c)
                return currentEndpoint.Username_Prod__c;
            else
                return currentEndpoint.Username_Test__c;
            
        }
        catch(Exception e){
            // return default Endpoint;
            system.debug(logginglevel.error, 'Exception Raised into integrationUtils.getFullEndpoint : '+ e.getmessage());
            return '';
        }
      
      }
      
      
        
    /*
    * Author : Claudio Quaranta
    * Date : 27/10/2015
    * Description : retrive service password
    * Input :  Name          Type          Description
    *          serviceName   String         Custom Setting's Name
    *output :
    *               -        String         Username
    */
      public Static String getServicePassword(string serviceName){
      		try{
            // retrive Custom setting
            Endpoints__C currentEndpoint  = Endpoints__C.getValues(serviceName);
            // check if retrive the Production parameter or test parameter
            if (currentEndpoint.IsProd__c)
                return currentEndpoint.Password_Prod__c;
            else
                return currentEndpoint.Password_Test__c;
            
        }
        catch(Exception e){
            // return default Endpoint;
            system.debug(logginglevel.error, 'Exception Raised into integrationUtils.getFullEndpoint : '+ e.getmessage());
            return '';
        }
      
      }
}