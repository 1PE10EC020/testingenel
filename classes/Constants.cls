Global  without sharing  class Constants{

    // Property that contais all Business hours
    public static map<String,ID> mapBusinessHours {
        get{
                if (mapBusinessHours == null ){
                    mapBusinessHours = new Map<String,Id>();
                    list<BusinessHours> listofBusinessHours = [select id,name from businesshours];
                    if(listofBusinessHours != null && !listofBusinessHours.isEmpty()){
                        for (BusinessHours hours : listofBusinessHours ){
                            mapBusinessHours.put(hours.name,hours.id);
                        }
                    }
                }
                return mapBusinessHours;
        }
        private set;
    }
    
    public static map<Id,RecordType> mapIdNameRTCase {
    
        get{
                if ( mapIdNameRTCase == null){
                        mapIdNameRTCase = new map<Id,RecordType>([select id, developerName from recordtype where SobjectType = :CASE_LABEL]) ;
                }
                return mapIdNameRTCase;
        }
        private set;
    
    }
    
    
    // WS Arrricchimento Constants
    public static final String WS_FASCIA_ORARIA = 'Orario';
    public static final String WS_FASCIA_NON_ORARIA = 'Non orario';
    public static final String WS_POD_PRESENTE = 'S';
    public static final String WS_POD_NON_PRESENTE = 'N';
    public static final String WS_POD_ERRORE_COMUNICAZIONE= 'E';

    
    
    
    
    // Business Hours Constants
    public static string  DEFAULT_BUSINESS_HOURS='Default'; // Business hours name 
    
    
    // Global Constants
    //public final static String majorityAge = 'Attenzione la data di nascita del cliente è relativa ad un minorenne.';
    public final static String CONSTANT_OK = 'OK';
    public final static String CONSTANT_KO = 'KO';
    
    // Fiscal Code Constants
    public static final String FISCAL_CODE_REGEX = '^[a-zA-Z]{6}[0-9]{2}[abcdehlmprstABCDEHLMPRST]{1}[0-9]{2}[a-zA-Z]{1}[0-9]{3}[a-zA-Z]{1}$';
    public static final String PATTERN_FISCAL_CODE = '^[a-zA-Z]{6}[0-9a-zA-Z]{2}[abcdehlmprstABCDEHLMPRST]{1}[0-9a-zA-Z]{2}([a-zA-Z]{1}[0-9a-zA-Z]{3})[a-zA-Z]{1}$';
    
    // Endpoint Constants
    public final static Integer DEFAULT_TIMEOUT = 5000;
    public final static String DEFAULT_ENDPOINT = 'http://127.0.0.1:8080';
    
    // RecordType Constants
    public final static String CASE_LABEL ='Case';
    
    //Unita' di competenza Constants
    public final static String ZONA ='Zona';
    public final static String DTR ='DTR';
    
    // Case Constants
    public final static String CASE_CONNESSIONE ='Connessione';
    public final static String CASE_DA_DEFINIRE ='Da_Definire';
    public final static String CASE_DA_DEFINIRE_NAME ='Da Definire';
    public final static String CASE_MISURA ='Misura';
    public final static String CASE_SOGGETTO_TERZO ='Soggetto_Terzo';
    public final static String CASE_TRASPORTO ='Trasporto';
    public final static String CASE_PROGETTI ='Progetti';
    public final static String LIVELLO_IN_PRELIEVO='In prelievo';
    public final static String LIVELLO_IN_PRELIEVO_E_IMMISSIONE='In prelievo e Immissione';
    public final static String LIVELLO_GIA_CONNESSO = 'Già Connesso';
    public final static String LIVELLO_IN_CONNESSIONE = 'In Connessione';
    public final static String LIVELLO_DA_CONNETTERE = 'Da Connettere';
    public final static String LIVELLO_CONNESSO_BT = 'Connesso alla rete BT';
    public final static String LIVELLO_CONNESSO_ATMT = 'Connesso alla rete AT/MT';
    public final static String LIVELLO_CONNESSO_MT = 'Connesso alla rete MT';
    public final static String LIVELLO_CONNESSO_AT = 'Connesso alla rete AT';
    public final static String LIVELLO_TENSIONE_FORNTITURA_BT='BT';
    public final static String LIVELLO_TENSIONE_FORNTITURA_MT='MT';
    public final static String LIVELLO_TENSIONE_FORNTITURA_AT='AT';
    public final static string LIVELLO_3_ATMT ='MT/AT';
    
    public final static string CASE_STATUS_CHIUSO ='Chiuso';
    public final static string CASE_STATUS_ANNULLATO ='Annullato';
    public final static string CASE_STATUS_CHIUSO_PENDING ='Chiuso Pending';
    public final static String CASE_STATUS_NUOVO = 'Nuovo' ; 
    public final static String CASE_STATUS_INCARICOFO = 'In carico al FO' ; 
    public final static String CASE_STATUS_INCARICOBO = 'In carico al BO' ; 
    public final static String CASE_STATUS_INCARICO_AL_II_LIVELLO = 'In carico al II Livello' ; 
    public final static String CASE_STATUS_INCARICO_A_FIBRA = 'In carico a Fibra' ; 
    public final static String CASE_STATUS_INCARICO_ESPERTO_DI_ZONA = 'In carico Esperto di Zona' ; 
    public final static String CASE_NUM_NOT_FOUND = 'NUM NOT FOUND';
    public final static String CASE_STATUS_SCALATO = 'Scalato'; 
    public final static String CASE_FAMIGLIA_MOBILITA = 'Mobilità';
    public final static String CASE_FAMIGLIA_ENEL_INFO_PIU = 'Info+';
    
    // email Constants
    
    public final static string emailTemplate = 'Support_Invia_Email_II_Livello';
    public final static string escalationEmail= 'Support: Escalation Email Team Leader';
    
    
    
    //IVR Constants
    public final static string IVR_CONNESSIONI = 'Connessioni';
    public final static string IVR_MISURE = 'Misure';
    public final static string IVR_TRASPORTO = 'Trasporto';
    public final static string IVR_SOGGTERZO = 'Soggetto Terzo';
    public final static string IVR_CambioCE = 'Contatore';
    public final static string IVR_PosaFO = 'Fibra ottica';
    public final static string IVR_Mobilita = 'Mobilità';
    public final static string IVR_EnelInfoPiu = 'Enel Info+';
    
    //Account Constants
    public final static string ACCOUNT_PERSONA_FISICA ='Persona_Fisica';
    public final static string ACCOUNT_PERSONA_GIURIDICA ='Persona_Giuridica';
    public final static string ACCOUNT_NAZIONALITA = 'Italiana';
    
    public final static string ORIGIN_TELEFONO = 'Telefono';
    public final static string ORIGIN_WEB = 'Web';
    public final static string ORIGIN_EMAIL = 'Email';
    
    //page ED_AccountSelector_Page parameters
    public final static string PHONE_PARAMETER='Phone';
    public final static string IVR_PATH_PARAMETER='IVR';
    public final static string CONNID_PARAMETER='CONNID';
    public final static string DN_PARAMETER='DN';
    public final static string TENANT_PARAMETER='TENANT';
    public final static string IDPRATICA_PARAMETER='IDPRATICA';
    public final static string POD_PARAMETER='POD';
    public final static string UNDEFINED='undefined';
    public final static string CASENUM_PARAMETER='CASENUM';
    // role constants
    public final static string ROLE_FO  =   'Front Office';
    public final static string ROLE_BO  =   'Back Office' ;
    public final static string ROLE_TL_FO     =   'Team Leader Front Office' ;
    public final static string ROLE_TL_BO     =   'Team Leader Back Office' ;
    public final static string ROLE_Contact_Management = 'Contact Management';
    //public final static string ROLE_TECH_FO     =   'Front_Office';
    //public final static string ROLE_TECH_BO     =   'Back_Office' ;
    
    public final static string ROLE_TECH_FO     =   'Front Office';
    public final static string ROLE_TECH_BO     =   'Back Office' ;
    
    
    public final static string ROLE_TECH_TL_BO  = 'Team_Leader_BO';
    public final static String ROLE_Gruppo_Info_piu = 'Gruppo Info+';
    
    // Queue Constants
    public final static String QUEUE_FO = 'Front Office';
    public final static String QUEUE_BO = 'Back Office';
    public final static String QUEUE_Contatore_e_Fibra = 'Contatore e Fibra';
    public final static String ROLE_CONTAIN_PED = 'Ped';
    public final static String QUEUE_CallBack = 'CallBack';
  
    
    //ProfileConstants
    public final static String PROFILE_I_LIV ='Operatore CM I Livello';
    public final static string PROFILE_II_LIV = 'Operatore CM II Livello' ;
    public final static string PROFILE_FIBRA = 'Operatore II Livello Fibra';
    public final static string PROFILE_II_LIV_INFOPIU = 'Operatore CM II Livello Info+';
    public final static string Amministratore_CM = 'Amministratore CM';
    public final static string Amministratore_KB_CM = 'Amministratore KB CM';
    public final static string Amministratore_Report_e_Survey = 'Amministratore_Report_e_Survey';
    public final static string Integration_User = 'Integration User';
    public final static string System_Administrator = 'System Administrator';
    
    // Task Constants
    public final static String TASK_PRIORITY_NORMAL = 'Normal';
    public final static String TASK_STATUS_NOTSTARTED = 'Not Started';
    public final static String TASK_STATUS_COMPLETED ='Completed';
    //public final static String TASK_SUBJECT = '' ;
    
    public final static String EMAIL_STATUS_INVIATO = 'Inviato';
    
    // Constants fasce orarie
    public final static String FASCIA_ORARIA_1='1';
    public final static String FASCIA_ORARIA_2='2';
    public final static String FASCIA_ORARIA_3='3';
    
    
    // Constants for RecordingService
    public final static string RESULT_OK ='0';
    
    //Constants Trigger Case
    public final static string TRIGGER_ON = 'On';
    
}