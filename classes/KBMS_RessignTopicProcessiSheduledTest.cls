@isTest(seeAllData = true)
public class KBMS_RessignTopicProcessiSheduledTest{
    public Static testMethod void testWithDataCategory_executeMeth(){
       list<Processi__kav> lsttst = new  list<Processi__kav>();
       
            Processi__kav faqtst = new Processi__kav();
            faqtst.Title = 'TestProcessi';
            faqtst.Summary = 'Test Processi Summary ';
            faqtst.URLName = 'TestProcessi';                
            faqtst.Language=Label.KBMS_Language;
        insert faqtst;              
        lsttst.add(faqtst);
        
                
        String dataCategory='Modifica_potenza';               
        String GroupDataCategory='KBMS_Operai_Tecnici';             
        String articleType='Processi';       
        
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= faqtst.Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;
        
        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :faqtst.Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        PageReference pageRef = new PageReference('/'+faqtst.Id);
        Test.setCurrentPage(pageRef);
        
        
        Test.startTest();
        KBMS_RessignTopicProcessiBatchSheduled b = new KBMS_RessignTopicProcessiBatchSheduled();
      //  b.execute(null);                
        database.executebatch(b);               
        //b.finish(null);            
        Test.stopTest();                    
                
    }
}