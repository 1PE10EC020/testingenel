/**********************************************************************************************
@author:       	Jyotsna Tiwari
@date:         	17 Jun,2017
@description:  	This wrapper class is used by PT_Utilities apex class to bing picklist key-value pair 
**********************************************************************************************/
public without sharing class PT_PicklistAuraComponent {
	
    @AuraEnabled 
    public Map<String,List<String>> pickListValues;
       
    public PT_PicklistAuraComponent()
    {
       pickListValues = new  Map<String,List<String>>();
    }

/**********************************************************************************************
@author:       	Jyotsna Tiwari
@date:         	17 Jun,2017
@description:  	Wrapper class to fetch the label and value for the  picklist field
**********************************************************************************************/
    public without sharing class SelectOptionWrapper
    {
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
        
/**********************************************************************************************
@author:       	Jyotsna Tiwari
@date:         	17 Jun,2017
@description:  	constructor to fetch the label and value of the picklist 
**********************************************************************************************/
        public SelectOptionWrapper(String label, String value)
        {
            this.value = value;
            this.label = label;
        }
    }
}