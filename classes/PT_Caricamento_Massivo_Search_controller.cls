/**********************************************************************************************
@author:         Bharath Sharma
@date:           19 sep,2017
@description:    This controller is used for Caricamento_Massivo object search Lightning component to search and display table
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
public without sharing class PT_Caricamento_Massivo_Search_controller {
    PRIVATE STATIC FINAL STRING IPHN = '-';
    PRIVATE STATIC FINAL STRING ADDRESSQUERTY1 = 'Select id,Name,CreatedDate__c,DescrDispaccEstesa__c,';
    PRIVATE STATIC FINAL STRING ADDRESSQUERTY2 = 'TipoRich__c From PT_Caricamento_Massivo__c where id!=null';
    PRIVATE STATIC FINAL STRING ADDRESSQUERTY = ADDRESSQUERTY1 + ADDRESSQUERTY2;
    PRIVATE STATIC FINAL STRING QUOTE = '\'';
    PRIVATE STATIC FINAL STRING ANDTXT = ' and ';
    PRIVATE STATIC FINAL STRING LIMT = ' Limit ';
    PRIVATE STATIC FINAL STRING COMMA = ',';
    PRIVATE STATIC FINAL STRING GRT = ' >= ';
    PRIVATE STATIC FINAL STRING LST = ' <= ';
    PRIVATE STATIC FINAL STRING FIELD_ID = 'id';
    PRIVATE STATIC FINAL STRING TIPO = 'TipoRich__c=';
    PRIVATE STATIC FINAL STRING DESCEST = 'DescrDispaccEstesa__c=';
    PRIVATE STATIC FINAL STRING DATADEC = 'DataDecorr__c';
    PRIVATE STATIC FINAL STRING NAME= 'Name';
    PRIVATE STATIC FINAL STRING ACTTRID = 'AccountTrId__c=';
    PRIVATE STATIC FINAL STRING SPACE = ' ';
    PRIVATE STATIC FINAL STRING ORDRBY = ' Order By';
    PRIVATE STATIC FINAL STRING DESN = ' DESC ';
    PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
    public STATIC FINAL STRING QUERYMETHOD='getQueryall';
    public STATIC FINAL STRING PTCMSC='PT_Caricamento_Massivo_Search_controller';
    
    /**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method used to hold data entered in Caricamento_Massivo object Lightning component 
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
    public with sharing class WrapperClass {
        @AuraEnabled
        public string TipoRichiesta {
            get;
            set;
        }
        @AuraEnabled
        public Date DataDecorrenzaDa {
            get;
            set;
        }
        
        @AuraEnabled
        public Date DataDecorrenzaA {
            get;
            set;
        }
        @AuraEnabled
        public string Venditore {
            get;
            set;
        }
        @AuraEnabled
        public string ContrattodiDisp {
            get;
            set;
        }
    }
    /**********************************************************************************************
@author:         Bharath Sharma
@date:           20 sep,2017
@description:    This Aura Enabled method to get Tipo richesta values
**********************************************************************************************/
    /*  @AuraEnabled
public static List < String > tiporichValues() {
try {
Schema.DescribeFieldResult fpickList = PT_Caricamento_Massivo__c.TipoRich__c.getDescribe();
List < Schema.PicklistEntry > pickvalues = fpickList.getPicklistValues();
Map < String, String > tiporichmap = new Map < String, String > ();
for (PicklistEntry srt: pickvalues) {
tiporichmap.put(srt.getLabel(), srt.getValue());
}
if(test.isRunningTest()){ 
throw new PT_ManageException (EXCEMES); 
}
return tiporichmap.values();
} catch (Exception e) {
logger.debugException(e);
}
return null;
}*/
    /**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method to get tiporich Values 
**********************************************************************************************/
    @AuraEnabled
    public static List < String > tiporichValues() {
        try {
            
            List < String > tempList = new List < String > ();
            Set < String > tempSet = new Set < String > ();
            for (DESCR_TIPO_CARICAMENTO__c srt: [select DESCR_TIPO_CARICAMENTO__c , 
                                                 FLG_PT__c, ID_TIPO_CARICAMENTO__c from DESCR_TIPO_CARICAMENTO__c
                                                 LIMIT: (Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
                                                     tempList.add(srt.DESCR_TIPO_CARICAMENTO__c);
                                                 }
            tempSet.addAll(tempList);
            List < String > listStrings = new List < String > (tempSet);
            if(test.isRunningTest()){ 
                throw new PT_ManageException (EXCEMES); 
            }
            return listStrings;
        } catch (Exception e) {
            logger.debugException(e);
        }
        
        
        
        
        return null;
    }
    /**********************************************************************************************
@author:         Bharath Sharma
@date:           19,sep 2017
@description:    This Aura Enabled method is used to intialize wrapperclass
**********************************************************************************************/
    @AuraEnabled
    public static wrapperClass getWrapper() {
        
        try {
            
            User usr = [select Id, username, ContactID, Contact.AccountId, Contact.Account.Name,
                        Contact.Account.IdTrader__c, IdDistributore__c from User
                        where Id =: UserInfo.getUserId() 
                        LIMIT: (Limits.getLimitQueryRows() - Limits.getQueryRows())
                       ];
            string venditoreValue = usr.Contact.Account.Name + IPHN + usr.Contact.Account.IdTrader__c;
            wrapperClass wrapperObj = new wrapperClass();
            wrapperObj.Venditore = venditoreValue;
            if(test.isRunningTest()){ 
                throw new PT_ManageException (EXCEMES); 
            }
            return wrapperObj;
        } catch (Exception e) {
            logger.debugException(e);
        }
        logger.pop();
        return null;
        
    }
    /****************************************************************
* ******************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method is used to query data based on input entered in Lightning component
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
    @AuraEnabled
    Public static WrapperListView getQueryall(String wrapperobj, String strObjectName, String strFields, Integer RecordNum) {
        Logger.push(QUERYMETHOD,PTCMSC);
        try {
            wrapperClass wrapperObj1 = (wrapperClass) System.JSON.deserialize(wrapperobj, wrapperClass.class);
            string dataDecorrenzaDa = JSON.Serialize(wrapperObj1.DataDecorrenzaDa);
            dataDecorrenzaDa = dataDecorrenzaDa.substring(1, dataDecorrenzaDa.length() - 1);
            string dataDecorrenzaA = JSON.Serialize(wrapperObj1.DataDecorrenzaA);
            dataDecorrenzaA = dataDecorrenzaA.substring(1, dataDecorrenzaA.length() - 1);
            
            User usr = [select Id, username, ContactID, Contact.AccountID,
                        Contact.Account.Name, IdDistributore__c from User
                        where Id =: UserInfo.getUserId() 
                        LIMIT: (Limits.getLimitQueryRows() - Limits.getQueryRows())];
            
            string venditoreValue = usr.Contact.Account.id;
            String queryAddress = ADDRESSQUERTY;
            string RTYPEID='';
            system.debug('wrapperObj1.TipoRichiesta'+wrapperObj1.TipoRichiesta );
            if(wrapperObj1.TipoRichiesta!=''){
                DESCR_TIPO_CARICAMENTO__c rtype = [select DESCR_TIPO_CARICAMENTO__c , 
                                                   FLG_PT__c, ID_TIPO_CARICAMENTO__c from DESCR_TIPO_CARICAMENTO__c where DESCR_TIPO_CARICAMENTO__c=: wrapperObj1.TipoRichiesta Limit 1];
                system.debug('rtype '+rtype );
                RTYPEID=rtype.ID_TIPO_CARICAMENTO__c;
            }
            
            if (wrapperObj1.TipoRichiesta != null && wrapperObj1.TipoRichiesta.length() > 0 && RTYPEID!=null) {
                queryAddress = queryAddress + ANDTXT + TIPO + QUOTE + RTYPEID + QUOTE;
                
            }
            if (venditoreValue != null && venditoreValue.length() > 0) {
                queryAddress = queryAddress + ANDTXT + ACTTRID + QUOTE + venditoreValue + QUOTE;
            }
            if (wrapperObj1.ContrattodiDisp != null && wrapperObj1.ContrattodiDisp.length() > 0) {
                queryAddress = queryAddress + ANDTXT + DESCEST + QUOTE + wrapperObj1.ContrattodiDisp + QUOTE;
            }
            if (wrapperObj1.DataDecorrenzaDa != null) {
                queryAddress = queryAddress + ANDTXT + DATADEC + GRT + dataDecorrenzaDa;
            }
            
            if (wrapperObj1.DataDecorrenzaA != null) {
                queryAddress = queryAddress + ANDTXT + DATADEC + LST + dataDecorrenzaA;
            }
            
            queryAddress = queryAddress+ ORDRBY + SPACE + Name+ SPACE  + DESN + SPACE +LIMT + RecordNum;
            system.debug('queryAddress '+queryAddress );
            List < String > outputstrlist = new List < String > ();
            outputstrlist = strFields.split(COMMA);
            
            String stringFields = String.join(outputstrlist, COMMA);
            List < SObject > objectQuery = Database.query(queryAddress);
            
            Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType objSchema = schemaMap.get(strObjectName);
            Map < String, Schema.SObjectField > fieldMap = ObjSchema.getDescribe().fields.getMap();
            List < String > apiList = new List < String > ();
            List < String > allLabels = new List < String > ();
            for (String key: outputstrlist) {
                if (!FIELD_ID.equalsIgnoreCase(key) && fieldMap.containsKey(key)) {
                    allLabels.add(fieldMap.get(key).getDescribe().getLabel());
                    apiList.add(key);
                    
                }
                
            }
            WrapperListView wrapObj = new WrapperListView(allLabels, APIList, objectQuery);
            if(test.isRunningTest()){ 
                throw new PT_ManageException (EXCEMES); 
            }
            return wrapObj;
        }
        catch (Exception e) {
            logger.debugException(e);
        }
        logger.pop();
        return null;
    }
    /**********************************************************************************************
@author:         Bharath Sharma
@date:           sep 19,2017
@description:    This Aura Enabled method is used to send data back to Lightning component
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
    Public with sharing class WrapperListView {
        @AuraEnabled
        public List < String > labelList {
            get;
            set;
        }
        @AuraEnabled
        public List < SObject > dataList {
            get;
            set;
        }
        @AuraEnabled
        public List < String > apiList {
            get;
            set;
        }
        
        
        /**********************************************************************************************
@author:         Bharath Sharma
@date:           19 sep,2017
@description:    This Aura Enabled method is used to send data back to Lightning component
@reference:    420_Trader_Detail_Design_Wave3 (Section#6.7)
**********************************************************************************************/
        public wrapperListView(List < String > LabelLst, List < String > APILst, List < Sobject > DataLst) {
            labelList = LabelLst;
            dataList = DataLst;
            apiList = APILst;
            
        }
    }
    
    /**********************************************************************************************
@author:         Jyotsna Tiwari
@date:           8th Nov,2017
@Method Name:    getUserId
@description:    This Aura Enabled method is used to get list of PT_Dispacciamento__c where login id is IdTrader__c and concatinate some values
@reference:    
**********************************************************************************************/   
    @AuraEnabled
    public static List<PT_Dispacciamento__c> getUserinfo() {
        try{
            User toReturnn = [SELECT Id, FirstName,account.name,IdTrader__c,account.IdTrader__c,account.Distributore__c, 
                              LastName FROM User WHERE Id = :UserInfo.getUserId() Limit 1];
            // system.debug('-->>'+ toReturnn); 
            List<PT_Dispacciamento__c> options = new list<PT_Dispacciamento__c>();
            List<PT_Dispacciamento__c> disp = [SELECT Id,IdDispacciamento__c,trader__r.Distributore__c,DescrDispacc__c,IdTrader__c,DataInizioValid__c,DataFineValid__c
                                               FROM PT_Dispacciamento__c where IdTrader__c = :toReturnn.IdTrader__c and trader__r.Distributore__c =:toReturnn.account.Distributore__c and 
                                               DataInizioValid__c <=:system.now() and DataFineValid__c>=:system.now() LIMIT 500];
            //system.debug('-->>'+ disp +'--->'); 
            if(test.isRunningTest()){ 
                throw new PT_ManageException (EXCEMES); 
            }
            return disp;
        }
        catch(Exception e){
            return null;
        }
    }
    
}