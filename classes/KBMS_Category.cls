global class KBMS_Category implements Comparable 
{
	public String uniqueName;
    public String label;
    public List<KBMS_SubCategory> subcategories; 
    public Integer articleCount;
    public Integer subcategoryCount;
    
    public KBMS_Category (String uniqueName, String label, List<KBMS_SubCategory> subcategories, Integer articleCount, Integer subcategoryCount)
    {
        this.uniqueName= uniqueName;
        this.label = label;
        this.subcategories = subcategories;
        this.articleCount = articleCount;
        this.subcategoryCount = subcategoryCount;
    }
    
    // Implement the compareTo() method
    global Integer compareTo(Object compareTo) 
    {
        KBMS_Category compareToCat = (KBMS_Category)compareTo;
        if (articleCount == compareToCat.articleCount)
        { 
            return 0;
        }
        if (articleCount < compareToCat.articleCount) //Descending
        {
            return 1;
        }
        return -1;
    }
}