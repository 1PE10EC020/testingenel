public class PT_Contributi_Page_Controller {
/*   
   @AuraEnabled
    Public static WrapperListView getQueryreqrecord(String strObjectName, String recordtypeName, String sectionName,String objid) {
    PT_Costi__c contributiDetail = null;
        try {
            
           contributiDetail = [Select id,RecordType.Name,name,ProtocolloRic__c,Cognome__c ,Nome__c, 
           RagioneSociale__c ,NumeroTelefono__c,NoteContatto__c,Sottostato__c,
           TempoEsecuzioneLavori__c,DataAccettazione__c,TipologiaIntervento__c,
           Totale__c,Fascia__c,EsitoRicalcolo__c,MessRicalcolo__c,MsgPredeterm__c,
           SendOutbound__c from PT_Costi__c where ProtocolloRic__c =: objid limit 1]; 
           
           string rTypeName=contributiDetail.RecordType.Name;
           
            List < Contributi_Page_Detail__c > customRec = new List < Contributi_Page_Detail__c > ();
            Set < String > tempSet = new Set < String > ();
            for (Contributi_Page_Detail__c ord: [select Name, Field_1_API_Name__c, Record_Type__c, Section_Name__c, Field_2_API_Name__c, Order__c, Field_1_Label__c, Field_2_Label__c from
                    Contributi_Page_Detail__c where Section_Name__c =: sectionName AND Record_Type__c =: rTypeName ORDER BY Order__c
                ]) {
                
                customRec.add(ord);


            }
        if(contributiDetail!=null){
            WrapperListView wrapObj = new WrapperListView(contributiDetail,customRec);
            system.debug('wrapObj' + wrapObj);
            return wrapObj;
        }
        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
    Public with sharing class WrapperListView {
        @AuraEnabled
        public List < String > Field_1_API_Name {
            get;
            set;
        }
        @AuraEnabled
        public PT_Costi__c Costi_Rec{
            get;
            set;
        }
        @AuraEnabled
        public List < String > Field_2_API_Name {
            get;
            set;
        }
        @AuraEnabled
        public List < Contributi_Page_Detail__c > Field_Rec {
            get;
            set;
        }

        @AuraEnabled
        public List < String > Field_2_Label {
            get;
            set;
        }

        /**********************************************************************************************
            @author:         Bharath Sharma
            @date:           26 July,2017
            @description:    This Aura Enabled method is used to send data back to Lightning component
            @reference:    420_Trader_Detail_Design_Wave1 (Section#4.2.2)
            **********************************************************************************************/
       /* public wrapperListView(PT_Costi__c CDetail,List < Contributi_Page_Detail__c > fieldrec) {
            Costi_Rec=CDetail;
            Field_Rec=fieldrec;


        }
    }*/
    
    @AuraEnabled
    public static List<Sobject>  attachmentDetails(string strObjectName,string fieldname, string parentId){
        List<Sobject> attachmentDetailsList = new List<Attachment>(); 
        List<ID> attachmentIDList = new List<Id>();            

            try{
                     attachmentDetailsList = [select id,name from Attachment
                                                  where Parentid=:parentId];
                                                   for(integer i=0;i<=attachmentDetailsList.size()-1;i++){
                                                   attachmentIDList.add(attachmentDetailsList[i].id);
                         return attachmentDetailsList ;
                                  
               }
             
                                                  
            }
            catch(Exception e){
                return null;
            }
        
        
        return attachmentDetailsList ;
    } 

    
}