@IsTest
public class WSCreateCase_Test {

	 @testSetup
  	private static void setup() {
	  	Account acc = new Account();
	  	acc.Nome__c = 'Claudio' ; 
	    acc.Name = 'Quaranta';
	    acc.codicefiscale__c  ='QRNCLD85B26F839L';
	    acc.phone='12312312312';
	    insert acc;
  		
  		case parentCase = new Case ();
  		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        insert c;
  		
  		percorsoTelefonico__c per = new percorsoTelefonico__c();
  		per.CodIVR__c  = '12C';
  		per.Contesto__c ='Misura';
  		per.MenuSecondoLivello__c ='test';
  		per.Scelta__c ='scelta';
  		insert per;
  	}
  	
  	@isTest
    static void testCaseCreation (){
    	
    	// case whit minimal information 
    	WSCreateCase.CallbackCase input = new WSCreateCase.CallbackCase();
    	input.CONNID='123123';
		input.PHONE='320998877';
		input.PHONE_CB='321998877';
		input.CF='';
		input.POD='';
		input.CASE_NUM='';
		input.ID_PR='123123';
		input.COD_IVR='12C';
		input.FASCIA_ORARIA='1';
		WSCreateCase.CallbackCaseResponse response= WSCreateCase.CreateCallBackCase(input);
		//system.assertEquals(response.codice,'000');
    	//system.assertEquals(response.codice,'100');
    	// case whit account associated 
    	input.CONNID='321';
		input.PHONE='320998877';
		input.PHONE_CB='321998877';
		input.CF='QRNCLD85B26F839L';
		input.POD='ITA00001';
		input.CASE_NUM=[select casenumber from case limit 1].casenumber;
		input.ID_PR='123123';
		input.COD_IVR='12C';
		input.FASCIA_ORARIA='3';
		response= WSCreateCase.CreateCallBackCase(input);
    	//system.assertEquals(response.codice,'000');
    	
    	// case with pod in input
    	FOURTypes.DashboardArrReplay_element expetedResponse = Four_Helper_Test.generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'Lazio Abruzzo e Molise','ROMA ESTERNA-RIETI', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
    	Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
    	input.CONNID='123123';
		input.PHONE='320998877';
		input.PHONE_CB='321998877';
		input.CF='QRNCLD85B26F839L';
		input.POD='ITA00001';
		input.CASE_NUM=[select casenumber from case limit 1].casenumber;
		input.ID_PR='123123';
		input.COD_IVR='12C';
		input.FASCIA_ORARIA='2';
		response= WSCreateCase.CreateCallBackCase(input);
    	//system.assertEquals(response.codice,'000');
    	
    }

}