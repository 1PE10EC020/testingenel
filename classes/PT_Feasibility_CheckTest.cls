/*********************************************************************************************
@Author : Priyanka Sathyamurthy
@date : 31 Aug 2017
@description : Test class for PT_Feasibility_Check.
**********************************************************************************************/
@isTest(SeeAllData = true)
private class PT_Feasibility_CheckTest {
      private static String errMsg;
      private static final String ALIAS = 'unEx123';
      private static final String SVALUE='S';
  /* 
    Author : Priyanka Sathyamurthy
    CreatedDate : 30/08/2017
    Description : createRequestRecord is a method for creation of Request record
*/  
private static testMethod void checkProcessTest(){    
  	User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
    Four_Comp_Fornit__c fornit = PT_TestDataFactory.createFornitRecord();
    PT_Richiesta__c request = PT_TestDataFactory.createRequest();
    Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_I01').getRecordTypeId();
    request.RecordTypeId=recordtypeid;
    request.IdTipoRichAEEG__c='I01';
    request.AccountDisId__c=null;
    request.AccountTrId__c=null;
   	request.IdRichTrader__c=SVALUE;
    request.AmmissDescrizione__c=null;
    request.IdMessaggioPT__c='0';
    request.Stato__c='INS';
    request.MandConnessione__c='S';
    request.PotImp__c='3';
    request.Tensione__c='BM';
    system.assertEquals('S', request.IdRichTrader__c);
    database.insert(request);
    
    	PT_Richiesta__c request1 = PT_TestDataFactory.createRequest();
        Id recordtypeid1 = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_I01').getRecordTypeId();
        request1.RecordTypeId=recordtypeid1;
    	request1.IdRichTrader__c=SVALUE;
    	request1.AmmissDescrizione__c=null;
    	request1.IdTipoRichAEEG__c='I01';
        request1.IdMessaggioPT__c='0';
    	request1.Stato__c='INS';
    	request1.MandConnessione__c='S';
    	request1.PotImp__c='3';
    	request1.Tensione__c='BM';
    	system.assertEquals('S', request.IdRichTrader__c);
        database.insert(request1);
    
        List<Id> reqId=new List<Id>();
        reqId.add(request.Id);
        //database.insert(request);
        PT_Feasibility_Check__c fesCheck= new PT_Feasibility_Check__c();
        fesCheck=[SELECT Id,Name,Controllo_Rich_Duplicato__c,Controllo_Trader_Moroso__c,Controllo_Comp_Fornit__c 
                   FROM PT_Feasibility_Check__c where Name=:'PT_I01' limit 1];
    	fesCheck.Controllo_Comp_Fornit__c=true;
    	database.update(fesCheck);
    system.runAs(runningUser){
               PT_Feasibility_Check.checkProcessConditions(reqId);
    }
               //PT_Feasibility_Check.controllo_Rich_Duplicato(request);
               //PT_Feasibility_Check.Controllo_Trader_Moroso(request);
               //PT_Feasibility_Check.Controllo_Comp_Fornit(request);
        //PT_Feasibility_Check.Cntrl_Comp_Fornit_2(request);
    
}


  /* 
    Author : Priyanka Sathyamurthy
    Apex Method : Cntrl_Comp_Fornit
    CreatedDate : 30/08/2017
    Description : Cntrl_Comp_Fornit is a method for PT_Feasibility_Check
*/  
private static testMethod void cntrl_Comp_FornitTest(){    
  		User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();
        //Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_I01').getRecordTypeId();
       // request.RecordTypeId=recordtypeid;
    	request.PotImp__c='30';
    	request.Tensione__c='MT';
    	request.IdUsoEnergia__c='012';
       // database.insert(request);
     system.runAs(runningUser){   
              PT_Feasibility_Check.Controllo_Comp_Fornit(request);
     }        
    request.PotImp__c=null;
    request.IdTipoContr__c='ILL';
    system.runAs(runningUser){
              PT_Feasibility_Check.Controllo_Comp_Fornit(request);
    }         
    request.IdUsoEnergia__c='004';
    request.Tensione__c='BT';
    system.runAs(runningUser){
              PT_Feasibility_Check.Controllo_Comp_Fornit(request);
    }         
	request.PotImp__c='35';
    request.IdTipoContr__c='ALT';
    request.IdUsoEnergia__c=null;
    system.runAs(runningUser){
              PT_Feasibility_Check.Controllo_Comp_Fornit(request);
    }
    request.PotImp__c='20';
    request.PotDisp__c=null;
    system.runAs(runningUser){
              PT_Feasibility_Check.Controllo_Comp_Fornit(request);
    }      
    system.assertEquals('S', request.FLG_POTENZA__c);
}
/* 
    Author : Priyanka Sathyamurthy
    Apex Method : Controllo_Trader_Fornit1ConCheck
    CreatedDate : 13/10/2017
    Description : Controllo_Trader_Fornit1ConCheck is a method for PT_Feasibility_Check to test Controllo_Trader_Fornit1 method
*/  
    private static testMethod void controllo_Trader_Fornit1ConCheck(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
      	PT_Richiesta__c request = PT_TestDataFactory.createRequest();
    	request.PotImp__c='20';
        request.IdTipoContr__c='ALT';
    	request.Tensione__c='BM';  
        request.PotDisp__c=17;
        system.runAs(runningUser){
        PT_Feasibility_Check.Controllo_Comp_Fornit(request);
        }
        request.IdTipoContr__c='ILL';
        request.Tensione__c=null;
        request.IdUsoEnergia__c='004';
        request.PotImp__c=null;
        request.PotDisp__c=null;
        system.runAs(runningUser){
        PT_Feasibility_Check.Controllo_Comp_Fornit(request);
        }
        system.assertEquals('N', request.FLG_POTENZA__c);
        
        request.IdTipoContr__c=null;
        request.IdUsoEnergia__c='002';
        system.runAs(runningUser){
        PT_Feasibility_Check.Controllo_Comp_Fornit(request);
        }
    }
/* 
    Author : Priyanka Sathyamurthy
    Apex Method : Controllo_Trader_Fornit2Check
    CreatedDate : 13/10/2017
    Description : Controllo_Trader_Fornit2Check is a method for PT_Feasibility_Check to test Controllo_Trader_Fornit2 method
*/  
    private static testMethod void controllo_Trader_Fornit2Check(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        Four_Comp_Fornit__c fornit = PT_TestDataFactory.createFornitRecord();
        database.insert(fornit);
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();
        request.MandConnessione__c='S';
        request.IdTipoRichAEEG__c='N02';
        request.IdConnessione__c='TEM';
        request.PotImp__c='3';
        request.IdUsoEnergia__c=null;
        request.FlagForfait__c='S';
        database.insert(request);
        system.assertEquals('TEM', request.IdConnessione__c);
        PT_Feasibility_Check.Cntrl_Comp_Fornit_2(request);
        
        request.PotImp__c=null;
        request.PotDisp__c=20.00;
        database.update(request);
        system.runAs(runningUser){
        PT_Feasibility_Check.Cntrl_Comp_Fornit_2(request);
        }     
    }

   /* 
    Author : Priyanka Sathyamurthy
    Apex Method : Controllo_Trader_Moroso
    CreatedDate : 30/08/2017
    Description : Controllo_Trader_MorosoTest is a method for PT_Feasibility_Check to test Controllo_Trader_Moroso
*/  
private static testMethod void controllo_Trader_MorosoTest(){  
    	User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
    	database.insert(runningUser);
    
  		PT_Richiesta__c request = PT_TestDataFactory.createRequest();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_I01').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
    	request.AccountDisId__c=null;
    	request.AccountTrId__c=null;
    	request.MandConnessione__c='S';
    	database.insert(request);
    
    	PT_Trasporto__c trans=PT_TestDataFactory.createTransportoRecord();
    	database.insert(trans);
    	system.assertEquals(trans.Trader__c, request.AccountTrId__c);
    
    	system.runAs(runningUser){
              PT_Feasibility_Check.Controllo_Trader_Moroso(request);
             }
    	//system.assertEquals('RES', request.Stato__c);
	}
}