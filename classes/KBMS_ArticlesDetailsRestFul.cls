@RestResource(urlMapping='/getArticleDetails/*') 
global with sharing class KBMS_ArticlesDetailsRestFul
{
    Public Static Map<String, String> articleTypeFieldMaps = new Map<String, String>
    {
        'FAQ' => 'Summary, Title, UrlName, Id, KnowledgeArticleId, LastModifiedDate, ' +
                'Allegati__ContentType__s, Allegati__Length__s, Allegati__Name__s, QUESITO__c, RISPOSTA__c ',
        
         'Processi' => 'Summary, Title, UrlName, Id, KnowledgeArticleId, LastModifiedDate, ' +
                ' Allegati__ContentType__s, Allegati__Length__s, Allegati__Name__s, Bacino_utenza__c, Dettaglio_Problematiche__c, Descrizione_Servizi__c',
        
        
        'Moduli' => 'Summary, Title, UrlName, Id, KnowledgeArticleId, LastModifiedDate, Allegati__ContentType__s, Allegati__Length__s, Allegati__Name__s'
    };
    
    @HttpGet
    global static ArticleWrapper getArticleDetails() 
    {
        String articleId = RestContext.request.params.get('articleId');
        String articleType = RestContext.request.params.get('articleType');
        
        String language = Label.KBMS_Language;
        
        KnowledgeArticleVersion knowledgeArticle = [SELECT Id FROM KnowledgeArticleVersion WHERE KnowledgeArticleId =: articleId AND PublishStatus = 'online'];
        System.debug('Query knowledgeArticle' +knowledgeArticle);
        // adding for like and dislike
        Map<Id,Integer> mapTotalUpVote=new Map<Id,Integer>();
        Map<Id,Integer> mapTotalDownVote=new Map<Id,Integer>(); 
        String articleParentType=articleType+'__ka'; 
        
        List<AggregateResult> groupedResults= [select Type,Count(Id)typecount,ParentId  from vote WHERE Parent.type=:articleParentType  Group By ParentId,Type];
                 
        for (AggregateResult ar : groupedResults)  {
            
            System.debug('ParentID--' + ar.get('ParentId'));
            System.debug('count-type--' + ar.get('typecount'));
            System.debug('type--' + ar.get('Type'));
            if(ar.get('Type')=='Up'){
                
                mapTotalUpVote.put((Id)ar.get('ParentId'),(Integer)ar.get('typecount')); 
                
            }else if(ar.get('Type')=='Down'){
                
                mapTotalDownVote.put((Id)ar.get('ParentId'),(Integer)ar.get('typecount'));
            }
            
        } 
        // code ended           
        String kavId = knowledgeArticle.Id;
        
        String firstPartQuery = 'SELECT ' + articleTypeFieldMaps.get(articleType) + ' FROM ' + articleType + '__kav';
        String secondPartQuery = ' WHERE publishStatus=\'online\' and Language =:language and Id =: kavId UPDATE VIEWSTAT';
          
        
        String query = firstPartQuery + secondPartQuery;
        
        system.debug('@@--query'+query );
        
        sObject res = Database.query(query);
        system.debug('@@--res'+res);
       
        
        List<Vote> votes = [SELECT Id, ParentId, Type FROM Vote WHERE ParentId =: articleId and CreatedById =: UserInfo.getUserId()];
        
        
        ArticleWrapper aw= new ArticleWrapper ();
        aw.article = res;
        
        if(mapTotalUpVote.containsKey(articleId)){
            
            aw.totalLikes=mapTotalUpVote.get(articleId);
        }
        else{
            aw.totalLikes=0;
        }
        if(mapTotalDownVote.containsKey(articleId)){
            
            aw.totalDislikes=mapTotalDownVote.get(articleId); 
        }
        else{
            aw.totalDislikes=0;
        }
       
        aw.attachmentUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/fileField?entityId=' + articleId + '&field=Allegati__Body__s';
        if (votes.size() > 0)
        {
            aw.userInput =  votes[0].type== 'Up' ? 'Like' : 'Dislike';
        }
        
        return aw;
        
    }
    ///services/apexrest/getArticleDetails?articleId=kA0260000000ALN&articleType=FAQ    
    global class ArticleWrapper
    {
        public sObject article;
        public String attachmentUrl;
        public String userInput;
        public Integer totalLikes;
        public Integer totalDislikes;
       
    } 
}