public class ED_Case_Technical_Data_ControllerX {

    Case mycase;
    string oldPod;
    string statusCase;
    string tipologiaCase;
    //public boolean pageProgetti {get;set;}
    public boolean toDisplayEmm {get;set;}
   // public String famiglia {get;set;}
   public Boolean refreshPage { get; set; }
   public ApexPages.StandardController stdController { get; set; }
    
   public string relatedWhereCondition {get;set;}
    
    public ED_Case_Technical_Data_ControllerX  (ApexPages.StandardController controller){
        if(!test.isRunningTest())
            controller.addFields(new List<String> {'Famiglia__c','TipologiaCase__c','DirezioneDTR__c','POD__c','IdPratica__c','DirezioneDTR__c','CognomeTitolarePOD__c','UnitaDiCompetenza__c','NomeTitolarePOD__c','LivelloTensioneDiFornitura__c','CodiceFiscaleTitolarePOD__c','Oraria__c','PartitaIVATitolarePOD__c','caseNumber','Account.RecordTypeId','RecordType.developerName','pod__c','Account.codicefiscale__c','Account.PartitaIVA__c','IdPratica__c','CF_Search_DS__c','PIVA_Search_DS__c','status'});
        mycase =(case) controller.getRecord();
        oldPod = mycase.pod__c;
        statusCase = mycase.status;
        tipologiaCase = mycase.TipologiaCase__c;
        toDisplayEmm = false;
       // famiglia = mycase.Famiglia__c;
       // ED_EMMDashbord_Extension_Component.famiglia = famiglia;
        //system.debug(logginglevel.Debug, 'ED_Case_Technical_Data_ControllerX -  mycase.Famiglia__c '+mycase.Famiglia__c);
        if ( Constants.CASE_FAMIGLIA_MOBILITA.equals(mycase.Famiglia__c))
        {
            toDisplayEmm = true;
        }
        
        
        
        if (!Constants.CASE_PROGETTI.equalsIgnoreCase(tipologiaCase))
        {
            //pageProgetti = true;
            if (!String.isBlank(mycase.PartitaIVATitolarePOD__c) )
                relatedWhereCondition = ' Where DelegatorFiscalCode__c=\''+mycase.CodiceFiscaleTitolarePOD__c+'\' LIMIT 10';

            else
                if (!String.isBlank(mycase.CodiceFiscaleTitolarePOD__c) )
                    relatedWhereCondition = ' Where DelegatorVATCode__c=\''+mycase.PartitaIVATitolarePOD__c+'\' LIMIT 10';

            // refreshinfo(mycase.id);
        }
        else
        {
            //pageProgetti = false;
        }
            
    system.debug(logginglevel.Debug, 'ED_Case_Technical_Data_ControllerX -  toDisplayEmm '+toDisplayEmm);
    }

    public void saveC(){
        //mycase.pod__c= pod;
        //if ( oldpod != null  &&  String.isNotBlank(mycase.pod__c))
        //  if (!oldPod.equals(mycase.pod__c) )
        //statusCase = [select status from case where id=:mycase.Id].status;
        system.debug(' STATUS CASE: '+statusCase);
        Four_Helper.Arricchisci_Case_Response serviceResponse;
        
        refreshPage = false;
        System.debug('*** page'+ApexPages.currentPage());
        
        if( String.isNotBlank(mycase.pod__c)){
        mycase.pod__c = mycase.pod__c.touppercase();
            try {
                 serviceResponse= Four_Helper.Arricchisci_Case(mycase);
                 /*
                 PageReference pRef = stdController.save();

		        if (pRef != null) {
		            // set this value last, after successful saving to cause the VF page to show the javascript block
		            refreshPage = true;
		        }
		        */
                
            }
            catch(Exception e){
                system.debug(logginglevel.error,'exception in ED_Case_Technical_DAta : '+e.getMessage() );
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Attenzione : comunicazione con i sistemi esterni non riuscita, riprovare più tardi'));
            }
            if ( serviceResponse.esito){
                mycase = serviceResponse.myCase;
                if (myCase != null && String.isblank(myCase.LivelloTensioneDiFornitura__c))
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,System.label.EXC_POD_NOT_FOUND));
                    
                    
            }
            else{
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,serviceResponse.ErrorMessage));
                sbiancaCampiCase(myCase);
            }
         }
        if(mycase != null && String.isBlank(mycase.pod__C)){
            sbiancaCampiCase(myCase);
        }
        if( myCase != null)
        {
            system.debug(' stato del case: '+mycase.Status);
            if (!Constants.CASE_STATUS_CHIUSO.equals(statusCase))
            {
                try{
                	/*[GP 28102016 S00000024 Modifica Upper case Cod fisc Deleghe ]*/
                	mycase.CodiceFiscaleTitolarePOD__c = mycase.CodiceFiscaleTitolarePOD__c.touppercase();
                	system.debug(' *** CodiceFiscaleTitolarePOD__c: '+mycase.CodiceFiscaleTitolarePOD__c);
                	/*END GP 28102016 S00000024]*/
                    update mycase;
                }
                catch(Exception e){
                    system.debug(logginglevel.error,'exception in ED_Case_Technical_DAta : '+e.getMessage() );
                    //Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Attenzione, è necessario accettare il Case prima di poterlo modifcare'));                 
                }
            }        
            else
            {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,System.label.EXC_ILLEGAL_CASE_CLOSED));          
            }
            if (!String.isBlank(mycase.PartitaIVATitolarePOD__c) )
                relatedWhereCondition = ' Where DelegatorVATCode__c=\''+mycase.PartitaIVATitolarePOD__c+'\' LIMIT 10';
            
            else
                if (!String.isBlank(mycase.CodiceFiscaleTitolarePOD__c) )
                    relatedWhereCondition = ' Where DelegatorFiscalCode__c=\''+mycase.CodiceFiscaleTitolarePOD__c+'\' LIMIT 10';
                
                
        }
    }
    
    private void sbiancaCampiCase ( Case myCase){
            mycase.CodiceFiscaleTitolarePOD__c='';
            mycase.NomeTitolarePOD__c ='';
            mycase.CognomeTitolarePOD__c='';
            mycase.PartitaIVATitolarePOD__c='';
            mycase.UnitaDiCompetenza__c='';
            mycase.DirezioneDTR__c='';
            mycase.Oraria__c = false;
            mycase.LivelloTensioneDiFornitura__c ='';
            mycase.Livello1__c = '';
            mycase.Livello2__c = '';
            mycase.Livello3__c = '';
    }
}