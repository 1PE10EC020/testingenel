public class RecordTypeController {
    public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues(){
        List<Schema.RecordTypeInfo> recordtypes = Account.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes){
            if(rt.getName() != 'Master')
            recordtypemap.put(rt.getRecordTypeId(), rt.getName());
        }        
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel){
        Id recid = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }  
    
    @AuraEnabled        
    public static List<String> fetchRecordTypeValuesReq(){
        List<Schema.RecordTypeInfo> recordtypes = PT_Richiesta__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes){
            if(rt.getName() != 'Master'){
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
                
            }
        }       
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeIdReq(String recordTypeLabel){
        Id recid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    } 
    
    @AuraEnabled
    public static PT_Richiesta__c CreateNewRichiesta (PT_Richiesta__c req) {
    system.debug('>>>> JT REQ : ' + req);
        upsert req;
        system.debug('>>>> JT upserted REQ: ' + req);
        return req;
    }
       
}