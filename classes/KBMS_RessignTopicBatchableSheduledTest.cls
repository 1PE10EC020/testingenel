@isTest(seeAllData=true)
public class KBMS_RessignTopicBatchableSheduledTest {

//public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    public Static testMethod void testWithDataCategory_executeMeth(){
       list<FAQ__kav> lsttst = new  list<FAQ__kav>();
       
           FAQ__kav faqtst = new FAQ__kav();
            faqtst.Title = 'TestFAQ';
            faqtst.Summary = 'KB Summary ';
            faqtst.URLName = 'TestFAQ';                
            faqtst.QUESITO__c='testno';
            faqtst.RISPOSTA__c='testno';
            faqtst.Language=Label.KBMS_Language;
        insert faqtst;              
        lsttst.add(faqtst);
        
                
        String dataCategory='Relazione_con_il_Cliente';               
        String GroupDataCategory='KBMS_Operai_Tecnici';             
        String articleType='FAQ';       
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtst.Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;
        
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtst.Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        PageReference pageRef = new PageReference('/'+faqtst.Id);
        Test.setCurrentPage(pageRef);
        
        
     Test.startTest();
        KBMS_RessignTopicBatchableSheduled b = new KBMS_RessignTopicBatchableSheduled();
      //  b.execute(null);                
        database.executebatch(b);               
       // b.finish(null);            
        Test.stopTest();    
    }
}