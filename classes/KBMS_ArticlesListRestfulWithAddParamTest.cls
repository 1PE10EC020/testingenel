/*
 * @author Atos India Pvt Ltd.
 * @date  Sept,2016
 * @description This class is test class of KBMS_ArticlesListRestfulWithAddParam
 */
@isTest(seeAllData = true)
public class KBMS_ArticlesListRestfulWithAddParamTest {
    
    static testMethod void testMethodOrderByLMD() { 
        String lang= Label.KBMS_Language;
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
            for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
         
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        
        FAQ__DataCategorySelection datacatTest1=new FAQ__DataCategorySelection();
        datacatTest1.ParentId= faqtest[0].Id;     
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1;
        
        FAQ__kav insertedTestArticle = [SELECT Id,KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
             PageReference pageRef = new PageReference('/'+faqtest[0].Id);

            Test.setCurrentPage(pageRef);

        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM FAQ__kav';
        String secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:lang';
        String thirdPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        
        
        String query = firstPartQuery + secondPartQuery + thirdPartQuery;
        
        List<sObject> knwoledgesObject = Database.query(query);
        system.debug('query*************:'+knwoledgesObject.size());
            
        Test.startTest();
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper aw = new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        aw.articles=knwoledgesObject[0];
        aw.likeScore=4.0;
        aw.dataCategoryName=dataCategory;
             
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
 
            datacate.categoryName='test9';    
                
       // String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM FAQ__kav where PublishStatus = \'online\' AND Language =:lang';
                
        
        //List<sObject> knwoledgesObject = Database.query(firstPartQuery);
        //sObject sObjKAV 
        
        
        //FAQ__kav sObjKAV = new FAQ__kav(Id=insertedTestArticle.Id);
        //insert sObjKAV;    
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList(); 
        
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByLMD2() { 
        String lang= Label.KBMS_Language;
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
            for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
         
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT Id,KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
             PageReference pageRef = new PageReference('/'+faqtest[0].Id);

            Test.setCurrentPage(pageRef);

        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','test test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM FAQ__kav';
        String secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:lang';
        String thirdPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        
        
        String query = firstPartQuery + secondPartQuery + thirdPartQuery;
        
        List<sObject> knwoledgesObject = Database.query(query);
        system.debug('query*************:'+knwoledgesObject.size());
            
        Test.startTest();
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper aw = new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        aw.articles=knwoledgesObject[0];
        aw.likeScore=4.0;
        aw.dataCategoryName=dataCategory;
             
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
 
            datacate.categoryName='test9';    
                
       // String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM FAQ__kav where PublishStatus = \'online\' AND Language =:lang';
                
        
        //List<sObject> knwoledgesObject = Database.query(firstPartQuery);
        //sObject sObjKAV 
        
        
        //FAQ__kav sObjKAV = new FAQ__kav(Id=insertedTestArticle.Id);
        //insert sObjKAV;    
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList(); 
        
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByAlphabetic() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','Alphabetic');
        RestRequestobject.addParameter('textSearch','test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByAlphabetic2() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','Alphabetic');
        RestRequestobject.addParameter('textSearch','test test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByLike() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','Like');
        RestRequestobject.addParameter('textSearch','test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByLike2() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','Like');
        RestRequestobject.addParameter('textSearch','test test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByView() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','View');
        RestRequestobject.addParameter('textSearch','test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
    static testMethod void testMethodOrderByView2() {       
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        
         for(Integer i=0;i<faqtest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType',articleType);
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0');
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','View');
        RestRequestobject.addParameter('textSearch','test test');    
         
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
         
            
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper art= new KBMS_ArticlesListRestfulWithAddParam.ArtcileWrapper();
        KBMS_ArticlesListRestfulWithAddParam.dataCategories datacate= new KBMS_ArticlesListRestfulWithAddParam.dataCategories();    
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3;    
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');    
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();
        RestRequestobject.addParameter('orderBy','Alphabetic');
 
            datacate.categoryName='test9';    
        
            
            
            
            
        KBMS_ArticlesListRestfulWithAddParam.getArticlesList();    
        Test.stopTest();     

    }
}