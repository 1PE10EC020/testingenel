public class InitlizeAuraComp
{
    @AuraEnabled 
    public Map<String,List<SelectOptionWrapper>> pickListValues;
    
    @AuraEnabled 
    public Sobject sObj;
    
       @AuraEnabled 
    public Sobject sObj1;
        
    @AuraEnabled 
    public boolean isRegulatedMarket ;
    
    public InitlizeAuraComp()
    {
       pickListValues = new  Map<String,List<SelectOptionWrapper>>();
       isRegulatedMarket = false;
    }
    public without sharing class SelectOptionWrapper
    {
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
        //constructor
        public SelectOptionWrapper(String label, String value)
        {
            this.value = value;
            this.label = label;
        }
    }
    
}