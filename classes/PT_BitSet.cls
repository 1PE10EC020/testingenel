/**********************************************************************************************
@author:        Sushant Yadav
@date:          26 Jun,2017
@description:   This class is used to select the picklist values based on the alphanumeric code and
				base64charcode and convert it to bytes.
**********************************************************************************************/
public without sharing class PT_BitSet {
    
 public static final String PLUS = '+';
 public static final String SLASH = '/';
 public static final String PT_BITSET = 'PT_BitSet';
 public static final String LOADCHARCODES = 'LoadCharCodes';
 public static final String TESTBITS = 'testBits';   
    
 public Map < String, Integer > alphaNumCharCodes {
  get;
  set;
 }
 public Map < String, Integer > base64CharCodes {
  get;
  set;
 }
 
/**********************************************************************************************
@author:        Sushant Yadav
@date:          26 Jun,2017
@description:   This constructor is used to call the LoadCharCodes() method
**********************************************************************************************/
 public PT_BitSet() {
  LoadCharCodes();
 }

/**********************************************************************************************
@author:        Sushant Yadav
@date:          26 Jun,2017
@Method Name:   LoadCharCodes
@description:   Method loads the character codes for all letters
**********************************************************************************************/ 
 public void LoadCharCodes() {
Logger.push(LOADCHARCODES,PT_BITSET);
     try{     
  alphaNumCharCodes = new Map < String, Integer > {
   'A' => 65,
   'B' => 66,
   'C' => 67,
   'D' => 68,
   'E' => 69,
   'F' => 70,
   'G' => 71,
   'H' => 72,
   'I' => 73,
   'J' => 74,
   'K' => 75,
   'L' => 76,
   'M' => 77,
   'N' => 78,
   'O' => 79,
   'P' => 80,
   'Q' => 81,
   'R' => 82,
   'S' => 83,
   'T' => 84,
   'U' => 85,
   'V' => 86,
   'W' => 87,
   'X' => 88,
   'Y' => 89,
   'Z' => 90,
   '/' => 47
  };
  base64CharCodes = new Map < String, Integer > ();
  //all lower cases
  Set < String > pUpperCase = alphaNumCharCodes.keySet();
  for (String pKey: pUpperCase) {
   //the difference between upper case and lower case is 32
   alphaNumCharCodes.put(pKey.toLowerCase(), alphaNumCharCodes.get(pKey) + 32);
   //Base 64 alpha starts from 0 (The ascii charcodes started from 65)
   base64CharCodes.put(pKey, alphaNumCharCodes.get(pKey) - 65);
   base64CharCodes.put(pKey.toLowerCase(), alphaNumCharCodes.get(pKey) - (65) + 26);
  }
  //numerics
  for (Integer i = 0; i <= 9; i++) {
   alphaNumCharCodes.put(string.valueOf(i), i + 48);
   //base 64 numeric starts from 52
   base64CharCodes.put(string.valueOf(i), i + 52);
  }
     alphaNumCharCodes.put(PLUS, 43);
alphaNumCharCodes.put(SLASH, 47);
base64CharCodes.put(PLUS, 62);
base64CharCodes.put(SLASH, 63);
     }
     catch(Exception e){
           logger.debugException(e);   
        }
     logger.pop();
 }
 
/**********************************************************************************************
@author:        Sushant Yadav
@date:          26 Jun,2017
@Method Name:   testBits
@description:   Method loads the character codes for all letters
**********************************************************************************************/ 
 public List < Integer > testBits(String pValidFor, List < Integer > nList) {
Logger.push(TESTBITS,PT_BITSET);
     try{  
  List < Integer > results = new List < Integer > ();
  List < Integer > pBytes = new List < Integer > ();
  Integer bytesBeingUsed = (pValidFor.length() * 6) / 8;
  Integer pFullValue = 0;
  if (bytesBeingUsed <= 1)
   return results;
  for (Integer i = 0; i < pValidFor.length(); i++) {
   pBytes.Add((base64CharCodes.get((pValidFor.Substring(i, i + 1)))));
  }
  for (Integer i = 0; i < pBytes.size(); i++) {
   Integer pShiftAmount = (pBytes.size() - (i + 1)) * 6; //used to shift by a factor 6 bits to get the value
   pFullValue = pFullValue + (pBytes[i] << (pShiftAmount));
  }
 
  Integer bit=0;
  Integer targetOctet=0;
  Integer shiftBits=0;
  Integer tBitVal=0;
  Integer n=0;
  Integer nListSize = nList.size();
  for (Integer i = 0; i < nListSize; i++) {
   n = nList[i];
   bit = 7 - (Math.mod(n, 8));
   targetOctet = (bytesBeingUsed - 1) - (n >> bytesBeingUsed);
   shiftBits = (targetOctet * 8) + bit;
   tBitVal = ((Integer)(2 << (shiftBits - 1)) & pFullValue) >> shiftBits;
   if (tBitVal == 1)
    results.add(n);
  }
  return results;
 }
      catch(Exception e){
           logger.debugException(e);   
        } 
        logger.pop();
     	return null;
}    
}