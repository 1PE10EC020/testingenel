/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for PT_DatiVenditore.
**********************************************************************************************/
@isTest(SeeAllData = false)
private class PT_DatiVenditoreTest{
     PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING TESTS='test23';
  PRIVATE STATIC FINAL INTEGER LMT=1; 

   /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/
@testSetup static void createTestRecords() {
 Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
 User usr = new User(Alias = ALIAS, Email=EMAIL,
  EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
  LocaleSidKey=ENUS, ProfileId = pfl.Id,
  TimeZoneSidKey=TZK,
  UserName=UUNAME);
 database.insert(usr);
} 
    
    /**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Method to get user info for PT_DatiVenditore.
**********************************************************************************************/
    private static testMethod void userInfo(){
        //User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser) ;
        User usr=[Select id,name,LastName from User LIMIT: lmt];
  System.runAs(usr) {
    
        
        PT_DatiVenditore.getCurrentuserInfo();
        PT_DatiVenditore.getuserInfo();
       // system.assertNotEquals(null, PT_DatiVenditore.getCurrentuserInfo());
      //  System.runAs(runningUser){
        Test.startTest();
            PT_DatiVenditore.getCurrentuserInfo();
            PT_DatiVenditore.getuserInfo();  
        Test.stopTest();
                system.assertEquals(usr.LastName, 'Site Guest User');
            
        }
    }
 }