/*
* @author Atos India Pvt Ltd.
* @date  Dec,2016
* @description This class is test class of KBMS_SezioneContattiTrigger class & trigger
*/

@isTest(seeAllData= false)
public class KBMS_SezioneContattiTriggerTest{
    
    static testMethod void testInsertMethodPositive(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(5);//---Create list of Sezione_Contatti__c of 5 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),5);
        Test.stopTest();
        
    }
    static testMethod void testInsertMethodNegative(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(5);//---Create list of Sezione_Contatti__c of 5 records from the util class KBMS_TestDataFactory----//
        System.assertNotEquals(lstSezione.size(),0);
        Test.stopTest();
        
    }
    
    static testMethod void testUpdateMethod(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(3); //---Create list of Sezione_Contatti__c of 3 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),3);
        lstSezione[0].Priority__c= 3;
        update lstSezione;
        System.assertEquals(lstSezione[0].Priority__c,3);
        Test.stopTest();
        
    }
    
    static testMethod void tesDeleteMethod(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(3); //---Create list of Sezione_Contatti__c of 3 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),3);
       	delete lstSezione[0];
        List<Sezione_Contatti__c> lst = [select Id, IsDeleted  from Sezione_Contatti__c ];
        System.assertEquals(lst.size(), 2);
        Test.stopTest();
    }
    
    static testMethod void tesDeleteMethodNegative(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(3); //---Create list of Sezione_Contatti__c of 3 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),3);
        List<Sezione_Contatti__c> delList = new  List<Sezione_Contatti__c>();
        delList.add(lstSezione[0]);
        delete lstSezione;
        List<Sezione_Contatti__c> lst = [select Id, IsDeleted  from Sezione_Contatti__c ];
        system.debug('the size:' + lst.size());
        KBMS_SezioneContattiTrigger cl = new KBMS_SezioneContattiTrigger();
        cl.deleteMethod(delList);
        System.assert(lst.size()== 0, 'delete not successful');
        try{
            update lstSezione;
        }
        catch(DmlException ex){
            system.assert(ex.getMessage() != null, 'Deletion not successful');
        }
        Test.stopTest();
    }
    
    static testMethod void tesUndeleteMethod(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(3); //---Create list of Sezione_Contatti__c of 3 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),3);
        List<Sezione_Contatti__c> delList = new  List<Sezione_Contatti__c>();
        delList.add(lstSezione[0]);
        delete delList ;
        KBMS_SezioneContattiTrigger.flag = true;
        undelete [select id from Sezione_Contatti__c where IsDeleted = true limit 1 ALL ROWS] ;
        //KBMS_SezioneContattiTrigger cl = new KBMS_SezioneContattiTrigger();
        //cl.undeleteMethod(delList);
        List<Sezione_Contatti__c> lst = [select Id, IsDeleted  from Sezione_Contatti__c ];
        System.assert(lst.size()== 3, 'Undelete not successful');
        Test.stopTest();
        
    }
    static testMethod void tesUndeleteMethodNegative(){
        Test.startTest();
        List<Sezione_Contatti__c> lstSezione = KBMS_TestDataFactory.createSezioneTestRecords(3); //---Create list of Sezione_Contatti__c of 3 records from the util class KBMS_TestDataFactory----//
        System.assertEquals(lstSezione.size(),3);
        List<Sezione_Contatti__c> delList = new  List<Sezione_Contatti__c>();
        delList.add(lstSezione[0]);
        delete delList ;
        undelete delList ;
        KBMS_SezioneContattiTrigger cl = new KBMS_SezioneContattiTrigger();
        cl.undeleteMethod(delList);
        List<Sezione_Contatti__c> lst = [select Id, IsDeleted  from Sezione_Contatti__c where IsDeleted = true ALL ROWS ];
        system.assert(lst.size()==0,'Undelete not successful');
        Test.stopTest();
        
    }
    
    
}