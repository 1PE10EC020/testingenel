@isTest
public class Registrazione_Helper_Test {
    
    @isTest
    static void testStartRecording (){
    	case parentCase = new Case ();
  		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.IVR_dn__C = '1233';
    	c.IVR_connid__C = '213412312';
        insert c;
        
        case cDN = new case();
        cDN.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cDN.TipologiaCase__c ='Da Definire';
        cDN.origin = 'Email';
        cDN.status='In carico al II Livello';
        cDN.DescrizioneStato__c='In Lavorazione';
        cDN.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cDN.pod__C = 'testPod';
        cDN.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cDN.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cDN.LivelloTensioneDiFornitura__c ='BT';
        cDN.livello1__C ='In prelievo';
        cDN.livello2__C = 'Già Connesso';
        cDN.livello3__C ='MT/AT' ;
        //c2.IVR_dn__C = '1233';
    	cDN.IVR_connid__C = '213412312';
        insert cDN;
        
        case cConn = new case();
        cConn.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cConn.TipologiaCase__c ='Da Definire';
        cConn.origin = 'Email';
        cConn.status='In carico al II Livello';
        cConn.DescrizioneStato__c='In Lavorazione';
        cConn.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cConn.pod__C = 'testPod';
        cConn.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cConn.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cConn.LivelloTensioneDiFornitura__c ='BT';
        cConn.livello1__C ='In prelievo';
        cConn.livello2__C = 'Già Connesso';
        cConn.livello3__C ='MT/AT' ;
        cConn.IVR_dn__C = '1233';
    	//c3.IVR_connid__C = '213412312';
        insert cConn;
    	RegistrazioneService.StartRegistrazioneResponse_element  expetedResponse = new RegistrazioneService.StartRegistrazioneResponse_element ();
    	expetedResponse.CodiceEsito ='0';
    	expetedResponse.DescrizioneEsito='';
    	expetedResponse.IDRegistrazione ='123';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_START_Moch(expetedResponse));  
    	 c = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C != null)  limit 1 ];
    	 cDN = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C = null AND IVR_connid__C != null)  limit 1 ];
    	 cConn = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C = null)  limit 1 ];
    	
    	// set the mandatory field
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
    	Test.startTest();
    	
    	Registrazione_Helper controller = new Registrazione_Helper();
    	Registrazione_Helper.StartRecording(c.IVR_ConnID__c, c.IVR_DN__c, c.IVR_Tenant__c, c.TelefonoInputIVR__c, c.CodiceFiscaleRichiedente__c, c.pod__c, c.CaseNumber, c.IdPratica__c,c.CodicePercorsoTelefonico__c);
    	
    	Test.stopTest();
    }
    
}