/**********************************************************************************************
@author:        Priyanka Sathyamurthy
@date:          02 Aug,2017
@description:   This controller is used by SF_PT_RequestInsertionProcess.
It performs the Feasibility checks on request record and then inserts the request record
if its feasible.
@reference:     420_Trader_Detail_Design_Wave2_v1.0 (Section#7.2.1.1)
**********************************************************************************************/
public without sharing class PT_Feasibility_Check {
    
    public static final String IDMESSDUPLI = '107';
    public static final String IDMESSTRADERMOR = '16';
    public static final String IDMESSFORNIT = '613';
    public static final String CODCAUSDUP = '005';
    public static final String CODCAUSTRADERMOR = '072';
    public static final String CODCAUSFORNIT = '004';
    public static final String DESCDUPLI = 'La richiesta (identificata dal codice ' +
                                            'pratica utente) è già pervenuta';
    public static final String DESCTRADERMOR = 'Il contratto di trasporto è sospeso';
    public static final String DESCFORNIT = 'Combinazione Uso energia, Livello di tensione,'+
                                            'Tipo contratto e Potenza non ammissibile';
    public static final String MOTIVDUPLI = '_La richiesta (identificata dal codice pratica utente) è già pervenuta';
    public static final String MOTIVTRADERMOR = '_Il contratto di trasporto è sospeso';
    public static final String MOTIVFORNIT = '_Combinazione Uso energia, Livello di tensione, '+
                                                'Tipo contratto e Potenza non ammissibile';
    public static final String STATO = 'RES';
    public static final String STATORES = 'RES.001';
    public static final String MESSAGGIO0 = '0';
    public static final String MTVALUE = 'MT';
    public static final String ATVALUE = 'AT';
    public static final String ONEVALUE = '001';
    public static final String THREE = '03';
    public static final String FORNITONE = '01';
    public static final String TWOVALUE = '002';
    public static final String SVALUE = 'S';
    public static final String NVALUE = 'N';
    public static final String ONE2 = '012';
    public static final String ILLVALUE = 'ILL';
    public static final String BTVALUE = 'BT';
    public static final String BMVALUE = 'BM';
    public static final String TEMVALUE = 'TEM';
    public static final String N02VALUE = 'N02';
    PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
/**********************************************************************************************
@author:        Priyanka Sathyamurthy
@date:          02 Aug,2017
@Method Name:   checkProcessConditions
@description:   This InvocableMethod method is used to perform feasibility check for the process 
@reference:     420_Trader_Detail_Design_Wave2_v1.0 (Section#7.2.1.1)
**********************************************************************************************/
    @InvocableVariable(label='getId')
    public Id reqid;
    @InvocableMethod(label='Get id From Process Builder')
/**********************************************************************************************
@author:        Priyanka Sathyamurthy
@date:          02 Aug,2017
@Method Name:   checkProcessConditions
@description:   This InvocableMethod method is used to perform feasibility check for the process 
@reference:     420_Trader_Detail_Design_Wave2_v1.0 (Section#7.2.1.1)
**********************************************************************************************/
    public static void checkProcessConditions(List<Id> requestid){   
         try{
        PT_Richiesta__c request= [SELECT Id,RecordTypeId FROM PT_Richiesta__c where Id=:requestid limit 1];
        String rectypename = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosById().get(request.RecordTypeId).getname();  
        if((rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_PD2))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_PD4))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_DM1))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_GM1))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_SCA))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_SV1))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_SM1))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_V03))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_VL3))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_VL2))&&
           (rectypename!= (System.Label.PT_RICHIESTE_RecordTypeName_PT_VL1))){
        PT_Richiesta__c newRequest= [SELECT Id,AccountTrId__c,RecordTypeId,IdTrader__c,Sottostato__c,IdRichTrader__c,AmmissMotivazione__c,
                                      AmmissDescrizione__c,AmmissCodCausale__c,IdMessaggioPT__c,Stato__c,PotImp__c,
                                      Tensione__c,IdUsoEnergia__c,FLG_POTENZA__c,IdTipoContr__c,PotDisp__c,IdConnessione__c,
                                      IdTipoRichAEEG__c,Iddistributore__c FROM PT_Richiesta__c 
                                      where Id=:requestid limit 1];
         String recordtypename = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosById().get(newRequest.RecordTypeId).getname();  
         PT_Feasibility_Check__c fesCheck= new PT_Feasibility_Check__c();
         fesCheck=[SELECT Id,Name,Controllo_Rich_Duplicato__c,Controllo_Trader_Moroso__c,Controllo_Comp_Fornit__c 
                   FROM PT_Feasibility_Check__c where Name=:recordtypename limit 1];
        if(fesCheck.Controllo_Comp_Fornit__c){
         controllo_Comp_Fornit(newRequest);
         cntrl_Comp_Fornit_2(newRequest);
        }
         if(fesCheck.Controllo_Trader_Moroso__c){
         controllo_Trader_Moroso(newRequest);
        }
         if(fesCheck.Controllo_Rich_Duplicato__c){
         controllo_Rich_Duplicato(newRequest);
         system.debug('after duplicato check');
         system.debug('newRequest.IdMessaggioPT__c'+newRequest.IdMessaggioPT__c);
        }
        
        
       // PT_RequestRecordCreator.finalcheck(newRequest.IdMessaggioPT__c, 
                               //   newRequest.AmmissCodCausale__c, newRequest.AmmissDescrizione__c);*/
       
        }
             if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
         }
        
         catch(Exception e){
            logger.debugException(e);   
        } 
    }

/*********************************************************************************************************
@reference:     420_PT_ControlliPrevalidazione_Bloccanti_v1.0 (Controllo_Rich_Duplicato-CNTRL_RICH_ DUPL_1)
***********************************************************************************************************/
    public static void controllo_Rich_Duplicato(PT_Richiesta__c newRequest){
        //Integer count=0;
        //Boolean check;
        try{
        if(newRequest.IdRichTrader__c!=null){
            Integer count=[select count() from PT_Richiesta__c 
                           where (IdRichTrader__c=:newRequest.IdRichTrader__c
                                 and IdTipoRichAEEG__c=:newRequest.IdTipoRichAEEG__c
                                 and AccountTrId__c=:newRequest.AccountTrId__c
                                 and (not(Id=:newRequest.Id))
                                 and Iddistributore__c=:newRequest.Iddistributore__c) 
                                 and (((IdTipoRichAEEG__c in('M01','M02','M03') 
                                 and Stato__c NOT in('ANN','RES', 'EVA')))  
                                 or (IdTipoRichAEEG__c NOT in('M01','M02','M03') 
                                 and Stato__c NOT in('RES'))) limit 100]; 
            if(count!=0){
                newRequest.IdMessaggioPT__c=IDMESSDUPLI;
                newRequest.AmmissCodCausale__c=CODCAUSDUP;
                newRequest.AmmissDescrizione__c =DESCDUPLI;
                newRequest.AmmissMotivazione__c=MOTIVDUPLI;
                newRequest.Stato__c=STATO;
                newRequest.Sottostato__c=STATORES;
                database.update(newRequest);
                /*  PT_RequestRecordCreator.finalcheck(newRequest.IdMessaggioPT__c, 
                                  newRequest.AmmissCodCausale__c, newRequest.AmmissDescrizione__c);*/
               }
               else{
                  // system.debug('in else condition');
                   newRequest.IdMessaggioPT__c=MESSAGGIO0;
                  // return newRequest;
               }
           } 
            if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
        }
        catch(Exception e){
            //return null;
            Exception excep = e;
            
        }
    }
    
 /*********************************************************************************************************
@reference:     420_PT_ControlliPrevalidazione_Bloccanti_v1.0 (Controllo_Trader_Moroso-CNTRL_RICH_ MOROSA_1)
***********************************************************************************************************/
      public static void controllo_Trader_Moroso(PT_Richiesta__c newRequest){
                 Integer count=0;             
          try{
            count=[select count() from PT_Trasporto__c 
                   where IdTrader__c=:newRequest.IdTrader__c 
                   and IdSocieta__c=:newRequest.Iddistributore__c and FlgMoroso__c=:SVALUE 
                   and DataInizioValid__c <=:system.now() and DataFineValid__c>=:system.now() limit 100];
            if(count!=0){
                   newRequest.IdMessaggioPT__c=IDMESSTRADERMOR;
                   newRequest.AmmissCodCausale__c=CODCAUSTRADERMOR;
                   newRequest.AmmissDescrizione__c =DESCTRADERMOR;
                   newRequest.AmmissMotivazione__c=MOTIVTRADERMOR; 
                   newRequest.Stato__c=STATO;
                   newRequest.Sottostato__c=STATORES;
                   database.update(newRequest);

                   
                  /* PT_RequestRecordCreator.finalcheck(newRequest.IdMessaggioPT__c, 
                                        newRequest.AmmissCodCausale__c, newRequest.AmmissDescrizione__c);*/
               }
               else{
                   newRequest.IdMessaggioPT__c=MESSAGGIO0;
               } 
              if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
           //return null;
          }
          catch(Exception e){
             // return null;
             Exception excep = e;
            
          }
    }
     /* Public with sharing class wrapperFeasibility {
        @AuraEnabled
        public String IdMessaggioPT {
            get;
            set;
        }
          @AuraEnabled
                             public string AmmissCodCausale {
            get;
            set;
        }
        @AuraEnabled
        public String AmmissDescrizione {
            get;
            set;
        }

       
        
            public wrapperFeasibility(String IdMessaggioPT_val,String request_val,String AmmissDescrizione_val) {
            IdMessaggioPT = IdMessaggioPT_val;
            AmmissCodCausale = request_val;
            AmmissDescrizione = AmmissDescrizione_val;
            

        }
    
        }*/
/*********************************************************************************************************
@reference:     420_PT_ControlliPrevalidazione_Bloccanti_v1.0 (Controllo_Comp_Fornit-CNTRL_COMP_FORNIT_1)
***********************************************************************************************************/
    public static void controllo_Comp_Fornit(PT_Richiesta__c newRequest){  
        try{
              if(newRequest.PotImp__c!=null && (newRequest.Tensione__c==MTVALUE||newRequest.Tensione__c==ATVALUE) &&
               newRequest.IdUsoEnergia__c!=TWOVALUE){
                   newRequest.FLG_POTENZA__c=SVALUE;
                   database.update(newRequest);
               }
        else if(newRequest.IdTipoContr__c==ILLVALUE&&(newRequest.IdUsoEnergia__c==TWOVALUE ||newRequest.IdUsoEnergia__c==ONE2)
                && (newRequest.Tensione__c==MTVALUE||newRequest.Tensione__c==ATVALUE)){
                             newRequest.FLG_POTENZA__c=NVALUE;
                    		 database.update(newRequest);
        }
        
        else if((newRequest.IdTipoContr__c!=ILLVALUE&& newRequest.IdTipoContr__c!=null)&&
                (newRequest.Tensione__c==BTVALUE||newRequest.Tensione__c==BMVALUE)){
            if((Integer.valueOf(newRequest.PotImp__c))>30){
                newRequest.FLG_POTENZA__c=SVALUE;
                database.update(newRequest);
            }
            else if((Integer.valueOf(newRequest.PotImp__c))<17){
                newRequest.FLG_POTENZA__c=NVALUE;
                database.update(newRequest);
            }
            else if(((Integer.valueOf(newRequest.PotImp__c))>=17 && (Integer.valueOf(newRequest.PotImp__c))<=30) &&
                   (Integer.valueOf(newRequest.PotImp__c)==newRequest.PotDisp__c || newRequest.PotDisp__c==null)){
                newRequest.FLG_POTENZA__c=SVALUE;
                database.update(newRequest);
            }
            else if(((Integer.valueOf(newRequest.PotImp__c))>=17 && (Integer.valueOf(newRequest.PotImp__c))<=30) &&
                   (Integer.valueOf(newRequest.PotImp__c)!=newRequest.PotDisp__c || newRequest.PotDisp__c!=null)){
                newRequest.FLG_POTENZA__c=NVALUE;
                database.update(newRequest);
            }
            else{
                 //do nothing       
              }
        }
        else if(newRequest.IdTipoContr__c==ILLVALUE&&(newRequest.IdUsoEnergia__c!=TWOVALUE ||newRequest.IdUsoEnergia__c!=ONE2)
                && (newRequest.Tensione__c==BTVALUE||newRequest.Tensione__c==BMVALUE)){
                             newRequest.FLG_POTENZA__c=SVALUE;
                    		 database.update(newRequest);
        }
        else if(newRequest.IdTipoContr__c==ILLVALUE && newRequest.IdUsoEnergia__c!=TWOVALUE){
                             newRequest.FLG_POTENZA__c=NVALUE;
            				 database.update(newRequest);
        }
        else{
            
        }
        if(newRequest.IdUsoEnergia__c==TWOVALUE){
            newRequest.FLG_POTENZA__c=NVALUE;
            database.update(newRequest);
        }
        
        }
        catch(Exception e){
            //do nothing
            Exception excep = e;
            
        }
    }
/*********************************************************************************************************
@reference:     420_PT_ControlliPrevalidazione_Bloccanti_v1.0 (Controllo_Comp_Fornit-CNTRL_COMP_FORNIT_2)
***********************************************************************************************************/
    public static void cntrl_Comp_Fornit_2(PT_Richiesta__c newRequest){
             // Integer count; 
        try{
        Decimal potImpValue=Integer.valueof(newRequest.PotImp__c);
		Decimal potDispValue=newRequest.PotDisp__c;
        if(newRequest.IdTipoRichAEEG__c==N02VALUE && newRequest.IdConnessione__c==TEMVALUE){
           if(newRequest.IdUsoEnergia__c==null){
                newRequest.IdUsoEnergia__c=ONEVALUE;
                 database.update(newRequest);
                    
            }  
           Integer count=0;
           
			if(newRequest.FlagForfait__c==SVALUE){
            count=[select count() from Four_Comp_Fornit__c 
                          where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                          and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                          and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                          and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                          and POTENZA_MIN__c<:potImpValue
                          and POTENZA_MAX__c>:potImpValue
                   		  and ID_TIPO_FORNIT__c=:THREE
                          and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
                if(newRequest.PotImp__c==null){
            		count=count+[select count() from Four_Comp_Fornit__c 
                          where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                          and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                          and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                          and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                          and POTENZA_MIN__c<:potDispValue
                          and POTENZA_MAX__c>:potDispValue
						  and ID_TIPO_FORNIT__c=:FORNITONE
                          and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
                }
            }
            else{
            count=[select count() from Four_Comp_Fornit__c 
                          where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                          and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                          and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                          and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                          and POTENZA_MIN__c<:potImpValue
                          and POTENZA_MAX__c>:potImpValue 
						  and ID_TIPO_FORNIT__c=:FORNITONE
                          and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
                if(potImpValue==null){
            count=count+[select count() from Four_Comp_Fornit__c 
                          where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                          and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                          and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                          and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                          and POTENZA_MIN__c<:potDispValue
                          and POTENZA_MAX__c>:potDispValue
						  and ID_TIPO_FORNIT__c=:FORNITONE
                          and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
                }}
            //system.debug('count in fornit 2 else-->'+count);
            //system.debug('newRequest.PotImp__c'+newRequest.PotImp__c);
                   if(count==0){
                   newRequest.IdMessaggioPT__c=IDMESSFORNIT;
                   newRequest.AmmissCodCausale__c=CODCAUSFORNIT;
                   newRequest.AmmissDescrizione__c =DESCFORNIT;
                   newRequest.AmmissMotivazione__c=MOTIVFORNIT;
                   newRequest.Stato__c=STATO;
                   newRequest.Sottostato__c=STATORES;
                   database.update(newRequest);
                  /* PT_RequestRecordCreator.finalcheck
                       (newRequest.IdMessaggioPT__c, newRequest.AmmissCodCausale__c, newRequest.AmmissDescrizione__c);*/
               }
               else{
                   newRequest.IdMessaggioPT__c=MESSAGGIO0;
               }          
        }           
            
        else{
            Integer count=[select count() from Four_Comp_Fornit__c 
                           where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                           and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                           and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                           and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                           and POTENZA_MIN__c<:potImpValue
                           and POTENZA_MAX__c>:potImpValue
                          and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
            if(potImpValue==null){
            count=count+[select count() from Four_Comp_Fornit__c 
                          where ID_CONNESSIONE__c=:newRequest.IdConnessione__c 
                          and ID_TIPO_CONTR__c=:newRequest.IdTipoContr__c 
                          and ID_LIV_TENSIONE__c=:newRequest.Tensione__c 
                          and FLG_POTENZA__c=:newRequest.FLG_POTENZA__c
                          and POTENZA_MIN__c<:potDispValue
                          and POTENZA_MAX__c>:potDispValue
                         and ID_USO_ENERGIA__c=:newRequest.IdUsoEnergia__c limit 100];
            }
                  if(count==0){
                   newRequest.IdMessaggioPT__c=IDMESSFORNIT;
                   newRequest.AmmissCodCausale__c=CODCAUSFORNIT;
                   newRequest.AmmissDescrizione__c =DESCFORNIT;
                   newRequest.AmmissMotivazione__c=MOTIVFORNIT;
                   newRequest.Stato__c=STATO;
                   newRequest.Sottostato__c=STATORES;
                   database.update(newRequest);
                   /*PT_RequestRecordCreator.finalcheck
                       (newRequest.IdMessaggioPT__c, newRequest.AmmissCodCausale__c, newRequest.AmmissDescrizione__c);*/
               }
               else{
                   newRequest.IdMessaggioPT__c=MESSAGGIO0;
               } 
        }
    }
        catch(Exception e){
            //do nothing
            Exception excep = e;
            
        }
    }
    


}