/*******************************************************************************
* @author         Cloud Sherpas
* @date           5.19.2014
* @return         
* @description    Test class for testing the class named 
*                 logger
*******************************************************************************/
@isTest
public class LoggerTest {

/*******************************************************************************
* @author         Cloud Sherpas
* @date           5.19.2014
* @return         
* @description    This test methods creates log exceptions intentionally to check
                  the logger methods of logger class
* @Revision(s) 
*******************************************************************************/

    static testMethod void testlogger() {
        
        User usr = buildTestUser(1); 
        insert usr;
        System.runAs(usr){
             list<RecordType> apexLogrts = [SELECT Id, Name from RecordType WHERE SobjectType =: 'apexLog__c'];
             LogConfig__c logconfig = new LogConfig__c();
             
             logconfig.LOGRecordTypeIDDebug__c=apexLogrts.size() > 0 ? apexLogrts[0].Id : null;
             logconfig.LOGRecordTypeIDException__c=apexLogrts.size() > 1 ? apexLogrts[1].Id : apexLogrts.size() > 0 ? apexLogrts[0].Id : null;
             logconfig.ExceptionsEnabled__c=true;
             logconfig.debugsEnabled__c=true;
             insert logconfig;
            Test.startTest(); 
            
            //test push method
            logger.push('createAccount','AccountDML');     
            logger.debug('Test debug message');
            //generate exception
            try 
            {
              insert new Account();
            
            }   
            catch (System.DmlException ex) {
                
              logger.debugException(ex); 
              logger.debugException(ex.getmessage());   
          
            }
            
            //test pop method
            logger.pop();
            logger.setInConstuctor();
            List<apexlog__c> testLog = [Select ExceptionType__c, Message__c from apexlog__c];
            System.assertEquals('Test debug message', testLog[0].Message__c);
            System.assert(testLog[1].Message__c.indexOf('fail') > -1);
            Test.stopTest();
            
        }
     
   }
   
   /**
     * @author Original: Cloud Sherpas,
     * @date Original: 16th December 2014
     * @description Helper method to create a test user record(can be called iteratively to create more than 1 user.)
     * @param i An integer that will serve as index for iteration (bulk).
     */
    public static User buildTestUser(Integer i){
        Profile p = [select pf.id from profile pf WHERE pf.name= 'System Administrator' limit 1];
        User usr = new User();
        usr.emailencodingkey    = 'UTF-8';
        usr.languagelocalekey   = 'en_US';
        usr.localesidkey        = 'en_US';
        usr.timezonesidkey      = 'America/Los_Angeles'; 
        usr.ProfileId = p.Id;
        usr.alias           = 'TUser' + i;
        usr.email           = 'TUser' + i + '@gs.com';
        usr.lastname        = 'TUser' + i;
        usr.username        = 'TUser' + i + '@gs.com';
        return usr; 
    } 

    

    
   
}