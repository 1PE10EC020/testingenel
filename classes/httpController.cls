public class httpController {
	// private constants used to parse html response
	private static string rootNameContent = 'content' ; 
	private static string rootNameLink = 'link' ; 
	private static string rootNameInline = 'inline' ; 
	private static string rootNameEntry = 'entry' ; 
	private static string rootNameTitle = 'title';
	private static string rootNameSummary = 'summary';
	
	
	public String concatenateName {get;set;}
	public list<DelegateSingle> listOfDelegate{get;set;}
	
	// private constatns used to compose the url
	private static String sent = 'SENT';
	private static String recived = 'RECEIVED';
	private static string requestVatCode = 'requestvatcode' ; 
	private static string requestFiscalCode = 'requestfiscalcode' ; 
	
	// public variables used into component
	public String blockTitle {get;set;}
	public String callMode {get;set;}
	public String afiscalCode {get;set;}
	public String vatCode {get;set;}
	public boolean errorToDisplay {get;set;}
	public String textToDisplay {get;set;} 
	
	public boolean showButton{ get {
		if (string.isBlank(afiscalCode) && string.isBlank(vatCode))
			return false;
		return true;
	}
    private set;}
    
	public boolean showTable {get {
		if (string.isBlank(afiscalCode) && string.isBlank(vatCode))
			return false;
		if ( listOfDelegate !=  null && !listOfDelegate.isEmpty())
			return true;
		return false;
	}
	private set;}
	
	public boolean showtableDeleganti {get {
		if (string.isBlank(afiscalCode) && string.isBlank(vatCode))
			return false;
		if ( listOfDelegate !=  null && !listOfDelegate.isEmpty())
			return true;
		return false;
	}
	private set;}
	
	
	
	private string  composeURL(){
		string url = '' ;
		if (sent.equals(callMode) ) {
			url = IntegrationUtils.getfullEndpoint('PED_SENT');
		}	
		if (recived.equals(callMode) ){
			url = IntegrationUtils.getfullEndpoint('PED_RECEIVED');
		}
		system.debug(logginglevel.Debug, 'baseurl is '+url);
		
		//requestvatcode=%2701835830066%27&requestfiscalcode=%27%27&$expand=Services'
		//url += requestVatCode+'=%27'+vatCode+'%27&'+requestFiscalCode+'=%27'+afiscalCode+'%27&$expand=Services';
		//Giorgio
		if(!string.isBlank(vatCode))
		{
			url += requestVatCode+'=\''+vatCode+'\'&'+requestFiscalCode+'=\'\'&$expand=Services';
			system.debug(logginglevel.Debug, 'url with parameter is '+url);
		}else{
			if(!string.isBlank(afiscalCode)){
				if (afiscalCode.length() == 16){
					/*[GP 13102016] isnerita conversione del cf a uppercase per avere i delegati */
					afiscalCode = afiscalCode.toUpperCase();
					/*END [GP 13102016]*/
					url += requestVatCode+'=\'\'&'+requestFiscalCode+'=\''+afiscalCode+'\'&$expand=Services';
					system.debug(logginglevel.Debug, 'url with parameter is '+url);
				}else{
					url += requestVatCode+'=\''+vatCode+'\'&'+requestFiscalCode+'=\'\'&$expand=Services';
					system.debug(logginglevel.Debug, 'url with parameter is '+url);
				}
				
			}
		}
		
		
		return url ; 
	}
	
    public void request (){
		errorToDisplay= false;
		
	      HttpRequest req = new HttpRequest();   
	    //string url = 'http://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/GetReceivedDelegation?requestvatcode=\'01835830066\'&requestfiscalcode=\'\'&$expand=Services' ;
	    //string url = 'http://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/GetReceivedDelegation?requestvatcode=%2701835830066%27&requestfiscalcode=%27%27&$expand=Services' ;
	    //http://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/GetSentDelegation?requestvatcode='01835830066'&requestfiscalcode=''&$expand=Services
	    
	    //string url = composeURL();//'http://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/GetSentDelegation?requestvatcode=%2701835830066%27&requestfiscalcode=%27%27&$expand=Services' ;
	    
	    //string url = 'https://eneldistribuzione.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/Delegations?$top=11&$filter=DelegateVATCode+eq+\'01830320386\'&$orderby=DelegationCode&$inlinecount=allpages&$select=DelegateDenomination,DelegateEmail,DelegateFiscalCode,DelegateId,DelegateVATCode,DelegationCode, Method=GET';
	    //req.setEndpoint( url );
	    
	   String username =''; 
	   String password =''; 
	 		if (sent.equals(callMode) ) {
				username = IntegrationUtils.getServiceUsername('PED_SENT');
				password = IntegrationUtils.getServicePassword('PED_SENT');
			}	
			if (recived.equals(callMode) ){
				username = IntegrationUtils.getServiceUsername('PED_RECEIVED');
				password = IntegrationUtils.getServicePassword('PED_RECEIVED');
			}
		
		//String username ='DelegheUser';
	    //String password ='Password$123';
	    
	 	system.debug(logginglevel.debug,'username is '+username);
	 	system.debug(logginglevel.debug,'password is '+password);
	    Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
	    
	    system.debug(' **** authorizationHeader '+authorizationHeader);
	    req.setHeader('Authorization', authorizationHeader);
	    
	    
	    req.setHeader('Content-Type','application/json');
	    //req.setTimeout(20000);
	    
	    req.setHeader('Accept', '');
	    req.setMethod('GET');
	    system.debug(' **** authorizationHeader '+req.getHeader('Content-Type'));
	    
	   //REQUEST ODATA2
	    //string url = 'https://eneldistribuzione.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/Delegations?$top=11&$filter=DelegateVATCode+eq+\'01830320386\'&$orderby=DelegationCode&$inlinecount=allpages&$select=DelegateDenomination,DelegateEmail,DelegateFiscalCode,DelegateId,DelegateVATCode,DelegationCode';
	   //REQUEST NON ODATA2   
	    string url = composeURL();
	    
	    req.setEndpoint( url );
	    HttpResponse response;            
	  	try{
	  	    Http http = new Http();
	        system.debug('**** request is' +req);
	        response = http.send(req);
	        
	        system.debug( response);
	        system.debug( response.getBody());
	        
	        Dom.Document doc = response.getBodyDocument();
	        system.debug('Document is '+doc);
	        Dom.XMLNode address = doc.getRootElement();
	         system.debug('address is '+address);
	         
	        // create the first Delegate 
	       //listOfDelegate = new list<Delegate>();
	       listOfDelegate = new list<DelegateSingle>();
	       
	       
	       
	        system.debug('**provo a fare la find '+listOfDelegate);
	        integer counter = 0 ; 
	        concatenateName = '';
	        // Dom.XMLNode singleNode = address.getChildElement(rootNameLink,'http://www.w3.org/2005/Atom');
	        // Dom.XMLNode singleNode2 = address.getChildElement(rootNameLink,'http://www.w3.org/2005/Atom');
	        for(Dom.XMLNode child : address.getChildElements()) {
	       		list<Dom.XMLNode> hasChild = child.getChildElements();
	       		//integer counter = 0 ; 
	       		system.debug('search for error '+child.getName());
	       		if (!hasChild.isEmpty()){
	       			//Delegate deleg1;
	       			DelegateSingle deleg1;
	  				
	       			try{
		            	for (Dom.XMLNode child2 : child.getChildElements()) {
		            		if( child2 != null){
			           		    //deleg1 = new Delegate();
			           		    //deleg1.listOfDelegateService = new list<DelegateService>();
			           		    deleg1 = new DelegateSingle();
			           		    system.debug('search for error2 '+child2.getName());
			           		    System.debug('Testo'+child2.getText());
			           		    if ( child2.getText() .Contains('Error')){
			           		    	errorToDisplay =true;
			           		    	textToDisplay = 'Errore nel recuper dei delegati, riprovare piu tardi';
			           		    }
			           			if (rootNameContent.equalsignorecase(child2.getName()) ){
			           				system.debug('Content tag found ');
			           				
		           				retriveDelegateInformation (child2,listOfDelegate[counter] );
		           				system.debug('**** Post retriveDelegateInformation Deleg1:   '+deleg1+'  COUNTER: '+counter);
			           			}// end if content
			           			if (rootNameLink.equalsignorecase(child2.getName()) ){
			           				system.debug('link tag found ');
			           				list<Dom.XMLNode> haslinkChild = child2.getChildElements();
			           				if (!haslinkChild.isEmpty()){
				           				try{
					           				for (Dom.XMLNode linkChild : child2.getChildElements()) {
					           					System.debug('linkChild node NAme is '+linkChild.getName());
					           					System.debug('linkChild node Text is '+linkChild.getText());
					           					system.debug('find here ');
					           					if (rootNameInline.equalsIgnoreCase(linkChild.getName())) 
					           					{
					           						//deleg1.listOfDelegateService = retriveAllService(linkChild);
					           						deleg1.nameOfService = retriveNameAllService(linkChild);
					           						deleg1.counter = counter;
					           						listOfDelegate.add(deleg1);
					           						system.debug(' *** listOfDelegate is '+listOfDelegate);
					           						//system.debug(' deleg1.listOfDelegateService is '+deleg1.listOfDelegateService);
					           					}
					           				} // end for
				           				}	
				           				catch(Exception e){
				           					system.debug('exception using link');
				           				}
			           				}// end if
			           			}// end if link
		            		}
		            	}
		            counter++;
		            system.debug('*** Final listOfDelegate: '+listOfDelegate);
	       			}
	            	catch(Exception e){
	            		 System.debug('Exception in subfor is '+e.getMessage());
	            	}
	            	system.debug('**** deleg1.nameOfService '+deleg1.nameOfService);
	            	system.debug(' deleg 1 is '+deleg1);
	            	/*if ( deleg1 != null)
	            		listOfDelegate.add(deleg1); */
	       		}
	       		//counter++;
	       		
	    	}
	    	
	    	system.debug('**** listOfDelegate: '+listOfDelegate);
   			if ( listOfDelegate ==  null || listOfDelegate.isEmpty()) {
   				//errorToDisplay =true;
				//textToDisplay = 'Nessun Delegato trovato ';
				system.debug('**** ricerca senza risultati');
        		ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'La ricerca non ha restituito risultati'));
   			}
		
        
	    	
      	}
      	catch(Exception e)
      	{
			System.debug('Exception in response '+e.getMessage());
			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'La ricerca non è andata buon fine'));
			
	  	}

    	/*
    	system.debug('**** Fuori dai For '+counter);
    	if(counter==1){
    		system.debug('**** ricerca senza risultati');
        	ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'La ricerca non ha restituito risultati'));
    	}*/
      //  }
   /* catch(IOException e){
    System.debug('Exception is '+e.getMessage());

}*/
    
}

	/*
	Modify by Giorgio
	Commented for view only row in table with single cell with Name (Title)
	
	public list<DelegateService> retriveAllService (Dom.XMLNode root){
		 list<DelegateService> listofAllSerice = new  list<DelegateService>();
		 list<Dom.XMLNode> firstChild = root.getChildElements();
		 if (firstChild != null) {
		 	// this item should be Feed
		 		list<Dom.XMLNode> 	hasEntryChild = firstChild[0].getChildElements();
		 		if (hasEntryChild != null ){
		 			try{
			 			for (Dom.XMLNode entryChild : firstChild[0].getChildElements()) {
			 				system.debug('entry child name is '+entryChild.getName());
			 				if (rootNameEntry.equalsIgnoreCase(entryChild.getName()) ) {
			 					system.debug('entry child found');
			 					DelegateService singleService = new DelegateService();
			 					try{
			 						for (Dom.XMLNode entryNode : entryChild.getChildElements()  ){
			 							system.debug('Guarda qui : '+entryNode.getName());
			 							if ( rootNameTitle.equalsIgnoreCase(entryNode.getName()))
			 								singleService.title = entryNode.getText();
			 							if ( rootNameSummary.equalsIgnoreCase(entryNode.getName()))
			 								singleService.summary = entryNode.getText();
			 							
			 								
			 						}
			 					}
			 					catch(Exception e){
			 						System.debug('Exception found during search on entry'+e.getMessage());
			 					}
			 					listofAllSerice.add(singleService);
			 				}
			 			}
		 			}
		 			catch(Exception e){
		 				System.debug('Exception found during search on Feed' +e.getMessage());
		 			}
		 		}
		 }
		 system.debug('listofAllSerice is ' +listofAllSerice);
		return listofAllSerice;
	}
	
	*/
	
	public String retriveNameAllService (Dom.XMLNode root){
		 list<DelegateService> listofAllSerice = new  list<DelegateService>();
		 list<Dom.XMLNode> firstChild = root.getChildElements();
		 
		 if (firstChild != null) {
		 	// this item should be Feed
		 		list<Dom.XMLNode> 	hasEntryChild = firstChild[0].getChildElements();
		 		if (hasEntryChild != null ){
		 			try{
			 			for (Dom.XMLNode entryChild : firstChild[0].getChildElements()) {
			 				system.debug('entry child name is '+entryChild.getName());
			 				if (rootNameEntry.equalsIgnoreCase(entryChild.getName()) ) {
			 					system.debug('entry child found');
			 					//DelegateService singleService = new DelegateService();
			 					//Variable for concatenate name
			 					//concatenateName = '';
			 					try{
			 						for (Dom.XMLNode entryNode : entryChild.getChildElements()  ){
			 							system.debug('Guarda qui : '+entryNode.getName());
			 							if ( rootNameTitle.equalsIgnoreCase(entryNode.getName())){
			 								//singleService.title = entryNode.getText();
			 								
			 								concatenateName = concatenateName+' - '+entryNode.getText()+'\r\n';
			 								system.debug('*** Concatenate Name is '+concatenateName);	
			 							}
			 									 								
			 						}
			 					}
			 					catch(Exception e){
			 						System.debug('Exception found during search on entry'+e.getMessage());
			 					}
			 					//listofAllSerice.add(singleService);
			 				}
			 			}
		 			}
		 			catch(Exception e){
		 				System.debug('Exception found during search on Feed' +e.getMessage());
		 			}
		 		}
		 }
		 //system.debug('listofAllSerice is ' +listofAllSerice);
		//return listofAllSerice;
		return concatenateName;
	}
	
	
	
	
	/*
	public void retriveDelegateInformation (Dom.XMLNode root,Delegate deleg1 ){
		system.debug('*** content Delegate is: '+deleg1);
		list<Dom.XMLNode> firstChild = root.getChildElements();
		if (! firstChild.isEmpty()){
			for (Dom.XMLNode allChild : root.getChildElements()) {
				list<Dom.XMLNode> 	haslinkChild = allChild.getChildElements();
				if (!haslinkChild.isEmpty() ){
					try{
						for (Dom.XMLNode linkChild : allChild.getChildElements()) {
							system.debug('content child name is '+linkChild.getName()+' *** linkChild.getText(): '+linkChild.getText());
							
							if ('DelegatorDenomination'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorDenomination = linkChild.getText();
						   
							if ('DelegateDenomination'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateDenomination = linkChild.getText();
							
							if ('DelegatorEmail'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorEmail = linkChild.getText();	
							
							if ('DelegateEmail'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateEmail = linkChild.getText();	
						}
					}
					catch(Exception e){
						    System.debug('Exception in retriveDelegateInformation is '+e.getMessage());
					}
				}
			}
		}
	}
	*/
	
	public void retriveDelegateInformation (Dom.XMLNode root,DelegateSingle deleg1 ){
		system.debug('*** content Delegate is: '+deleg1);
		list<Dom.XMLNode> firstChild = root.getChildElements();
		if (! firstChild.isEmpty()){
			for (Dom.XMLNode allChild : root.getChildElements()) {
				list<Dom.XMLNode> 	haslinkChild = allChild.getChildElements();
				if (!haslinkChild.isEmpty() ){
					try{
						for (Dom.XMLNode linkChild : allChild.getChildElements()) {
							system.debug('content child name is '+linkChild.getName()+' *** linkChild.getText(): '+linkChild.getText());
							
							if ('DelegatorDenomination'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorDenomination = linkChild.getText();
						   
							if ('DelegateDenomination'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateDenomination = linkChild.getText();
							
							if ('DelegatorEmail'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorEmail = linkChild.getText();	
							
							if ('DelegateEmail'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateEmail = linkChild.getText();	
							/*Giorgio Modifica per Segn futura dopo 16 novembre 2016: agiunta colonna CF/PIVA per delegati e deleganti*/
							if ('DelegatorFiscalCode'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorFiscalCode = linkChild.getText();	
							if ('DelegateFiscalCode'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateFiscalCode = linkChild.getText();
								
							if ('DelegatorVATCode'.equalsignoreCase(linkChild.getName()))
								deleg1.delegatorVATCode = linkChild.getText();	
							if ('DelegateVATCode'.equalsignoreCase(linkChild.getName()))
								deleg1.delegateVATCode = linkChild.getText();
									
							deleg1.delegatorCf_PIVA = 	deleg1.delegatorFiscalCode+' / '+deleg1.delegatorVATCode;
							/*End Segn futura dopo 16 novembre 2016*/
						}
					}
					catch(Exception e){
						    System.debug('Exception in retriveDelegateInformation is '+e.getMessage());
					}
				}
			}
		}
	}
	
	
	Public class DelegateService {
		public string title {get;set;}
		public string summary {get;set;}
		public String serviceCode {get;set;}
		
		
		public DelegateService () {
			title ='';
			summary ='';
			serviceCode='';
		}
		
		public DelegateService (string title, string summary, string serviceCode) {
			this.title =title;
			this.summary =summary;
			this.serviceCode=serviceCode;
		}
	}
	
	/*
	public class Delegate {
		public String delegatorEmail {get;set;}
		public String delegateEmail {get;set;}
		public String delegatorDenomination {get;set;}
		public String delegateDenomination {get;set;}
		public integer counter  {get;set;}
		public list<DelegateService> listOfDelegateService {get;set;}
			public Delegate (){
				counter = 0 ; 
			}
		
	}
	public list<Delegate> listOfDelegate{get;set;}
	*/
	
	/*
		Version 2 for delegate class
	*/
	
	public class DelegateSingle {
		public String delegatorEmail {get;set;}
		public String delegateEmail {get;set;}
		public String delegatorDenomination {get;set;}
		public String delegateDenomination {get;set;}
		public integer counter  {get;set;}
		public String nameOfService {get;set;}
		/*Giorgio Modifica per Segn futura dopo 16 novembre 2016: agiunta colonna CF/PIVA per delegati e deleganti*/		
		public String delegatorFiscalCode {get;set;}
		public String delegateFiscalCode {get;set;}
		public String delegatorVATCode {get;set;}
		public String delegateVATCode {get;set;}	
		public String delegatorCf_PIVA {get;set;}
	}
	
	
	public httpController(){ /*
	listOfDelegate = new list<Delegate>();
	Delegate deleg1 = new Delegate();
	deleg1.delegateEmail = 'claudio.quaranta@gmail.com';
	deleg1.delegatorDenomination = 'claudio quaranta';
	deleg1.listOfDelegateService = new list<DelegateService>();
	deleg1.listOfDelegateService.add(new DelegateService('Enel Service 1','Servizio generico','Codice 111'));
	deleg1.listOfDelegateService.add(new DelegateService('Enel Service 2','Servizio primario','Codice 222'));
	deleg1.listOfDelegateService.add(new DelegateService('Enel Service 2','Servizio secondario','Codice 333'));
	
	listOfDelegate.add(deleg1);
	
	Delegate deleg2 = new Delegate();
	deleg2.delegateEmail = 'andrea.andretta@gmail.com';
	deleg2.delegatorDenomination = 'claudio quaranta';
	deleg2.listOfDelegateService = new list<DelegateService>();
	deleg2.listOfDelegateService.add(new DelegateService('Enel Service 1','Servizio generico','Codice 999'));
	deleg2.listOfDelegateService.add(new DelegateService('Enel Service 2','Servizio primario','Codice 888'));
	deleg2.listOfDelegateService.add(new DelegateService('Enel Service 2','Servizio secondario','Codice 777'));
	listOfDelegate.add(deleg2);
	*/
		System.debug('**** Input PIVA: '+vatCode+' CF '+afiscalCode);
	}
	
}