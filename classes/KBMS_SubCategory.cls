global class KBMS_SubCategory implements Comparable 
{
	public String uniqueName;
    public String label;
    public Integer articleCount;
    
    public KBMS_SubCategory (String uniqueName, String label, Integer articleCount)
    {
        this.uniqueName = uniqueName;
        this.label = label;
        this.articleCount = articleCount;
    }
    
    // Implement the compareTo() method
    global Integer compareTo(Object compareTo) 
    {
    	KBMS_SubCategory compareToSubCat = (KBMS_SubCategory)compareTo;
        if (articleCount == compareToSubCat.articleCount) 
        {
            return 0;
        }
        if (articleCount < compareToSubCat.articleCount) //Descending
        {
            return 1;
        }
        return -1;        
    }
}