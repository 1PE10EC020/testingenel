/**********************************************************************************************
@author:       	Jyotsna Tiwari
@date:         	17 Jun,2017
@description:  	This utility class is setup to support Request creation process in Trader Portal.
**********************************************************************************************/
public without sharing class PT_Utility 
{
    //String Constant for Success Message 
    public static final string  SUCCESS_MESSAGE = 'Record created successfully';
    
    //String Constant for Error Message
    public static final string  ERROR_MESSAGE = 'Error Message : ';
    
    public static final string  NONE = '--None--';
    public static final string  NOVALUE = '';

    
/**********************************************************************************************
@author:       	Jyotsna Tiwari
@date:         	17 Jun,2017
@methodname:	getPickListValues
@description:  	This method is used to retrive picklist values of a specific field in any custom/standard object.
**********************************************************************************************/
    public static List<String> getPickListValues(String objectName,String fieldName)
    {
        List<String> retList = new List<String>();
        /* try
        {
            Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
            Schema.SObjectType pType = objGlobalMap.get(objectName);
            Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
            List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(fieldName).getDescribe().getPicklistValues();
            retList.add(new PT_PicklistAuraComponent.SelectOptionWrapper(NONE,NOVALUE));
            for( Schema.PicklistEntry f : ctrl_ple)
            {
               retList.add(new PT_PicklistAuraComponent.SelectOptionWrapper(f.getLabel(), f.getValue()));
            }
        }
        catch(Exception ex)
        {
            ex.getMessage();
        }*/
        return retList;
    }
    
    public static List<string> getPickListValues(String objectName,String fieldName, Map<String, Schema.SObjectField> objFieldMap)
    {
        List<String> retList = new List<String>();
        try
        {
            List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(fieldName).getDescribe().getPicklistValues();
            retList.add(NOVALUE+'~'+NONE);
            for( Schema.PicklistEntry f : ctrl_ple)
            {
               retList.add(f.getValue()+'~'+f.getLabel());
            }
        }
        catch(Exception ex)
        {
            ex.getMessage();
        }
        return retList;
    }
    
    
}