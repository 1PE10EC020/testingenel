/*
 * @author : Accenture
 * @date : 2011-09-28
 */

public class SECURITYclass {

    public static Integer indexGen {get; set;}
    
     public class Securitywrapper {
    
        public security__c securitysetting;
        public Integer index;  

         public Securitywrapper() {
         Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            System.Debug('Key is ' + key);
            String strParentObjectID = lstParams.get(key);
            this.securitysetting = new security__c (Security_Name__c = 'Enter name', Object__c = strParentObjectID );

            //this.securitysetting = new security__c (Security_Name__c = 'Enter name', Object__c = ApexPages.currentPage().getParameters().get(label.CL00040));
            this.index = indexGen++;
        }
        
        public security__c getsecuritySetting() {
            return securitysetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 

    public List<Securitywrapper> Securitylist;
    public Integer numRows {get; set;}
    
     
     public SECURITYclass(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;
        Securitylist = new List<Securitywrapper>();
        numRows = 1;
    }
    
    public List<Securitywrapper> getSecuritylist() {
        return Securitylist;
}
 

public Pagereference save() {
        try {
            List<security__c> tempList = new List<security__c>();
            for(Integer i=0; i<Securitylist.size(); ++i)
                tempList.add(Securitylist[i].getsecuritySetting());
            upsert(tempList);
            return new pagereference('/' + ApexPages.currentPage().getParameters().get('retURL') );
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }


        


    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    Securitylist.add(new Securitywrapper());
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        
        }
    }
    public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                Securitywrapper securityWrap = new Securitywrapper();
                securityWrap.securitysetting = [SELECT Configuration_Environment__c,Default_Access__c,Development_Environment__c,Grant_Access_Using_Hierarchies__c,Group_Name__c,Object__c,Object_Access__c,Owned_By_Members_of__c,Production_Environment__c,Public_Group__c,record_Type__c,Refrence_No__c,Roles__c,Roles_and_Subordinates__c,  Security_Name__c,Share_With__c,UAT_Environment__c,Users__c FROM Security__c WHERE ID = :CurrentRecordID];
                Securitylist.add(securityWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    
    public void clear() {
        Securitylist.clear();
        numRows = 1;
    }
    
  
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<Securitylist.size(); ++i)
                if(Securitylist[i].index == delIndex) {
                    Securitylist.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}