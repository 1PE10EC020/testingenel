@isTest
private class  HistoryDetails_Test
{

   static testmethod void HistoryDetails_TestClass()
    {
    
     test.startTest();
     HistoryDetails hd=new  HistoryDetails ();
     hd.setObjectName('Account_History__c');
     hd.setParentId('Test');
     hd.setField('Test 2');
     hd.setCreatedById('Abc');
     hd.setIsDeleted('True');
     hd.setNewValue('testing');
     hd.setOldValue('Old');
     hd.setUserName('Neha');
     hd.setCreatedDate('27/4/2012');
     hd.setId('12345');
     hd.getObjectName();
     hd.getParentId();
     hd.getField();
     hd.getCreatedById();
     hd.getIsDeleted();
     hd.getNewValue();
     hd.getOldValue();
     hd.getUserName();
     hd.getCreatedDate();
     hd.getId();
            
     Account a=new Account(name='Acc');  
     Database.insert(a);
     test.stopTest();
    }



}