@RestResource(urlMapping='/getFaqDetailId/*')
global with sharing class KBMS_ArticlesFaqDetailsRestFul
{
    @HttpGet
    global static FaqWrapper getCategoryList() 
    {
        String articleId = RestContext.request.params.get('articleId');
        
        String language = Label.KBMS_Language;
        
        String firstPartQuery = 'SELECT Allegati__ContentType__s,Allegati__Length__s,Allegati__Name__s,Id,KnowledgeArticleId,LastModifiedDate,QUESITO__c,RISPOSTA__c,Summary,Title,UrlName FROM FAQ__kav ';
        String secondPartQuery = ' WHERE publishStatus=\'online\' and Language =:language and Id =:articleId ORDER BY LastModifiedDate DESC NULLS FIRST  UPDATE VIEWSTAT';
                
        String query = firstPartQuery + secondPartQuery;
        
        List<FAQ__kav> res = Database.query(query);
        
        FaqWrapper fw = new FaqWrapper();
        if(res.size()==1)
        {
            fw.faq=res[0];
            fw.attachmentUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/fileField?entityId=' + articleId + '&field=Allegati__Body__s';
        }

        return fw;
        
    }
    
    global class FaqWrapper
    {
        public FAQ__kav faq {get;set;}
        public String attachmentUrl {get;set;}
    } 
}