public without sharing class TaskHelper{

/* 
* Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
* Createddate : 07-09-2015
* Description : Method invoked to create a task when an email arrives
* Input :	Name 					Type				Description
*			message					EmailMessage		input email message
*			u						User				User that own the task
* Output :  
*			N/A						Task				the new task created	
*/		
	 public static Task createTask ( EmailMessage message, User u){
	 	Task newTask = new Task();
  		newTask.Priority = Constants.TASK_PRIORITY_NORMAL;
  		newTask.WhatId = message.ParentId;
  		newTask.Status = Constants.TASK_STATUS_NOTSTARTED;
  		newTask.Subject = LABEL.TASK_STATUS;
  		newTask.OwnerId = u.id;
  		newTask.ActivityDate = date.today();
  		newTask.IsReminderSet = true;
  		newTask.ReminderDateTime = message.MessageDate;
  		return newTask;
	 }
/* 
* Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
* Createddate : 08-09-2015
* Description : Method invoked to close all task related to the same case when 1 of them is closed
* Input :	Name 					Type				Description
*			taskIn					list<Task>			list of task updated
* Output :  
*			N/A						Task				all related task is updated
*/		 
	 public static void closeRelatedTask (list<Task> taskIn){
	 	if (taskIn != null && !taskIn.isEmpty() ){
	 		list<String> listOfParentClosed = new list<String>();
	 		for(Task taskClosed :taskIn ){
	 			// check if task is closed and is an auto-generated task ( searcing into subject)
	 			if (Constants.TASK_STATUS_COMPLETED.equals(taskClosed.status) && LABEL.TASK_STATUS.equals(taskClosed.Subject)){
	 				// add the whatid to a list
	 				listOfParentClosed.add(taskClosed.WhatId);
	 			}
	 		}
	 		if (!listOfParentClosed.isEmpty()){
	 			try{
	 				// using the whatid search for all task not completed whit the wathid found after
	 				list<Task> listOfTaskToClose = [select id, status from Task where whatid in : listOfParentClosed and status != :Constants.TASK_STATUS_COMPLETED ];
		 			if (listOfTaskToClose != null && !listOfTaskToClose.isEmpty() ){
		 				// set the status 'Completed' for all task
		 				for (Task t :listOfTaskToClose ){
		 					t.status = Constants.TASK_STATUS_COMPLETED;
		 				}
		 				update listOfTaskToClose;
		 			}
	 			
	 			}
	 			catch(Exception e){
	 				system.debug(logginglevel.Error,'Exception in closeRelatedTask : '+e.getMessage());
	 			}
	 		}
	 	}
	 
	 }
}