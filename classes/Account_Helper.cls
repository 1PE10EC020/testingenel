public without sharing class Account_Helper {


	
	public static map<String,id> mapRecordTypeAccountbyName {
            get{
                    if(mapRecordTypeAccountbyName == null){
                    
                        list<RecordType> listOfRecordType = [Select id, developername from recordtype where sobjecttype ='Account'];
                        if (listOfRecordType != null && !listOfRecordType.isEmpty() ){
                            mapRecordTypeAccountbyName = new map<String,id> ();
                            for (RecordType r :listOfRecordType )
                                mapRecordTypeAccountbyName.put(r.developername, r.id);
                        }
                    }
                return mapRecordTypeAccountbyName;
            }
            private set;
	}
	
   /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 10-07-2015
    * Description : Utility method used to retrive all the account from a list with a fiscal code's length specified from the imput parameter
    * Input :	Name 					Type					Description
    *			listAccIn				list<Account>			list of account to analize
    *			lenght					integer					fiscal code's length we are looking for
    * Output :  
    *			N/A						list<Account>			account's list with specified fiscal code length
    * 
    */
    public static list<Account> retriveLenght(list<Account> listAccIn, integer length){
        list<Account> listAccountToReturn = new list<Account>();
        try{
            if (listAccIn != null && !listAccIn.isEmpty() )
            for (Account singleAcc : listAccIn ){
                if (singleAcc.CodiceFiscale__c.length() == length )
                    listAccountToReturn.add ( singleAcc);
            
            }
            
        }
        Catch(Exception e){
            system.debug(logginglevel.error,' exception raised into Account_Helper ' + e.getMessage());
        
        }
        return listAccountToReturn;
    
    }
    /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 10-07-2015
    * Description : Utility method used to retrive all the account with a vat number populated ( or fiscal code populated as a vat number)
    * Input :	Name 					Type					Description
    *			listAccIn				list<Account>			list of account to analize
    * Output :  
    *			N/A						list<Account>			account's list with vat number
    * 
    */
    public static list<Account> retrivePIVA(list<Account> listAccIn){
        list<Account> accountWhitPIVA = new list<Account> ();
            if (listAccIn != null && !listAccIn.isEmpty() ) {
                for(Account singleAccount : listAccIn) {
                    if(! String.isBlank(singleAccount.PartitaIVA__c) && singleAccount.PartitaIVA__c.length() == 11 )
                        accountWhitPIVA.add(singleAccount);
                    if(! String.isBlank(singleAccount.CodiceFiscale__c) && singleAccount.CodiceFiscale__c.length() == 11 )
                        accountWhitPIVA.add(singleAccount);
                }   
            }
        return accountWhitPIVA;
    
    }
    
      /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 10-07-2015
    * Description : Utility method used to validate the fiscal code or vat number
    * Input :	Name 					Type					Description
    *			listAccIn				list<Account>			list of account to analize
    */
    public static void manageAccountOnTrigger (list<Account> listAccIn ){
        list<Account> AccountWith16Digit = Account_Helper.retriveLenght (listAccIn, 16); // Account's with fiscal code populated with 16 digit
        list<Account> AccountWithPIVA = Account_Helper.retrivePIVA(listAccIn);// Account's with fiscal code or vat number populated with 11 digit
        // chehc if Fiscal code is valid (when nationality is Italian)
        if (AccountWith16Digit != null && !AccountWith16Digit.isEmpty() ){          
            for(Account singleAcc : AccountWith16Digit){
            	if(Constants.ACCOUNT_NAZIONALITA.equals(singleAcc.Nazionalita__c)){
	                String validationCFResult = CodiceFiscale_Helper.CheckFiscalCode(singleAcc.CodiceFiscale__c,singleAcc.Nome__c,singleAcc.Name);
	                if (!'OK'.equals ( validationCFResult ))
	                    singleAcc.addError(validationCFResult);
	            }
            }
        }
        // check if PIVA is valid ( or Fiscal code with 11 digit) when nationality is Italian
        if (AccountWithPIVA != null && !AccountWithPIVA.isEmpty() ){
            for ( Account singleAccount : AccountWithPIVA) {
	            if(Constants.ACCOUNT_NAZIONALITA.equals(singleAccount.Nazionalita__c)){
	                String result = '';
	                if (!String.isBlank(singleAccount.CodiceFiscale__c) && singleAccount.CodiceFiscale__c.length() == 11 ) {
	                    result =      PartitaIVA_Helper.chechPIVA (singleAccount.CodiceFiscale__c);
	                    if ( !'OK'.equals(result))
	                        singleAccount.addError(result);
	                }
	                if (!String.isBlank(singleAccount.PartitaIVA__c)) {
	                    result =      PartitaIVA_Helper.chechPIVA (singleAccount.PartitaIVA__c);
	                    if ( !'OK'.equals(result))
	                        singleAccount.addError(result);
	                }
	  
	            }

            }
        
        }
     //list<Account> toReturn = new list<Account>();
     //toReturn.addall(AccountWith16Digit);
     //toReturn.addall(AccountWithPIVA);
     //return toReturn;
    }
    
      /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 10-07-2015
    * Description : Utility method used to searc an account by his fiscal code
    * Input :	Name 					Type					Description
    *			fiscalCode				String					fiscal code to search
    *output : 
    *			N/A						Account					Account found
    */
    public static Account retriveAccountByFiscalCode (string fiscalCode){
   	system.debug(logginglevel.info,'Account_Helper - retriveAccountByFiscalCode - START');
    	try{
    		if(string.isNotBlank(fiscalCode)){
    			// query to search the account, used limi 1 beacuse codiceFiscale__c is a unique's field.
    			Account acc = [Select id from Account where CodiceFiscale__c = : fiscalCode limit 1];
    			system.debug(logginglevel.debug,'Account_Helper - retriveAccountByFiscalCode - IVR PAth  found with id : '+acc.id);
    			return acc;
    		}
    	}
    	catch(Exception e){
    		system.debug(logginglevel.error, ' Exception in Account_Helper - retriveAccountByFiscalCode : '+e.getMEssage());
    	}
    	return null;
    }
    
      /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 10-07-2015
    * Description : Utility method used to searc an account by his fiscal code
    * Input :	Name 					Type					Description
    *			fiscalCode				String					fiscal code to search
    *output : 
    *			N/A						Account					Account found
    */
    public static list<Account> retriveAccountByPhoneNumber(string phoneNumber){
    system.debug(logginglevel.info,'Account_Helper - retriveAccountByPhoneNumber - START');
    system.debug(logginglevel.debug,'Account_Helper - retriveAccountByPhoneNumber - phoneNumber :'+phoneNumber);
		list<Account>accountFound = new list<Account>();
    	try{
    		if(string.isNotBlank(phoneNumber)){
    			// query to search the account, used limi 1 beacuse codiceFiscale__c is a unique's field.
    			//List<List <sObject>> searchList = [FIND :phoneNumber IN PHONE FIELDS RETURNING Account (Id, Name,Nome__c,recordtype.name,CodiceFiscale__c,PartitaIVA__c,Email__c)];
    		//	system.debug(logginglevel.debug,'Account_Helper - retriveAccountByPhoneNumber - searchList :'+searchList);
    			//accountFound = searchList[0];
    			accountFound = [select Id, Name,Nome__c,recordtype.name,CodiceFiscale__c,PartitaIVA__c,Email__c from account where phone = :phoneNumber OR   telefono1__C = :phoneNumber OR telefono2__C = :phoneNumber OR telefono3__C = :phoneNumber OR telefono4__C = :phoneNumber OR telefono5__C = :phoneNumber];
    			system.debug(logginglevel.debug,'Account_Helper - retriveAccountByPhoneNumber - listOfAccountFound : '+accountFound);
    			return accountFound;
    		}
    	}
    	catch(Exception e){
    		system.debug(logginglevel.error, ' Exception in Account_Helper - retriveAccountByFiscalCode : '+e.getMEssage());
    	}
    	return null;
    }
    
    
    
    
    
    
    
    public static Account createNewAccount(String name, String lastName, String fiscalCode, String pIva, String recordType, String email){
    	try{
    		Account richiedente = new Account();
    		if (name != null && String.isNotBlank(name) && lastName != null && String.isNotBlank(lastName) 
    			&& fiscalCode != null && String.isNotBlank(fiscalCode) && recordType != null && String.isNotBlank(recordType))
    			{
    				richiedente.recordtypeid= Account_Helper.mapRecordTypeAccountbyName.get(Constants.ACCOUNT_PERSONA_FISICA);
    				richiedente.Nome__c = name;
    				richiedente.Name = lastName;
    				richiedente.CodiceFiscale__c = fiscalCode;
    				richiedente.PartitaIVA__c = pIva;
    				richiedente.Email__c = email; 
    				richiedente.Nazionalita__c = 'Italiana';   				
    				return richiedente;
    			}
    		else{
    			return null;
    		}
	    	
    	}
    	catch(Exception e){
                system.debug(logginglevel.error,'Exception in Case_Helper - createNewCase : '+e.getMessage());
            }
        return null;
    	
    }

}