Global class WSPedCase {
    
    /* 
    * Autor : Giorgio Peresempio <giorgio.peresempio@accenture.com>
    * Createddate : 8-10-2015
    * Description : Webservice class used as input for the webservice  
    * Input :  
    *           Email                       String              open case email 
    *           Cognome                     String              Account Name
    *           Nome                        String              Account.Nome__c
    *           CodiceFiscale               String              Account.CodiceFiscale__c
    *           PartitaIva                  String              Account.PartitaIVA__c
    *           Nazionalità                 String              Account.Nazionalita__c
    *           Telefono                    String              inbound phone number
    *           Tipo                        String              type of issue
    *           CF                          String              customer fiscal code
    *           Livello1                    String              Depend of the enviroment
    *           Livello2                    String              Depend of the enviroment 
    *           Livello3                    String              Depend of the enviroment
    *           CaseRecordType              String              RecordType of the Case
    *           ParentCase                  String              related case number
    *           Descrizione                 String              desciption of the issue
    *           ServizioFunzionalita        String              Value choice from the customer
    *           DataOccorrenzaProblema      String              Date when has been catched the issue
    *           POD                         String              pod related to the case 
    */
   
    global class WSPedCase_Input{
        WebService String Email;
        /*Campi per inserimento Account*/
        WebService String Cognome;
        WebService String Nome;
        WebService String CodiceFiscale;
        WebService String PartitaIva;
       
        
        WebService String Telefono;
        WebService String Tipo;
        WebService String Livello1;
        WebService String Livello2;
        WebService String Livello3;
        WebService String CaseRecordType;
        WebService String ParentCase;
        WebService String Descrizione;
        WebService String ServizioFunzionalita;
        WebService String DataOccorrenzaProblema;
        WebService String POD;
        /*[GP CR S00000030]*/
        WebService String Oggetto;
        WebService String Priorita;
        WebService String Argomenti;
        WebService String Famiglia;
        WebService String SituazionePrestazioneSottesa;
        WebService String SottofamigliaSituazionePrestazionesot;
        /*[END CR S00000030]*/
    }
    
    /* 
    * Autor : Giorgio Peresempio <giorgio.peresempio@accenture.com>
    * Createddate : 8-10-2015
    * Description : Webservice class used to return the result on external service       
    * Output :  
    *           codice                      String              result Code ( 001 --> error found case not created, 000 --> no error found case  created)
    *           descrizione                 String              error description
    *           risultato                   String              result (OK case created, KO case not created)
    */

    Global class  WSPedCase_output {
        WebService String risultato;  
        WebService String codice;     
        WebService String descrizione;
        
        public WSPedCase_output (string res, string code, string des) {
            risultato = res;
            codice = code;
            descrizione = des;
        }
    }
    
    /* 
    * Autor : Giorgio Peresempio <giorgio.peresempio@accenture.com>
    * Createddate : 8-10-2015
    * Description : Webservice used to create a case with web origin
    * input :  
    *           inCase          WSPedCase_Input         input data for case's creation
    *output :
    *                           WSPedCase_output        response for the operation  
    *       
    */
    
    webservice static WSPedCase_output CreatePEDCase (WSPedCase_Input inCase){        
        try
        {
            String emailTemplate = System.Label.LAB_ET_RichiedenteAutoReply;
            Case newCase;
            boolean found;
            boolean creaAccount;
            list<Account> listAccount = new list<Account>();
            Account richiedente; 
            //Check if Email is setted in the request (email is mandatory)
            if (inCase.Email != null && String.isNotBlank(inCase.Email))
            {
				//Tipologia obbligatoria
				
				//if(inCase.CaseRecordType != null && String.isNotBlank(inCase.CaseRecordType))
				//{
					//Argomenti__c Obbligatorio
					//if(inCase.Argomenti != null && String.isNotBlank(inCase.Argomenti))
					//Argomenti__c Obbligatorio se la Tipologia (CaseRecordType) è PROGETTI	
					/*[GP CR S00000030]*/
					if(('PROGETTI'.equalsIgnoreCase(inCase.CaseRecordType)) && (inCase.Argomenti == null || String.isBlank(inCase.Argomenti)))					
					{
						//Error response if the Famiglia (Argomenti__c) value is not setted
						system.debug(logginglevel.error,'Per Tipologia Progetti Famiglia obbligatoria');
						return new WSPedCase_output('KO','001','Per Tipologia Progetti Famiglia obbligatoria');
					}
					else
					{
						/*END [GP CR S00000030]*/
						if(inCase.CaseRecordType != null && String.isNotBlank(inCase.CaseRecordType))
						{

							//Create a new Case
							try
							{
								/*[GP 06092016 - S00000020]*/
								found = false;
								creaAccount = true;
								if (inCase.PartitaIva == null || String.isBlank(inCase.PartitaIva))
								{
									listAccount = [Select Id, email__c, CodiceFiscale__c, Nome__c, Name From Account where email__c =: inCase.email];
									if (listAccount.size() >= 1)
									{
										//richiedente = listAccount.get(0);
										//Verifico che il richiedente inviato dal PED esiste in SFDC
										for (Account singleAccount : listAccount){
											if (inCase.CodiceFiscale != null && String.isNotBlank(inCase.CodiceFiscale) 
												&& inCase.Nome != null && String.isNotBlank(inCase.Nome)
												&& inCase.Cognome != null && String.isNotBlank(inCase.Cognome)
												&& singleAccount.CodiceFiscale__c.equalsIgnoreCase(inCase.CodiceFiscale)
												&& inCase.Nome.equalsIgnoreCase(singleAccount.Nome__c)
												&& inCase.Cognome.equalsIgnoreCase(singleAccount.Name))
												{
													System.debug('*** Trovato 1 Account con l email del PED ed è già presente in SFDC: '+singleAccount);
													newCase = Case_Helper.createNewCase(singleAccount,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
													newCase.Email_Richiedente__c = inCase.Email;
													found = true;
												}
											if(!found)
											{
												if (inCase.CodiceFiscale != null && String.isNotBlank(inCase.CodiceFiscale)
													&& singleAccount.CodiceFiscale__c.equalsIgnoreCase(inCase.CodiceFiscale))
													{
														System.debug('*** esiste un account in SFDC con il cf uguale a quello passato dal PED: '+singleAccount);
														creaAccount = false;
													}
													else 
													{
														if(inCase.Nome == null || String.isBlank(inCase.Nome)
															|| inCase.Cognome == null || String.isBlank(inCase.Cognome))
															{
																creaAccount = false;
															}
													}
											}

										}
										if(!found && creaAccount)
										{
											System.debug('*** Trovato 1 Account con l email del PED ma i dati dell Account passati dal PED non coincidono con SFDC: ');                 
											/*[GP] 19092016*/
											
											String validationCFResult = CodiceFiscale_Helper.CheckFiscalCode(inCase.CodiceFiscale,inCase.Nome,inCase.Cognome);
											
											
											if ('OK'.equals ( validationCFResult ))
											{
												richiedente = Account_Helper.createNewAccount(inCase.Nome, inCase.Cognome, inCase.CodiceFiscale, inCase.PartitaIva, Constants.ACCOUNT_PERSONA_FISICA,inCase.Email);
												if (richiedente != null)
												{
													insert richiedente;
													newCase = Case_Helper.createNewCase(richiedente,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
												}
												else
												{
													newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
												}
												
											}   
											else
											{
												newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
												if(newCase.Description == null)
												 {
													newCase.Description = '';
												 }
												newCase.Description = 'Nome: '+inCase.Nome+'\r\n  Cognome: '+inCase.Cognome+'\r\n  CodiceFiscale: '+inCase.CodiceFiscale;
											}   
											
											
											newCase.Email_Richiedente__c = inCase.Email;
										}
										if(!found && !creaAccount)
										{
											newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
											newCase.Email_Richiedente__c = inCase.Email;
											if(inCase.Nome != null && String.isNotBlank(inCase.Nome))
											 {
												 //newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
												 if(newCase.Description == null)
												 {
													newCase.Description = '';
												 }
												 newCase.Description = 'Nome: '+inCase.Nome;
											 }
											 if(inCase.Cognome != null && String.isNotBlank(inCase.Cognome))
											 {
												
												if(newCase.Description == null)
												{
													newCase.Description = '';
												}
												newCase.Description = newCase.Description+'\r\n  Cognome: '+inCase.Cognome;
											 }
											 if (inCase.CodiceFiscale != null && String.isNotBlank(inCase.CodiceFiscale))
											 {
												 if(newCase.Description == null)
												 {
													newCase.Description = '';
												 }
												newCase.Description = newCase.Description+'\r\n  CodiceFiscale: '+inCase.CodiceFiscale;
											 }
										}
										
									}
									else
									{
										if (listAccount.size() == 0)
										{// Account non torvato attraverso l'email fornita
											if (inCase.Nome != null && String.isNotBlank(inCase.Nome)
												&& inCase.Cognome != null && String.isNotBlank(inCase.Cognome)
												&& inCase.CodiceFiscale != null && String.isNotBlank(inCase.CodiceFiscale) )
												{
													System.debug('*** Nessun Account trovato con Email del PED, inserisco nuovo account: ');
														
													String validationCFResult = CodiceFiscale_Helper.CheckFiscalCode(inCase.CodiceFiscale,inCase.Nome,inCase.Cognome);
											
											
													if ('OK'.equals ( validationCFResult ))
													{
														richiedente = Account_Helper.createNewAccount(inCase.Nome, inCase.Cognome, inCase.CodiceFiscale, inCase.PartitaIva, Constants.ACCOUNT_PERSONA_FISICA,inCase.Email);
														if (richiedente != null)
														{
															insert richiedente;
															newCase = Case_Helper.createNewCase(richiedente,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
														}
														else
														{
															newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
														}
														
													}   
													else
													{
														newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
														if(newCase.Description == null)
														 {
															newCase.Description = '';
														 }
														newCase.Description = 'Nome: '+inCase.Nome+'\r\n  Cognome: '+inCase.Cognome+'\r\n  CodiceFiscale: '+inCase.CodiceFiscale;
													}   
											
											
													newCase.Email_Richiedente__c = inCase.Email;
												
												}
												else
												{
													System.debug('*** Nessun Account trovato con Email del PED, NON inserisco nuovo account: '+richiedente);
													newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
													newCase.Email_Richiedente__c = inCase.Email;
													if(newCase.Description == null)
													{
														newCase.Description = '';
													}
													newCase.Description = 'Nome: '+inCase.Nome+'\r\n  Cognome: '+inCase.Cognome+'\r\n  CodiceFiscale: '+inCase.CodiceFiscale;
												}
										}
									}
								//}
								}
								else 
								{
									System.debug('*** Case Creato con Partita Iva associata alla request');
									newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);
									if(newCase.Description == null)
									{
										newCase.Description = '';
									}                                           
									newCase.Description ='Nome: '+inCase.Nome+'\r\n  Cognome: '+inCase.Cognome+'\r\n  PartitaIva: '+inCase.PartitaIva+'\r\n  CodiceFiscale: '+inCase.CodiceFiscale;
									newCase.Email_Richiedente__c = inCase.Email;
								}    
													
							}
							catch(Exception e){
								newCase = Case_Helper.createNewCase(null,null,'','','',inCase.POD,'',Constants.ORIGIN_WEB,inCase.Telefono,Constants.CASE_STATUS_NUOVO);  
							}
								   
							newCase.SuppliedEmail = inCase.Email;
							//For method setToAddresses
							List<String> emaiToSend = new List<String>{newCase.SuppliedEmail};
							
							//Verify if email is setted for do the query
							try{
								if (inCase.CaseRecordType != null && String.isNotBlank(inCase.CaseRecordType)){
									newCase.RecordTypeId = [SELECT Id FROM RecordType where name =:inCase.CaseRecordType  limit 1].Id;
								}
								else{
									newCase.RecordTypeId = [SELECT Id FROM RecordType where name = :Constants.CASE_DA_DEFINIRE_NAME  limit 1].Id;
								}
							}
							catch(Exception e){
									
								newCase.RecordTypeId = [SELECT Id FROM RecordType where name = :Constants.CASE_DA_DEFINIRE_NAME  limit 1].Id;
							}
							
							//Enanched value fields case with the request   
							newCase.Type = inCase.Tipo;
							newCase.Livello1__c = inCase.Livello1; 
							newCase.Livello2__c = inCase.Livello2; 
							newCase.Livello3__c = inCase.Livello3; 
							
							/*[GP 06092016 - S00000020]*/
							newCase.Priority = inCase.Priorita; 
							newCase.Famiglia__c = incase.Argomenti;
							/*[GP CR S00000030]*/
							newCase.Argomenti__c = incase.Famiglia;
							newCase.SituazionePrestazioneSottesa__c = incase.SituazionePrestazioneSottesa;
							newCase.SottofamigliaSituazionePrestazionesot__c = incase.SottofamigliaSituazionePrestazionesot;
							newCase.Data_Apertura_PED__c = inCase.DataOccorrenzaProblema;
							newCase.Tipo_di_Richiesta__c = inCase.ServizioFunzionalita;
							newCase.Note__c = inCase.Descrizione;
							
							/* [GP] 19092016 modifica per aggiunta dell'oggetto di default*/
							newCase.Subject = incase.Oggetto;
							/*[END CR S00000030]*/
							
						   /* END [GP] 19092016 modifica per aggiunta dell'oggetto di default*/     
							if (String.isNotBlank(inCase.ParentCase) ){
								Case parentCase  = Case_Helper.retriveCaseFromCaseNum (inCase.ParentCase);
								if (parentCase != null)
									newCase.parentid = parentCase.id;
							 }
							if(newCase.Description == null){
								newCase.Description = '';
							}
							newCase.Description = newCase.Description+'\r\n Note: '+inCase.Descrizione+'\r\n Tipo di Richiesta: '+inCase.ServizioFunzionalita+'\r\n Data Apertura PED: '+inCase.DataOccorrenzaProblema;
							newcase.ownerid = [select id from Group where type='Queue' and  name  =:Constants.QUEUE_BO limit 1].id;
							
							//S0000020 [GP 12092016]
							System.debug('*** prima del set DataPrevistaRisoluzioneCriticita__c '+newcase);
							newcase.DataPrevistaRisoluzioneCriticita__c = Date.today().addDays(15);
							System.debug('*** Dopo del set DataPrevistaRisoluzioneCriticita__c '+newcase.DataPrevistaRisoluzioneCriticita__c);
							insert newCase;
							
							
							//sendEmail_Helper.wfSendEmail(newCase,emailTemplate); //IL METODO E' STATO CREATO PERCHe' non Funzionava l'autoreply email lanciata da WF nel case di PEDtoCase
							/*
							String numberCase = [SELECT CaseNumber from case where id=:newCase.id].get(0).CaseNumber;
							System.debug('*** numberCase: '+numberCase);
							
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							//Used in setTargetObjectId that is mandatory method
							Id fake = [Select id from Contact].get(0).id;
							
							mail.setTargetObjectId(fake);
							mail.setTreatTargetObjectAsRecipient(false);
							
							
							mail.setWhatId(newCase.id);
							
							mail.setToAddresses(emaiToSend);
							
							EmailTemplate et = [SELECT  Description, Body, Id, Name, Encoding, FolderId, HtmlValue,  BrandTemplateId, Markup,  TemplateStyle, Subject, SystemModstamp, TemplateType, 
												DeveloperName FROM EmailTemplate where DeveloperName like '%Support_Richiedente_Auto_reply%'].get(0);
							
							mail.setTemplateId(et.id);
							Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
						   */
							System.debug('*** WSPedCase_output, emailTemplate: '+emailTemplate);
							sendEmail_Helper.TrackEmail(newCase,emailTemplate);
							
							/*
							EmailMessage emailDB = new EmailMessage();
							String bodyEmail = et.Body;
							
							bodyEmail = bodyEmail.replace('{!Case.CaseNumber}', numberCase);
							//emailDB.TextBody = et.Body;
							//emailDB.TextBody.replaceAll('{!Case.CaseNumber}', numberCase);
							emailDB.TextBody = bodyEmail;
							
							
							String subj = ' Auto-Replay: Enel Distribuzione : '+numberCase;
							emailDB.Subject = subj;
							//emailDB.Subject = et.Subject;
							
							emailDB.ToAddress = newCase.SuppliedEmail;
							emailDB.MessageDate = Datetime.now();
							emailDB.ParentId = newCase.id;
							
							insert emailDB;
							*/
							/*
							SELECT ActivityId, BccAddress, CcAddress, ParentId, CreatedById, CreatedDate, IsDeleted, Id, FromAddress, FromName, HtmlBody,  Headers, Incoming, 
							 LastModifiedDate, MessageDate, Status, Subject, SystemModstamp, TextBody, ToAddress FROM EmailMessage
							*/
							
							
							return new WSPedCase_output('OK','000','');
						  /*  Response.codice = '000';
							Response.risultato = 'OK';
							Response.descrizione = '';
							*/
						}
						else
						{
							//Error response if the Tipologia value is not setted
							system.debug(logginglevel.error,'Tipologia obbligatoria');
							return new WSPedCase_output('KO','001','Tipologia obbligatoria mancante');
						}
					}	
				
			}       
			else
			{
				//Error response if the email value is not setted
				system.debug(logginglevel.error,'Email obbligatoria');
				return new WSPedCase_output('KO','001','Email obbligatoria mancante');
			}

			
        }
        catch(Exception e){
            //return an generic error response
            system.debug(logginglevel.error, 'Generic Exception in WSPedCase - CreatePedCase : '+ e.getmessage());
            return new WSPedCase_output('KO','001','Errore generico');  
        }  
    }

}