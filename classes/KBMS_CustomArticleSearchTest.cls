/* 
* @author Atos India Pvt Ltd. 
* @date  Sept,2016
* @description This class is test class of KBMS_CustomArticleSearch
*/
@isTest(SeeAllData=false)

public class KBMS_CustomArticleSearchTest{    
    
    static testMethod void testMethod_FAQ_Search() {   
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
         
            
        List<Moduli__kav> modulitest = KBMS_TestDataFactory.createModuliTestRecords(2);
        System.assertEquals(modulitest.size(), 2);
        System.Assert(modulitest[0].Id != null, 'The Test Moduli article typed id not inserted properly');    
            
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(faqtest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        //To cover KBMS_UtilityToCheckFLS class
        List<String> sezioneContactFields = new String [] {'Nome_Sezione__c','Tipologia_Richiedente__c'};
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isAccessible');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isQueryable');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isUndeletable');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isUpdateable');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isDeletable');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isCreateable');
        KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'NegativeCheck');
        KBMS_UtilityToCheckFLS.checkFLSPermission('Sezione_Contatti__c', sezioneContactFields, 'isAccessible');
        KBMS_UtilityToCheckFLS.checkFLSPermission('Sezione_Contatti__c', sezioneContactFields, 'isCreateable');
        KBMS_UtilityToCheckFLS.checkFLSPermission('Sezione_Contatti__c', sezioneContactFields, 'isUpdateable');
        
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav'; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');   
        obj.searchString =  'a';         
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult(); 
            
        obj.dataCategory = 'KBMS_Operai_Tecnici';
        obj.selectedArticle = 'FAQ__kav'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');     
        obj.SearchResult(); 
            
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  'test1,test2,test3';      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult(); 
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
  //  obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
    //obj.renderOutputPanel();        
            
    }    
    
    static testMethod void testMethod_Moduli_Search() {          
        
        List<Moduli__kav> modulitest = KBMS_TestDataFactory.createModuliTestRecords(2);
        System.assertEquals(modulitest.size(), 2);
        System.Assert(modulitest[0].Id != null, 'The Test Moduli article typed id not inserted properly');    
            
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Moduli__DataCategorySelection datacatTest=new Moduli__DataCategorySelection();
        
        datacatTest.ParentId= modulitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(modulitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Moduli__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :modulitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c(); 
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();  
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult();
        
        obj.selectedArticle = 'Tutto';
        obj.searchString =  'test1,test2'; 
        System.assertEquals(obj.selectedArticle, 'Tutto');    
        obj.SearchResult();  
        
        obj.selectedArticle = 'Tutto';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Tutto');    
        obj.SearchResult(); 
       
        obj.renderScontacts=false;           
      //  obj.renderOutputPanel();
            
        obj.renderScontacts=true;         
       // obj.renderOutputPanel();        
            
    }  
    
    static testMethod void testMethod_Processi_Search() {   
        
        List<Processi__kav> processitest= KBMS_TestDataFactory.createProcessTestRecords(2); 
        System.assertEquals(processitest.size(), 2);
        System.Assert(processitest[0].Id != null, 'The Test Processi article typed id not inserted properly');
                    
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= processitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(processitest[0].Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(processitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :processitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav'; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');   
        obj.searchString =  'atos test';         
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult(); 
            
        obj.dataCategory = 'KBMS_Operai_Tecnici';
        obj.selectedArticle = 'FAQ__kav'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');     
        obj.SearchResult(); 
            
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  'test1,test2,test3';      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult(); 
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
   // obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
   // obj.renderOutputPanel();        
            
    }
    
    static testMethod void testMethod_Tutto_Search() {   
        
        List<Moduli__kav> modulitest = KBMS_TestDataFactory.createModuliTestRecords(2);
        System.assertEquals(modulitest.size(), 2);
        System.Assert(modulitest[0].Id != null, 'The Test Moduli article typed id not inserted properly');  
            
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Moduli__DataCategorySelection datacatTest=new Moduli__DataCategorySelection();
        
        datacatTest.ParentId= modulitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(modulitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Moduli__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :modulitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav'; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');   
        obj.searchString =  'atos test';         
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult(); 
            
        obj.dataCategory = 'KBMS_Operai_Tecnici';
        obj.selectedArticle = 'FAQ__kav'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');     
        obj.SearchResult(); 
            
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  'test1,test2,test3';      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult(); 
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
  //  obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
   // obj.renderOutputPanel();        
            
    }  
    
    static testMethod void testMethod_Contact_Search() {   
        
       user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
        
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
        
        
        
       List<Contact> contacttest= KBMS_TestDataFactory.createContactTestRecords(2);        
       System.assertEquals(contacttest.size(), 2);       
       System.Assert(contacttest[0].Id != null, 'The Test Moduli article typed id not inserted properly'); 
       system.debug('contacttest::'+contacttest);
        
       List<Sezione_Contatti__c> sezionecontattitest= KBMS_TestDataFactory.createSezioneTestRecords(2);        
       System.assertEquals(sezionecontattitest.size(), 2);
       system.debug('sezionecontattitest::'+sezionecontattitest); 
       System.Assert(sezionecontattitest[0].Id != null, 'The Test Moduli article typed id not inserted properly');  
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        test.startTest(); 
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
        obj.dataCategory = '';
        obj.selectedArticle = 'Contact'; 
        obj.searchString =  'atos test';
        obj.SearchResult(); 
        
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult();
        
        
        obj.SearchResult();
        
        system.runAs(usr)
        {
        
        obj.dataCategory = '';
        obj.selectedArticle = 'Contact'; 
        obj.searchString =  'atos test';
        obj.SearchResult();  
         
        
        }  
        
        test.stopTest();        
        
        }  
    
    static testMethod void testMethod_Negative_FAQ_Search() {   
        
        List<Processi__kav> processitest= KBMS_TestDataFactory.createProcessTestRecords(2); 
        System.assertEquals(processitest.size(), 2);
        System.Assert(processitest[0].Id != null, 'The Test Processi article typed id not inserted properly');
                    
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= processitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(processitest[0].Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(processitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :processitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Processi__kav'; 
        System.assertEquals(obj.selectedArticle, 'Processi__kav');   
        obj.searchString =  'atos test';         
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Moduli__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Moduli__kav');    
        obj.SearchResult();             
                
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
   // obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
   // obj.renderOutputPanel();        
            
    }   
   
    static testMethod void testMethod_Negative_Moduli_Search() {          
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');    
            
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(faqtest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
       
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c(); 
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();  
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'FAQ__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');    
        obj.SearchResult();
        
        obj.selectedArticle = 'Tutto';
        obj.searchString =  'test1,test2'; 
        System.assertEquals(obj.selectedArticle, 'Tutto');    
        obj.SearchResult();  
        
        obj.selectedArticle = 'Tutto';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Tutto');    
        obj.SearchResult(); 
       
        obj.renderScontacts=false;           
      //  obj.renderOutputPanel();
            
        obj.renderScontacts=true;         
       // obj.renderOutputPanel();        
            
    } 
   
    static testMethod void testMethod_Negative_Processi_Search() {   
        
        List<Moduli__kav> modulitest = KBMS_TestDataFactory.createModuliTestRecords(2);
        System.assertEquals(modulitest.size(), 2);
        System.Assert(modulitest[0].Id != null, 'The Test Moduli article typed id not inserted properly');    
            
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Moduli__DataCategorySelection datacatTest=new Moduli__DataCategorySelection();
        
        datacatTest.ParentId= modulitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(modulitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Moduli__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :modulitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
                   
        obj.dataCategory = 'KBMS_Operai_Tecnici';
        obj.selectedArticle = 'FAQ__kav'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');     
        obj.SearchResult(); 
            
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  'test1,test2,test3';      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult(); 
        
        obj.selectedArticle = 'Tutto'; 
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Tutto');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
   // obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
   // obj.renderOutputPanel();        
            
    }
        
     static testMethod void testMethod_Negative_Tutto_Search() {   
        
        List<Processi__kav> processitest= KBMS_TestDataFactory.createProcessTestRecords(2); 
        System.assertEquals(processitest.size(), 2);
        System.Assert(processitest[0].Id != null, 'The Test Processi article typed id not inserted properly');
                    
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= processitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(processitest[0].Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(processitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :processitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
         
         KBMS_CustomSearch__c moduli = new KBMS_CustomSearch__c();        
        
        moduli.Name='Moduli';
        moduli.Fields__c='Title,Summary';        
        insert moduli;
        
        test.startTest();       
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Processi__kav'; 
        System.assertEquals(obj.selectedArticle, 'Processi__kav');   
        obj.searchString =  'atos test';         
        
        obj.SearchResult();      
         
        obj.dataCategory = '';
        obj.selectedArticle = 'Processi__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'Processi__kav');    
        obj.SearchResult(); 
            
                
        
        obj.searchString =  'test1,test2,test3';      
        System.assertEquals(obj.selectedArticle, 'Processi__kav');     
        obj.SearchResult(); 
        
        obj.searchString =  NULL;      
        System.assertEquals(obj.selectedArticle, 'Processi__kav');     
        obj.SearchResult();         
            
       
    obj.renderScontacts=false;       
           
  //  obj.renderOutputPanel();
            
    obj.renderScontacts=true;         
   // obj.renderOutputPanel();        
            
    }  
   
    
    static testMethod void testMethod_Negative_Contact_Search() {   
        
       user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(faqtest.size(), 2);
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
        
        
        List<Processi__kav> processitest= KBMS_TestDataFactory.createProcessTestRecords(2); 
        System.assertEquals(processitest.size(), 2);
        System.Assert(processitest[0].Id != null, 'The Test Processi article typed id not inserted properly');
                    
        String prepositionWorld = Label.KBMS_prepositionWords;
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;    
        
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= processitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest; 
        System.assert(processitest[0].Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(processitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :processitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
         
        insert customSearchFaq;
       
        KBMS_CustomSearch__c customSearchContact = new KBMS_CustomSearch__c();        
        
        customSearchContact.Name='Contact';
        customSearchContact.Fields__c='FirstName,LastName,Email';
         
        insert customSearchContact;
        
        KBMS_CustomSearch__c customSearchSezione = new KBMS_CustomSearch__c();        
        
        customSearchSezione.Name='Sezione_Contatti__c';
        customSearchSezione.Fields__c='Nome_Sezione__c';        
        insert customSearchSezione;
        
        test.startTest(); 
        
        KBMS_CustomArticleSearch obj =new KBMS_CustomArticleSearch();
        
        obj.dataCategory = '';
        obj.selectedArticle = 'Processi__kav'; 
        obj.searchString =  'atos test';
        obj.SearchResult(); 
        
        obj.dataCategory = '';
        obj.selectedArticle = 'FAQ__kav';
        obj.searchString =  NULL; 
        System.assertEquals(obj.selectedArticle, 'FAQ__kav');    
        obj.SearchResult();
        
        
        obj.SearchResult();
        
        system.runAs(usr)
        {
        
        obj.dataCategory = '';
        obj.selectedArticle = 'Contact'; 
        obj.searchString =  'atos test';
        obj.SearchResult();  
         
        
        }  
        
        test.stopTest();        
        
        }  
    
    
}