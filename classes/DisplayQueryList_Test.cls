@IsTest
public class DisplayQueryList_Test {
    /*
    @testSetup
	private static void setup() {
		
		 
  	 	User fibra = CreateTestUser(Constants.PROFILE_FIBRA, 'Team Leader Contatore e Fibra');
  		fibra.username = 'classTestEmailHelperFibra@enel.test.com';
  		insert fibra;
  		
  		User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
    	backoffice.username = 'classTestEmailHelperBO@enel.test.com';
   		insert backOffice;
  	
  		
  		User admin = CreateTestUser('System Administrator', 'Contact Management');
        admin.username = 'classTestEmailHelperAdmin@enel.test.com';
        insert admin;
		
	}
    */
     @isTest
     static void loginProfiloFibra(){
  	     User fibra = new User();
  	     fibra.firstname = 'User';
            fibra.lastName = 'test';
            fibra.email = 'userTestEmailHelper@enel.test.com';
            fibra.alias ='alias';
            fibra.TimeZoneSidKey = 'GMT';
         fibra.LocaleSidKey = 'it_IT'; 
         fibra.LanguageLocaleKey ='it';
         fibra.EmailEncodingKey='ISO-8859-1';
  	     fibra.username = 'classTestEmailHelperFibra@enel.test.com';
  	     fibra.profileid =  [Select id from profile where name = :'Operatore II Livello Fibra' limit 1].id;
         fibra.UserRoleId = [Select id from Userrole where name = : 'Team Leader Contatore e Fibra' limit 1 ].id;
         insert fibra;
  	     fibra = [select id from user where username = 'classTestEmailHelperFibra@enel.test.com'];
  	
  		 test.startTest();
  		 System.runAs(fibra){
  		 	
  		 DisplayQueryList controller = new DisplayQueryList();
  		 
  		
  		 AggregateResult[] rec = controller.Records;
  		 
  		 //controller.DisplayQueryList();
  		 
  		 }
  		
  		 
  		 test.stopTest(); 
  }
  
    
   @isTest
     static void loginProfiloFibraOperatore(){
  	     User fibra = new User();
  	     fibra.firstname = 'User';
            fibra.lastName = 'test';
            fibra.email = 'userTestEmailHelper@enel.test.com';
            fibra.alias ='alias';
            fibra.TimeZoneSidKey = 'GMT';
         fibra.LocaleSidKey = 'it_IT'; 
         fibra.LanguageLocaleKey ='it';
         fibra.EmailEncodingKey='ISO-8859-1';
  	     fibra.username = 'classTestEmailHelperFibra@enel.test.com';
  	     fibra.profileid =  [Select id from profile where name = :'Operatore II Livello Fibra' limit 1].id;
         fibra.UserRoleId = [Select id from Userrole where name = : 'Contatore e Fibra' limit 1 ].id;
         insert fibra;
  	     fibra = [select id from user where username = 'classTestEmailHelperFibra@enel.test.com'];
  	
  		 test.startTest();
  		 System.runAs(fibra){
  		 	
  		 DisplayQueryList controller = new DisplayQueryList();
  		 
  		 
  		
  		 AggregateResult[] rec = controller.Records;
  		 
  		 //controller.DisplayQueryList();
  		 
  		 }
  		
  		 
  		 test.stopTest(); 
  }
  
       @isTest
       static void loginProfiloAdmin (){
  	
  	 User admin = new User();
  	     admin.firstname = 'User';
            admin.lastName = 'test';
            admin.email = 'userTestEmailHelper@enel.test.com';
            admin.alias ='alias';
            admin.TimeZoneSidKey = 'GMT';
         admin.LocaleSidKey = 'it_IT'; 
         admin.LanguageLocaleKey ='it';
         admin.EmailEncodingKey='ISO-8859-1';
  	     admin.username = 'classTestEmailHelperAdmin@enel.test.com';
  	     admin.profileid =  [Select id from profile where name = :'System Administrator' limit 1].id;
         admin.UserRoleId = [Select id from Userrole where name = : 'Contact Management' limit 1 ].id;
         insert admin;
  	 admin = [select id from user where username = 'classTestEmailHelperAdmin@enel.test.com'];
  		
  		 test.startTest();
  		 System.runAs(admin){
  		 	
  		 DisplayQueryListAmministratore controller = new DisplayQueryListAmministratore();
  		 
  		 AggregateResult[] rec = controller.Records;
  		 
  		 //controller.DisplayQueryList();
  		 
  		
  	
  		 }
  		 test.stopTest(); 
  }
 
  
    
     /*
    public static User CreateTestUser(string profileName, string rolename){
            User u = new User();
            u.username = 'classTestEmailHelper@enel.test.com';
            u.firstname = 'User';
            u.lastName = 'test';
            u.email = 'userTestEmailHelper@enel.test.com';
            u.alias ='alias';
            u.TimeZoneSidKey = 'GMT';
            u.LocaleSidKey = 'it_IT'; 
            u.EmailEncodingKey='ISO-8859-1';
            u.profileid =  [Select id from profile where name = :profileName limit 1].id;
            u.UserRoleId = [Select id from Userrole where name = : rolename limit 1 ].id;
            u.LanguageLocaleKey ='it';
            return u;
        }
       */
}