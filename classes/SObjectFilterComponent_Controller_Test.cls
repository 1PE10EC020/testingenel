@isTest(SeeAllData = true)
public class SObjectFilterComponent_Controller_Test {

	static testMethod void TestgetTranslationPicklist(){

		//PT_ALLEGATO TEST
        String sobjectAllegato = 'PT_Allegato__c';
        PT_Allegato__c allegato = new PT_Allegato__c();
        allegato.IdAllegato__c = 932720;
        allegato.TipoFile__c = 'Fine Opere Cliente';
        allegato.Direzione__c = 'I';
        insert allegato;
        String allegatoId = allegato.Id;
        SObjectFilterComponent_Controller.PaginationWrapper wrapper;
        String fieldName = 'Direzione__c';

        Test.startTest();

        wrapper = SObjectFilterComponent_Controller.getTranslationPicklist(allegatoId, allegato, fieldName);
        
        Test.stopTest();
	}

}