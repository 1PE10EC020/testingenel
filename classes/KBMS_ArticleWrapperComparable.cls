global class KBMS_ArticleWrapperComparable implements Comparable 
{
    	public sObject articles;
        public String parentName;
        public String parentLabel;
        public String dataCategoryName;
        public String dataCategoryLabel;
        public Double likeScore; 
        public Integer viewCount;      
        public Integer totalLikes; 
        public Integer totalDislikes;
    	public Static String sortBy;
    	public Static Boolean sortByDesc;
    	
    public KBMS_ArticleWrapperComparable(sObject articles, String parentName, String parentLabel, String dataCategoryName, String dataCategoryLabel, Double likeScore, Integer viewCount, Integer totalLikes, Integer totalDislikes)
    {
        this.parentName =  parentName;
        this.parentLabel =   parentLabel;
        this.dataCategoryName =   dataCategoryName;
        this.dataCategoryLabel =   dataCategoryLabel;
        this.likeScore =   likeScore;
        this.viewCount =   viewCount;
        this.totalLikes =   totalLikes;
        this.totalDislikes =   totalDislikes;
        this.articles =   articles;
        
        
    }
    
    // Implement the compareTo() method of Comparable Interface
    global Integer compareTo(Object compareTo) 
    {
        	KBMS_ArticleWrapperComparable compareToSubCat = (KBMS_ArticleWrapperComparable)compareTo;
        	if(sortBy == 'likeScore'){
                return sortByLikes(compareToSubCat);
                
            }else if(sortBy == 'viewCount'){
                return sortByViews(compareToSubCat);
            }
        	
        	return 0;  
     }
    
    /**
        * Sorts by the viewCount of the article
        * 
        * @return the integer value of the comparison between the objects
        */
        private Integer sortByViews(KBMS_ArticleWrapperComparable article) {
            if(sortByDesc){
                if (this.viewCount < article.viewCount) {//Descending
                return 1;
            	}
            }else{
                if (this.viewCount > article.viewCount) {//Ascending
                return 1;
            	}
            }
            

            if (this.viewCount == article.viewCount) {
                return 0;
            }

            return -1;
        }

        /**
        * Sorts by the LikeScore of the article
        * 
        * @return the integer value of the comparison between the objects
        */
        private Integer sortByLikes(KBMS_ArticleWrapperComparable article) {
           if(sortByDesc){
                if (this.likeScore < article.likeScore) {//Descending
                return 1;
            	}
            }else{
                if (this.likeScore > article.likeScore) {//Ascending
                return 1;
            	}
            }

            if (this.likeScore == article.likeScore) {
                return 0;
            }

            return -1;
        }

}