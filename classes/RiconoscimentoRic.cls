global class RiconoscimentoRic {
  
   webservice String nTelefono; 
   
   global class Esito {
       webservice String risultato; 
       webservice String codice;
       webservice String descrizione;
       webservice String codFiscale;
       webservice String partIva;
       webservice String email;
        
   }
  
    webservice static Esito RiconoscimentoRicService (String nTelefono) {
        Esito esito = new Esito();
        try{
            list<Account> listAcc = Account_Helper.retriveAccountByPhoneNumber(nTelefono);
            if(listAcc != null && !listAcc.isEmpty())
            {
                if(listAcc.size() == 1){
                    // One account found
                    Account accFound = listAcc[0];   
                    esito.risultato = 'OK';
                    esito.codice ='000';
                    esito.descrizione = 'Operation succesfully executed';
                    esito.codFiscale = accFound.CodiceFiscale__c;
                    if(String.isNotBlank(accFound.Email__c) )
                        esito.email = accFound.Email__c+';';
                    else{
                         // if the email field is blank, the resalt should be "account not found"
		                esito.risultato = 'KO';
		                esito.codice ='001';
		                esito.descrizione = 'No Record Found';
		                esito.codFiscale = '';
		                esito.email = '';
                    }
                }
                else{
                    // More than 1 account found
                    esito.risultato = 'KO';
                    esito.codice ='001';
                    esito.descrizione = 'No Record Found';
                    esito.codFiscale = '';
                    esito.email = '';
                }
            }
            else{
                // No Account Found
                esito.risultato = 'KO';
                esito.codice ='001';
                esito.descrizione = 'No Record Found';
                esito.codFiscale = '';
                esito.email = '';
            }
        }
        catch(Exception e){
         // Generic Exception Raised
            esito.risultato = 'KO';
            esito.codice ='001';
            esito.descrizione = 'No Record Found';
            esito.codFiscale = '';
            esito.email = '';
            system.debug(logginglevel.error,'Exception in WS RiconoscimentoRic :'+e.getMessage());
        }
       return esito;
  }
  

}