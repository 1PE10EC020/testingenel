public class KaranPicklistTest {

    @AuraEnabled   
    public static DateTime PT_getEndDate(String startDate){
       	DateTime stDate = date.valueOf(startDate);
        DateTime newEndDT = stDate.addHours(22);
        newEndDT = newEndDT.addMinutes(-1);
        return newEndDT;
    } 
/*public static PT_PicklistAuraComponent getRichiestaPicklistValues(String RecordTypeName, String FieldAPINAME, String ContFieldAPI, String ContFieldVal, String ContField2API, String ContField2Val){
    system.debug('xxxxxxxxx'+RecordTypeName+'yyyyyyyy'+FieldAPINAME);

    	List<Richieste__c> reqAllPicklist = Richieste__c.getall().values();
 system.debug('xxreqqqqqqqq'+reqAllPicklist);
    	set<string> picklistValueSet = new set<string>();
        
        for(Richieste__c reqPicklist:reqAllPicklist){
            if(reqPicklist.Record_Type__c==RecordTypeName && reqPicklist.Picklist_Api_Name__c==FieldAPINAME){
                if(ContFieldAPI!='' && ContFieldVal!=''){
                    if(ContField2API!='' && ContField2Val!=''){
                        if(reqPicklist.Controlling_Field_API__c == ContFieldAPI && reqPicklist.Controlling_Field_Value__c == ContFieldVal &&
                          reqPicklist.Controlling_Field_2_API__c == ContField2API && reqPicklist.Controlling_Field_2_Value__c == ContField2Val)  {
                    		picklistValueSet.add(reqPicklist.Picklist_Value__c);  
                    	}
                    }    
                    else{
                        if(reqPicklist.Controlling_Field_API__c == ContFieldAPI && reqPicklist.Controlling_Field_Value__c == ContFieldVal)  {
                    		picklistValueSet.add(reqPicklist.Picklist_Value__c);  
                    	}
                    }  
                }
                else{    
                	picklistValueSet.add(reqPicklist.Picklist_Value__c);
                }    
            }
        }
        
        List<PT_PicklistAuraComponent.SelectOptionWrapper> picklistValueList = new List<PT_PicklistAuraComponent.SelectOptionWrapper>();
        List<PT_PicklistAuraComponent.SelectOptionWrapper> picklistValueListFinal = new List<PT_PicklistAuraComponent.SelectOptionWrapper>();
        
        picklistValueList = PT_Utility.getPickListValues('PT_Richiesta__c',FieldAPINAME);
        
        for(PT_PicklistAuraComponent.SelectOptionWrapper pickRecord:picklistValueList){
            if(picklistValueSet.contains(pickRecord.value))
            {
                picklistValueListFinal.add(pickRecord);
            }
        }
        
		PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        obj.pickListValues.put(FieldAPINAME,picklistValueListFinal);
        system.debug('final List@@@@'+picklistValueListFinal);
        system.debug('Final map@@@@'+obj);
        
        return obj;
    }

    @AuraEnabled   
public static PT_PicklistAuraComponent getAllRichiestaPicklistValues(String RecordTypeName){  
    	PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        obj.pickListValues.put('IdConnessione__c',PT_Utility.getPickListValues('PT_Richiesta__c','IdConnessione__c')); 
        system.debug('@@@@@@'+obj);
        return obj;
    }
    
    @AuraEnabled 
public static WrapperPicklist getAllRichiestaPicklistValuesCntr(String RecordTypeName){
    
    	PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        obj.pickListValues.put('IdConnessione__c',PT_Utility.getPickListValues('PT_Richiesta__c','IdConnessione__c')); 
        obj.pickListValues.put('IdTipoContr__c',PT_Utility.getPickListValues('PT_Richiesta__c','IdTipoContr__c')); 
        obj.pickListValues.put('Tensione__c',PT_Utility.getPickListValues('PT_Richiesta__c','Tensione__c')); 
        
        List<Richieste__c> reqAllPicklist = Richieste__c.getall().values();
    
        WrapperPicklist wrapperlist= new WrapperPicklist(obj,reqAllPicklist);
        system.debug('@@@@@@'+wrapperlist);
        return wrapperlist;
    }
    
    Public with sharing class WrapperPicklist {
        @AuraEnabled
        public PT_PicklistAuraComponent picklistObjectMap{get;set;}
        @AuraEnabled
        public List<Richieste__c> reqPicklistCustomSetting{ get;set;}
        public WrapperPicklist(PT_PicklistAuraComponent pickVals,List<Richieste__c> pickCustomSetting) {
            picklistObjectMap=pickVals;
            reqPicklistCustomSetting=pickCustomSetting;
            
        }
    }*/

}