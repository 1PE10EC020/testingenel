/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddEmailTrackingSentByButton_Test {


    static testMethod void invioEmailalBO() {
        list<string> listOfCaseID = new list<string>();
        list<Case> listOfCase = new list<Case>();
        
        case c = BHaddFromProcess_Test.GenerateCase();
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = true;
        //User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
        
        
        c.OwnerId= [select name, id from Group where type='Queue' and name like 'Back%' limit 1].Id;
        //c.OwnerId = '00G26000000UVmREAW';
        system.debug(logginglevel.debug,' *** Test OwnerId :'+c.OwnerId);
        //UnitaDiCompetenza__c unitaDiCompetenza = new UnitaDiCompetenza__c();
        
        
        insert c;
        //Case_Helper_Test.setup();
        listOfCaseID.add(c.id);
        
        listOfCase.add(c);
        
       AddEmailTrackingSentByButton.trackEmailSent(listOfCaseID);
       AddEmailTrackingSentByButton.trackEmailSent(listOfCase);
       AddEmailTrackingSentByButton.trackEmailSent(c);
       
       
        
    }
     /*
        static testMethod void invioEmailaFibra() {
        list<string> listOfCaseID = new list<string>();
        list<Case> listOfCase = new list<Case>();
        
            case c = new case();
       c.recordTypeId = [select id from recordtype where developername = 'Progetti' ].id ;
       
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.TipologiaCase__c ='Progetti';
        c.famiglia__c='Cambio CE';

        c.tech_IsStatusIILivello__c = true;
        //User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
        
        
        c.OwnerId= [select name, id from Group where type='Queue' and name like 'Contatore%' limit 1].Id;
        //c.OwnerId = '00G26000000UVmREAW';
        system.debug(logginglevel.debug,' *** Test OwnerId :'+c.OwnerId);
        //UnitaDiCompetenza__c unitaDiCompetenza = new UnitaDiCompetenza__c();
        insert c;
        
    
        //Case_Helper_Test.setup();
        listOfCaseID.add(c.id);
        
       
        
       AddEmailTrackingSentByButton.trackEmailSent(listOfCaseID);
    
       
        
    }
   
   
 static testMethod void invioEmail_ZonaIILivelo() {
        list<string> listOfCaseID = new list<string>();
        
        case c = BHaddFromProcess_Test.GenerateCase();
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = true;
        c.Description = 'Descrizione Zona';
        c.Eventuali_Note_Richiedente__c = 'Note richiedente';
        c.Famiglia__c = 'famiglia';
        c.SottofamigliaSituazionePrestazionesot__c = 'sottofamiglia';
        c.Livello3__c = 'livello3';
        c.SituazionePrestazioneSottesa__c = ' prestazione sottesa';
        c.POD__c ='IT001E020070030';
        c.DirezioneDTR__c = 'Piemonte';
        
        
        c.status = '';
        c.tipologiacase__c = '';
        c.UnitaDiCompetenza__c = 'ALESSANDRIA-ASTI';
        c.IdPratica__c = '2302300';
        c.Danni__c = 'danni';
        c.ReiteroDaAltroCanale__c = true;
        
        
        
        //User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
        
        
        
        
        c.OwnerId= [select name, id from Group where type='Queue' and name like 'ALESSANDRIA%' limit 1].Id;
        //c.OwnerId = '00G26000000UX3ZEAW';
        system.debug(logginglevel.debug,' *** Test OwnerId :'+c.OwnerId);
        UnitaDiCompetenza__c unitaDiCompetenza = new UnitaDiCompetenza__c();
        unitaDiCompetenza.name = 'ALESSANDRIA-ASTI';
        unitaDiCompetenza.TipoUnitOrganizzativa__c = 'Zona';
        insert unitaDiCompetenza;
        
        
        insert c;
        //Case_Helper_Test.setup();
        listOfCaseID.add(c.id);
        
       AddEmailTrackingSentByButton.trackEmailSent(listOfCaseID);
        
    }
   
     static testMethod void invioEmail_DTRIILivelo() {
        list<string> listOfCaseID = new list<string>();
        list<Case> listOfCase = new list<Case>();
        
        case c = BHaddFromProcess_Test.GenerateCase();
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = true;
        //User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
        
        
        c.OwnerId= [select name, id from Group where type='Queue' and name like 'Piemonte%' limit 1].Id;
        //c.OwnerId = '00G26000000UX4XEAW';
        system.debug(logginglevel.debug,' *** Test OwnerId :'+c.OwnerId);
        UnitaDiCompetenza__c unitaDiCompetenza = new UnitaDiCompetenza__c();
        unitaDiCompetenza.name = 'Piemonte Liguria';
        unitaDiCompetenza.TipoUnitOrganizzativa__c = 'DTR';
        insert unitaDiCompetenza;
        
        
        //insert c;
        //Case_Helper_Test.setup();
        listOfCaseID.add(c.id);
        
       listOfCase.add(c);
        
       AddEmailTrackingSentByButton.trackEmailSent(listOfCaseID);
       AddEmailTrackingSentByButton.trackEmailSent(listOfCase);
       AddEmailTrackingSentByButton.trackEmailSent(c);
        
    }*/ 
   
}