public class Four_Helper {
        public Case myCase {get;set;}
        public AsyncFOURTypes.DashboardArrReplay_elementFuture responseCont;
        /*
        *@author Claudio Quaranta
        *@created 09/07/2015
        *@Description : Constructor with 1 parameter
        *
        *Inputs :   Name                        Type                                    Description
        *           inCase                      Case                                    Case to manage
        */
            public Four_Helper (Case inCase){
                this.mycase = inCase;
            }
    
        /*
        *@author Claudio Quaranta
        *@created 09/07/2015
        *@Description : Method to invoke async callout WS 
        *
        *Inputs :   Name                        Type                                    Description
        *    N/A
        *Outputs:
        *           con                         Continuation                            technical object used to manage the continuation
        */
        /* COMMENTED ASYNC CALL 
        public Continuation CallArricchimentoServiceASync (){
            Continuation con = new Continuation(120);
            con.continuationMethod= 'ManageAsyncResponse';
            AsyncFOURService.AsyncPortTypeEndpoint1 asyncWService = new AsyncFOURService.AsyncPortTypeEndpoint1();
            responseCont = asyncWService.beginArricchimento(con,myCase.POD__C);
            return con;
        
        }
        */
        /*
        *@author Claudio Quaranta
        *@created 09/07/2015
        *@Description : Callback method , used to manage the continuation response
        *
        *Inputs :   Name                        Type                                    Description
        *    N/A
        *Outputs:
        *   N/A 
        */
        /* commented async manage response
        public void ManageAsyncResponse (){
            FourService_Arricchimento_Response complexResponse; // custom class, used to manage the WS response
            try{
                // get the response object from the Continuation Object
                FOURTypes.DashboardArrReplay_element response =  responseCont.getValue(); 
                if (response != null )
                    complexResponse = new FourService_Arricchimento_Response(response, true);
                else
                    complexResponse = new FourService_Arricchimento_Response(false);
            }
            catch(Exception e){
                complexResponse = new FourService_Arricchimento_Response(false);
            }
            // Call the static metod to parse the WS Response
            myCase = Four_Helper.ManageFourResponse(complexResponse, myCase);
        }
    */
        /*
        *@author Claudio Quaranta
        *@created 25/06/2015
        *@Description : Output Class used into the webservice
        */
        public class FourService_Arricchimento_Response {
            public boolean esito {get;set;} // Rappresent the webservice outcome ( true -> no error | false -> error)
            public FOURTypes.DashboardArrReplay_element wsResponseElement {get;set;}  // rapresent the WS outcome
            
            // Default 0-arg constructor
            public FourService_Arricchimento_Response (){
                esito  = false;
                wsResponseElement = new FOURTypes.DashboardArrReplay_element();
            }
            
            // constructor with response
            public FourService_Arricchimento_Response (FOURTypes.DashboardArrReplay_element inRes ){
                esito  = true;
                wsResponseElement = inRes;
            }
            
            // constructor with outcome
            public FourService_Arricchimento_Response (boolean es ){
                esito  = es;
                wsResponseElement = null;
            }
        
            // constructor with 2 arg
            public FourService_Arricchimento_Response (FOURTypes.DashboardArrReplay_element inRes, boolean es ){
                esito  = es;
                wsResponseElement = inRes;
            }
        }


        /*
        *@author Claudio Quaranta
        *@created 25/06/2015
        *@Description : Helper class that invoche the sync service "Arricchimento"
        *
        *Inputs :   Name                        Type                                    Description
        *           pod                         String                                  Pod input
        *Outputs : 
        *           response.esito              Boolean                                 true -> no error had occurred | false -> an error occurred
        *           response.wsResponseElement  FOURTypes.DashboardArrReplay_element    contains the Webservice's response
        */
        
        public static FourService_Arricchimento_Response CallArricchimentoServiceSync (String pod){
            /* TO DO - insert header section */
            FourService_Arricchimento_Response response ;
            try{
                if ( !String.isBlank(pod)){
                    FOURService.PortTypeEndpoint1 WService= new  FOURService.PortTypeEndpoint1();
                    system.debug(logginglevel.debug,' CallArricchimentoServiceSync - endpoint called is : '+WService.endpoint_x);
                    response  = new FourService_Arricchimento_Response(WService.Arricchimento(pod)); 
                } 
                else
                    response = new FourService_Arricchimento_Response(false);
            }
            catch(CalloutException CExc){
                system.debug(Logginglevel.error,'Callout exception into Four_Helper.CallArricchimentoServiceSync :'+ CExc.getMessage());
                /* TO DO - Insert callout exception handling */
                response = new FourService_Arricchimento_Response(false);
            }
            catch (Exception e){
                system.debug(Logginglevel.error,'Generic exception into Four_Helper.CallArricchimentoServiceSync :'+ e.getMessage());
                /* TO DO - Insert Generic exception handling */
                response = new FourService_Arricchimento_Response(false);
            }
            return response;
        }
        
        /*
        *@author Claudio Quaranta
        *@created 09/07/2015
        *@Description : static method, used to parse the WS Response (Async or sync is the same), and assing the reponse value to case object
        *
        *Inputs :   Name                        Type                                    Description
        *       response                        FourService_Arricchimento_Response      Response generated from the WS
        *       inCase                          Case                                    Case to update
        *Outputs:
        *                                       Case                                    case updated    
        */
        
        private static Case ManageFourResponse(FourService_Arricchimento_Response response, Case inCase){
            if (response.esito ){
            	system.debug(logginglevel.info,'ManageFourResponse - response.wsResponseElement.CodiceFiscaleTitolare : '+response.wsResponseElement.CodiceFiscaleTitolare);
                system.debug(logginglevel.info,'ManageFourResponse - response.wsResponseElement.Nome : '+response.wsResponseElement.Nome);
                system.debug(logginglevel.info,'ManageFourResponse - response.wsResponseElement.RagioneSociale : '+response.wsResponseElement.RagioneSociale);
                system.debug(logginglevel.info,'ManageFourResponse -  response.wsResponseElement.Cognome : '+ response.wsResponseElement.Cognome);
                system.debug(logginglevel.info,'ManageFourResponse -  response.wsResponseElement.PartitaIvaTitolare : '+ response.wsResponseElement.PartitaIvaTitolare);
                system.debug(logginglevel.info,'ManageFourResponse -  response.wsResponseElement.UnitaCompetenza : '+ response.wsResponseElement.UnitaCompetenza);
                system.debug(logginglevel.info,'ManageFourResponse -  response.wsResponseElement.UnitaCompetenza : '+ response.wsResponseElement.DirezioneDtr);
                system.debug(logginglevel.info,'ManageFourResponse - response.wsResponseElement.TrattamentoOrarioPdm : '+ response.wsResponseElement.TrattamentoOrarioPdm);
                system.debug(logginglevel.info,'ManageFourResponse - response.wsResponseElement.LivelloTensioneFornitura : '+ response.wsResponseElement.LivelloTensioneFornitura);
                
                inCase.CodiceFiscaleTitolarePOD__c = response.wsResponseElement.CodiceFiscaleTitolare;
                inCase.NomeTitolarePOD__c = response.wsResponseElement.Nome;
                inCase.CognomeTitolarePOD__c = '';
                if (!String.isBlank(response.wsResponseElement.RagioneSociale) )
                    inCase.CognomeTitolarePOD__c = response.wsResponseElement.RagioneSociale;
                if (!String.isBlank(response.wsResponseElement.Cognome) )
                    inCase.CognomeTitolarePOD__c = response.wsResponseElement.Cognome;
                inCase.PartitaIVATitolarePOD__c  = response.wsResponseElement.PartitaIvaTitolare;
                try{
                	if ( String.isNotBlank( response.wsResponseElement.UnitaCompetenza)){
                		inCase.UnitaDiCompetenza__c = [SELECT Name FROM UnitaDiCompetenza__c WHERE CodiceDTZOUR__c = :  response.wsResponseElement.UnitaCompetenza limit 1].name;
                	}
                	else
                	inCase.UnitaDiCompetenza__c='';
                }
                catch(Exception e){
                	inCase.UnitaDiCompetenza__c='';
                	system.debug(logginglevel.info,'Unita di competenza non trovata');
                }
                try{
                	
                	if(String.isNotBlank(response.wsResponseElement.DirezioneDtr))
                		inCase.DirezioneDTR__c  = [SELECT Name FROM UnitaDiCompetenza__c WHERE CodiceDTZOUR__c = :  response.wsResponseElement.DirezioneDtr limit 1].name;
                	else
                		inCase.DirezioneDTR__c='';
                }
                catch(Exception e){
             	    system.debug(logginglevel.info,'Unita di competenza non trovata');
             	    inCase.DirezioneDTR__c='';
                }
                
                if (Constants.WS_FASCIA_ORARIA.equals (response.wsResponseElement.TrattamentoOrarioPdm))
                    inCase.Oraria__c = true;
                if (Constants.WS_FASCIA_NON_ORARIA.equals (response.wsResponseElement.TrattamentoOrarioPdm) || String.isBlank(response.wsResponseElement.TrattamentoOrarioPdm)) 
                    inCase.Oraria__c = false;
                inCase.LivelloTensioneDiFornitura__c = response.wsResponseElement.LivelloTensioneFornitura;
                
                PopulateFirstLevel(inCase,response);
                PopulateSecondLevel(inCase,response);
                PopulateThirdLevel(inCase,response);
            }
            else 
            throw new ResponseMissingException(Label.EXC_Missing_WS_RESP);
            return inCase;
        }
        
public class MissingPodException extends Exception {}
public class ResponseMissingException extends Exception {}      
        /*
        *@author Claudio Quaranta
        *@created 01/10/2015
        *@Description : static method used to invoke the Sync version of WS
        *
        *Inputs :   Name                        Type                                    Description
        *       response                        FourService_Arricchimento_Response      Response generated from the WS
        *       inCase                          Case                                    Case to update
        *Outputs:
        *                                       Case                                    case updated    
        */
        public class Arricchisci_Case_Response {
        	public case myCase ;
        	public boolean esito;
        	public string errorMessage ;
        	
        	public Arricchisci_Case_Response (Case c, boolean es , string err){
        		myCase = c;
        		esito = es;
        		errorMessage = err;
        	}
        
        }
        
         public static Arricchisci_Case_Response Arricchisci_Case(Case  inCase){
         try{   
         	   if (!String.isBlank(inCase.POD__c) ){
                    FourService_Arricchimento_Response response = CallArricchimentoServiceSync(inCase.POD__c);
                    if ( response.esito ){
                    	inCase = ManageFourResponse(response,inCase);
                   		return new Arricchisci_Case_Response(inCase,true,'');
                    }
                    else{
                    	return new Arricchisci_Case_Response(inCase,false,System.Label.EXC_Timeout);
                    }
                    //return inCase;
                    
                }
                else
                    throw new MissingPodException(Label.EXC_Missing_POD);
        
            }
            catch(Exception e){
                /* TO DO - Exception handling */
                system.debug('Exception Raised into Four_Helper.Arricchisci_Case '+e.getMessage());
            	return new Arricchisci_Case_Response(inCase,false,System.Label.EXC_Timeout);
            }
            
         }
        
        /*
        *@author Claudio Quaranta
        *@created 09/07/2015
        *@Description : static method used to invoke the Sync version of WS
        *
        *Inputs :   Name                        Type                                    Description
        *       response                        FourService_Arricchimento_Response      Response generated from the WS
        *       inCase                          Case                                    Case to update
        *Outputs:
        *                                       Case                                    case updated    
        */
        @future (callout=true)
        public static void Arricchisci_Case(String  inCaseID){
            try{
                Case inCase = [select   id ,
                                        pod__C,
                                        CodiceFiscaleTitolarePOD__c,
                                        NomeTitolarePOD__c,
                                        CognomeTitolarePOD__c,
                                        PartitaIVATitolarePOD__c,
                                        UnitaDiCompetenza__c,
                                        DirezioneDTR__c,
                                        Oraria__c,
                                        LivelloTensioneDiFornitura__c,
                                        recordtypeid,
                                        Livello1__c,
                                        Livello2__c,
                                        Livello3__c
                                         from case where id =:inCaseID ];
                system.debug(logginglevel.debug,'FOUR_HELPER - Arricchisci_Case - inCase : '+inCase);
                if (!String.isBlank(inCase.POD__c) ){
                    FourService_Arricchimento_Response response = CallArricchimentoServiceSync(inCase.POD__c);
                    inCase = ManageFourResponse(response,inCase);
                    update inCase;
                    
                }
                else
                    throw new MissingPodException(Label.EXC_Missing_POD);
        
            }
            catch(Exception e){
                /* TO DO - Exception handling */
                system.debug('Exception Raised into Four_Helper.Arricchisci_Case '+e.getMessage());
            }
        }


        /*
        *@author Claudio Quaranta
        *@created 25/06/2015
        *@Description : private class that populate case's field Livello1__C
        *
        *Inputs :   Name                        Type                                    Description
        *           inCase                      Case                                    input's case
        *           response                    FourService_Arricchimento_Response      response from WS
        *Outputs : 
        *       None
        */
        private static Void PopulateFirstLevel (Case inCase, FourService_Arricchimento_Response response ){
            string caseRT = Constants.mapIdNameRTCase.get(inCase.recordtypeid).developerName;
            system.debug('PopulateFirstLevel + response.wsResponseElement.PresenzaPod' + response.wsResponseElement.PresenzaPod );
            // se RT Connessione e risposta S o N , livello = In prelievo
            if (   Constants.WS_POD_NON_PRESENTE.equals (response.wsResponseElement.PresenzaPod) 
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO;
            
            // se RT Connessione e risposta S 
                if ( Constants.WS_POD_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE;
                   
            // se RT Misura e risposta N 
                if ( Constants.WS_POD_NON_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO;
                   
             // se RT Misura e risposta S 
                if ( Constants.WS_POD_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE;
                                 
             // se RT Trasporto e risposta N 
                if ( Constants.WS_POD_NON_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_TRASPORTO.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO;     
                   
                // se RT Trasporto e risposta S
                if ( Constants.WS_POD_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_TRASPORTO.equals(caseRT) ) 
                   inCase.Livello1__c = Constants.LIVELLO_IN_PRELIEVO_E_IMMISSIONE;   
        }

        
        /*
        *@author Claudio Quaranta
        *@created 25/06/2015
        *@Description : private class that populate case's field livello2__C
        *
        *Inputs :   Name                        Type                                    Description
        *           inCase                      Case                                    input's case
        *           response                    FourService_Arricchimento_Response      response from WS
        *Outputs : 
        *       None
        */
        private static Void PopulateSecondLevel (Case inCase, FourService_Arricchimento_Response response ){
            string caseRT = Constants.mapIdNameRTCase.get(inCase.recordtypeid).developerName;
                    system.debug('PopulateSecondLevel + response.wsResponseElement.LivelloTensioneFornitura' + response.wsResponseElement.LivelloTensioneFornitura );
            // if RT = Connessione and response = S 
                if (  Constants.WS_POD_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_GIA_CONNESSO; 
            // if RT = Connessione and response = N 
                if (  Constants.WS_POD_NON_PRESENTE.equals (response.wsResponseElement.PresenzaPod)  
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_DA_CONNETTERE;    
            // if RT = Misura and livello tensione di fornitura = BT
                if (  Constants.LIVELLO_TENSIONE_FORNTITURA_BT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_CONNESSO_BT;
            // if RT = Misura and livello tensione di fornitura = MT oppure AT
                if (  (Constants.LIVELLO_TENSIONE_FORNTITURA_MT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                        ||  Constants.LIVELLO_TENSIONE_FORNTITURA_AT.equals (response.wsResponseElement.LivelloTensioneFornitura)   )
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_CONNESSO_ATMT;
            //SE Case Record Type ="Trasporto" E POD con "Livello Tensione di Fornitura" = BT in SGM001 Smile
            if (  Constants.LIVELLO_TENSIONE_FORNTITURA_BT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                   && Constants.CASE_TRASPORTO.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_CONNESSO_BT;       
            //SE Case Record Type ="Trasporto" E POD con "Livello Tensione di Fornitura" = MT in SGM001 Smile
            if (  Constants.LIVELLO_TENSIONE_FORNTITURA_MT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                   && Constants.CASE_TRASPORTO.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_CONNESSO_MT;  
            //SE Case Record Type ="Trasporto" E POD con "Livello Tensione di Fornitura" = AT in SGM001 Smile
               if (  Constants.LIVELLO_TENSIONE_FORNTITURA_AT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                   && Constants.CASE_TRASPORTO.equals(caseRT) ) 
                   inCase.Livello2__c = Constants.LIVELLO_CONNESSO_AT;      
                    
        }
        
        
        /*
        *@author Claudio Quaranta
        *@created 25/06/2015
        *@Description : private class that populate case's field livello3__C
        *
        *Inputs :   Name                        Type                                    Description
        *           inCase                      Case                                    input's case
        *           response                    FourService_Arricchimento_Response      response from WS
        *Outputs : 
        *       None
        */
        private static Void PopulateThirdLevel (Case inCase, FourService_Arricchimento_Response response ){
            string caseRT = Constants.mapIdNameRTCase.get(inCase.recordtypeid).developerName;   
            system.debug('PopulateSecondLevel + response.wsResponseElement.TrattamentoOrarioPdm' + response.wsResponseElement.TrattamentoOrarioPdm );
        // SE Case Record Type ="Connessione" E POD con "Livello Tensione di Fornitura" = BT in SGM001 Smile
            if (  Constants.LIVELLO_TENSIONE_FORNTITURA_BT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello3__c = Constants.LIVELLO_TENSIONE_FORNTITURA_BT;   
        //SE Case Record Type ="Connessione" E POD con "Livello Tensione di Fornitura" = MT oppure AT in SGM001 Smile
            if ( ( Constants.LIVELLO_TENSIONE_FORNTITURA_AT.equals (response.wsResponseElement.LivelloTensioneFornitura)  
                    || Constants.LIVELLO_TENSIONE_FORNTITURA_MT.equals (response.wsResponseElement.LivelloTensioneFornitura)   )
                   && Constants.CASE_CONNESSIONE.equals(caseRT) ) 
                   inCase.Livello3__c = Constants.LIVELLO_3_ATMT;
        // SE Case Record Type ="Misura" E POD con "Trattamento orario del PdM" = "Orario" in SGM001 Smile
            if (  Constants.WS_FASCIA_ORARIA.equals (response.wsResponseElement.TrattamentoOrarioPdm)  
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello3__c = Constants.WS_FASCIA_ORARIA; 
        // SE Case Record Type ="Misura" E POD con "Trattamento orario del PdM" = Blank in SGM001 Smile
            if (  (Constants.WS_FASCIA_NON_ORARIA.equals (response.wsResponseElement.TrattamentoOrarioPdm)  
                   || String.isBlank (response.wsResponseElement.TrattamentoOrarioPdm ))
                   && Constants.CASE_MISURA.equals(caseRT) ) 
                   inCase.Livello3__c = Constants.WS_FASCIA_ORARIA; 
        }
}