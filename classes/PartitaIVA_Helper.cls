public without sharing class PartitaIVA_Helper {

	/*
	* @Author : Quaranta Claudio
	* @Date : 22/06/2015
	* @Description : methods to check the 11 digit on vat number
	* @Input :	Name 					Type					Description
	*			vatNumber				String					input VAT Number
	* @Output:  result					String					'OK' -- > the check digit is ok, otherwise it contains an error message
	*/
	
	public static string chechPIVA ( string vatNumber) {
	system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - vatNumber : '+vatNumber );
	String result = '';
		try {
			/* check digiti (c) is : 
			Sia X la somma delle prime cinque cifre in posizione dispari
			Sia Y la somma dei doppi delle cinque cifre in posizione pari, sottraendo 9 se il doppio della cifra è superiore a 9
			Sia T=(X+Y) mod 10 l'unità corrispondente alla somma dei numeri sopra calcolati
			Allora la cifra di controllo C = (10-T) mod 10
			*/
			integer primacifra  = Integer.valueof(vatNumber.substring(0,1));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - primacifra : '+primacifra );
			integer secondacrifa  = Integer.valueof(vatNumber.substring(1,2))*2 > 9? Integer.valueof(vatNumber.substring(1,2))*2 -9 : Integer.valueof(vatNumber.substring(1,2))*2;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - secondacrifa : '+secondacrifa );
			integer terzacifra  = Integer.valueof(vatNumber.substring(2,3));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - terzacifra : '+terzacifra );
			integer quartacifra  = Integer.valueof(vatNumber.substring(3,4))*2> 9? Integer.valueof(vatNumber.substring(3,4))*2 -9 : Integer.valueof(vatNumber.substring(3,4))*2;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - quartacifra : '+quartacifra );
			integer quintacifra  = Integer.valueof(vatNumber.substring(4,5));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - quintacifra : '+quintacifra );
			integer sestacifra  = Integer.valueof(vatNumber.substring(5,6))*2> 9? Integer.valueof(vatNumber.substring(5,6))*2 -9 : Integer.valueof(vatNumber.substring(5,6))*2;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - sestacifra : '+sestacifra );
			integer settimacifra  = Integer.valueof(vatNumber.substring(6,7));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - settimacifra : '+settimacifra );
			integer ottavacifra  = Integer.valueof(vatNumber.substring(7,8))*2> 9? Integer.valueof(vatNumber.substring(7,8))*2 -9 : Integer.valueof(vatNumber.substring(7,8))*2;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - ottavacifra : '+ottavacifra );
			integer nonacifra  = Integer.valueof(vatNumber.substring(8,9));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - nonacifra : '+nonacifra );
			integer decimacifra  = Integer.valueof(vatNumber.substring(9,10))*2> 9? Integer.valueof(vatNumber.substring(9,10))*2 -9 : Integer.valueof(vatNumber.substring(9,10))*2;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - decimacifra : '+decimacifra );
			integer checkdigit  = Integer.valueof(vatNumber.substring(10,11));
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - checkdigit : '+checkdigit );
			
			integer x = primacifra + terzacifra + quintacifra + settimacifra + nonacifra;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - x : '+x );
			integer y = secondacrifa + quartacifra + sestacifra + ottavacifra + decimacifra;
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - y : '+y );
			integer t = Math.mod(x+y,10);
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - t : '+t );
			integer c =  Math.mod(10-t,10);
			system.debug(logginglevel.debug, 'PartitaIVA_Helper - chechPIVA - c : '+c );
			if (String.valueof(checkdigit).equals(String.Valueof(c)) )
				return Constants.CONSTANT_OK;
			else
				return label.PIVA_CheckDigit_Wrong;
			
		}
		catch(Exception e){
			system.debug(logginglevel.error, 'Unandled exeption in PartitaIVA_Helper :' +e.getMessage() );
			return e.getMessage();
		}
	
	}



}