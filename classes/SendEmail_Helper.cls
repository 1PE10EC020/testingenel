public with sharing class SendEmail_Helper {
    
    
    
    public static String bodyEmail;
    public static String subj;
    public static boolean sent = false;
    public static EmailMessage emailDB;
    
    
    
    public SendEmail_Helper(){
    }
    
    //Federica Aloia 26-01-2017 se il case in stato scalato inviare l'email ai team leader corrispondenti
    @InvocableMethod
    public static void SendEmail_TL(List<Id> CaseId){
	list<Case> caseList=[select id, ownerId, CaseNumber, CreatedDate, Priority, TipologiaCase__c, Description from Case where id IN :CaseId];
	system.debug('SendEmail_TL caseList is: ' + caseList);
    String RoleOwnerCase = Case_Helper.getOwnerCase(caseList, true); // ruolo dell'owner del case
	system.debug('SendEmail_TL RoleOwnerCase is: ' + RoleOwnerCase);
    List<User> UserList=new List<User>();    
    if(RoleOwnerCase!='CallBack'){
    	String ParentRoleId=[select id, ParentRoleId from UserRole where name=: RoleownerCase].ParentRoleId; // id del ruolo del teamleader del corrispondente ruolo
    	system.debug('SendEmail_TL ParentRoleId is: ' + ParentRoleId);
    	UserList = [select id, email from User where UserRoleId=:ParentRoleId];
    	system.debug('SendEmail_TL UserList is: ' + UserList);
    }else{
        String ParentRoleId=[select id, ParentRoleId from UserRole where name='Back Office'].ParentRoleId; // id del ruolo del teamleader del corrispondente ruolo
    	system.debug('SendEmail_TL ParentRoleId is: ' + ParentRoleId);
    	UserList = [select id, email from User where UserRoleId=:ParentRoleId];
    	system.debug('SendEmail_TL UserList is: ' + UserList);
    }
        
	/*List<String> emailList = new List<String>();
	List<Id> userId = new List<Id>();
	
	for(User u : UserList){
		emailList.add(u.email);
		userId.add(u.Id);
	}   
*/
	
	Id templateId=[SELECT id FROM EmailTemplate WHERE Name = : Constants.escalationEmail].id;
	String ordAdrrid = [SELECT DisplayName, Address,Id FROM OrgWideEmailAddress where DisplayName =: Label.DISPLAY_NAME].get(0).id;
        if(UserList !=null && !UserList.isEmpty()){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        Id fake = [Select id from Contact].get(0).id;
        for(User currentUser : UserList){
         	List<String> emailList=new List<String>();   
        	emailList.add(currentUser.email); 
         	email.setTargetObjectId(fake);
         	email.setTreatTargetObjectAsRecipient(false); 
         	email.setToAddresses(emailList);
         	email.setWhatId(caseList.get(0).Id);  
        	email.setTemplateId(templateId);
         	email.setSaveAsActivity(false);
            system.debug('Display name is: ' + Label.DISPLAY_NAME + ' email is: '+ordAdrrid);
            email.setOrgWideEmailAddressId(ordAdrrid);
         	Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            system.debug('SendEmail_TL email send: ' + email);
         }
	}
	
	
}
    //IL METODO E' STATO CREATO PERCHe' non Funzionava l'autoreply email lanciata da WF nel case di PEDtoCase
    /*
    public static void wfSendEmail (Case inCase, String typeEmailTemplate){
        
        
        List<String>  emaiToSend;
        //if (queueOwner != null && Constants.QUEUE_BO.equals(queueOwner.name)){
            
        //}
        //Set To Email Address for autoReply t Customer
        if(typeEmailTemplate.equalsIgnoreCase(system.label.LAB_ET_RichiedenteAutoReply)){
            emaiToSend = new List<String>{inCase.SuppliedEmail};
        }else{
            if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_BO')){
                
            }
        }
        
        
        
        
        //List<String> emaiToSend = new List<String>{inCase.SuppliedEmail};
        String numberCase = [SELECT CaseNumber from case where id=:inCase.id].get(0).CaseNumber;
        System.debug('*** numberCase: '+numberCase);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Used in setTargetObjectId that is mandatory method
        Id fake = [Select id from Contact].get(0).id;
        
        mail.setTargetObjectId(fake);
        mail.setTreatTargetObjectAsRecipient(false);
        String ordAdrrid = [SELECT DisplayName, Address,Id FROM OrgWideEmailAddress where Address = 'servizioclienti@eneldistribuzione.it'].get(0).id;
        
        mail.setOrgWideEmailAddressId(ordAdrrid);
        
        
        mail.setWhatId(inCase.id);
        
        mail.setToAddresses(emaiToSend);
        EmailTemplate et = [SELECT  Description, Body, Id, Name, Encoding, FolderId, HtmlValue,  BrandTemplateId, Markup,  TemplateStyle, Subject, SystemModstamp, TemplateType, 
                                    DeveloperName FROM EmailTemplate where DeveloperName like:typeEmailTemplate].get(0);
                
        mail.setTemplateId(et.id);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   
        
    }
    */
    
    /*
    Autor : Giorgio Peresempio <giorgio.peresempio@accenture.com>
        * Createddate : 26-03-2016
        * Description : this method is for email Tracking
        * Input :   Name                    Type                        Description
        *           inCase                  Case                        case that send email (corrispond to Trigger.new)
        *           typeTemplate            String                      Type of email Tempalate
        * Output :  
        *           N/A         
    */
    
    public static void TrackEmail (Case inCase, String typeEmailTemplate){
        
        //Case caseEmail;
        
        
        emailDB = new EmailMessage();
        try{
            EmailTemplate et = [SELECT  Description, Body, Id, Name, Encoding, FolderId, HtmlValue,  BrandTemplateId, Markup,  TemplateStyle, Subject, SystemModstamp, TemplateType, 
                                    DeveloperName, TimesUsed FROM EmailTemplate where DeveloperName like:typeEmailTemplate].get(0);
            
           System.debug('*** inCase:'+inCase);      
            
            System.debug('*** TrackEmail, emailTemplate: '+typeEmailTemplate);              
            if(typeEmailTemplate.equalsIgnoreCase(system.label.LAB_ET_RichiedenteAutoReply)){
                System.debug('*** TrackEmail nell if, emailTemplate: '+typeEmailTemplate);  
                emailDB.ToAddress = inCase.SuppliedEmail;
            }else{
                if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_BO')){
                    System.debug('*** nell if Support_Invia_Email_BO' );
                    emailDB.ToAddress = 'CodaBackOffice@e-distribuzione.it';
                    //caseEmail.EmailBOSent__c = true;
                    //update caseEmail;
                }
                else{
                    if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_II_Livello'))
                        emailDB.ToAddress = 'CodaZonaIILivello@e-distribuzione.it';
                    if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_DTR_II_Livello'))
                        emailDB.ToAddress = 'CodaDTRIILivello@e-distribuzione.it';
                    if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_Info_Piu_II_Livello'))
                        emailDB.ToAddress = 'CodaEnelInfo+@e-distribuzione.it';
                    if(typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_Fibra'))
                        emailDB.ToAddress = 'CodaFibraEContatori@e-distribuzione.it';
                    if(typeEmailTemplate.equalsIgnoreCase('Support_Escalation_Email_Team_Leader'))
                        emailDB.ToAddress = 'CodaTeamLeader@e-distribuzione.it';
                }
                /*
                if (typeEmailTemplate.equalsIgnoreCase('Support_Case_Closure_Survey_Richiedente') && inCase.Account.Email__c != null ){
                    //emailDB.ToAddress = caseEmail.SuppliedEmail;
                    emailDB.ToAddress = inCase.Account.Email__c;
                }
                if (typeEmailTemplate.equalsIgnoreCase('Support_Case_ClosurePending_Survey_Richiedente') && inCase.Account.Email__c != null ){
                    //emailDB.ToAddress = caseEmail.SuppliedEmail;
                    emailDB.ToAddress = inCase.Account.Email__c;
                }
                
                if (typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_Esperto_Zona') && inCase.AssegnatoAllEspertoDiDTRZona__r.Email != null)
                {
                    emailDB.ToAddress = inCase.AssegnatoAllEspertoDiDTRZona__r.Email;
                }
                */
            }
            
            
            bodyEmail = et.Body;
            subj = et.Subject;
            System.debug('*** Prima di setCaseParameterEmail ***');
            
            setCaseParameterEmail (inCase, typeEmailTemplate );
            
            //emailDB.TextBody = et.Body;
            //emailDB.TextBody.replaceAll('{!Case.CaseNumber}', numberCase);
            
            
            emailDB.TextBody = bodyEmail;
            System.debug ('*** emailDB.TextBody '+emailDB.TextBody);
            System.debug ('*** emailDB.ToAddress '+emailDB.ToAddress);
            
            //String subj = ' Auto-Replay: Enel Distribuzione : '+numberCase;
            System.debug ('*** subj '+subj);
            emailDB.Subject = subj;
            //emailDB.Subject = et.Subject;
            
            //emailDB.ToAddress = inCase.SuppliedEmail;
            emailDB.MessageDate = Datetime.now();
            emailDB.ParentId = inCase.id;
            try{
                system.debug('**** sent: '+sent);
                if (sent == false)
                {
                    insert emailDB;
                    
                    sent = true;
                }
                
                else{
                    if(!typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_BO') && !typeEmailTemplate.equalsIgnoreCase('Support_Invia_Email_Esperto_Zona'))
                    {
                        insert emailDB;
                    }
                }
                
                    
            }catch(Exception e ){
                system.debug(logginglevel.error, 'Exception in Insert Email for Trach sent :'+e.getMessage());
            }
        }catch(Exception e ){
                system.debug(logginglevel.error, 'Exception in Track Email query :'+e.getMessage());
            }

        
    }
    /*
    Autor : Giorgio Peresempio <giorgio.peresempio@accenture.com>
        * Createddate : 26-03-2016
        * Description : this method set the parameter of email template for email Tracking
        * Input :   Name                    Type                        Description
        *           caseForEmail            Case                        case that send email (corrispond to Trigger.new)
        *           typeTemplate            String                      Type of email Tempalate
        * Output :  
        *           N/A         
    */
    
    public static void setCaseParameterEmail (Case caseForEmail, String typeTemplate){
        //Case caseForEmail;
        Contact sme;
        try{
            //Query per i campi non diretti del case (es dell'Account) e per i campi che servono all'email del PedToCase
            Case caseLookupParam = [SELECT id,Note__c, Tipo_di_Richiesta__c, Data_Apertura_PED__c, DataPrevistaRisoluzioneCriticita__c, Livello1__c, Livello2__c, Date_Time_Closed__c,POD__c,DirezioneDTR__c,IdPratica__c,Danni__c,ReiteroDaAltroCanale__c,SituazionePrestazioneSottesa__c,SottofamigliaSituazionePrestazionesot__c,Famiglia__c, UnitaDiCompetenza__c,Owner.name,ThreadID__c,Subject,OriginForEmail__c, Email_Richiedente__c, caseNumber, CreatedDate, ClosedDate, AssegnatoAllEspertoDiDTRZona__r.id, AssegnatoAllEspertoDiDTRZona__c, Account.name, Account.Email__c, AssegnatoAllEspertoDiDTRZona__r.Email,Account.Phone
                                from case where id=:caseForEmail.id].get(0);
            
            /*Aggiunta query perchè non prendeva l'email corretta con la select AssegnatoAllEspertoDiDTRZona__r.Email*/ 
            
            
            
            if (caseForEmail.AssegnatoAllEspertoDiDTRZona__c != null)           
            {
                System.debug('*** Assegnato SME: '+caseForEmail.AssegnatoAllEspertoDiDTRZona__c);
                sme = [SELECT email, id from contact where id = :caseForEmail.AssegnatoAllEspertoDiDTRZona__c limit 1];
                System.debug('*** sme : '+sme);
            }                   
            
            System.debug('*** setCaseParameterEmaile caseLookupParam: '+caseLookupParam+' *** typeTemplate: '+typeTemplate);
            
            
            //Per settare ToAddress per il chiuso e chiuso pending e fare solo una query sul case
            System.debug('*** caseForEmail.Email_Richiedente__c:'+caseForEmail.Email_Richiedente__c+' *** caseLookupParam.Email_Richiedente__c: '+caseLookupParam.Email_Richiedente__c);                                    
            if ((typeTemplate.equalsIgnoreCase('Support_Case_Closure_Survey_Richiedente') || typeTemplate.equalsIgnoreCase('Support_Case_ClosurePending_Survey_Richiedente'))
                 && caseForEmail.Email_Richiedente__c != null )
            {
                //emailDB.ToAddress = caseEmail.SuppliedEmail;
                /*[GP 23082016 Segn00168624 ]*/
                //emailDB.ToAddress = caseLookupParam.Account.Email__c;
                emailDB.ToAddress = caseForEmail.Email_Richiedente__c;
            }
			if ((typeTemplate.equalsIgnoreCase('Support_Invia_Email_Esperto_Zona') || typeTemplate.equalsIgnoreCase('Support_Invia_Email_Esperto_Zona_PED')) /*&& caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.Email != null*/&& sme.Email!= null) //[GP 12122016] S00000030 email sme per ped
            {
                //System.debug('*** Support_Invia_Email_Esperto_Zona, caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.Email: '+caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.Email);
                System.debug('*** Support_Invia_Email_Esperto_Zona da trigger.new, caseForEmail.AssegnatoAllEspertoDiDTRZona__r.Email: '+caseForEmail.AssegnatoAllEspertoDiDTRZona__r.Email);
                //emailDB.ToAddress = caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.Email;
                emailDB.ToAddress = sme.Email;
            }
            
            
            
            
                
            //valorized the parameter in Subject e body of email
            bodyEmail = bodyEmail.replace('{!Case.CaseNumber}', caseLookupParam.CaseNumber);
            subj = subj.replace('{!Case.CaseNumber}', caseLookupParam.CaseNumber);
            
            if (caseLookupParam.Subject != null)
            {
                subj = subj.replace('{!Case.Subject}', caseLookupParam.Subject);
                bodyEmail = bodyEmail.replace('{!Case.Subject}', caseLookupParam.Subject);
            }
            
            else
            {
                subj = subj.replace('{!Case.Subject}', '');
                bodyEmail = bodyEmail.replace('{!Case.Subject}', '');
            }
            /*[GP 12122016] S00000030 email sme per ped*/
            
            if (caseLookupParam.Note__c != null)
            {
                subj = subj.replace('{!Case.Note__c}', caseLookupParam.Note__c);
                bodyEmail = bodyEmail.replace('{!Case.Note__c}', caseLookupParam.Note__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.Note__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Note__c}', '');
            }
            
            if (caseLookupParam.Data_Apertura_PED__c != null)
            {
                System.debug('*** data apertura ped '+caseLookupParam.Data_Apertura_PED__c);
                subj = subj.replace('{!Case.Data_Apertura_PED__c}',' '+caseLookupParam.Data_Apertura_PED__c);
                //bodyEmail = bodyEmail.replace(' {!Case.CreatedDate}',' '+ string.valueOf(caseLookupParam.CreatedDate));
                bodyEmail = bodyEmail.replace('{!Case.Data_Apertura_PED__c}',caseLookupParam.Data_Apertura_PED__c);
            System.debug('*** data apertura ped bodyEmail: '+bodyEmail);
                                    
            }           
            else
            {
                System.debug('*** Else create date ');
                subj = subj.replace('{!Case.Data_Apertura_PED__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Data_Apertura_PED__c}',  '');
            }
            
            
            if (caseLookupParam.DataPrevistaRisoluzioneCriticita__c != null)
            {
                System.debug('*** DataPrevistaRisoluzioneCriticita__c date '+caseLookupParam.DataPrevistaRisoluzioneCriticita__c.day()+'/'+caseLookupParam.DataPrevistaRisoluzioneCriticita__c.month()+'/'
            +caseLookupParam.DataPrevistaRisoluzioneCriticita__c.year());
                subj = subj.replace('{!Case.DataPrevistaRisoluzioneCriticita__c}',' '+caseLookupParam.DataPrevistaRisoluzioneCriticita__c.day()+'/'+caseLookupParam.DataPrevistaRisoluzioneCriticita__c.month()+'/'
                                    +caseLookupParam.DataPrevistaRisoluzioneCriticita__c.year());
                //bodyEmail = bodyEmail.replace(' {!Case.CreatedDate}',' '+ string.valueOf(caseLookupParam.CreatedDate));
                bodyEmail = bodyEmail.replace('{!Case.DataPrevistaRisoluzioneCriticita__c}',caseLookupParam.DataPrevistaRisoluzioneCriticita__c.day()+'/'+caseLookupParam.DataPrevistaRisoluzioneCriticita__c.month()+'/'
                                    +caseLookupParam.DataPrevistaRisoluzioneCriticita__c.year());
            System.debug('*** DataPrevistaRisoluzioneCriticita__c date bodyEmail: '+bodyEmail);
                                    
            }           
            else
            {
                System.debug('*** Else create date ');
                subj = subj.replace('{!Case.DataPrevistaRisoluzioneCriticita__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.DataPrevistaRisoluzioneCriticita__c}',  '');
            }
            
            if (caseLookupParam.Tipo_di_Richiesta__c != null)
            {
                subj = subj.replace('{!Case.Tipo_di_Richiesta__c}',' '+caseLookupParam.Tipo_di_Richiesta__c);
                bodyEmail = bodyEmail.replace('{!Case.Tipo_di_Richiesta__c}',' '+caseLookupParam.Tipo_di_Richiesta__c);
            }           
            else
            {
                subj = subj.replace('{!Case.Tipo_di_Richiesta__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Tipo_di_Richiesta__c}', '');
            }
            
            if (caseLookupParam.Livello1__c != null)
            {
                subj = subj.replace('{!Case.Livello1__c}',' '+caseLookupParam.Livello1__c);
                bodyEmail = bodyEmail.replace('{!Case.Livello1__c}',' '+caseLookupParam.Livello1__c);
            }           
            else
            {
                subj = subj.replace('{!Case.Livello1__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Livello1__c}', '');
            }
            
            if (caseLookupParam.Livello2__c != null)
            {
                subj = subj.replace('{!Case.Livello2__c}',' '+caseLookupParam.Livello2__c);
                bodyEmail = bodyEmail.replace('{!Case.Livello2__c}',' '+caseLookupParam.Livello2__c);
            }           
            else
            {
                subj = subj.replace('{!Case.Livello2__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Livello2__c}', '');
            }
            /*END [GP 12122016] S00000030 email sme per ped*/
                
            /* Create Date Fare la conversione*/    
            //if (caseForEmail.CreatedDate != null)
            if (caseLookupParam.CreatedDate != null)
            {
                System.debug('*** created date '+caseLookupParam.CreatedDate.day()+'/'+caseLookupParam.CreatedDate.month()+'/'
            +caseLookupParam.CreatedDate.year());
                subj = subj.replace('{!Case.CreatedDate}',' '+caseLookupParam.CreatedDate.day()+'/'+caseLookupParam.CreatedDate.month()+'/'
                                    +caseLookupParam.CreatedDate.year());
                //bodyEmail = bodyEmail.replace(' {!Case.CreatedDate}',' '+ string.valueOf(caseLookupParam.CreatedDate));
                bodyEmail = bodyEmail.replace('{!Case.CreatedDate}',caseLookupParam.CreatedDate.day()+'/'+caseLookupParam.CreatedDate.month()+'/'
                                    +caseLookupParam.CreatedDate.year());
            System.debug('*** create date bodyEmail: '+bodyEmail);
                                    
            }           
            else
            {
                System.debug('*** Else create date ');
                subj = subj.replace('{!Case.CreatedDate}', String.valueOf(Date.today()));
                bodyEmail = bodyEmail.replace('{!Case.CreatedDate}',  String.valueOf(Date.today()));
            }
            
            /*{!caseForEmail.Account.Email__c}*/
            
            system.debug(' *** caseForEmail.Account.Email__c'+caseLookupParam.Account.Email__c);
            if (caseLookupParam.Account.Email__c != null)
            {
                subj = subj.replace('{!Account.Email__c}',' '+caseLookupParam.Account.Email__c);
                bodyEmail = bodyEmail.replace('{!Account.Email__c}',' '+caseLookupParam.Account.Email__c);
            }           
            else
            {
                subj = subj.replace('{!Account.Email__c}', '');
                bodyEmail = bodyEmail.replace('{!Account.Email__c}', '');
            }
            
            /*{!caseForEmail.Account.Phone}*/
            if (caseLookupParam.Account.Phone != null)
            {
                subj = subj.replace('{!Account.Phone}',' '+caseLookupParam.Account.Phone);
                bodyEmail = bodyEmail.replace('{!Account.Phone}',' '+caseLookupParam.Account.Phone);
            }           
            else
            {
                subj = subj.replace('{!Account.Phone}', '');
                bodyEmail = bodyEmail.replace('{!Account.Phone}', '');
            }
            
            if (caseForEmail.Priority != null)
            {
                subj = subj.replace('{!Case.Priority}', caseForEmail.Priority);
                bodyEmail = bodyEmail.replace('{!Case.Priority}', caseForEmail.Priority);
            }
            
            else
            {
                subj = subj.replace('{!Case.Priority}', '');
                bodyEmail = bodyEmail.replace('{!Case.Priority}', '');
            }
            
            if (caseForEmail.Status != null)
            {
                subj = subj.replace('{!Case.Status}', caseForEmail.Status);
                bodyEmail = bodyEmail.replace('{!Case.Status}', caseForEmail.Status);
            }
            
            else
            {
                subj = subj.replace('{!Case.Status}', '');
                bodyEmail = bodyEmail.replace('{!Case.Status}', '');
            }
            
            if (caseForEmail.TipologiaCase__c != null)
            {
                subj = subj.replace('{!Case.TipologiaCase__c}', caseForEmail.TipologiaCase__c);
                bodyEmail = bodyEmail.replace('{!Case.TipologiaCase__c}', caseForEmail.TipologiaCase__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.TipologiaCase__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.TipologiaCase__c}', '');
            }
            
            // Nino 06-12-2016
            // DTR nell'oggetto dell'email tracciata
            /*
            if (caseLookupParam.Owner.name != null)
            {
                subj = subj.replace('{!Case.OwnerFullName}', caseLookupParam.Owner.name);
                bodyEmail = bodyEmail.replace('{!Case.OwnerFullName}', caseLookupParam.Owner.name);
            }
            
            else
            {
                subj = subj.replace('{!Case.OwnerFullName}', '');
                bodyEmail = bodyEmail.replace('{!Case.OwnerFullName}', '');
            }
            */
            // fine Nino 06-12-2016 
            
            if (caseForEmail.Description != null)
            {
                subj = subj.replace('{!Case.Description}', caseForEmail.Description);
                bodyEmail = bodyEmail.replace('{!Case.Description}', caseForEmail.Description);
            }
            
            else
            {
                subj = subj.replace('{!Case.Description}', '');
                bodyEmail = bodyEmail.replace('{!Case.Description}', '');
            }
            
            String urlCase = System.Url.getSalesforceBaseURL().toExternalForm();
            if (caseForEmail.id != null)
            {
                subj = subj.replace('{!Case.Link}', ' '+urlCase+'/'+caseForEmail.id);
                bodyEmail = bodyEmail.replace('{!Case.Link}', ' '+urlCase+'/'+caseForEmail.id);
            }
            
            else
            {
                subj = subj.replace('{!Case.Link}', '');
                bodyEmail = bodyEmail.replace('{!Case.Link}', '');
            }
            
             /* {!Case.Id} */
            if (caseForEmail.Id != null)
            {
                subj = subj.replace('{!Case.Id}', caseForEmail.Id);
                bodyEmail = bodyEmail.replace('{!Case.Id}', caseForEmail.Id);
            }
            
            else
            {
                subj = subj.replace('{!Case.Id}', '');
                bodyEmail = bodyEmail.replace('{!Case.Id}', '');
            }   
            
             /* {!Case.Nome__c} */
            if (caseForEmail.Nome__c != null)
            {
                subj = subj.replace('{!Case.Nome__c}', caseForEmail.Nome__c);
                bodyEmail = bodyEmail.replace('{!Case.Nome__c}', caseForEmail.Nome__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.Nome__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Nome__c}', '');
            }   
            
            /* {!Case.OriginForEmail__c} */
            if (caseLookupParam.OriginForEmail__c != null)
            {
                subj = subj.replace('{!Case.OriginForEmail__c}',' '+string.valueOf(caseLookupParam.OriginForEmail__c));
                bodyEmail = bodyEmail.replace('{!Case.OriginForEmail__c}', ' '+string.valueOf(caseLookupParam.OriginForEmail__c));
            }
            
            else
            {
                subj = subj.replace('{!Case.OriginForEmail__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.OriginForEmail__c}', '');
            }   
            
            
            /* {!Case.ClosedDate} */
            if (caseForEmail.ClosedDate != null)
            {
                subj = subj.replace('{!Case.ClosedDate}', ' '+caseForEmail.ClosedDate.day()+'/'+caseLookupParam.ClosedDate.month()+'/'
                                    +caseLookupParam.ClosedDate.year());
                bodyEmail = bodyEmail.replace('{!Case.ClosedDate}', ' '+caseForEmail.ClosedDate.day()+'/'+caseLookupParam.ClosedDate.month()+'/'
                                    +caseLookupParam.ClosedDate.year());
            }
            
            else
            {
                subj = subj.replace('{!Case.ClosedDate}', '');
                bodyEmail = bodyEmail.replace('{!Case.ClosedDate}', '');
            }
            
            /*[GP 25102016 Modificato per mettere la data di chiusura effettiva sul tracciamento email]*/
            
            System.debug('*** caseForEmail.Date_Time_Closed__c: '+caseForEmail.Date_Time_Closed__c+' **** caseLookupParam.Date_Time_Closed__c: '+caseLookupParam.Date_Time_Closed__c);
            /* {!Case.Date_Time_Closed__c} */
            if (caseLookupParam.Date_Time_Closed__c != null)
            {
                subj = subj.replace('{!Case.Date_Time_Closed__c}', ' '+caseLookupParam.Date_Time_Closed__c.day()+'/'+caseLookupParam.Date_Time_Closed__c.month()+'/'
                                    +caseLookupParam.Date_Time_Closed__c.year());
                bodyEmail = bodyEmail.replace('{!Case.Date_Time_Closed__c}', ' '+caseLookupParam.Date_Time_Closed__c.day()+'/'+caseLookupParam.Date_Time_Closed__c.month()+'/'
                                    +caseLookupParam.Date_Time_Closed__c.year());
            }
            
            else
            {
                //subj = subj.replace('{!Case.Date_Time_Closed__c}', '');
                //bodyEmail = bodyEmail.replace('{!Case.Date_Time_Closed__c}', '');
                subj = subj.replace('{!Case.Date_Time_Closed__c}',' '+Date.today().day()+'/'+Date.today().month()+'/'
                                    +Date.today().year());
                bodyEmail = bodyEmail.replace('{!Case.Date_Time_Closed__c}',' '+Date.today().day()+'/'+Date.today().month()+'/'
                                    +Date.today().year());
            }      
            
            /* {!Case.Eventuali_Note_Richiedente__c} e {!Case.Note_aggiuntive_dell_operatore__c} */
            if (caseForEmail.Eventuali_Note_Richiedente__c != null)
            {
                subj = subj.replace('{!Case.Eventuali_Note_Richiedente__c}', ' '+caseForEmail.Eventuali_Note_Richiedente__c);
                bodyEmail = bodyEmail.replace('{!Case.Eventuali_Note_Richiedente__c}', ' '+caseForEmail.Eventuali_Note_Richiedente__c);
                bodyEmail = bodyEmail.replace('{!Case.Note_aggiuntive_dell_operatore__c}', 'Note aggiuntive dell\' operatore: ');
            }
            
            else
            {
                subj = subj.replace('{!Case.Eventuali_Note_Richiedente__c} ', '');
                bodyEmail = bodyEmail.replace('{!Case.Eventuali_Note_Richiedente__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Note_aggiuntive_dell_operatore__c}', '');
            }   
            
            /* {!Case.Contact.id}  */
            if (caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.id != null)
            {
                subj = subj.replace('{!Contact.id}',caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.id);
                bodyEmail = bodyEmail.replace('{!Contact.id}', caseLookupParam.AssegnatoAllEspertoDiDTRZona__r.id);
            }
            
            else
            {
                subj = subj.replace('{!Contact.Id}', '');
                bodyEmail = bodyEmail.replace('{!Contact.Id}', '');
            }   
            
            /*  {!Case.Account}  */
            if (caseLookupParam.Account.name != null)
            {
                subj = subj.replace('{!Case.Account}', ' '+caseLookupParam.Account.name);
                bodyEmail = bodyEmail.replace('{!Case.Account}', ' '+caseLookupParam.Account.name);
            }
            
            else
            {
                subj = subj.replace('{!Case.Account}', '');
                bodyEmail = bodyEmail.replace('{!Case.Account}', '');
            }
            
            /*   {!Case.ThreadID__c}  */
            if (caseLookupParam.ThreadID__c != null)
            {
                System.debug('*** Case.ThreadID__c: '+Case.ThreadID__c);
                subj = subj.replace('{!Case.ThreadID__c}', ' '+caseLookupParam.ThreadID__c);
                bodyEmail = bodyEmail.replace('{!Case.ThreadID__c}', ' '+caseLookupParam.ThreadID__c);
            }
            
            else
            {
            System.debug('*** ELSE Case.ThreadID__c: '+Case.ThreadID__c);
                subj = subj.replace('{!Case.ThreadID__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.ThreadID__c}', '');
            }
            
            /*    {!Case.Livello3__c}   */
            if (caseForEmail.Livello3__c != null)
            {
                subj = subj.replace('{!Case.Livello3__c}', ' '+caseForEmail.Livello3__c);
                bodyEmail = bodyEmail.replace('{!Case.Livello3__c}', ' '+caseForEmail.Livello3__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.Livello3__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Livello3__c}', '');
            }
            
             /*     {!Case.UnitaDiCompetenza__c}     */
           
            if (caseLookupParam.UnitaDiCompetenza__c != null)
            {
                System.debug('*** Case.UnitaDiCompetenza__c: '+caseLookupParam.UnitaDiCompetenza__c);
                subj = subj.replace('{!Case.UnitaDiCompetenza__c}', ' '+caseLookupParam.UnitaDiCompetenza__c);
                bodyEmail = bodyEmail.replace('{!Case.UnitaDiCompetenza__c}', ' '+caseLookupParam.UnitaDiCompetenza__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.UnitaDiCompetenza__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.UnitaDiCompetenza__c}', '');
            }
            
            /*     {!Case.Famiglia__c}   */
            if (caseLookupParam.Famiglia__c != null)
            {
                subj = subj.replace('{!Case.Famiglia__c}', ' '+caseLookupParam.Famiglia__c);
                bodyEmail = bodyEmail.replace('{!Case.Famiglia__c}', ' '+caseLookupParam.Famiglia__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.Famiglia__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Famiglia__c}', '');
            }
            
            /*    {!Case.SottofamigliaSituazionePrestazionesot__c}    */
            if (caseLookupParam.SottofamigliaSituazionePrestazionesot__c != null)
            {
                subj = subj.replace('{!Case.SottofamigliaSituazionePrestazionesot__c}', ' '+caseLookupParam.SottofamigliaSituazionePrestazionesot__c);
                bodyEmail = bodyEmail.replace('{!Case.SottofamigliaSituazionePrestazionesot__c}', ' '+caseLookupParam.SottofamigliaSituazionePrestazionesot__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.SottofamigliaSituazionePrestazionesot__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.SottofamigliaSituazionePrestazionesot__c}', '');
            }
            
            /*    {!Case.SituazionePrestazioneSottesa__c}    */
            if (caseLookupParam.SituazionePrestazioneSottesa__c != null)
            {
                subj = subj.replace('{!Case.SituazionePrestazioneSottesa__c}', ' '+caseLookupParam.SituazionePrestazioneSottesa__c);
                bodyEmail = bodyEmail.replace('{!Case.SituazionePrestazioneSottesa__c}', ' '+caseLookupParam.SituazionePrestazioneSottesa__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.SituazionePrestazioneSottesa__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.SituazionePrestazioneSottesa__c}', '');
            }
            
            /*     {!Case.POD__c}     */
            if (caseLookupParam.POD__c != null)
            {
                subj = subj.replace('{!Case.POD__c}', ' '+caseLookupParam.POD__c);
                bodyEmail = bodyEmail.replace('{!Case.POD__c}', ' '+caseLookupParam.POD__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.POD__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.POD__c}', '');
            }
            
            /*     {!Case.DirezioneDTR__c}     */
            if (caseLookupParam.DirezioneDTR__c != null)
            {
                subj = subj.replace('{!Case.DirezioneDTR__c}', ' '+caseLookupParam.DirezioneDTR__c);
                bodyEmail = bodyEmail.replace('{!Case.DirezioneDTR__c}', ' '+caseLookupParam.DirezioneDTR__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.DirezioneDTR__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.DirezioneDTR__c}', '');
            }
            
           
            
            /*      {!Case.IdPratica__c}      */
            if (caseLookupParam.IdPratica__c != null)
            {
                subj = subj.replace('{!Case.IdPratica__c}', ' '+caseLookupParam.IdPratica__c);
                bodyEmail = bodyEmail.replace('{!Case.IdPratica__c}', ' '+caseLookupParam.IdPratica__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.IdPratica__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.IdPratica__c}', '');
            }
            
            /*      {!Case.Danni__c}      */
            if (caseLookupParam.Danni__c != null)
            {
                subj = subj.replace('{!Case.Danni__c}', ' '+caseLookupParam.Danni__c);
                bodyEmail = bodyEmail.replace('{!Case.Danni__c}', ' '+caseLookupParam.Danni__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.Danni__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.Danni__c}', '');
            }
            
            /*      {!Case.ReiteroDaAltroCanale__c}      */
            if (caseLookupParam.ReiteroDaAltroCanale__c != null)
            {
                subj = subj.replace('{!Case.ReiteroDaAltroCanale__c}', ' '+caseLookupParam.ReiteroDaAltroCanale__c);
                bodyEmail = bodyEmail.replace('{!Case.ReiteroDaAltroCanale__c}', ' '+caseLookupParam.ReiteroDaAltroCanale__c);
            }
            
            else
            {
                subj = subj.replace('{!Case.ReiteroDaAltroCanale__c}', '');
                bodyEmail = bodyEmail.replace('{!Case.ReiteroDaAltroCanale__c}', '');
            }
                
        }catch(Exception e ){
            system.debug(logginglevel.error, 'Exception in SendEmail_Helper.setCaseParameterEmail :'+e.getMessage());
        }
        
        
    }
    
}