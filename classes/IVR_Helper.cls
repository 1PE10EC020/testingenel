global class IVR_Helper{
	   /* 
        * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
        * Createddate : 10-07-2015
        * Description : Private method , used to retrive the PercorsoTelefonico__c id from the CodIVR__c
        * Input :	Name 					Type					Description
        *			pathIVR					String					path IVR, used as key to found a PercorsoTelefonico__c
        * Output :  
        *			N/A						PercorsoTelefonico__c	PercorsoTelefonico__c found ( or null if no PercorsoTelefonico__c is found)
        * 
        */
        public static PercorsoTelefonico__c findIVRPath (string pathIVR){
        	try{
        		if (String.isNotBlank(pathIVR)){
        			PercorsoTelefonico__c pathIVRFound = [Select id,name,MenuSecondoLivello__c,ramo__C,CodIVR__c,Contesto__c from PercorsoTelefonico__c where CodIVR__c = :pathIVR limit 1];
        			system.debug(logginglevel.debug,'IVR_Helper - findIVRPath - IVR PAth  found with id : '+pathIVRFound.id);
        			return pathIVRFound;
        		}
        	}
        	catch(Exception e){
        		   system.debug(logginglevel.error,'Exception in Case_Helper - findIVRPath :' + e.getMessage() );
        	}
        	return null;
        
        }
        
      
        
}