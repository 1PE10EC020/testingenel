/*
 * @author: Accenture
 * @date: 2011-09-21
 */
public with sharing class profileClass {

    public static integer index {get; set;}
    public List<profDelWrapper> profileList;
    public Integer numRows {set;get;}
    
    public class profDelWrapper {
        public integer count;
        public Profile__c profileWrapper;        
        
        public profDelWrapper() {
            this.profileWrapper = new Profile__c(Profile_Name__c = 'Enter name');
            this.count = index++;
        }
        public Profile__c getProfileWrapper(){
            return profileWrapper;
        }
        public integer getCount(){
            return count;
        }
    }
    
    public profileClass(ApexPages.StandardController controller) {
        if(index == null) index = 1;
        
        profileList = new List<profDelWrapper>();
        numRows=1;
    }
    
    public List<profDelWrapper> getProfileList() {
        return profileList;
    }

    public PageReference save(){
    
        List<Profile__c> newList = new List<Profile__c>();
        for(Integer i=0;i<profileList.size();i++){
            newList.add(profileList[i].getProfileWrapper());
        }
        try{
            if(newList!=null){
                upsert(newList);
            }
         } 
         catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
    }
    
    public void add(){
        try {
            if(numRows>0) {
                for(integer i=0;i<numRows;i++) {
                    profileList.add(new profDelWrapper());
                }
            }
        }
        catch(Exception e) {
            ApexPages.addMessages(e);
        }
    } 
    
     public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                profDelWrapper profileWrap = new profDelWrapper();
                profileWrap.profileWrapper = [SELECT API_Enabled__c,Configuration_Environment__c,Description__c,Desktop_Integration_Client_OffLine__c,Development_Environment__c,  IP_Restrict_Requests__c,Password_Never_Expires__c,Production_Environment__c,Profile_Name__c,Refrence_No__c,Send_Outbound_Messages_Checked__c,Show_Custom_Sidebar_On_All_Pages__c,UAT_Environment__c FROM Profile__c WHERE ID = :CurrentRecordID];
                profileList.add(profileWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    public void clear() {
        profileList.clear();
        numRows = 1;
    }
    
    public void del(){
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delId'));
            
            for(Integer i=0; i<profileList.size(); i++)
                if(profileList[i].count == delIndex) {
                    profileList.remove(i);
                    break;
                }
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
    }
    
    
    
   

}