public with sharing class CaseMgmtListController {
    
    public String assignSelected {get;set;}
    public PageReference pageRef {get;set;}
    private ApexPages.StandardController controller {get; set;}
     private Case c1;
    
    public string redirecturl{get;set;}
    public string redirectlabel{get;set;}
    
    public boolean showheader{get{
        try{
            boolean esito =  Boolean.valueOf(ApexPages.currentPage().getParameters().get('showHeader'));
            return esito;
        }
        catch(Exception e){
            return false;
        }
        
    }private set;}
    
    public CaseMgmtListController(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        this.c1 = (Case)controller.getRecord();

    }

public List<SelectOption> getcaseAssignments() {
	List<SelectOption> assignOptions = new List<SelectOption>();
    	Map<Id, Group> qIdToQName = new Map<Id, Group>([SELECT g.Id, g.Name FROM Group g WHERE g.Type = 'Queue' AND g.Name != 'Back Office' AND g.Name != 'CallBack']);
    	List<GroupMember> qMembers = new List<GroupMember>([SELECT m.GroupId, m.UserOrGroupId FROM GroupMember m]);
    	
    	//Federica Aloia 03-02-2017 aggiunto il codice per escludere le code di Atos Fibra dalla visualforce page
    	List<String> queueNameList=new List<String>();
    
    	for(Group currentGroup:qIdToQName.Values()){
        	queueNameList.add(currentGroup.name);
        }
    
    	list<UserRole> alluserRole = [select Id,ParentRoleId, name from UserRole];
    	Id idContactManagement=[select id from UserRole where name='Contact Management' limit 1].Id;
    	List<String> roleOK=new List<String>();
    	List<UserRole> RoleList =[select id,ParentRoleId,name  from UserRole where name IN : queueNameList];
    	
    	for(UserRole currentRole : RoleList){
        	Set<Id> superRole = new Set<Id>();
        	superRole=Case_Helper.getAllSuperRole(currentRole.Id, alluserRole);
        	if(superRole.contains(idContactManagement)){
            	roleOK.add(currentRole.name);
        	}        
    	}
    	system.debug('CaseMgmtListController.getcaseAssignments : role ok: '+roleOK); 
     	Map<Id, Group> filteredQueue  = new Map<Id, Group>([SELECT g.Id, g.Name FROM Group g WHERE g.Type = 'Queue' AND g.Name IN : roleOK]);
    	//Federica Aloia 03-02-2017 end
    	List<Id> uIds = new List<Id>();
    	for (GroupMember gm : qMembers) {
    		uIds.add(gm.UserOrGroupId);	
    	}
    	Map<Id, User> uIdToUName = new Map<Id, User>([SELECT u.Id, u.Name FROM User u WHERE u.Id IN :uIds]);
    	if (filteredQueue.size() > 0) {
           for (Id q : filteredQueue.keySet()) {
              assignOptions.add(new SelectOption(q, '<b>' + filteredQueue.get(q).Name + ' </b>'));		//Create a tab option for the queue so the Case can be assign to a queue
              for (GroupMember gm : qMembers) {               //Now search for users that are members of this queue
        	if (gm.GroupId == q) {
        	   if (uIdToUName.containsKey(gm.UserOrGroupId)) {
        		assignOptions.add(new SelectOption(gm.UserOrGroupId, '<i>-  ' + uIdToUName.get(gm.UserOrGroupId).Name+ '</i>'));	//Create a tab option for the User so the Case can be assigned	
        		
        	   }
        	}
            }
         }
       }
       if (assignOptions.size() > 0) {
          for (SelectOption s : assignOptions) {
            s.setEscapeItem(false);	     //This is needed because of the embedded html tags in the assignOptions
          }
       }
        
       return assignOptions;       
    }
/**/

/*
*  getter method for the assign radio buttons
*/
public String getassignSelected() {
   return assignSelected;
}
/**/
 	
/*
*  setter method for the assign radio buttons
*/
public void setassignSelected(String selection) {
   this.assignSelected = selection;
}
/**/
	
/*
*	Assigns a case to the queue or user selected from the radio button list
*	For reasons specific to my implementation, I re-query the case before changing the owner.
*/
public void assignOk() {
    
	System.debug('*** 1 CaseMgmtListController.assignOk()');
   String caseId = ApexPages.currentPage().getParameters().get('id');	//The case ID is passed in the URL
   if (caseId != null) {
      try {
      	// GP 01022017 fix status esperto di zona: faccio la query sul owner.userrole.name in quanto l'owner di provenienza quando si arrivaa sulla VF sarà sempre uno User
	    Case c = [SELECT owner.userrole.name,OwnerId, Status, tech_change_owner__c,casenumber,AssegnatoIILivello__c, AssegnatoAllEspertoDiDTRZona__c FROM Case WHERE id = :caseId.trim()];
	    System.debug('*** CaseMgmtListController.assignOk() case: '+c.owner.userrole.name);
	    //String userRoleId = UserInfo.getUserRoleId();
	    //String userRoleName = [SELECT Name, id FROM UserRole where id =: userRoleId limit 1].name;
	    String queueChoice = [SELECT id,Name, type FROM Group where id =:assignSelected and type = 'Queue' limit 1].name;
	    System.debug('*** l operatore ha scelto: '+queueChoice);
	    System.debug('*** CaseMgmtListController.AssignOk() lo status: '+c.status);
	    /*[GP 20022017] S00000046_SFDC_Gestione case su Assegnato SME: sbianca sme e assegna al II livello se assegnato ad una coda o user diverso da quello di partenza*/
        if(queueChoice.equalsIgnorecase(c.owner.userrole.name) && c.AssegnatoAllEspertoDiDTRZona__c != null)
        {
        	System.debug('*** if per settare In carico esperto di Zona: '+c.status);
        	c.Status= 'In carico Esperto di Zona';
        }
        else
        {
        	System.debug('*** nel Else CaseMgmtListController.AssignOk(), status: '+c.status);
        	c.AssegnatoAllEspertoDiDTRZona__c = null;
        	c.Status= 'In carico al II Livello';
        }
        /*END [GP 20022017] S00000046_SFDC_Gestione case su Assegnato SME*/
        c.tech_change_owner__c = false;
		c.AssegnatoIILivello__c = DateTime.newInstance(2000, 1, 1, 1, 10, 1);
        //c.Status= 'In carico al II Livello';
	    c.OwnerId = assignSelected;
	    c.isEscalated = false;
        redirectlabel = c.casenumber;
        redirectURL = '/'+c.id;  
	    update c;
      } catch (System.DmlException e) {
         for (Integer i = 0; i < e.getNumDml(); i++) {
         	 
         	 System.debug('*** Catch exception Pulsante Assegna VF II Livello: '+e.getDmlMessage(i));
        }	
     }
   }
   
  PageReference reference=new PageReference('/'+caseId.trim());
  reference.setRedirect(true);
    System.debug('Reference = ' + reference);

  //return reference;     //Also for reasons specific to my implementation, I re-render the screen
}
    
    
    public void CancelOk() {
    
	String urlVal = Apexpages.currentPage().getUrl();
    String currentStatus = urlVal.substringAfterLast('status=');
    String currentStatus2 = currentStatus.replace('+',' ');
    String caseId = ApexPages.currentPage().getParameters().get('id');	//The case ID is passed in the URL
	//case c = [select id, casenumber from case where id = :caseId];
        
          if (caseId != null) {
      try {
	    Case c = [SELECT id,OwnerId, Status,casenumber  FROM Case WHERE id = :caseId.trim()];
     	c.Status= currentStatus2;
        redirectlabel = c.casenumber;
    	redirectURL = '/'+c.id;  
	    update c;
      } catch (System.DmlException e) {
         for (Integer i = 0; i < e.getNumDml(); i++) {
            System.debug(e.getDmlMessage(i));
        }	
     }
   }
        
  PageReference reference=new PageReference('/'+caseId.trim());
  reference.setRedirect(true);
    System.debug('Reference = ' + reference);

  //return reference;      //Also for reasons specific to my implementation, I re-render the screen
}
}