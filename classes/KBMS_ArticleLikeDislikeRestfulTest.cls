// This is the test class of  KBMS_ArticleLikeDislikeRestful class

@isTest(seeAllData = false)

public class KBMS_ArticleLikeDislikeRestfulTest {

    static testmethod void likeDislikePositive() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ArticleLikeDislikeRestful/*';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        List<FAQ__kav> articles = KBMS_TestDataFactory.createFAQTestRecords(1); 

        FAQ__kav faq =  articles[0];
        
        
        Test.startTest();
        faq = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :faq.Id];
        
        KbManagement.PublishingService.publishArticle(faq.KnowledgeArticleId, true);
        String articleId = faq.KnowledgeArticleId;

        KBMS_ArticleLikeDislikeRestful.WrapperClass resp = new KBMS_ArticleLikeDislikeRestful.WrapperClass();
        resp = KBMS_ArticleLikeDislikeRestful.postMethodLikeDislike(articleId, 'like');
        
        List<Vote> vtList = [SELECT Id,ParentId,Type FROM Vote where ParentId=:articleId ];
        system.assertEquals(vtList[0].Type,'Up');

        Test.stopTest();

    }
    
    static testmethod void likeDislikeNegative() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ArticleLikeDislikeRestful/*';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        List<FAQ__kav> articles = KBMS_TestDataFactory.createFAQTestRecords(1); 

        FAQ__kav faq =  articles[0];
        
        Test.startTest();
        faq = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :faq.Id];
        
        KbManagement.PublishingService.publishArticle(faq.KnowledgeArticleId, true);
        String articleId = faq.KnowledgeArticleId;

        KBMS_ArticleLikeDislikeRestful.WrapperClass resp = new KBMS_ArticleLikeDislikeRestful.WrapperClass();
        resp = KBMS_ArticleLikeDislikeRestful.postMethodLikeDislike(articleId, 'like');
        
        KBMS_ArticleLikeDislikeRestful.WrapperClass resp2 = new KBMS_ArticleLikeDislikeRestful.WrapperClass();
        resp2 = KBMS_ArticleLikeDislikeRestful.postMethodLikeDislike(articleId, 'like');
        List<Vote> vtList = [SELECT Id,ParentId,Type FROM Vote where ParentId=:articleId ];
        system.assertEquals(vtList.size(),1);
        
        KBMS_ArticleLikeDislikeRestful.WrapperClass resp3 = new KBMS_ArticleLikeDislikeRestful.WrapperClass();
        resp3 = KBMS_ArticleLikeDislikeRestful.postMethodLikeDislike(articleId, 'DisLike');
        vtList = [SELECT Id,ParentId,Type FROM Vote where ParentId=:articleId ];
        system.assertEquals(vtList.size(),1);
        system.assertNotEquals(vtList[0].Type,'Down');
        Test.stopTest();

    }

}