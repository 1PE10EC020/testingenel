/*********************************************************************************************
@Author : Bharath Sharma
@date : 21 Aug 2017
@description : Test class for PT_ContributiDetail_Controller.
**********************************************************************************************/
@isTest
private class PT_ContributiDetail_Controller_Test {
     PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
    PRIVATE STATIC FINAL STRING TESTS='test23';
      PRIVATE STATIC FINAL INTEGER LMT=1; 

   /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/
@testSetup static void createTestRecords() {
   Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
  User usr = new User(Alias = ALIAS, Email=EMAIL,
    EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
    LocaleSidKey=ENUS, ProfileId = pfl.Id,
    TimeZoneSidKey=TZK,
    UserName=UUNAME);
  database.insert(usr);
}
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 21 aug 2017
@description : Test class for PT_ContributiDetail_Controller possitive scenario
**********************************************************************************************/
static testMethod void pt_ContributiDetail_ControllerTest() {

     User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
        list<PT_Richiesta__c > lstInsertRequest = new list<PT_Richiesta__c >();
        PT_Richiesta__c request = PT_TestDataFactory.createRequestF05();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_PD2').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
        lstInsertRequest.add(request);
        database.insert(lstInsertRequest);

        list<PT_Costi__c > lstInsertcosti = new list<PT_Costi__c >();
        PT_Costi__c costi = new PT_Costi__c();
        costi.ProtocolloRic__c=lstInsertRequest[0].id;
        lstInsertcosti.add(costi);
        database.insert(lstInsertcosti);

        list<PT_Componente_Costo__c > lstInsertcosto = new list<PT_Componente_Costo__c >();
        PT_Componente_Costo__c costo = new PT_Componente_Costo__c();
        costo.CostiComponente__c=lstInsertcosti[0].id;
        costo.Descrizione__c='Quota Distanza';
        costo.Totale__c=123;
        lstInsertcosto.add(costo);

        PT_Componente_Costo__c costo1 = new PT_Componente_Costo__c();
        costo1.CostiComponente__c=lstInsertcosti[0].id;
        costo1.Descrizione__c='Quota Potenza';
        costo1.Totale__c=456;
        lstInsertcosto.add(costo1);

        PT_Componente_Costo__c costo2 = new PT_Componente_Costo__c();
        costo2.CostiComponente__c=lstInsertcosti[0].id;
        costo2.Totale__c=789;
        costo2.Descrizione__c='Oneri Amministrativi';
        lstInsertcosto.add(costo2);
        database.insert(lstInsertcosto);
    
        PT_ContributiDetail_Controller.Costiobjectquery(lstInsertRequest[0].id);
         PT_Costi__c costie = new PT_Costi__c ();
        
         costie.FlagAccettazione__c ='S';  
         costie.DataAccettazione__c=Date.today();
         costie.SendOutbound__c=True;
         database.insert (costie);

         PT_Costi__c costie1 = new PT_Costi__c ();
         costie1.FlagAccettazione__c ='N'; 
         costie1.DataAccettazione__c=Date.today();
         costie1.SendOutbound__c=True;
         database.insert (costie1);
          Test.startTest();
         PT_ContributiDetail_Controller.updateACCETTAZIONECONTRIBUTI(costie);
         PT_ContributiDetail_Controller.presetRifiutaperform(costie1);
         Test.stopTest();
       System.assertEquals(costi.ProtocolloRic__c,lstInsertRequest[0].id);
   }
  }
}