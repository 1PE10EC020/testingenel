/*
* @author Atos India Pvt Ltd.
* @date  Sept,2016
* @description This class is test class of KBMS_DataCategoryTree
*/
@isTest
public class KBMS_DataCategoryTreeTest {  
 public class CategoryInfo {      
  public final String name;
  public final String label;
        
  public CategoryInfo(String n, String l){
     this.name = n;
     this.label = l;
  }
  
  public String getName(){
     return this.name;
  }
  
  public String getLabel(){
     return this.label;
  }
 }
static testMethod void testMethod1() {         

//    **************************
     String lang= Label.KBMS_Language;
    List<FAQ__kav> lstFaq = new List<FAQ__kav>();
    
    for(integer i=0;i<5;i++) {
        
        FAQ__kav faqtst = new FAQ__kav();
        
        faqtst.Title = 'TestFAQ'+i;
        faqtst.Summary = 'KB Summary ';
        faqtst.URLName = 'TestFAQ'+i;                
        faqtst.QUESITO__c='testno';
        faqtst.RISPOSTA__c='testno';
        faqtst.Language=lang;
        lstFaq.add(faqtst);
    }
    insert lstFaq;       
    
  // Accessing custom Labels
    String dataCategory=Label.KBMS_DataCategory; 
    String GroupDataCategory=Label.KBMS_GroupDataCategory;
    String articleType=Label.KBMS_ArticleType;
    FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
    
    datacatTest.ParentId= lstFaq[0].Id;     
    datacatTest.DataCategoryName=dataCategory;
    datacatTest.DataCategoryGroupName=GroupDataCategory;
    
    insert datacatTest;
    system.debug('*************faq id'+datacatTest);       
  
    
    system.debug('*************welcome***************'+lstfaq[0].id);  
    List<FAQ__DataCategorySelection> listdatacat = new List<FAQ__DataCategorySelection>();
    String dataCategory1=Label.KBMS_DataCategory; 
    String GroupDataCategory1=Label.KBMS_GroupDataCategory;        
  
  for(integer i=0;i<=0;i++)
   {            
      FAQ__DataCategorySelection datacat1 = new FAQ__DataCategorySelection();             
      datacat1.ParentId= lstFaq[3].Id; 
      datacat1.DataCategoryName=dataCategory;
      datacat1.DataCategoryGroupName=GroupDataCategory;      
      listdatacat.add(datacat1); 
   }      
insert listdatacat;

   ApexPages.StandardController sc = new ApexPages.StandardController(datacatTest);
   KBMS_DataCategoryTree datacategorytree = new KBMS_DataCategoryTree();
   KBMS_DataCategoryTree dtree = new KBMS_DataCategoryTree(sc);
   KBMS_DataCategoryTree.getCategoriesJson('KnowledgeArticleVersion');     
    
    
   List<DescribeDataCategoryGroupStructureResult>describeResult = KBMS_DataCategoryTree.getCategories('KnowledgeArticleVersion');
  //Creating category info
  CategoryInfo world = new CategoryInfo('World', 'World');
  CategoryInfo asia = new CategoryInfo('Asia', 'Asia');
  CategoryInfo northAmerica = new CategoryInfo('NorthAmerica',
                                              'North America');
  CategoryInfo southAmerica = new CategoryInfo('SouthAmerica',
                                              'South America');
  CategoryInfo europe = new CategoryInfo('Europe', 'Europe');
  
  List<CategoryInfo> info = new CategoryInfo[] {
    asia, northAmerica, southAmerica, europe
 };
  
  for (Schema.DescribeDataCategoryGroupStructureResult result : describeResult) {
     String name = result.getName();
     String label = result.getLabel();
     String description = result.getDescription();
     String objectNames = result.getSobject();
     
     DataCategory [] topLevelCategories = result.getTopCategories();
     System.assert(topLevelCategories.size() == 1,
     'Incorrect number of top level categories returned: ' + topLevelCategories.size());
    
     KBMS_DataCategoryTree.getAllCategories(topLevelCategories);  
          DataCategory [] children = topLevelCategories[0].getChildCategories();
     
     
  }
    
      
  
}
    
  
}