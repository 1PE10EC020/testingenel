public class ChangeHistoryController {

    /* Contacts created just to use date fields as input of startdate and enddate */
    private Contact currentDate;
    private Contact pastDate;

    /**
     * List of 'HistoryDetails' objects which contains all the history in an
     * object format.
     *
     * @see 'HistoryDetails' for more details.
     */
    List<HistoryDetails> HistoryDetailss;
    
    /**
     * List of history in String format.
     * Used to generate 'HistoryDetails' objects.
     */
    List<String> historyList;
    
    /**
     * Constructor
     * -----------
     * Initializes variables
     * Sets currentDate to today and pastDate to yesterday.
     * Calls history() to calculate all values at the start.
     */
    public ChangeHistoryController() {
        currentDate = new Contact();
        currentDate.birthDate = Date.today();
        pastDate = new Contact();
        pastDate.birthDate = Date.today() - 1;
        
        history();
    }
    
    /**
     * Getter for 'pastDate' contact
     * -----------------------------
     * @return the 'pastDate' Contact
     */
    public Contact getPastDate() {
        return pastDate;
    }
    
    /**
     * Getter for 'currentDate' contact
     * --------------------------------
     * @return the 'currentDate' Contact
     */
    public Contact getCurrentDate() {
        return currentDate;
    }
    
    /**
     * Getter for 'history'
     * --------------------
     * @return list of history for all objects in String format
     */
    public List<String> getHistory() {
        return historyList;
    }
    
    /**
     * Getter for 'HistoryDetailss'
     * ---------------------------
     * @return list of 'HistoryDetails' objects
     */
    public List<HistoryDetails> getHistoryDetailss() {
        return HistoryDetailss;
    }
    
    /**
     * Method to create HistoryDetails objects from list of strings of history.
     *
     * It basically tokenizes the string representation of history containing
     * all the information into an object of 'HistoryDetails'.
     */
    public void createHistoryDetailsList() {
        
        for(String strHist: getHistory()) {
        
            Integer si = 0;
            Integer ei = 0;
            Integer oi = 0;
            
            while(true) {

                if(strHist.indexOf('OBJECT:', si) == -1) break;
                HistoryDetails hd = new HistoryDetails();

                si = strHist.indexOf('OBJECT:', si) + 'OBJECT:'.length();
                ei = strHist.indexOf(':', si);
                hd.setObjectName(strHist.substring(si, ei));
                
                si = strHist.indexOf('ParentId=', si) + 'ParentId='.length();
                ei = strHist.indexOf(',', si);
                hd.setParentId(strHist.substring(si, ei));
                
                si = strHist.indexOf('Field=', si) + 'Field='.length();
                ei = strHist.indexOf(',', si);
                hd.setField(strHist.substring(si, ei));
                
                si = strHist.indexOf('CreatedById=', si) + 'CreatedById='.length();
                ei = strHist.indexOf(',', si);
                hd.setCreatedById(strHist.substring(si, ei));
                hd.setUserName(retrieveUserName(strHist.substring(si, ei)));
                
                si = strHist.indexOf('CreatedDate=', si) + 'CreatedDate='.length();
                ei = strHist.indexOf(',', si);
                hd.setCreatedDate(strHist.substring(si, ei));

                si = strHist.indexOf('IsDeleted=', si) + 'IsDeleted='.length();
                ei = strHist.indexOf(',', si);
                hd.setIsDeleted(strHist.substring(si, ei));

                si = strHist.indexOf('NewValue=', si) + 'NewValue='.length();
                ei = strHist.indexOf(',', si);
                hd.setNewValue(strHist.substring(si, ei));

                si = strHist.indexOf('Id=', si) + 'Id='.length();
                ei = strHist.indexOf(',', si);
                hd.setId(strHist.substring(si, ei));

                si = strHist.indexOf('OldValue=', si) + 'OldValue='.length();
                ei = strHist.indexOf('}', si);
                hd.setOldValue(strHist.substring(si, ei));
                
                HistoryDetailss.add(hd);
                System.debug('History Details'+HistoryDetailss);
            }
        }
    }
    
    /**
     * @param 'userId' salesforce id of the user
     *
     * @return user name of modifier of record using his/her user Id
     */
    public String retrieveUserName(String userId) {
        User user = [SELECT name FROM User WHERE Id = :userId];
        System.debug(String.valueOf(user.name));
        return String.valueOf(user.name);
    }
    
    /**
     * calculates history based on dates provided
     */
    public void history() {
        List<SObjectType> sObjList;
        Map<String, List<SObjectField>> objectFieldMap;
        HistoryDetailss = new List<HistoryDetails>();
        
        sObjList = getHistoryObjectList();
        objectFieldMap = createObjectFieldMap(sObjList);
        
        Date cd = currentDate.birthDate; // current date
        Date pd = pastDate.birthDate; // past date
        
        List<String> strList = new List<String>();
        for(SObjectType sobjType: sObjList) {
            String str = '';
        System.debug('Query Return'+Database.query(getQuery(sobjType, objectFieldMap)));
            try {
                for(SObject sobj: Database.query(getQuery(sobjType, objectFieldMap)))
                    str += 'OBJECT:' + String.valueOf(sobj);
            } catch (Exception ex) {
                System.debug(ex);
            }
            
            strList.add(str);
        }
        System.debug('String List'+strList);
        historyList = strList;
        createHistoryDetailsList();
    }
    
    /**
     * @param 'sobj' the SObjectType whose fields are to be retrieved
     * @param 'objectFieldMap' map from where the fields would be retrieved
     *
     * @return query to get history records from salesforce.com
     */
     private String getQuery(SObjectType sobj, Map<String, List<SObjectField>> objectFieldMap) {
     
         String query = 'SELECT ';
         for(SObjectField field: objectFieldMap.get(String.valueOf(sobj))) {
             query += String.valueOf(field) + ', ';
         }
         
         query = query.subString(0, query.length() - 2); // remove last ', '
         query += ' FROM ' + String.valueOf(sobj);
         query += ' WHERE CreatedDate <= :cd AND CreatedDate >= :pd';
         query += ' ORDER BY CreatedDate DESC';
         System.debug('Query Method'+query);
         return query;
     }
    
    /**
     * @param 'sobj' the SObjectType whose fields are to be retrieved
     * @param 'objectFieldMap' map from where the fields would be retrieved
     *
     * @return the list of SObjectField for a specific SObject
     */
    public List<SObjectField> getFields(sObject sobj, Map<String, List<SObjectField>> objectFieldMap) {
        return objectFieldMap.get(String.valueOf(sobj));
    }
    
    /**
     * creates a map of SObjectType as key and list of its corresponding SObjectField's as value
     *
     * @param 'sObjList' a list of 'SObjectsType' objects
     *
     * @return a map of 'SObjectType' and 'list of SObjectField' for related SObject
     */
    private Map<String, List<SObjectField>> createObjectFieldMap(List<SObjectType> sObjList) {
    
        Map<String, List<SObjectField>> objectFieldMap = new Map<String, List<SObjectField>>();
    
        for(SObjectType sot: sObjList) {
            List<SObjectField> sFldList = new List<SObjectField>();
            sFldList = sot.getDescribe().fields.getMap().values();
            objectFieldMap.put(String.valueOf(sot), sFldList);
        }
        System.debug(' objectFieldMap '+objectFieldMap);
        return objectFieldMap;
    }
    
    /**
     * @return list of object whose history tracking is enabled
     */
    private List<SObjectType> getHistoryObjectList() {
    
        List<SObjectType> sObjList = new List<SObjectType>();
        
        for(SObjectType sot: Schema.getGlobalDescribe().Values()) {
            if(String.valueOf(sot).endsWith('__History')) {
                sObjList.add(sot);
                System.debug('List of Objects' + sObjList);
            }
        }
        
        return sObjList;
    }
}