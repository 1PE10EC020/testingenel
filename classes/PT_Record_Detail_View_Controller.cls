public class PT_Record_Detail_View_Controller {

    @AuraEnabled
    Public static WrapperListView  getRecordDetails(String strObjectName, String recordtypeName, String sectionName, String objid, String queryField) {
        try {
            ID recordTypeID = Schema.getGlobalDescribe().
                              get(strObjectName).getDescribe().getRecordTypeInfosByName().
                              get(recordtypeName).getRecordTypeId();
            system.debug('@@@@Recordtypeid' + recordTypeID);
            String queryRecordDetail = 'Select ID from ' + strObjectName + ' where ' + queryField + '=' + '\'' + objid
                                       + '\' and recordtypeid=\'' + recordTypeID  + '\' LIMIT 1';
            system.debug('Query@@@@' + queryRecordDetail);
            SObject recDetail = Database.query (queryRecordDetail);
            system.debug('recDetail@@@@@' + recDetail);
            if (recDetail != null) {
                List < PT_Record_Detail_View__c > customSettingRecords = [select Name, Field_1_API_Name__c, Record_Type__c, Section_Name__c,
                                                  Field_2_API_Name__c, Order__c from
                                                  PT_Record_Detail_View__c where Object_Name__c = :strObjectName and
                                                          Section_Name__c = : sectionName AND
                                                                  Record_Type__c = : recordtypeName ORDER BY Order__c];
                system.debug('custom setting list' + customSettingRecords);
                WrapperListView wrapObj = new WrapperListView(recDetail.Id, customSettingRecords);
                return wrapObj;
            }

        } catch (Exception e) {
            logger.debugException(e);
        }
        return null;
    }
    Public with sharing class WrapperListView {
        @AuraEnabled
        public ID recID {
            get;
            set;
        }
        @AuraEnabled
        public List < PT_Record_Detail_View__c > Field_Rec {
            get;
            set;
        }

        public wrapperListView(ID recIDstr, List < PT_Record_Detail_View__c > fieldrec) {
            recID = recIDstr;
            Field_Rec = fieldrec;
        }
    }
}