/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	16 Jun,2017
@description:  	This controller is used to query request record type details to be displayed on Record type selection lightning component.
@reference:		420_Trader_Detail_Design_Wave0 (Section#2.8.1.10)
**********************************************************************************************/
public without sharing class PT_RequestRecordTypeController {
    
    PRIVATE STATIC FINAL STRING REQUESTOBJNAME='PT_Richiesta__c';
    PRIVATE STATIC FINAL STRING MASSIVOOBJNAME='PT_Caricamento_Massivo__c';
    public static final String SEMICOLON = ';';
    PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
    
/**********************************************************************************************
@author:       	Karan Kanagra
@date:         	26 Jun,2017
@Method Name:  	getRecordTypeDetails
@description:  	This method splits the component input attribute value by ';' to get each record type name configured and then performs SOQL
				query to get record type details like ID, Description, Name
@reference:		420_Trader_Detail_Design_Wave0 (Section#2.8.1.10)
**********************************************************************************************/    
    @AuraEnabled
    public static List<RecordType> getRecordTypeDetails(String rcdTypeName){
        List<RecordType> recordTypeList = new List<RecordType>();        	
        if(rcdTypeName!=null){
            List<String> recTypeAPINames = rcdTypeName.split(SEMICOLON);

        	try{
           		     recordTypeList = [select id,name,developerName,description from RecordType
                                                   where sobjectType=:REQUESTOBJNAME and developername in :recTypeAPINames];
       		    	    
                      
                if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }

            }
            catch(Exception e){
                return null;
       		}
        }
        else
        {
            recordTypeList = null;
        }
         
        
       	return recordTypeList;
        
    }
   
/**********************************************************************************************
@author:       	Priyanka Sathyamurthy
@date:         	25 Sep,2017
@Method Name:  	getMasRecordTypeDetails
@description:  	This method splits the component input attribute value by ';' to get each record type name configured and then performs SOQL
				query to get record type details like ID, Description, Name
@reference:		420_Trader_Detail_Design_Wave3
**********************************************************************************************/    
    @AuraEnabled
    public static List<RecordType> getMasRecordTypeDetails(){
        List<RecordType> recordTypeList = new List<RecordType>();        	
       // if(rcdTypeName!=null){
           // List<String> recTypeAPINames = rcdTypeName.split(SEMICOLON);

        	try{
           		     recordTypeList = [select id,name,developerName,description from RecordType
                                                   where sobjectType=:MASSIVOOBJNAME and (developerName=:'PT_Contributi_Massivi'
                                                     or developerName=:'PT_Richieste_Massive') LIMIT 5];
       		}
            catch(Exception e){
                return null;
       		}
        //}
        
       	return recordTypeList;
    } 
    
 /**********************************************************************************************
@author:       	Priyanka Sathyamurthy
@date:         	25 Sep,2017
@Method Name:  	getOtherTypeDetails
@description:  	This method splits the component input attribute value by ';' to get each record type name configured and then performs SOQL
				query to get record type details like ID, Description, Name
@reference:		420_Trader_Detail_Design_Wave3
**********************************************************************************************/    
  /*  @AuraEnabled
    public static List<String> getOtherTypeDetails(){
        List<String> otherTypeList = new List<String>{'E01 Accettazione Preventivi (esecuzione lavoro)','Conferma A03 - N01 - N02 - S02 - VL2 - MC1',
            							''};
      otherTypeList.add('E01 Accettazione Preventivi (esecuzione lavoro)');
      return otherTypeList;  
    }*/
}