/**********************************************************************************************
@author:        Priyanka
@date:          26 Sep,2017
@description:   This controller is used by PT_Richiesta_Trigger.
				It performs the Pre validation checks on request object and then inserts the request record.
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.1)
**********************************************************************************************/
public without sharing class PT_Richesta_Helper {
	PRIVATE STATIC FINAL String VALIDATECONDITIONS='validateConditions';
    PRIVATE STATIC FINAL String PTRICHESTAHELPER='PT_Richesta_Helper';
    PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
/**********************************************************************************************
@author:        Priyanka Sathyamurthy
@date:          26 Sep,2017
@Method Name:   validateConditions
@description:   This method is used to perform all the Prevalidations across RFI component fields before request can be inserted.
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.2 to 2.8.1.9)
**********************************************************************************************/ 
public static void validateConditions(List<PT_Richiesta__c> rcdCreated){
  Logger.push(VALIDATECONDITIONS,PTRICHESTAHELPER);
  String errMsg=null;
  try{
    if(rcdCreated!=null){
        for(PT_Richiesta__c request:rcdCreated){
            errMsg=null;
            system.debug('errMsg'+errMsg);
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateRegex(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }   
            
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateDatiVenditore(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false);
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateDati_Intestatario(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false);
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateDati_Contrattuali(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateDisalimentabilita(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateAltri_Dati(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateNCA(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateLettureEffettuate(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateParametriDiRicerca(request);
                if(errMsg != null)  {
                    request.addError(errMsg, false); 
                }
            }
            if (request!= null && errMsg == null){
                errMsg = PT_RFI_Prevalidation.validateDatiRichiesta(request);
                if(errMsg != null)  {
                   request.addError(errMsg, false); 
               }
           }
                   /* if (request!= null && errMsg == null){
                        errMsg = PT_RFI_Prevalidation.validateDatiRichiestaCliente(request);
                        if(errMsg != null)  {
                            request.addError(errMsg, false); 
                        }
                        } */                 
                        if (request!= null && errMsg == null){
                            errMsg = PT_RFI_Prevalidation.validateAddress(request);
                            if(errMsg != null)  {
                               request.addError(errMsg, false); 
                           }
                       }
                       
                   } 
               } 
               if(test.isRunningTest()){ 
                throw new PT_ManageException (EXCEMES); 
            }
        }
        catch(Exception e){
            logger.debugException(e);
        } 
        logger.pop();
    }
}