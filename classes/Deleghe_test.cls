/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Deleghe_test {

    static testMethod void delegheTest() {
        // TO DO: implement unit test
         /*
         httpController delegheController  = new httpController();
         delegheController.callMode = 'SENT';
         
       
         RestRequest req = new RestRequest(); 
		 RestResponse res = new RestResponse();
		
		 
		HttpRequest req = HttpRequest();
		HttpResponse res = HttpResponse();
		
			
		req.requestURI = 'https://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/';  
		req.httpMethod = 'GET';
	
		RestContext.request = req;
		RestContext.response = res;
		//req.setEndpoint('https://eneldistribuzione-coll.enel.it/_vti_bin/MyED.WebServices.ProxyUser/DelegationService.svc/');
		delegheController.request();
		
		System.assertEquals('true', results.success);
		System.assertEquals(10, results.records.size());
		System.assertEquals('Query executed successfully.', results.message);
        */
        
        // Set mock callout class 
	    //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	
	     
	
	    // Call method to test.
	
	    // This causes a fake response to be sent
	
	    // from the class that implements HttpCalloutMock.
	   //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    httpController testRequest = new httpController();
	   
	    testRequest.vatCode = '1234567891234567';
	    testRequest.callMode = 'SENT';
	    

	  
	    
	    
	    
	    boolean Button = testRequest.showButton;
	    boolean Table = testRequest.showTable;
	    boolean tableDeleganti = testRequest.showtableDeleganti;
	    
	    testRequest.request();
	
	     
	
	    // Verify response received contains fake values
	/*
	    String contentType = res.getHeader('Content-Type');
	
	    System.assert(contentType == 'application/json');
	
	    String actualValue = res.getBody();
	
	    String expectedValue = '{"foo":"bar"}';
	
	    System.assertEquals(actualValue, expectedValue);
	
	    System.assertEquals(200, res.getStatusCode()); 
	      */  
	    }
    
    
    static testMethod void delegantiTest() {
        // TO DO: implement unit test
         httpController delegheController  = new httpController();
         delegheController.afiscalCode = 'SPSGNR80A01F839U';
	    
	   
	    
	    boolean Button = delegheController.showButton;
	    boolean Table = delegheController.showTable;
	    boolean tableDeleganti = delegheController.showtableDeleganti;
         delegheController.callMode = 'RECEIVED';
         delegheController.request();
    }
}