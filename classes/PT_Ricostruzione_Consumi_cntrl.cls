/**********************************************************************************************
@author:       	Martina Testa
@date:         	02 Oct,2017
@Class Name:  	PT_Ricostruzione_Consumi_cntrl
@description:  	This controller is used for PT_Ricostruzione_Consumi Lightning component to search 
				and display PT_Ricostruzione_Consumi records in CONSUMI tab.
@reference:		420_Trader_Detail_Design_Wave3_v1.1 (par. 8.9.1)
**********************************************************************************************/


public class PT_Ricostruzione_Consumi_cntrl {
    
     @AuraEnabled
     public static String recordId{get;set;}
    
    
/**********************************************************************************************
@author:        Martina Testa
@date:          02 Oct,2017
@Name:          getPicklistTrCl
@description:   This Aura Enabled method is used to create the dynamic picklist “Coppia Trader/Cliente”.
                The picklist values are determined from the distinct “Coppia Trader/Cliente” values of the Ricostruzione Consumi 
                records related to the on-screen request;

**********************************************************************************************/
    @AuraEnabled
    public static PaginationWrapper getPicklistTrCl(String recordId){
        
        system.debug('****PT_Ricostruzione_Consumi**** recordId= '+recordId);
        PaginationWrapper wrapObj = new PaginationWrapper();
        
        List<String> coppiaTrCl_lst= new List<String>();
        
        
        List<PT_Ricostruzione_consumi__c> ricConsRecord_lst  = new List<PT_Ricostruzione_consumi__c>();
        ricConsRecord_lst=[select id,name, COPPIA_CLIENTE_TRADER__c, protocolloRic__c 
                           from PT_Ricostruzione_consumi__c
                           where protocolloRic__c =:recordId];
         system.debug('****PT_Ricostruzione_Consumi**** ricConsRecord_lst= '+ricConsRecord_lst);
        
        for(PT_Ricostruzione_consumi__c currentRecordList:ricConsRecord_lst){
           coppiaTrCl_lst.add(currentRecordList.COPPIA_CLIENTE_TRADER__c);
             }
        
        wrapObj.coppiaTrCl_set = new Set<String>( coppiaTrCl_lst);
        system.debug('****PT_Ricostruzione_Consumi**** coppiaTrCl_set= '+ wrapObj.coppiaTrCl_set);
        
        return wrapObj;
        
     
    }
    
 
/**********************************************************************************************
@author:        Martina Testa
@date:          02 Oct,2017
@Name:          getListRicConsumi
@description:   This Aura Enabled method is used to retrieve all the records of PT_Ricostruzione_Consumi related to 
                the selected value of picklist “Coppia Trader/Cliente” and the on-screen request, and organize the 
                retrived data in order to be able to show them in a tree structure. 

**********************************************************************************************/
    @AuraEnabled
    public static PaginationWrapper getListRicConsumi(String recordId, String coppiaTrCl){
       PaginationWrapper wrapObj = new PaginationWrapper();
        
        //List that will contain all the results of the query
        List<PT_Ricostruzione_consumi__c> recordConsumi=new List<PT_Ricostruzione_consumi__c>();
        
       //List that will contain the keys of the map(year,list(months))
       List<String> numAnni=new List<String>();
        
       system.debug('****PT_Ricostruzione_Consumi**** recordId= '+recordId);
        
       recordConsumi=[select id,name,MESE_RIFERIMENTO__c, ANNO_RIFERIMENTO_RIC__c, COD_FISCALE_CL__c,COPPIA_CLIENTE_TRADER__c,
                        PARTITA_IVA_TR__c,ID_DISPACC__c,ID_TRADER__c, 
                        protocolloRic__c,
                        CONS_ATTIVA_IMMESSA_F1_MIS__c,CONS_ATTIVA_IMMESSA_F1_RIC__c,CONS_ATTIVA_IMMESSA_F2_MIS__c,
                        CONS_ATTIVA_IMMESSA_F2_RIC__c,CONS_ATTIVA_IMMESSA_F3_MIS__c,CONS_ATTIVA_IMMESSA_F3_RIC__c,
                        CONS_ATTIVA_IMMESSA_F4_MIS__c,CONS_ATTIVA_IMMESSA_F4_RIC__c,CONS_ATTIVA_PRELEVATA_F1_MIS__c,
                        CONS_ATTIVA_PRELEVATA_F1_RIC__c,CONS_ATTIVA_PRELEVATA_F2_MIS__c,CONS_ATTIVA_PRELEVATA_F2_RIC__c,
                        CONS_ATTIVA_PRELEVATA_F3_MIS__c,CONS_ATTIVA_PRELEVATA_F3_RIC__c,CONS_ATTIVA_PRELEVATA_F4_MIS__c,
                        CONS_ATTIVA_PRELEVATA_F4_RIC__c,PERIODO_DI_RIFERIMENTO__c,POTENZA_MIS_ATT__c,POTENZA_MIS_PASS__c,
                        POTENZA_RIC_ATT__c,POTENZA_RIC_PASS__c,STATO_CONTRATTO__c,TIPOLOGIA_MISURATORE__c
                      from PT_Ricostruzione_consumi__c 
                      where COPPIA_CLIENTE_TRADER__c =:coppiaTrCl and protocolloRic__c =:recordId
                     order by ANNO_RIFERIMENTO_RIC__c DESC]; 
        
        for(PT_Ricostruzione_consumi__c currentRecordList:recordConsumi){
           numAnni.add(currentRecordList.ANNO_RIFERIMENTO_RIC__c);
             }
       
       wrapObj.ricConsumi_Lst=recordConsumi;
       wrapObj.anni_set = new Set<String>(numAnni);
       system.debug('****PT_Ricostruzione_Consumi**** Record List Ric = '+ wrapObj.ricConsumi_Lst);
       system.debug('****PT_Ricostruzione_Consumi****  wrapObj.anni_set lista di anni= '+ wrapObj.anni_set);
        
        //Initialization of the two maps used to create tree branches
        
        //For the first depth level(years): map(year,List(months))
        Map <String, List<String>> mapAnnoMesi= new Map <String, List<String>>();
        
        //For the second depth level(years): map(year+month, PT_Ricostruzione_consumi__c record)
        Map <String, PT_Ricostruzione_consumi__c> mapMeseRecord= new  Map < String,PT_Ricostruzione_consumi__c>();
        
        List<String> dateRic=new List<String>();
        
        //For each year the months list is retrieved, and used to build the map
        for(String anno:wrapObj.anni_set){
             List<String> mesiLst=new List<String>();
            
            // mesiLstTr will contain the 'translations' of the values of MESE_RIFERIMENTO__c
             List<String> mesiLstTr=new List<String>();
           
             for(PT_Ricostruzione_consumi__c currentRecordList:recordConsumi){
               if (currentRecordList.ANNO_RIFERIMENTO_RIC__c==anno){
                     mesiLst.add(currentRecordList.MESE_RIFERIMENTO__c);
                     mesiLst.sort();
               }
    
             } 
            
            
            system.debug('****PT_Ricostruzione_Consumi****calendarmonths is:'+mesiLst);
            //CalendarMonths__c custom settings associate the value of MESE_RIFERIMENTO__c with the value to show (e.g. '01'-->January)
             for(String mese:mesiLst){
                   	 CalendarMonths__c cs = CalendarMonths__c.getValues(mese);
                     mesiLstTr.add(cs.mese__c);
                     system.debug('****PT_Ricostruzione_Consumi****calendarmonths is:'+cs.mese__c);
                    
                     mapAnnoMesi.put(anno, mesiLstTr);
                     
                     }
         
           
            Set<String> mesiSet = new Set<String>(mesiLst);
            system.debug('****PT_Ricostruzione_Consumi****  mesiSet '+ mesiSet);
            //For each year, build string year+month to use as key for mapMeseRecord
           
            for(String mese:mesiSet){
                
                for(PT_Ricostruzione_consumi__c currentRecordList:recordConsumi){
                    if (currentRecordList.MESE_RIFERIMENTO__c==mese && currentRecordList.ANNO_RIFERIMENTO_RIC__c==anno){
                        CalendarMonths__c cs = CalendarMonths__c.getValues(mese);
                        mapMeseRecord.put(anno+''+cs.mese__c,currentRecordList);
                       
                    }
                }
            }
                        
        }
        
        
        wrapObj.dateSetRic=mapMeseRecord.keySet();
        system.debug('****PT_Ricostruzione_Consumi****  wrapObj.dateSetRic= '+ wrapObj.dateSetRic);
        
        system.debug('****PT_Ricostruzione_Consumi****  mapAnnoMesi= '+ mapAnnoMesi);
        wrapObj.mapMonthsForYear=mapAnnoMesi;
        system.debug('****PT_Ricostruzione_Consumi****  mapMeseRecord= '+ mapMeseRecord);
        wrapObj.mapRecordForMonths=mapMeseRecord;
      
       return wrapObj;
      
    }
    

/**********************************************************************************************
@author:        Martina Testa
@date:          02 Oct,2017
@Name:          PaginationWrapper
@description:   This Aura Enabled method is a Wrapper Class used for the pagination 

**********************************************************************************************/
 
        public class PaginationWrapper{
            
        @AuraEnabled
        public Set<String> coppiaTrCl_set {get;set;}   
      
        @AuraEnabled
        public Set<String> anni_set {get;set;}   
      
        @AuraEnabled
        public List<PT_Ricostruzione_consumi__c> ricConsumi_Lst {get;set;}
        
        @AuraEnabled
        public Map <String, List<String>> mapMonthsForYear {get;set;} 
            
        @AuraEnabled
        public Map <String, PT_Ricostruzione_consumi__c> mapRecordForMonths {get;set;} 
         
        @AuraEnabled
        public Set<String> dateSetRic {get;set;} 
            
        public PaginationWrapper(){
            Set<String> anni_set = new Set<String>();
            Set<String> coppiaTrCl_set = new Set<String>();
            ricConsumi_Lst  = new List<PT_Ricostruzione_consumi__c>();
            mapMonthsForYear= new Map <String, List<String>>();
            mapRecordForMonths= new  Map < String,PT_Ricostruzione_consumi__c>();
            dateSetRic= new Set<String>();
         
        }
        
    }
    
    
}