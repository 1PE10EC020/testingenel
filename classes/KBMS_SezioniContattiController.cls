public class KBMS_SezioniContattiController {
    
    @AuraEnabled 
    public static List<Sezione_Contatti__c> getSezioneContatti (String selectedValue)
    {               
       List<Sezione_Contatti__c> sezionelist = new List<Sezione_Contatti__c>();
       if (selectedValue == null || selectedValue =='' || selectedValue == 'Tutto')
       {
            sezionelist = [SELECT Id,Tipologia_Richiedente__c,Nome_Sezione__c 
                           FROM Sezione_Contatti__c 
                           ORDER BY Priority__c];     
       }
       else
       {
            sezionelist = [SELECT Id,Tipologia_Richiedente__c,Nome_Sezione__c 
                           FROM Sezione_Contatti__c 
                           WHERE Tipologia_Richiedente__c LIKE :'%' +selectedValue+'%'

                           ORDER BY Priority__c];     
       }
       return sezionelist;        
    }

    @AuraEnabled    
    public static List<contact> findContact(Id scId) 
    {       
       List<Contact> contactlist = [SELECT name,MailingCity,MailingStreet,MailingState,MailingPostalCode,MailingCountry,Sito__c,App__c,Zona__c,Phone,MobilePhone,Email,Fax,Sezione_Contatti__c 
                                    FROM Contact  
                                    WHERE Sezione_Contatti__c =: scId];       
       return contactlist;        
    }   
   
  }