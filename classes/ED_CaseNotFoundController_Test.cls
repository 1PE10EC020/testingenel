@IsTest
public class ED_CaseNotFoundController_Test {

/*
	@testSetup
	private static void setup() {
		Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		Account acc2 = new Account();
		acc2.CodiceFiscale__c =  'DOEJNA80A41H501Z';
		acc2.name = 'Doe';
		Acc2.nome__c ='Jane';
		acc2.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc2.phone = '3333111';
		insert acc2;
		
		PercorsoTelefonico__c percorso = new PercorsoTelefonico__C();

	 	percorso.CodIVR__c = '111';
	 	percorso.Contesto__c = 'Test';
	 	insert percorso;
	}
*/

	@isTest
	static void test(){
		
		Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		Account acc2 = new Account();
		acc2.CodiceFiscale__c =  'DOEJNA80A41H501Z';
		acc2.name = 'Doe';
		Acc2.nome__c ='Jane';
		acc2.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc2.phone = '3333111';
		insert acc2;
		
		PercorsoTelefonico__c percorso = new PercorsoTelefonico__C();

	 	percorso.CodIVR__c = '111';
	 	percorso.Contesto__c = 'Test';
	 	insert percorso;
		
		test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.ED_CaseNotFoundController')); 
		System.currentPageReference().getParameters().put(Constants.PHONE_PARAMETER, constants.UNDEFINED);
		System.currentPageReference().getParameters().put(Constants.IVR_PATH_PARAMETER, '111');
		System.currentPageReference().getParameters().put(Constants.CASENUM_PARAMETER, constants.UNDEFINED);
		

		ED_CaseNotFoundController myController = new ED_CaseNotFoundController ();
		myController.redirect();
		
		boolean errorfound = false;
		try{
			myController.redirect();
		}
		catch(Exception e){
			errorfound = true;
		}
		system.assertEquals(false,errorfound);
		
		myController.redirect();
		test.stopTest();
	
	}


}