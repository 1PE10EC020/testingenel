/*
 * @author : Accenture
 * @date : 2011-09-28
 */

public class CustomButtonLinkclass {

   public static Integer indexGen {get; set;}
    public List<CBLwrapper> CBLlist;
    public Integer numRows {get; set;}
    

     public class CBLwrapper {
    
        private     Custom_Buttons_and_Links__c CBLsetting;
        private Integer index;  

        public CBLwrapper() {
            Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            System.Debug('Key is ' + key);
            String strParentObjectID = lstParams.get(key);
            
            this.CBLsetting = new   Custom_Buttons_and_Links__c (Custom_Object__c = strParentObjectID);
            this.index = indexGen++;
        }
        
        public  Custom_Buttons_and_Links__c getCBLsetting() {
            return CBLsetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 
     
     public CustomButtonLinkclass(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;

        CBLlist = new List<CBLwrapper>();
        numRows = 1;
    }
    
    public List<CBLwrapper> getCBLlist() {
        return CBLlist;
}
 

public PageReference save() {
        try {
            List<   Custom_Buttons_and_Links__c> tempList = new List< Custom_Buttons_and_Links__c>();
            for(Integer i=0; i<CBLlist.size(); ++i)
                tempList.add(CBLlist[i].getCBLsetting());
            upsert(tempList);
            return new PageReference ('/' + ApexPages.currentPage().getParameters().get('retURL'));
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }


        


    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    CBLlist.add(new CBLwrapper());
        } 
        catch (Exception ex) {
        ApexPages.addMessages(ex);
        
        }
    }
   public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                CBLwrapper cblWrap = new CBLwrapper();
                cblWrap.CBLsetting = [SELECT Behaviour__c,Comment__c,Content_Source__c,Custom_Object__c,Display_Type__c,Label__c,Name__c,Requirement_no__c FROM Custom_Buttons_and_Links__c WHERE ID = :CurrentRecordID];
                CBLList.add(cblWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    
    public void clear() {
        CBLlist.clear();
        numRows = 1;
    }
    
  
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<CBLlist.size(); ++i)
                if(CBLlist[i].index == delIndex) {
                    CBLlist.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}