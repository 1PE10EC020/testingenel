/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for RequestRecordTypeController.
**********************************************************************************************/

@isTest(SeeAllData = False)
/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for RequestRecordTypeController.
**********************************************************************************************/
private class PT_RequestRecordTypeControllerTest{
    private static final String ALIAS = 'unEx124';
    /* 
    Author : Akansha Lakhchaura
    Apex Method : getRecordTypeDetails
    CreatedDate : 04/07/2017
    Description : Test class for RequestRecordTypeController for PT_N02
    */  
    private static testMethod void getRecordTypeDetails(){
        // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
         //database.insert(runningUser);
        
        List<RecordType> recordTypeList = new List<RecordType>(); 
        // System.runAs(runningUser){ 
        recordTypeList = PT_RequestRecordTypeController.getRecordTypeDetails('PT_N02');
        // }
       // System.debug('record type size'+ recordTypeList.size());         
        System.assertEquals(null, recordTypeList);
    } 
    
    /* 
    Author : Akansha Lakhchaura
    Apex Method : getRecordTypeDetailsForNull
    CreatedDate : 04/07/2017
    Description : Test class for RequestRecordTypeController for null 
   */   
    private static testMethod void getRecordTypeDetailsForNull(){
        // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);
        
        List<RecordType> recordTypeList = new List<RecordType>();
        // System.runAs(runningUser){ 
        recordTypeList = PT_RequestRecordTypeController.getRecordTypeDetails(null);
        // }
        System.debug('record type size'+ recordTypeList);         
        System.assertEquals(null, recordTypeList);
    } 
  
    
     private static testMethod void getMasRecordTypeDetails(){
        // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
         //database.insert(runningUser);
        
        List<RecordType> recordTypeList = new List<RecordType>(); 
        // System.runAs(runningUser){ 
        recordTypeList = PT_RequestRecordTypeController.getMasRecordTypeDetails();
        // }
       // System.debug('record type size'+ recordTypeList.size());         
        //System.assertEquals('PT_Contributi_Massivi ', recordTypeList[0].Name);
    } 
   
}