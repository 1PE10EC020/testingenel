@isTest
public class KBMS_ArticleFAQViewBatchableTest 
{
    public Static testMethod void testArticleHistoryBatch()
    {
        List<FAQ__kav> articles = KBMS_TestDataFactory.createFAQTestRecords(10);  
        
        String defaultDataCategory = Label.KBMS_AppDataCategory; 
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        
        List<FAQ__DataCategorySelection> datacatSections = new List<FAQ__DataCategorySelection>();
        
        for (FAQ__kav f: articles  )
        {
            FAQ__DataCategorySelection datacatTest = new FAQ__DataCategorySelection();
            datacatTest.ParentId= f.Id;     
            datacatTest.DataCategoryName=dataCategory;
            datacatTest.DataCategoryGroupName=GroupDataCategory;
            datacatSections.add(datacatTest); 
            
            FAQ__DataCategorySelection datacatTest1 = new FAQ__DataCategorySelection();
            datacatTest1.ParentId= f.Id;     
            datacatTest1.DataCategoryName='OT';
            datacatTest1.DataCategoryGroupName='KBMS';
            datacatSections.add(datacatTest1); 
            
            
        }
        insert datacatSections;
        
        List<FAQ__kav> insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID In: articles ]; 
        
        for (FAQ__kav f: insertedTestArticle )
        {
            KbManagement.PublishingService.publishArticle(f.KnowledgeArticleId,true);      
        }
        Test.startTest();
        KBMS_ArticleFAQViewBatchable  b = new KBMS_ArticleFAQViewBatchable();
        database.executebatch(b);
        Test.stopTest();
    }
}