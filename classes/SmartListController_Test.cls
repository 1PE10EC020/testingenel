@isTest(SeeAllData = true)
public class SmartListController_Test {

    static testMethod void TestgetRecords() {
        

        //CREATE REQUEST
        PT_Richiesta__c request = new PT_Richiesta__c();    
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Richiesta__c; 
        Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id n02_id = richiestaRecordTypeInfo.get('PT_N02').getRecordTypeId();
        request.RecordTypeId= n02_id;
        request.PotImp__c='1.5';
        database.insert(request);

        //PT_NOTIFICA TEST 
        PT_Notifica__c notifica = new PT_Notifica__c();
        notifica.IdNotFour__c = '12345';
        notifica.ProtocolloRic__c = request.Id;
        notifica.DettXml__c = 'Test Test';
        notifica.DettCsv__c = 'test Test';
        notifica.CodFlusso__c = 'TEST TEST';
        notifica.CodServizio__c = 'test test xcodServ';
        notifica.DataOraNot__c = System.today();
        insert notifica;

        String otherSoql='';
        Integer sObjOffset=5;
        List<String> strList = new List<String>();
        strList.add('IdNotFour__c');
        strList.add('CodFlusso__c');
        strList.add('CodServizio__c');
        strList.add('DataOraNot__c');
        Integer maxNoOfRecord=8;
        String sobjApiName='PT_Notifica__c';
        String recordId = request.Id; //**[G.Tedesco]
        List<String> downlist = new List<String>();
        downlist.add('DettXml__c');
        downlist.add('DettCsv__c');
        String whereClause = '';
        SmartListController.PaginationWrapper wrapObj;

        Test.startTest();
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,sobjApiName,false,recordId,true,downlist,whereClause,false);
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,sobjApiName,true,recordId,true,downlist,whereClause,true);
       
        whereClause = ' CodServizio__c='+'\''+notifica.CodServizio__c+'\'';
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,sobjApiName,true,recordId,true,downlist,whereClause,true);
        
        //CREATE REQUEST with componente costo
        PT_Richiesta__c request2 = new PT_Richiesta__c();    
        Schema.DescribeSObjectResult cfrSchema2 = Schema.SObjectType.PT_Richiesta__c; 
        Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo2 = cfrSchema2.getRecordTypeInfosByName();
        Id request2_id = richiestaRecordTypeInfo2.get('PT_N02').getRecordTypeId();
        request2.RecordTypeId= request2_id;
        request2.PotImp__c='1.5';
        request2.Sottostato__c = 'INL.070';
        database.insert(request2);

        //TEST GET ERROR MESSAGE ON PT_COMPONENTE_COSTO
        strList.clear();
        strList.add('Descrizione__c');
        strList.add('Quantita__c');
        strList.add('QuotaFissa__c');
        strList.add('QuotaVariabile__c');
        strList.add('Totale__c');
        whereClause='';

        recordId = request2.Id;
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,'PT_Componente_Costo__c',false,recordId,false,null,whereClause,false);  

        request2.TipologiaContributi__c = 'Test Test';
        update request2;
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,'PT_Componente_Costo__c',false,recordId,false,null,whereClause,false);  
        
        request2.Sottostato__c='';
        update request2;
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,'PT_Componente_Costo__c',false,recordId,false,null,whereClause,false);  
        
        request2.TipologiaContributi__c = '';
        update request2;
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,'PT_Componente_Costo__c',false,recordId,false,null,whereClause,false);   
        
        wrapObj=SmartListController.getRecords(otherSoql,sObjOffset,strList,maxNoOfRecord,'PT_Componente_Costo__c',false,'',false,null,whereClause,false);
  
        Test.stopTest();
    }
    
   

}