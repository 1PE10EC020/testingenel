/**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method used to hold customized data for PT_F05_Search Lightning component 
                 used in PT_F05_Search class
@reference:    420_Trader_Detail_Design_Wave1 (Section#4.2.2)
**********************************************************************************************/
public class PT_F05_csv_Wrapper{

        @AuraEnabled
        public Id ReqId {
            get;
            set;
        }
        @AuraEnabled
        public string CODICE_RICHIESTA {
            get;
            set;
        }

        @AuraEnabled
        public datetime DATA_INSERIMENTO {
            get;
            set;
        }
        @AuraEnabled
        public string PRESA {
            get;
            set;
        }

        @AuraEnabled
        public string POD {
            get;
            set;
        }

        @AuraEnabled
        public string CONTRATTO_DI_DISPACCIAMENTO {
            get;
            set;
        }

        @AuraEnabled
        public string MANDATO_CONNESSIONE_RICHIESTO {
            get;
            set;
        }

        @AuraEnabled
        public string SERVIZIO_MISURA_RICHIESTO {
            get;
            set;
        }

        @AuraEnabled
        public datetime DATA_DECORRENZA {
            get;
            set;
        }

        @AuraEnabled
        public string TIPOLOGIA_RICHIESTA {
            get;
            set;
        }

        @AuraEnabled
        public string STATO_RICHIESTA {
            get;
            set;
        }

        @AuraEnabled
        public string DETTAGLIO_RICHIESTA {
            get;
            set;
        }

        @AuraEnabled
        public string DENOMINAZIONE_ATTUALE_CF {
            get;
            set;
        }

        @AuraEnabled
        public string CODICE_FISCALE_CF {
            get;
            set;
        }

        @AuraEnabled
        public string PARTITA_IVA_CF {
            get;
            set;
        }
        @AuraEnabled
        public string DENOMINAZIONE_CF {
            get;
            set;
        }
        @AuraEnabled
        public string CODICE_FISCALE {
            get;
            set;
        }

        @AuraEnabled
        public string PARTITA_IVA {
            get;
            set;
        }
        @AuraEnabled
        public string INDIRIZZO_FORNITURA {
            get;
            set;
        }

        @AuraEnabled
        public string LOCALITA_FORNITURA {
            get;
            set;
        }

        @AuraEnabled
        public string INDIRIZZO_NUOVA {
            get;
            set;
        }

        @AuraEnabled
        public string LOCALITA_NUOVA {
            get;
            set;
        }

        @AuraEnabled
        public string NUOVO_INDIRIZZO {
            get;
            set;
        }

        @AuraEnabled
        public string NUOVA_LOCALITA {
            get;
            set;
        }

        @AuraEnabled
        public string TIPOLOGIA_FORNITURA {
            get;
            set;
        }

        @AuraEnabled
        public date DATA_DEL {
            get;
            set;
        }
        @AuraEnabled
        public string ATTUALE_OPZIONE {
            get;
            set;
        }
        @AuraEnabled
        public string ATTUALE_TENSIONE {
            get;
            set;
        }
        @AuraEnabled
        public Decimal ATTUALE_POTENZA {
            get;
            set;
        }
        @AuraEnabled
        public string NUOVA_OPZIONE {
            get;
            set;
        }
        @AuraEnabled
        public string NUOVA_TENSIONE {
            get;
            set;
        }
        @AuraEnabled
        public Decimal NUOVA_POTENZA_DISPONIBILE {
            get;
            set;
        }
        @AuraEnabled
        public string PREVENTIVO {
            get;
            set;
        }
        @AuraEnabled
        public object Data_Lavoro {
            get;
            set;
        }
        @AuraEnabled
        public string A_Matr_Misu {
            get;
            set;
        }
        @AuraEnabled
        public Decimal A_Energia_Attiva_F1 {
            get;
            set;
        }
        @AuraEnabled
        public string CODICE_RICHIESTA_VENDITORE {
            get;
            set;
        }
        @AuraEnabled
        public object ULTIMO_CODICE_FLUSSO {
            get;
            set;
        }
        @AuraEnabled
        public object CODICE_DATA_PUBBLICAZIONE {
            get;
            set;
        }
    }