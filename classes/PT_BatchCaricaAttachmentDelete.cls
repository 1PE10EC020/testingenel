/*****************************************************************************************************
@author    Bharath Sharma
@date      27 SEP 2017
@description: This apex class is used to run a batch job on PT_Caricamento_Massivo__c object to delete Attachment
*******************************************************************************************************/
global with sharing class PT_BatchCaricaAttachmentDelete implements Database.Batchable < sObject > {


     private static Final STRING LST = '< ';
     private static Final STRING LST1 = '= ';
     Date d = System.today()-1;
     Datetime myDT = datetime.newInstance(d.year(), d.month(),d.day());
     String myDate = myDT.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX'); 
     private static Final string QUERYSTR2='Select Id,Name,FlagCar_Completato__c,(SELECT Id FROM Attachments) From PT_Caricamento_Massivo__c where FlagCar_Completato__c =\'s\'';
     private static Final string QUERYSTR = ' AND CreatedDate '+LST+LST1;
    /*Start Method - query the PT_Caricamento_Massivo__c record */
     string FINALQUERYSTR=QUERYSTR2+QUERYSTR+myDate ;
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('QUERY==>' + FINALQUERYSTR);
        return Database.getQueryLocator(FINALQUERYSTR);

    }

    /* Execute Logic */
    global void execute(Database.BatchableContext BC, List < PT_Caricamento_Massivo__c > CMList) {

        try {
       	Set < id > attachmentSet= new Set < id > ();
            for (SObject parentRecord : CMList){
            SObject[] childRecordsFromParent = parentRecord.getSObjects('Attachments');
              if (childRecordsFromParent != null) {
                for (SObject childRecord : childRecordsFromParent){
                  Object ChildFieldValue1 = childRecord.get('Id');
                    Id recid = (Id) childRecord.get('Id');
                  attachmentSet.add(recid );
                  System.debug(' Att Id: '+ ChildFieldValue1 );
                }
              } 
            }
            If(attachmentSet.size() > 0) {
                List < Attachment > casAttmtList = [SELECT Id, ParentId FROM Attachment WHERE id in: attachmentSet];
                system.debug('casAttmtList ==>' + casAttmtList);
                database.Delete(casAttmtList,false);
            }
            
   
        }
        Catch(Exception e) {
            logger.debugException(e);

        }

    }
    /* Finish Method*/
    global void finish(Database.BatchableContext BC) {

    }
}