@RestResource(urlMapping='/getArticleSearch/*')
global with sharing class KBMS_ArticleSearchRestful
{
    @HttpGet
    global static List<ArticleWrapper> getArticleByCategoryIdArticleType()
    {
        String articleType = RestContext.request.params.get('articleType');
        String limits = RestContext.request.params.get('limit');
        String froms = RestContext.request.params.get('from');
        String categoryId = RestContext.request.params.get('categoryId');
        String searchString= RestContext.request.params.get('textSearch');
        List<String> searchStringList = new List<String>();
         if(searchString != NULL && searchString != '')
    	 searchStringList = searchString.split(' ');
        //System.debug('>>>>>>>'+searchStringList);
        
        Integer noLimits = limits == null ? 0 : Integer.valueOf(limits);
        Integer offsets = froms == null ? 0 : Integer.valueOf(froms );
        
        String Language = System.Label.KBMS_Language;
        String PublishStatus = System.Label.KBMS_PublishStatus_Online;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String firstPartQuery='';
        String secondPartQuery='';
        String thirdPartQuery='';
        String query='';
        if(searchStringList.size()>1)
        {
        	firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM ' + articleType+'__kav ';
        	secondPartQuery = ' WHERE (PublishStatus =: PublishStatus AND Language =:language '; 
        
            	for(String eachSearchString : searchStringList)
            	{
            	    secondPartQuery += ' AND (Title LIKE \'%' +eachSearchString+'%\' OR Summary LIKE \'%'+ eachSearchString+'%\')';
            	}
        
        
        	thirdPartQuery = ') WITH DATA CATEGORY ' + defaultDataCategory +' BELOW ' + categoryId + '__c AND KBMS__c AT (OT__c) ORDER BY LastModifiedDate DESC LIMIT ' + noLimits + ' OFFSET ' + offsets;
        	query = firstPartQuery +secondPartQuery +thirdPartQuery ;
      		System.debug('>>>>>>>>>>>>>>>>>>'+query);
        }else
        {
            firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM ' + articleType+'__kav ';
        	secondPartQuery = ' WHERE (PublishStatus =: PublishStatus AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') )WITH DATA CATEGORY ' + defaultDataCategory +' BELOW ' + categoryId + '__c AND KBMS__c AT (OT__c) ORDER BY LastModifiedDate DESC';
      		thirdPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        	query = firstPartQuery +secondPartQuery +thirdPartQuery ;
        }
        
        List<sObject> knwoledgesObject = Database.query(query);
        
        
        List<ArticleWrapper> ArticleWrapper= new List<ArticleWrapper>();
        for (sObject sObjKAV  : knwoledgesObject)
        {
            ArticleWrapper aw = new ArticleWrapper();            
            aw.Title = String.valueOf(sObjKAV.get('Title'));
            aw.Id =String.valueOf(sObjKAV.get('Id'));
            aw.categoryId = categoryId;
            aw.articleType = articleType;
            ArticleWrapper.add(aw);
        }
        return ArticleWrapper;
    
    }
    global class ArticleWrapper
    {
        public String Title{get;set;}
        public String Id{get;set;}
        public String categoryId{get;set;}
        public String articleType{get;set;}
    } 
    
}