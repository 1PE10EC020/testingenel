@isTest
public class IntegrationUtils_test{
    
    @isTest
    static void retiveTimeout_noError(){
    
        // create custom setting
        Endpoints__c endp = createCustomSettings ( 'TEST_NAME');
        insert endp;
        // retrive test timeout
        Integer timeout = IntegrationUtils.getTimeout('TEST_NAME');
        system.assertEquals(timeout,60);
        endp.isProd__C = true;
        update endp;
        // retrive production timeout
        timeout = IntegrationUtils.getTimeout('TEST_NAME');
        system.assertEquals(timeout,50);
    
    }
    
    @isTest
    static void retiveTimeout_withError(){
    
        // Into this scenario we don't create a Custom Setting

        // retrive test timeout
        Integer timeout = IntegrationUtils.getTimeout('TEST_NAME');
        system.assertEquals(timeout,Constants.DEFAULT_TIMEOUT); // check if the defaul timeout is retrived
    
    }
    
    @isTest
    static void retiveEndpoint_noError(){
    
        // create custom setting
        Endpoints__c endp = createCustomSettings ( 'TEST_NAME');
        insert endp;
        // retrive test endpoint
        String response = IntegrationUtils.getFullEndpoint('TEST_NAME');
        system.assertEquals(response,'CD');
        endp.isProd__C = true;
        update endp;
        // retrive production endpoint
        response = IntegrationUtils.getFullEndpoint('TEST_NAME');
        system.assertEquals(response,'AB');
    
    }
    
    @isTest
    static void retiveEndpoint_withError(){
            String response = IntegrationUtils.getFullEndpoint('TEST_NAME');
        system.assertEquals(response,Constants.DEFAULT_ENDPOINT);
    
    
    }
    
    
    private static Endpoints__c createCustomSettings(string customSettingsName ){
        Endpoints__c endp = new Endpoints__c();
        endp.name = customSettingsName;
        endp.Timeout_Prod__c = 50;
        endp.Timeout_Test__c = 60;
        endp.isProd__c = false;
        endp.BaseURL_Prod__c = 'A';
        endp.BaseURL_Test__c = 'C';
        endp.Endpoint_Prod__c = 'B';
        endp.Endpoint_Test__c = 'D';
        return endp;
    }  
    
    
}