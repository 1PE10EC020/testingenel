public class PT_SetRTEsteso {

     public static void setDescriptionRT (Map<Id,PT_Richiesta__c> inRichiesta){
     try{          
            Map<id,PT_Richiesta__c> mapRichiesta =new Map<ID,PT_Richiesta__c> ([Select id, RecordType.Description, Record_Type_Esteso__c from PT_Richiesta__c where id in :inRichiesta.keySet()]);
             
            for(PT_Richiesta__c rich: mapRichiesta.values()){         
                inRichiesta.get(rich.id).Record_Type_Esteso__c = rich.RecordType.Description;
            }
         }
        catch (Exception e){
             system.debug(logginglevel.error,'Exception in PT_SetRTEsteso.setDescriptionRT: ' + e.getMessage() );
        } 
    }
}