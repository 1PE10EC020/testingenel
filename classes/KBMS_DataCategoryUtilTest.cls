/**
* @author Atos India Pvt Ltd.
* @date  Sept,2016
* @description This class is test class of KBMS_ArticleByCategoryIdRestful
*/

@isTest()
private class KBMS_DataCategoryUtilTest{
    
    static testMethod void testMethod1() {
    
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName= dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;  
        
        FAQ__DataCategorySelection datacatTest0=new FAQ__DataCategorySelection();
        datacatTest0.ParentId= faqtest[0].Id;     
        datacatTest0.DataCategoryName='Connessione';
        datacatTest0.DataCategoryGroupName=GroupDataCategory;
        
       // insert datacatTest0;
        
        FAQ__DataCategorySelection datacatTest1=new FAQ__DataCategorySelection();
        datacatTest1.ParentId= faqtest[0].Id;     
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 
        
        string datacategoryid=String.valueOf(datacatTest.Id);
        //KBMS_DataCategoryUtil.Category categorywrap=new KBMS_DataCategoryUtil.Category();
        //KBMS_DataCategoryUtil.Subcategory Subcategorywrap=new KBMS_DataCategoryUtil.Subcategory();
        Integer val=KBMS_DataCategoryUtil.getCategoryListCountByType(datacatTest.DataCategoryName,articleType);
        KBMS_DataCategoryUtil.getParentCategory(datacatTest.DataCategoryName);
        KBMS_DataCategoryUtil.getMapParentCategoryList();
        KBMS_DataCategoryUtil.getCategoryGroupStructure(datacatTest.DataCategoryGroupName,datacatTest.DataCategoryName);
        KBMS_DataCategoryUtil.getDescribeDataCategoryGroupStructureResults();
        KBMS_DataCategoryUtil.getArticleCountByCategory(datacatTest.DataCategoryName);
        
        //KBMS_DataCategoryUtil.getCategoryGroupStructure('Connessione',datacatTest.DataCategoryName);
    }
    
}