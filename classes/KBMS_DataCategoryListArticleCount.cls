@RestResource(urlMapping='/getCategoryListArticleCount/*')

global with sharing class KBMS_DataCategoryListArticleCount{
    
    @HttpGet
    global static Integer doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        String categoryId = RestContext.request.params.get('categoryId');
        String articleType = RestContext.request.params.get('articleType');
        
     Integer count =    KBMS_DataCategoryUtil.getCategoryListCountByType(categoryId,articleType);
     return count;
   }    
}