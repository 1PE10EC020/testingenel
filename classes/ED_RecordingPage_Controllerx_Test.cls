@isTest
public class ED_RecordingPage_Controllerx_Test{
	/*
  @testSetup
  	private static void setup() {		
  		case parentCase = new Case ();
  		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.IVR_dn__C = '1233';
    	c.IVR_connid__C = '213412312';
        insert c;
        
        case c2 = new case();
        c2.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c2.TipologiaCase__c ='Da Definire';
        c2.origin = 'Email';
        c2.status='In carico al II Livello';
        c2.DescrizioneStato__c='In Lavorazione';
        c2.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c2.pod__C = 'testPod';
        c2.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c2.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c2.LivelloTensioneDiFornitura__c ='BT';
        c2.livello1__C ='In prelievo';
        c2.livello2__C = 'Già Connesso';
        c2.livello3__C ='MT/AT' ;
        //c2.IVR_dn__C = '1233';
    	c2.IVR_connid__C = '213412312';
        insert c2;
        
        case c3 = new case();
        c3.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c3.TipologiaCase__c ='Da Definire';
        c3.origin = 'Email';
        c3.status='In carico al II Livello';
        c3.DescrizioneStato__c='In Lavorazione';
        c3.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c3.pod__C = 'testPod';
        c3.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c3.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c3.LivelloTensioneDiFornitura__c ='BT';
        c3.livello1__C ='In prelievo';
        c3.livello2__C = 'Già Connesso';
        c3.livello3__C ='MT/AT' ;
        c3.IVR_dn__C = '1233';
    	//c3.IVR_connid__C = '213412312';
        insert c3;
  	}
  	*/
  	@isTest
    static void testStartRecording (){
    	case parentCase = new Case ();
  		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.IVR_dn__C = '1233';
    	c.IVR_connid__C = '213412312';
        insert c;
        
        case cDN = new case();
        cDN.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cDN.TipologiaCase__c ='Da Definire';
        cDN.origin = 'Email';
        cDN.status='In carico al II Livello';
        cDN.DescrizioneStato__c='In Lavorazione';
        cDN.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cDN.pod__C = 'testPod';
        cDN.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cDN.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cDN.LivelloTensioneDiFornitura__c ='BT';
        cDN.livello1__C ='In prelievo';
        cDN.livello2__C = 'Già Connesso';
        cDN.livello3__C ='MT/AT' ;
        //c2.IVR_dn__C = '1233';
    	cDN.IVR_connid__C = '213412312';
        insert cDN;
        
        case cConn = new case();
        cConn.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cConn.TipologiaCase__c ='Da Definire';
        cConn.origin = 'Email';
        cConn.status='In carico al II Livello';
        cConn.DescrizioneStato__c='In Lavorazione';
        cConn.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cConn.pod__C = 'testPod';
        cConn.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cConn.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cConn.LivelloTensioneDiFornitura__c ='BT';
        cConn.livello1__C ='In prelievo';
        cConn.livello2__C = 'Già Connesso';
        cConn.livello3__C ='MT/AT' ;
        cConn.IVR_dn__C = '1233';
    	//c3.IVR_connid__C = '213412312';
        insert cConn;
    	RegistrazioneService.StartRegistrazioneResponse_element  expetedResponse = new RegistrazioneService.StartRegistrazioneResponse_element ();
    	expetedResponse.CodiceEsito ='0';
    	expetedResponse.DescrizioneEsito='';
    	expetedResponse.IDRegistrazione ='123';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_START_Moch(expetedResponse));  
    	 c = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C != null)  limit 1 ];
    	 cDN = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C = null AND IVR_connid__C != null)  limit 1 ];
    	 cConn = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C = null)  limit 1 ];
    	
    	// set the mandatory field
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
    	Test.startTest();
    	
    	ED_RecordingPage_Controllerx controller = new ED_RecordingPage_Controllerx(sc);
    	controller.startRec();
    	//system.assertEquals(c.RecordingID__c,'123');
    	
    	// simulate a KO response
    	expetedResponse.CodiceEsito ='1';
    	expetedResponse.DescrizioneEsito='Generic Error';
    	expetedResponse.IDRegistrazione ='';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_START_Moch(expetedResponse));  
    	sc = new ApexPages.StandardController(c);
    	controller = new ED_RecordingPage_Controllerx(sc);
    	controller.startRec();
    	// simulate a KO response  --> recording id is no updated
    	//system.assertEquals(c.RecordingID__c,'123');
    	
    	
    	
    	// blank 1 mandatory field ( DN )
    	expetedResponse.CodiceEsito ='1';
    	expetedResponse.DescrizioneEsito='Generic Error';
    	expetedResponse.IDRegistrazione ='';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_START_Moch(expetedResponse));  
    	sc = new ApexPages.StandardController(cdn);
    	controller = new ED_RecordingPage_Controllerx(sc);
    	controller.startRec();
    	// simulate a KO response  --> recording id is no updated
    	system.assertEquals(cdn.RecordingID__c,null);
    	
    	// blank 1 mandatory field (CONNID)
    	expetedResponse.CodiceEsito ='1';
    	expetedResponse.DescrizioneEsito='Generic Error';
    	expetedResponse.IDRegistrazione ='';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_START_Moch(expetedResponse));  
    	sc = new ApexPages.StandardController(cConn);
    	controller = new ED_RecordingPage_Controllerx(sc);
    	controller.startRec();
    	// simulate a KO response  --> recording id is no updated
    	system.assertEquals(cConn.RecordingID__c,null);
    	Test.stopTest();
    }
    
    @isTest
    static void testStopRecording (){
    	case parentCase = new Case ();
  		case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'testPod';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.IVR_dn__C = '1233';
    	c.IVR_connid__C = '213412312';
        insert c;
        
        case cDN = new case();
        cDN.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cDN.TipologiaCase__c ='Da Definire';
        cDN.origin = 'Email';
        cDN.status='In carico al II Livello';
        cDN.DescrizioneStato__c='In Lavorazione';
        cDN.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cDN.pod__C = 'testPod';
        cDN.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cDN.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cDN.LivelloTensioneDiFornitura__c ='BT';
        cDN.livello1__C ='In prelievo';
        cDN.livello2__C = 'Già Connesso';
        cDN.livello3__C ='MT/AT' ;
        //c2.IVR_dn__C = '1233';
    	cDN.IVR_connid__C = '213412312';
        insert cDN;
        
        case cConn = new case();
        cConn.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        cConn.TipologiaCase__c ='Da Definire';
        cConn.origin = 'Email';
        cConn.status='In carico al II Livello';
        cConn.DescrizioneStato__c='In Lavorazione';
        cConn.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        cConn.pod__C = 'testPod';
        cConn.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        cConn.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        cConn.LivelloTensioneDiFornitura__c ='BT';
        cConn.livello1__C ='In prelievo';
        cConn.livello2__C = 'Già Connesso';
        cConn.livello3__C ='MT/AT' ;
        cConn.IVR_dn__C = '1233';
    	//c3.IVR_connid__C = '213412312';
        insert cConn;
   		RegistrazioneService.StopRegistrazioneResponse_element expetedResponse = new  RegistrazioneService.StopRegistrazioneResponse_element();
    	expetedResponse.CodiceEsito ='0';
    	expetedResponse.DescrizioneEsito='';
    	expetedResponse.IDRegistrazione ='';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_STOP_Moch(expetedResponse));  
    	 c = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C != null)  limit 1 ];
    	 cDN = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C = null AND IVR_connid__C != null)  limit 1 ];
    	 cConn = [select id,IsRecordingExist__c,CaseNumber,IVR_ConnID__c,IVR_DN__c,IVR_Tenant__c,TelefonoInputIVR__c,CodiceFiscaleRichiedente__c,pod__c,IdPratica__c,CodicePercorsoTelefonico__c,RecordingID__c from case where (IVR_dn__C != null AND IVR_connid__C = null)  limit 1 ];
    	ApexPages.StandardController sc = new ApexPages.StandardController(c);
    	Test.startTest();
    	
    	ED_RecordingPage_Controllerx controller = new ED_RecordingPage_Controllerx(sc);
    	controller.endRec();

		// simulate KO Response
		expetedResponse.CodiceEsito ='1';
    	expetedResponse.DescrizioneEsito='generic error';
    	expetedResponse.IDRegistrazione ='';
    	Test.setMock(WebServiceMock.class, new RegistrazioneService_STOP_Moch(expetedResponse));  
    	controller.endRec();
    	
    	// call the end without dn parameter
    	sc = new ApexPages.StandardController(cDN);
    	controller = new ED_RecordingPage_Controllerx(sc);
    	controller.endRec();
    	// simulate a KO response  --> recording id is no updated
    	system.assertEquals(cConn.RecordingID__c,null);
    	test.stopTest();
    }
}