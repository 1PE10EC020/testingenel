public without sharing class CodiceFiscale_Helper {
	
  
  	/* Fiscal Code Check Message */
  	//private final static String fiscalCodeNotValid = 'Attenzione il Codice Fiscale inserito non è valido.';
  	//private final static String fiscalCodeLenght = 'Attenzione la lunghezza del Codice Fiscale non è corretta.';
	//private final static String fiscalCodeName = 'Le prime 6 lettere del codice fiscale non corrispondono al Nome/Cognome inserito.';
	//private final static String fiscalCodeCommune = 'il codice fiscale non presenta un Comune valido.';

  	
  	/*Fiscal Code check Pattern*/
  	/*
    *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
    *@created 2015/06/22
    *@Description : pattern to use into an regular expression in order to check fiscal code valodity
    */
   
  	
  	/*
    *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
    *@created 2015/06/22
    *@Description : this method caluclate the first fiscal's code letters.
    *@input :   Name 					Type 						description
    *			Nome					String						account first name
    *			Cognome					String						account last name
    *@output:
    *			  - 					String 						first fiscal's code 6 digit
    */ 	
  public static String calcNomeCognomeCF(String Nome, String Cognome){
	   Nome = Nome.toUpperCase(); //[MM - 20140416 - merged]
  	   Cognome = Cognome.toUpperCase();//[MM - 20140416 - merged]
       String s1 = '';
       String cod_cognome = '';
       String cod_nome = '';
       Integer i=0;
       String c='';
       Integer cont=0;
        
        //prima parte calcolo delle 3 lettere cognome
       s1 = Cognome;
        s1 = s1.trim();
        s1 = s1.replace(' ', '');
        s1 = s1.replace('\'', '');
        s1 = s1.replace('à', 'A');
        s1 = s1.replace('è', 'E');
        s1 = s1.replace('é', 'E');
        s1 = s1.replace('ì', 'I');
        s1 = s1.replace('ò', 'O');
        s1 = s1.replace('ù', 'U');
        s1 = s1.toUpperCase();
       
    String buf = '';
        
       for (i = 0; i < s1.length(); i++) {
            c = s1.substring(i,i+1);
            if ((c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U') && (buf.length() < 3)) {
              buf = buf + c;
            }
        }
        
    if (buf.length() < 3) {
            for (i = 0; i < s1.length(); i++) {
                c = s1.substring(i,i+1);
                if ((c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') && buf.length() < 3) {
                  buf = buf + c;
                }
            }
        }
        cod_cognome = buf;
        
        //nel caso in cui il cognome è composto da due o meno lettere metto la X
        if (cod_cognome.length() < 3) {
            if (cod_cognome.length() == 2) {
                cod_cognome += 'X';
            }
            if (cod_cognome.length() == 1) {
                cod_cognome += 'XX';
            }
        }
        
        //inizio calcolo tre lettere del nome
        s1 = Nome;
        s1 = s1.trim();
        s1 = s1.replace(' ', '');
        s1 = s1.replace('\'', '');
        s1 = s1.replace('à', 'A');
        s1 = s1.replace('è', 'E');
        s1 = s1.replace('é', 'E');
        s1 = s1.replace('ì', 'I');
        s1 = s1.replace('ò', 'O');
        s1 = s1.replace('ù', 'U');
        s1 = s1.toUpperCase();

        String buf2 = '';
        
        //calcolo del numero di consonanti presenti nel nome
        for (i = 0; i < s1.length(); i++) {
                c = s1.substring(i,i+1);
                if ((c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U')) {
                    cont++;
                }
        }
        
        if(cont > 3){
          for (i = 0; i < s1.length(); i++) {
                c = s1.substring(i,i+1);
                if ((c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U') && buf2.length() < 4) {
                    buf2 = buf2 + c;
                }
          }
          buf2 = buf2.substring(0,1) + buf2.substring(2,4);
        }
        else {//(cont <= 3)
          for (i = 0; i < s1.length(); i++) {
                c = s1.substring(i,i+1);
                if ((c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U') && buf2.length() < 3) {
                    buf2 = buf2 + c;
                }
          }
        }
        
        if (buf2.length() < 3) {
            for (i = 0; i < s1.length(); i++) {
                c = s1.substring(i,i+1);
                if ((c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') && buf2.length() < 3) {
                    buf2 = buf2 + c;
                }
            }
        }
        
    cod_nome = buf2;
        
        //nel caso in cui il nome è composto da due o meno lettere metto la X
        if (cod_nome.length() < 3) {
            if (cod_nome.length() == 2) {
                cod_nome += 'X';
            }
            if (cod_nome.length() == 1) {
                cod_nome += 'XX';
            }
        }
        
        System.debug('Nome: '+Nome+' - Cognome: '+Cognome+' - calcolo: '+cod_cognome+cod_nome);
        return cod_cognome+cod_nome;
     }
   
   
   
   
     // custom exception 
     public class FiscalCodeException extends Exception{}
     
     /*
    *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
    *@created 2015/06/22
    *@Description : this method calculate the control character from the fiscal code
    *@input :   Name 					Type 						description
    *			codFisc					String						fiscal code 
    *@output:
    *			  - 					string 						fiscal's code control char
    */ 	
     public static String calcControlCharCF(String codFisc){
     	codFisc = codFisc.toUpperCase();
     	String CF = codFisc.substring(0,15);
     	
     	//creazione mappa caratteri dispari per calcolo carattere di controllo
		map<String, integer> mapDispari = new map<String, integer>{'0' => 1, '1' => 0, '2' => 5, '3' => 7, '4' => 9, '5' => 13, '6' => 15, 
		'7' => 17, '8' => 19, '9' => 21, 'A' => 1, 'B' => 0, 'C' => 5, 'D' => 7, 'E' => 9, 'F' => 13, 'G' => 15, 'H' => 17, 'I' => 19, 'J' => 21, 
		'K' => 2, 'L' => 4, 'M' => 18, 'N' => 20, 'O' => 11, 'P' => 3, 'Q' => 6, 'R' => 8, 'S' => 12, 'T' => 14, 'U' => 16, 'V' => 10, 'W' => 22, 
		'X' => 25, 'Y' => 24, 'Z' => 23};
		
		//creazione mappa caratteri pari per calcolo carattere di controllo
		map<String, integer> mapPari = new map<String, integer>{'0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, 
		'6' => 6, '7' => 7, '8' => 8, '9' => 9, 'A' => 0, 'B' => 1, 'C' => 2, 'D' => 3, 'E' => 4, 'F' => 5, 'G' => 6, 'H' => 7, 
		'I' => 8, 'J' => 9, 'K' => 10, 'L' => 11, 'M' => 12, 'N' => 13, 'O' => 14, 'P' => 15, 'Q' => 16, 'R' => 17, 'S' => 18, 
		'T' => 19, 'U' => 20, 'V' => 21, 'W' => 22, 'X' => 23, 'Y' => 24, 'Z' => 25};

		//tabella mapping resto per calcolo carattere di controllo
		map<integer, String> mapResto = new map<integer, String>{0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E', 
		5 => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J', 10 => 'K', 11 => 'L', 12 => 'M', 13 => 'N', 14 => 'O', 
		15 => 'P', 16 => 'Q', 17 => 'R', 18 => 'S', 19 => 'T', 20 => 'U', 21 => 'V', 22 => 'W', 23 => 'X', 24 => 'Y', 25 => 'Z'};
     	
     	//calcolo del carattere ci controllo (si veda classe CheckSemanticoCF)
		List<String> pari = new List<String>();
		List<String> dispari = new List<String>();
		integer controlValue = 0;
		for(integer i = 0; i < CF.length(); i++)
		{
			
			integer valueToAdd = 0;
			if(math.mod((i+1), 2) == 0)
			{
				if(mapPari.containsKey(CF.substring(i, i+1)))
					valueToAdd = mapPari.get(CF.substring(i, i+1));
			}
			else
			{
				if(mapDispari.containsKey(CF.substring(i, i+1)))
					valueToAdd = mapDispari.get(CF.substring(i, i+1));
			}
			controlValue = controlValue + valueToAdd;
			
		}
		return mapResto.get(math.mod(controlValue, 26));
     }
     
      /*
    *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
    *@created 2015/06/22
    *@Description : this method verify the control character
    *@input :   Name 					Type 						description
    *			codFisc					String						fiscal code 
    *@output:
    *			  - 					bolean 						true -> the control char is correct, false -> the control char is not correct
    */ 	
     public static boolean checkControlCharCF(String codFisc){
	     	if(String.isBlank(codFisc) || !Pattern.matches(Constants.FISCAL_CODE_REGEX, codFisc)){
	     		if(!String.isBlank(codFisc) && codFisc.length() == 16){
	     			if(isNotOmocodico(codFisc)){
	     				system.debug('isNotOmocodico');
	     				return false;
	     			}
	     			else{
	     				String tmpCodFisc = CodifyOmocodica(codFisc);
	     				if(Constants.CONSTANT_KO.equals(tmpCodFisc)){
	     					system.debug('CodifyOmocodica return KO');
	     					return false;
	     				}
	     				else{
	     					system.debug('TUTTO OK');
	     					codFisc = tmpCodFisc;
	     				}
	     			}
	     		}
	     		else{
	     			return false;
	     		}
	     	}

     	codFisc = codFisc.toUpperCase();
 
     	String controlCharCF = CodiceFiscale_Helper.calcControlCharCF(codFisc);
     	System.debug('checkControlCharCF: '+controlCharCF+'='+codFisc.substring(15,16));
		if(controlCharCF.equals(codFisc.substring(15,16)))
			return true;
		else
			return false;
     }
     
     /*
    *CheckFiscalCode - check on Fiscal Code
    *Parameter cf is a string fiscal code
    *Parameter firstName is a first name to be verified
    *Parameter lastName is a last name to be verified
    *Parameter Birthdate to verify the age of majority
    *return a String status
    */
    
    public static String CheckFiscalCode(String cf, String firstName, String lastName)
    {
    	if(!String.isBlank(cf) && !String.isBlank(firstName) && !String.isBlank(lastName))
    	{
    		String codFisc = cf.toUpperCase();
    		String fName = firstName.toUpperCase();
    		String lName = lastName.toUpperCase();
	    	//Occurs if the fiscal code is 16 characters
	    	if(codFisc.length() < 16 || codFisc.length() > 16)
	    		return Label.CF_Lenght;//fiscalCodeLenght;
	    	
	    	//Occurs if the fiscal code has a pattern correct
	    	/*
            *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
            *@created 2014/09/08
            */
	    	if(String.isBlank(codFisc) || !Pattern.matches(Constants.PATTERN_FISCAL_CODE, codFisc)) 
	    		return Label.CF_Not_Valid;//fiscalCodeNotValid;//this check is made in checkCodeComuneCF and checkControlCharCF methods too.
	    	/* END*/
	    	
	    	//Check the first 6 letters the fiscal code with the first and last name
	    	if(!checkNomeCognomeCF(codFisc, CodiceFiscale_Helper.calcNomeCognomeCF(fName, lName)))
			{
				return Label.CF_First_6_Letter_Not_Valid;//fiscalCodeName;
			}
	    	//Check the last character the fiscal code
			if(!checkControlCharCF(codFisc))
			{
	            return Label.CF_Not_Valid;//fiscalCodeNotValid;
	        }
	       
	    	return Constants.CONSTANT_OK;  
    	}
    	else
    		return Label.CF_Not_Valid;//fiscalCodeNotValid;
    }
    
    /*Start:
    *@author Giorgio,Peresempio <giorgio.peresempio@ccenture.com> 
    *@created 2014/09/08
    */
    
     public static String CheckFiscalCode(String cf)
    {
    	if(!String.isBlank(cf))
    	{
    		String codFisc = cf.toUpperCase();
    		
	    	//Occurs if the fiscal code is 16 characters
	    	if(codFisc.length() < 16 || codFisc.length() > 16)
	    		return Label.CF_Lenght;//fiscalCodeLenght;
	    	
	    	//Occurs if the fiscal code has a pattern correct
	    	
	    	if(String.isBlank(codFisc) || !Pattern.matches(Constants.PATTERN_FISCAL_CODE, codFisc)) 
	    		return Label.CF_Not_Valid;//fiscalCodeNotValid;
	    	
	    	
	    	//Check the last character the fiscal code
			if(!checkControlCharCF(codFisc))
			{
	            return Label.CF_Not_Valid;//fiscalCodeNotValid;
	        }
	        //verify the age of majority
	        /*
	        Date Birthdate = returnBirthDate(cf);
	        if(Birthdate != null)
	        {
	    		if(Date.today() < Birthdate.addYears(18))
	    			return majorityAge;
	        }
	        */
	    	return Constants.CONSTANT_OK; 
    	}
    	else
    		return Label.CF_Not_Valid;//fiscalCodeNotValid;
    }
    /* END*/
    
    
    /*
    *checkNomeCognomeCF - compares the first 6 letters the fiscal code with the first 6 letters calculated by first and last name
    *Parameter cf is a string fiscal code
    *Parameter cfcalc is 6 letters calculated by first and last name
    *return true if comparision is ok
    */
    public static Boolean checkNomeCognomeCF(String cf, String cfcalc)
    {
        if (cf != null)
        { 
	        String firstLettersCF = (cf.substring(0, 6)).toUpperCase();
	        System.debug('firstLettersCF: '+firstLettersCF+' equals to: '+cfcalc);
	        
	        if (firstLettersCF.equals(cfcalc))
	        	return true;
	        else
	        	return false;
        }
        else
        	return false;                           
    }
    
    public static boolean isNotOmocodico(String CF){
    	String subCF = CF.substring(6,8)+CF.substring(9,11)+CF.substring(12,15);
    	return subCF.isNumeric();
    	
    }
    
    public static String CodifyOmocodica(String CF){
    	system.debug('CodifyOmocodica');
    	CF = CF.toUpperCase();
     	String controlCharCF = CodiceFiscale_Helper.calcControlCharCF(CF);
     	System.debug('checkControlCharCF: '+controlCharCF+'='+CF.substring(15,16));
		if(!controlCharCF.equals(CF.substring(15,16)))
			return Constants.CONSTANT_KO; 
    	String subCF3 = CF.substring(12,15);
    	String subCF2 = CF.substring(9,11);
    	String subCF1 = CF.substring(6,8);
    	String NewCF;
    	if(!subCF3.isNumeric()){
    		subCF3 = substituteNumber(subCF3);
    	}
    	if(!subCF2.isNumeric()){
    		subCF2 = substituteNumber(subCF2);
    	}
    	if(!subCF1.isNumeric()){
    		subCF1 = substituteNumber(subCF1);
    	}
    	
    	if(!subCF1.equals(Constants.CONSTANT_KO) && !subCF2.equals(Constants.CONSTANT_KO) && !subCF3.equals(Constants.CONSTANT_KO) ){  
    		system.debug('CodifyOmocodica valore subCF3 = ' +subCF3);
    		system.debug('CodifyOmocodica valore subCF2 = ' +subCF2);
    		system.debug('CodifyOmocodica valore subCF1 = ' +subCF1);
			NewCF = CF.substring(0,6)+subCF1+CF.substring(8,9)+subCF2+CF.substring(11,12)+subCF3;
			NewCF = NewCF + calcControlCharCF(NewCF);
			system.debug('CodifyOmocodica valore NewCF = ' +NewCF);
			if(!Pattern.matches(Constants.FISCAL_CODE_REGEX, NewCF))
				return Constants.CONSTANT_KO; 
			else
				return NewCF;
    	}
    	else{
    		return Constants.CONSTANT_KO; 
    	}
		
    }
    
    public static String substituteNumber(String SubCF){
    	String charTemp;
    	map<String, String> mapOmocodia = new map<String, String>{ 
    		'L' => '0', 'M' => '1', 'N' => '2', 'P' => '3', 'Q' => '4',
    		'R' => '5', 'S' => '6', 'T' => '7', 'U' => '8', 'V' => '9'
    	};
    	
    	for(Integer i = 0; i <= SubCF.length()-1 ; i++){
    		charTemp = SubCF.substring(i,i+1);
			if(!charTemp.isNumeric()){
				if(mapOmocodia.containsKey(charTemp))
					SubCF = SubCF.replace(charTemp,mapOmocodia.get(charTemp));
				else{
					system.debug('substituteNumber valore non presente');
					return Constants.CONSTANT_KO;  
				}
			}
    	}
    	return SubCF;	
    }
 
}