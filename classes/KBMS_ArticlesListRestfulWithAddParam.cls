@RestResource(urlMapping='/getArticlesListWithAddParam/*')
global with sharing class KBMS_ArticlesListRestfulWithAddParam
{
    @HttpGet
    global static List<ArtcileWrapper> getArticlesList()
    {
        String articleType = RestContext.request.params.get('articleType');
        String limits = RestContext.request.params.get('limit');
        String froms = RestContext.request.params.get('from');
        
        //limit, from, listCategory, articleType, orderBy, textSearch
        //Additional Parameters
        String listCategory = RestContext.request.params.get('listCategory');
        String orderBy =RestContext.request.params.get('orderBy');
        String searchstring =RestContext.request.params.get('textSearch'); 
        searchstring = searchstring == null?'':searchstring;
        
        List<String> searchStringList = new List<String>();
        if(searchstring != NULL && searchstring != '')
    	searchStringList = searchstring.split(' ');
        

        System.debug('searchStringList : '+searchStringList);        
        List<ArtcileWrapper> artcileWrappers = new List<ArtcileWrapper>();

        if(orderBy.equals('LastModifiedDate')){
            orderBy = 'LastModifiedDate';
        }else if(orderBy.equals('Alphabetic')){
            orderBy = 'Title';
        }
        
        
        
        String categoryList = '(';
        //Parsing Data Category JSON String
        JSONParser parser = JSON.createParser(listCategory);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) { 
                while (parser.nextToken() != null) {
                  if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                  dataCategories le = (dataCategories)parser.readValueAs(dataCategories.class);
                      categoryList = categoryList+le.categoryName+'__c,'; 
                  }
                  //categoryList = categoryList+',';  
                 }
                categoryList =categoryList.substring(0, categoryList.length()-1);
                categoryList = categoryList+')';
            }
            
        }
        System.debug('JSON'+categoryList);
        listCategory = categoryList;
        Integer noLimits = limits == null ? 0 : Integer.valueOf(limits);
        Integer offsets = froms == null ? 0 : Integer.valueOf(froms );
        //Organization orgDetails = [SELECT Id, LanguageLocaleKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
        String language = Label.KBMS_Language;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        
        if(orderBy.equals('Title') || orderBy.equals('LastModifiedDate')){
        //language = String.valueOf(orgDetails.get('LanguageLocaleKey'));
        String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav';
        String secondPartQuery;
        if(orderBy.equals('Title')){
            if(searchStringList.size()>1)
            {
                secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:language ';
            	for(String eachSearchString : searchStringList)
                {
                    secondPartQuery += ' AND (Title LIKE \'%' +eachSearchString+'%\' OR Summary LIKE \'%'+ eachSearchString+'%\') ';
                }
                secondPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +' BELOW '+listCategory+' AND KBMS__c AT (OT__c) ORDER BY '+orderBy;
            }else
            {
                //Now Used defaultDataCategory Custom label in Query
                secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\')  WITH DATA CATEGORY ' + defaultDataCategory +' BELOW '+listCategory+' AND KBMS__c AT (OT__c) ORDER BY '+orderBy;
 
            }
                    
        }
        else if(orderBy.equals('LastModifiedDate')){
            
            if(searchStringList.size()>1)
            {
                secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:language ';
            	for(String eachSearchString : searchStringList)
                {
                    secondPartQuery += ' AND (Title LIKE \'%' +eachSearchString+'%\' OR Summary LIKE \'%'+ eachSearchString+'%\') ';
                }
                secondPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +' BELOW '+listCategory+' AND KBMS__c AT (OT__c) ORDER BY '+orderBy+' DESC';
            }
            else
            {
                //Now Used defaultDataCategory Custom label in Query
                secondPartQuery = ' WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\')  WITH DATA CATEGORY ' + defaultDataCategory +' BELOW '+listCategory+' AND KBMS__c AT (OT__c) ORDER BY '+orderBy+' DESC';
            }
                    
        }
        String thirdPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        String query = firstPartQuery + secondPartQuery + thirdPartQuery;
        system.debug(query);
        List<sObject> knwoledgesObject = Database.query(query);
        Set<Id> parentArticleId = new Set<Id>();
        
        
        Set<String> kavIds = new Set<String>();
        
        Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
        for (sObject sObjKAV  : knwoledgesObject)
        {
            kavIds.add(String.valueOf(sObjKAV.get('Id')));
            parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
        }
        //VoteStat
        Map<String, Double> NormalizedScore = new  Map<String, Double>();
        firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
        secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId'; 
        thirdPartQuery = ' ORDER BY NormalizedScore DESC NULLS LAST ';
            
        
        query = firstPartQuery+secondPartQuery+thirdPartQuery;
        List<sObject> voteStatusObject = Database.query(query);
        for(sObject s : voteStatusObject){
            NormalizedScore.put(String.valueOf(s.get('ParentId')), Double.valueOf(s.get('NormalizedScore')));
        }
        //ViewStat
         Map<String, Integer> ViewCount = new  Map<String, Integer>();
        firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
        secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId';
        //thirdPartQuery = ' (SELECT KnowledgeArticleId FROM faq__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+')';
        thirdPartQuery = ' ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
        //String fourthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        query = firstPartQuery+secondPartQuery+thirdPartQuery;
        List<sObject> viewStatusObject = Database.query(query);
        for(sObject s : viewStatusObject){
            ViewCount.put(String.valueOf(s.get('ParentId')), Integer.valueOf(s.get('ViewCount')));
        }
        
        firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
        secondPartQuery = ' WHERE ParentId In: kavIds';
        query = firstPartQuery + secondPartQuery;
        
        List<sObject> dataCategoryObject = Database.query(query);
        System.debug('-->'+dataCategoryObject );
        
        Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
        for (sObject sObjDatCat  : dataCategoryObject)
        {
            if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))=='KBMS'){
                
            }
            if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c')){
                sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
            }
        }
      
        for (sObject sObjKAV  : knwoledgesObject)
        {
            ArtcileWrapper aw = new ArtcileWrapper();
           // aw.dataCategoryList = categoryList;
            aw.articles = sObjKAV;
            aw.likeScore = NormalizedScore.get(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
            aw.dataCategoryName = sObjectDataCategoryMaps.get(aw.articles.Id);
           
                                aw.viewCount = ViewCount.get(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
                            if(aw.dataCategoryName != null && aw.dataCategoryName != ''){
                                
                                List<String> parent = new List<String>();
                                parent = KBMS_DataCategoryUtil.getParentCategory(aw.dataCategoryName);
                                if(parent != null){
                                    aw.dataCategoryLabel= parent[0];
                                    aw.parentName = parent[1];
                                    aw.parentLabel = parent[2];
                                    
                                    artcileWrappers.add(aw);
                                
                            }
        }
        }
           
        }else if(orderBy.equals('Like')){
            
        String firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
        String secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In '; 
        String thirdPartQuery='';
            if(searchStringList.size()>1){
            	thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language ';
                for(String eachSearchString : searchStringList){
                    thirdPartQuery += ' AND (Title LIKE \'%' +eachSearchString+'%\' OR Summary LIKE \'%'+ eachSearchString+'%\') ';
                }
                thirdPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) )';
            }else
            {
               thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) )'; 
        	}
        String fourthPartQuery = ' ORDER BY NormalizedScore DESC NULLS LAST ';
        String fifthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        String query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery ;
        
        List<sObject> voteStatObjects = Database.query(query);
        
        Set<String> artcilesIds = new Set<String>();
        
        for (sObject sObjVS : voteStatObjects)
        {
            artcilesIds.add(String.valueOf(sObjVS.get('ParentId')));
        }
        
            
        
        firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav ';
        secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language  WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory;
        //thirdPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets';
        query = firstPartQuery + secondPartQuery;
        
        List<sObject> knwoledgesObject = Database.query(query);
        Set<Id> parentArticleId = new Set<Id>();
        Set<String> kavIds = new Set<String>();
        
        Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
        for (sObject sObjKAV  : knwoledgesObject)
        {
            sObjectKAVMaps.put(String.valueOf(sObjKAV.get('KnowledgeArticleId')), sObjKAV);
            kavIds.add(String.valueOf(sObjKAV.get('Id')));
             parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
        }
            //ViewStat
         Map<String, Integer> ViewCount = new  Map<String, Integer>();
        firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
        secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId';
        //thirdPartQuery = ' (SELECT KnowledgeArticleId FROM faq__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+')';
        thirdPartQuery = ' ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
        //String fourthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        query = firstPartQuery+secondPartQuery+thirdPartQuery;
        List<sObject> viewStatusObject = Database.query(query);
        for(sObject s : viewStatusObject){
            ViewCount.put(String.valueOf(s.get('ParentId')), Integer.valueOf(s.get('ViewCount')));
        }
        
        firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
        secondPartQuery = ' WHERE ParentId In: kavIds ';
        query = firstPartQuery + secondPartQuery;
        
        List<sObject> dataCategoryObject = Database.query(query);
        System.debug('-->'+dataCategoryObject );
        
        Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
        for (sObject sObjDatCat  : dataCategoryObject)
        {
            if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c')){
                sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
            }
        }
        
        
        for (sObject sObjVS : voteStatObjects)
        {
            ArtcileWrapper aw =  new ArtcileWrapper();
            aw.likeScore= double.valueOf(sObjVS.get('NormalizedScore'));
            aw.articles = sObjectKAVMaps.get(String.valueOf(sObjVS.get('ParentId')));
            //sObject a = aw.articles;
            if(aw.articles!=null){
                aw.dataCategoryName = sObjectDataCategoryMaps.get(aw.articles.Id);
                aw.viewCount = ViewCount.get(String.valueOf(sObjVS.get('ParentId'))); 
            if(aw.dataCategoryName != null && aw.dataCategoryName != '' ){
                    List<String> parent = new List<String>();
                    parent = KBMS_DataCategoryUtil.getParentCategory(aw.dataCategoryName);
                    if(parent != null){
                        aw.dataCategoryLabel= parent[0];
                        aw.parentName = parent[1];
                        aw.parentLabel = parent[2];
                        artcileWrappers.add(aw);
                    }
                }
            }
        }
        }else if(orderBy.equals('View')){
            
            String firstPartQuery = 'SELECT ParentId, ViewCount, NormalizedScore FROM ' + articleType+'__ViewStat ';
        String secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In ';
        //Now Using 'articleType' Parameter instead of Explicit reference to an article type: "FROM faq__kav"
        String thirdPartQuery='';
            if(searchStringList.size()>1){
            	thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language ';
                for(String eachSearchString : searchStringList){
                    thirdPartQuery += ' AND (Title LIKE \'%' +eachSearchString+'%\' OR Summary LIKE \'%'+ eachSearchString+'%\') ';
                }
                thirdPartQuery += ' WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) )';
            }else
            {
               thirdPartQuery  = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) )'; 
        	}
        
        //String thirdPartQuery = ' (SELECT KnowledgeArticleId FROM ' + articleType+'__kav WHERE PublishStatus = \'online\' AND Language =:language AND (Title LIKE \'%' +searchstring+'%\' OR Summary LIKE \'%'+ searchString+'%\') WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory+' AND KBMS__c AT (OT__c) )';
        String fourthPartQuery = ' ORDER BY ViewCount DESC NULLS LAST, NormalizedScore DESC NULLS LAST ';
        String fifthPartQuery = ' LIMIT ' + noLimits + ' OFFSET ' + offsets;
        
        String query = firstPartQuery + secondPartQuery + thirdPartQuery+fourthPartQuery+fifthPartQuery ;
        
        List<sObject> voteStatObjects = Database.query(query);
        
        Set<String> artcilesIds = new Set<String>();
        Set<Id> parentArticleId = new Set<Id>();
            
        for (sObject sObjVS : voteStatObjects)
        {
            artcilesIds.add(String.valueOf(sObjVS.get('ParentId')));
        }
        
        firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM ' + articleType+'__kav ';
        secondPartQuery = ' WHERE KnowledgeArticleId In: artcilesIds AND PublishStatus = \'online\' AND Language =:language WITH DATA CATEGORY ' + defaultDataCategory +'  BELOW '+listCategory;
        query = firstPartQuery + secondPartQuery;
        
        List<sObject> knwoledgesObject = Database.query(query);
        
        Set<String> kavIds = new Set<String>();
        
        Map<String, sObject> sObjectKAVMaps = new  Map<String, sObject>();
        for (sObject sObjKAV  : knwoledgesObject)
        {
            sObjectKAVMaps.put(String.valueOf(sObjKAV.get('KnowledgeArticleId')), sObjKAV);
            kavIds.add(String.valueOf(sObjKAV.get('Id')));
            parentArticleId.add(String.valueOf(sObjKAV.get('KnowledgeArticleId')));
        }
        //VoteStat
        Map<String, Double> NormalizedScore = new  Map<String, Double>();
        firstPartQuery = 'SELECT ParentId, NormalizedScore FROM ' + articleType+'__VoteStat ';
        secondPartQuery = ' WHERE Channel=\'AllChannels\' and IsDeleted = false and ParentId In : parentArticleId'; 
        thirdPartQuery = ' ORDER BY NormalizedScore DESC NULLS LAST ';
        
        query = firstPartQuery+secondPartQuery+thirdPartQuery;
        List<sObject> voteStatusObject = Database.query(query);
        for(sObject s : voteStatusObject){
            NormalizedScore.put(String.valueOf(s.get('ParentId')), Double.valueOf(s.get('NormalizedScore')));
        }
            
            
        firstPartQuery = 'SELECT DataCategoryGroupName,DataCategoryName, ParentId FROM '+ articleType+'__DataCategorySelection ';
        secondPartQuery = ' WHERE ParentId In: kavIds ';
        query = firstPartQuery + secondPartQuery;
        
        List<sObject> dataCategoryObject = Database.query(query);
        System.debug('-->'+dataCategoryObject );
        
        Map<String, String> sObjectDataCategoryMaps = new  Map<String, String>();
        for (sObject sObjDatCat  : dataCategoryObject)
        {
            if(String.valueOf(sObjDatCat.get('DataCategoryGroupName'))==defaultDataCategory.removeEnd('__c')){
                sObjectDataCategoryMaps.put(String.valueOf(sObjDatCat.get('ParentId')), String.valueOf(sObjDatCat.get('DataCategoryName')));
            }
        }
        
        
        
        for (sObject sObjVS : voteStatObjects)
        {
            ArtcileWrapper aw =  new ArtcileWrapper();
            //aw.count= double.valueOf(sObjVS.get('NormalizedScore'));
            aw.viewCount = Integer.valueOf(sObjVS.get('ViewCount'));
            aw.articles = sObjectKAVMaps.get(String.valueOf(sObjVS.get('ParentId')));
            aw.likeScore = NormalizedScore.get(String.valueOf(sObjVS.get('ParentId')));
            if(aw.articles!=null){
                aw.dataCategoryName = sObjectDataCategoryMaps.get(aw.articles.Id); 
                if(aw.dataCategoryName != null && aw.dataCategoryName != ''){
                    List<String> parent = new List<String>();
                    parent = KBMS_DataCategoryUtil.getParentCategory(aw.dataCategoryName);
                    if(parent != null){
                        aw.dataCategoryLabel= parent[0];
                        aw.parentName = parent[1];
                        aw.parentLabel = parent[2];
                        artcileWrappers.add(aw);
                    }
                }
            }
        }
        }
        return artcileWrappers;
    }
    public class dataCategories
    { 
        public String categoryName{get;set;}
    }
    
    global class ArtcileWrapper
    {
        public sObject articles {get;set;}
        public String parentName{get;set;}
        public String parentLabel{get;set;}
        public String dataCategoryName{get;set;}
        public String dataCategoryLabel{get;set;}
        public Double likeScore{get;set;} 
        public Integer viewCount{get;set;}
       // public Integer articleCount{get; set;}
       // public Integer subCategoryCount{get; set;}
        //public String dataCategoryList{get;set;}
        
    } 
}