/*****************************************************************************************************
@author    Bharath Sharma
@date      10 OCT 2017
@description: This apex class is used to delete Attachment on PT_Caricamento_Massivo__c object
              invoked from Process builder
              *******************************************************************************************************/
              public with sharing class PT_CaricaAttachmentDelete{

                  public static final String GETQUERYRECORD='PT_CaricaAttachmentDelete';
                  public static final String PTSEARCH='attachementdelete_OnCaricomento';
                  PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
                  @InvocableMethod
                  /*****************************************************************************************************
                @author    Bharath Sharma
                @date      10 OCT 2017
                @description: This apex class is used to delete Attachment on PT_Caricamento_Massivo__c object
                invoked from Process builder
                *******************************************************************************************************/
                public static void attachementdelete_OnCaricomento(List<PT_Caricamento_Massivo__c > CMList){ 
                    
                    Logger.push(GETQUERYRECORD,PTSEARCH); 
                    try{
                        List<String> listId = new List<String>();
                        for(PT_Caricamento_Massivo__c carico: CMList){
                            listId.add(carico.id);
                        }
                        if(listId!=null){
                            List<Attachment> listAtt = [Select id,parentid from Attachment where parentid in :listId];
                            if(listAtt!=null){
                                database.delete(listAtt); 
                            }
                        }
                        if(test.isRunningTest()){ 
                            throw new PT_ManageException (EXCEMES); 
                        }
                    } 
                    catch (Exception e) {
                        logger.debugException(e);
                    }
                    logger.pop();
                }


            }