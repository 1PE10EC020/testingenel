/**********************************************************************************************
@Author : Akansha Lakhchaura
@date : 05 July 2017
@description : Test class for PT_AddressRFI_Controller.
**********************************************************************************************/

@isTest(SeeAllData = false)
/************/
private class PT_AddressRFI_ControllerTest{

 PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING TESTS='test23';
  PRIVATE STATIC FINAL INTEGER LMT=1; 

   /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/
@testSetup static void createTestRecords() {
 Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
 User usr = new User(Alias = ALIAS, Email=EMAIL,
  EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
  LocaleSidKey=ENUS, ProfileId = pfl.Id,
  TimeZoneSidKey=TZK,
  UserName=UUNAME);
 database.insert(usr);
}
 /************/    
 private static testMethod void fetchAddressnew(){

  User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);
        User usr=[Select id,name from User LIMIT: lmt];
  System.runAs(usr) {
        /**************/  
        PT_Toponimo__c address = PT_TestDataFactory.fetchAddress();
        PT_AddressRFI_Controller.retriveAddress(address.Comune__c, address.CodCap__c, address.DescrVia1__c, address.CodIstat__c);

        //PT_Toponimo__c addressNull = PT_TestDataFactory.fetchAddress();
        PT_AddressRFI_Controller.retriveAddress(null,null,null,null);

         //System.runAs(runningUser){
           PT_AddressRFI_Controller.retriveAddress(address.Comune__c, address.CodCap__c, address.DescrVia1__c, address.CodIstat__c);
           PT_AddressRFI_Controller.retriveAddress(null,null,null,null);
          //  }
          /*************/       
          Anag_Cap_Istat__c addressRecordCodInstat = PT_TestDataFactory.retrievRecordCodInstat();
          database.insert(addressRecordCodInstat);
          system.assertNotEquals(null, addressRecordCodInstat);
          PT_AddressRFI_Controller.setComuneCodIstatFields(addressRecordCodInstat.Comune__c);

          Anag_Cap_Istat__c addressRecordCodInstatNull = PT_TestDataFactory.retrievRecordCodInstat();
          database.insert(addressRecordCodInstatNull);
          PT_AddressRFI_Controller.setComuneCodIstatFields('ggg');


         //System.runAs(runningUser){
           PT_AddressRFI_Controller.setComuneCodIstatFields('ggg');
           PT_AddressRFI_Controller.setComuneCodIstatFields(addressRecordCodInstat.Comune__c);
        //    }        
        /*************/
        Anag_Cap_Istat__c addressRecordComunePod = PT_TestDataFactory.retrieveRecordComunePod();
        PT_AddressRFI_Controller.setCAPCodIstatFields('test');
        
        Anag_Cap_Istat__c addressRecordComunePodNull = PT_TestDataFactory.retrieveRecordComunePod();
        database.insert(addressRecordComunePodNull);
        PT_AddressRFI_Controller.setCAPCodIstatFields('abc');
         // System.runAs(runningUser){
          PT_AddressRFI_Controller.setCAPCodIstatFields('test');
          PT_AddressRFI_Controller.setCAPCodIstatFields('abc');
         //   }  

         /************/       
         Anag_Cap_Istat__c addressPicklist= PT_TestDataFactory.retrievePicklist();
         database.insert(addressPicklist);
         system.assertNotEquals(null, addressPicklist);
         PT_AddressRFI_Controller.getComumneCodIstatPicklistValues(addressPicklist.COMUNE__c);
         PT_AddressRFI_Controller.getComumneCodIstatPicklistValues('');

       //  System.runAs(runningUser){
        PT_AddressRFI_Controller.getComumneCodIstatPicklistValues(addressPicklist.COMUNE__c);
        //    }

        /************/

        ANAG_CAP_ISTAT__c addressList = PT_TestDataFactory.retrieveList();
        database.insert(addressList);
        PT_AddressRFI_Controller.setCAPFieldValue(addressList.COMUNE__c, addressList.FLG_COMPETENZA_ENEL__c);

        
        addressList.COD_ISTAT__c = 'Test';
        addressList.COMUNE__c='test';
        addressList.Name = 'test';
        
        database.update(addressList);
        PT_AddressRFI_Controller.setCAPFieldValue('', addressList.FLG_COMPETENZA_ENEL__c);
        PT_AddressRFI_Controller.setCAPFieldValue('', '');
        // System.runAs(runningUser){
          PT_AddressRFI_Controller.setCAPFieldValue('', addressList.FLG_COMPETENZA_ENEL__c);
          PT_AddressRFI_Controller.setCAPFieldValue('', '');

          }
        
      }


    }