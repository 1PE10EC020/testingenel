/*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search.
**********************************************************************************************/
@isTest
private class PT_F05_Search_Test {
  PRIVATE STATIC FINAL STRING PT_PD2 = 'PT_PD2';
  PRIVATE STATIC FINAL STRING PT_N01 = 'PT_N01';
  PRIVATE STATIC FINAL STRING SVALUE='S';
  PRIVATE STATIC FINAL STRING TESTS='test23';
  PRIVATE STATIC FINAL INTEGER LMT=1; 
  PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING REPONAME='ReportPrenotatoRichieste_100_10:49:00.csv';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY1 = 'SELECT id,Record_Type_Esteso__c,RecordType.Name,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY2 = 'RecordType.Description,Via_esaz__c,DataRicezioneRichiesta__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY3 = 'IdTipoRichiesta__c,IdDistributore__c,IdRichiestaFour__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY4 = 'IdRichTrader__c,toLabel(Stato__c),toLabel(Sottostato__c),';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY5 = 'DataDecorrenza__c,DataEvasione__c,Pod__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY6 = 'NumPresa__c,Eneltel__c,RagSoc_cl__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY7 = 'Cap_pod__c,Cap_esaz__c,Localita_esaz__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY8 = 'Provincia_esaz__c,IdConnessione__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY9 = 'IdUsoEnergia__c,Localita_pod__c,DescrDispaccEstesa__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY10 = 'Via_pod__c,Numero_Civico_pod__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY11 = 'CodFisc_cl__c,Nome_cl__c,Cognome_cl__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY12 = 'Provincia_pod__c,Name,DataNonEsegPrimaDel__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY13 = 'IdDispacc__c,MatricolaMisAtt__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY14 = 'MandConnessione__c,FlagCurveCarico__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY15 = 'Intestatario_pr__c,CodFisc_cl_pr__c,Att_F1_Mis__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY16= 'PartitaIVA_cl_pr__c,Piva_cl__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY17 = 'IdTipoRichAEEG__c,Tensione__c,';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY18 = 'PotDisp__c,NumCivico_esaz__c';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY19 = ' from PT_Richiesta__c' + ' where' + ' ' + 'Id!=null';
  PRIVATE STATIC FINAL STRING ADDRESSQUERTY = ADDRESSQUERTY1 + ADDRESSQUERTY2 + 
  ADDRESSQUERTY3 +ADDRESSQUERTY4 +
  ADDRESSQUERTY6 + ADDRESSQUERTY7 + 
  ADDRESSQUERTY8 + ADDRESSQUERTY9+
  ADDRESSQUERTY10+ ADDRESSQUERTY11+
  ADDRESSQUERTY12+ADDRESSQUERTY13+
  ADDRESSQUERTY15+ADDRESSQUERTY16+
  ADDRESSQUERTY17+ADDRESSQUERTY19+
  ADDRESSQUERTY18+ADDRESSQUERTY14;
   /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/
@testSetup static void createTestRecords() {
  list<PT_Richiesta__c > lstInsertRequest = new list<PT_Richiesta__c >();
  PT_Richiesta__c request = PT_TestDataFactory.createRequestF05();
  Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get(PT_PD2).getRecordTypeId();
  request.RecordTypeId=recordtypeid;

  lstInsertRequest.add(request);
  PT_Richiesta__c request1 = PT_TestDataFactory.createRequestF05_one();
  Id recordtypeid_1 = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get(PT_N01).getRecordTypeId();
  request1.RecordTypeId=recordtypeid_1;
  lstInsertRequest.add(request1);
  database.insert(lstInsertRequest);

  list<PT_Consuntivazione__c > lstInsertconsu = new list<PT_Consuntivazione__c >();
  PT_Consuntivazione__c con = new PT_Consuntivazione__c();
  con.ProtocolloRic__c=lstInsertRequest[0].id;
  con.DataEsec__c=system.today();
  lstInsertconsu.add(con);

  database.insert(lstInsertconsu);

  list<PT_Notifica__c  > lstNoti= new list<PT_Notifica__c  >();
  PT_Notifica__c  noti = new PT_Notifica__c ();
  noti.ProtocolloRic__c=lstInsertRequest[0].id;
  noti.CodFlusso__c=TESTS;
  noti.DataOraNot__c=system.today();
  lstNoti.add(noti);

  database.insert(lstNoti);

  list<PT_Costi__c   > lstcosti= new list<PT_Costi__c   >();
  PT_Costi__c   costi = new PT_Costi__c  ();
  costi.ProtocolloRic__c=lstInsertRequest[0].id;
  lstcosti.add(costi);

  database.insert(lstcosti);

  list<PT_F05Report__c   > lstrepo= new list<PT_F05Report__c   >();
  PT_F05Report__c   repo = new PT_F05Report__c  ();
  repo.Name=REPONAME;
  lstrepo.add(repo);

  database.insert(lstrepo);
  Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
  User usr = new User(Alias = ALIAS, Email=EMAIL,
    EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
    LocaleSidKey=ENUS, ProfileId = pfl.Id,
    TimeZoneSidKey=TZK,
    UserName=UUNAME);
  database.insert(usr);
}
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_F05_SearchTest() {
        //List < PT_F05_Wrapper  > wraplist = new List < PT_F05_Wrapper  > ();
        //Test.startTest();
       // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
       User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
         PT_F05_Search.getWrapper();PT_F05_Search.fetchRecordTypeValues();
         PT_F05_Search.statoRichValues();PT_F05_Search.PODValues();
         PT_F05_Search.lstOflabel();PT_F05_Search.lstOfAPI();
         PT_F05_Wrapper  wrap = new PT_F05_Wrapper ();
         PT_F05_Search.statotoRichValues(wrap.StatoRich);
         wrap.Ordinaper = 'IdRichiestaFour__c';wrap.StatoRich = 'Acquisita_';
         List < ANAG_STATO_RICHIESTA__c > asrList = new ANAG_STATO_RICHIESTA__c [2];
         asrList[0] = PT_TestDataFactory.insertstatoric('Rec1','Acquisita_','ACC');
         asrList[1] = PT_TestDataFactory.insertstatoric('Rec2','Acquisita_','ACQ');
         Database.Insert(asrList , false);
         RecordType recordt = [select id, DeveloperName, Name, Description 
         from RecordType where sObjecttype = 'PT_Richiesta__c' Limit 1];
         String myJSON = JSON.serialize(wrap);
         string strfields = 'IdRichiestaFour__c,IdTipoRichiesta__c,IdRichTrader__c';
         string objname = 'PT_Richiesta__c';
         PT_F05_Search.getQueryreqrecord(myJSON, objname, strfields, 100);
         PT_F05_csv_Wrapper wrapone = new PT_F05_csv_Wrapper();
         PT_F05_Search.statotoRichValues(wrap.StatoRich);
         wrapone.PRESA= 'IdRichiestaFour__c';
         wrapone.STATO_RICHIESTA= 'Acquisita_';
         String myJSONone = JSON.serialize(wrapone);
           // PT_F05_Search.csvGenerate('select IdRichiestaFour__c from PT_Richiesta__c');
           Test.startTest();
           PT_F05_Search.createF05Report(myJSON );
           Test.stopTest();
           system.assertEquals(wrap.Ordinaper, 'IdRichiestaFour__c');
         }
       }
        /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_F05_SearchTest_One() {
        //List < PT_F05_Wrapper  > wraplist = new List < PT_F05_Wrapper  > ();
        //Test.startTest();
        //User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        User usr=[Select id,name from User LIMIT: lmt];
        System.runAs(usr) {
          PT_F05_Wrapper  wrap = new PT_F05_Wrapper ();
          wrap.Ordinaper = 'IdRichiestaFour__c';
          wrap.StatoRich = 'EVASA_';
          wrap.CodRichDistribA = 1234;
          wrap.CodRichDistribDa = 1234;
          wrap.SottostatoRich = 'SF';
          wrap.POD = 'SF';
          wrap.NumPresa = 'SF';
          wrap.Eneltel = 'SF';
          wrap.Venditore = 'SF';
          wrap.ContrattodiDisp = 'SF';
          wrap.CFRich = 'SF';

          List < ANAG_STATO_RICHIESTA__c > asrList = new ANAG_STATO_RICHIESTA__c [3];
          asrList[0] = PT_TestDataFactory.insertstatoric('Rec3','EVASA_','ACQ');
          asrList[1] = PT_TestDataFactory.insertstatoric('Rec4','EVASA_','ANN');
          asrList[2] = PT_TestDataFactory.insertstatoric('Rec5','EVASA_','EVA');
          Database.Insert(asrList , false);
           // RecordType rt = [select id, DeveloperName, Name, Description from RecordType where sObjecttype = 'PT_Richiesta__c' Limit 1];
           String myJSON = JSON.serialize(wrap);
           string strfields = 'IdRichiestaFour__c,IdTipoRichiesta__c,IdRichTrader__c';
           string objname = 'PT_Richiesta__c';
           Test.startTest();
           PT_F05_Search.getQueryreqrecord(myJSON, objname, strfields, 100);
           Test.stopTest();
           system.assertEquals(wrap.Ordinaper, 'IdRichiestaFour__c');
         }
       }
      /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_F05_SearchTest_Two() {
        //List < PT_F05_Wrapper  > wraplist = new List < PT_F05_Wrapper  > ();
        //Test.startTest();
       // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
       User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
         PT_F05_Wrapper  wrap = new PT_F05_Wrapper();
         wrap.Ordinaper = 'IdRichiestaFour__c';
         wrap.StatoRich = 'In Lavorazione_';
         wrap.DataDecorrenzaDa = system.today();
         wrap.DataDecorrenzaA = system.today();
         wrap.DataDecorrenzaSwitchDa = '';
         wrap.DataDecorrenzaSwitchA = '';
         wrap.DataEvasioneDa = system.today();
         wrap.DataEvasioneA = system.today();
         wrap.PIVARich = 'SF';
         List < ANAG_STATO_RICHIESTA__c > asrList = new ANAG_STATO_RICHIESTA__c [3];

         asrList[0] = PT_TestDataFactory.insertstatoric('Rec6','In Lavorazione_','ANN');
         asrList[1] = PT_TestDataFactory.insertstatoric('Rec7','In Lavorazione_','EVA');
         asrList[2] = PT_TestDataFactory.insertstatoric('Rec8','In Lavorazione_','INL');

         Database.Insert(asrList , false);
            //RecordType rt = [select id, DeveloperName, Name, Description from RecordType where sObjecttype = 'PT_Richiesta__c' Limit 1];
            String myJSON = JSON.serialize(wrap);
            string strfields = 'IdRichiestaFour__c,IdTipoRichiesta__c,IdRichTrader__c';
            string objname = 'PT_Richiesta__c';
            Test.startTest();
            PT_F05_Search.getQueryreqrecord(myJSON, objname, strfields, 100);
            Test.stopTest();
            system.assertEquals(wrap.Ordinaper, 'IdRichiestaFour__c');
          }
        }
     /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_F05_SearchTest_Three() {
        //List < PT_F05_Wrapper  > wraplist = new List < PT_F05_Wrapper  > ();
        //Test.startTest();
       // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
       User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
         PT_F05_Wrapper  wrap = new PT_F05_Wrapper ();
         wrap.Ordinaper = 'Pod__c';
         wrap.StatoRich = 'Evasa';
         wrap.Nome = 'MARIO';
         wrap.Cognome = 'SF';
         wrap.RagSociale = 'AUS';
         wrap.ProvinciaPOD = 'SF';
            //wrap.TipoRich = 'PT_A01';
            wrap.CodRichVend = 'SF';
            List < ANAG_STATO_RICHIESTA__c > asrList = new ANAG_STATO_RICHIESTA__c [1];
            asrList[0] = PT_TestDataFactory.insertstatoric('Rec11','Evasa','EVA');
            Database.Insert(asrList , false);
            
           // RecordType rt = [select id, DeveloperName, Name, Description from RecordType where sObjecttype = 'PT_Richiesta__c' Limit 1];
           String myJSON = JSON.serialize(wrap);
           string strfields = 'IdRichiestaFour__c,IdTipoRichiesta__c,IdRichTrader__c';
           string objname = 'PT_Richiesta__c';
           Test.startTest();
           PT_F05_Search.getQueryreqrecord(myJSON, objname, strfields, 100);
           Test.stopTest();
           system.assertEquals(wrap.Ordinaper, 'Pod__c');
         }
       }
        /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_F05_SearchcsvTest() {
       // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
       User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
         PT_F05_csv_Wrapper wrapone = new PT_F05_csv_Wrapper();
         wrapone.PRESA= 'IdRichiestaFour__c';wrapone.Data_Lavoro=null;
         wrapone.STATO_RICHIESTA= 'ANN';wrapone.CONTRATTO_DI_DISPACCIAMENTO= 'ANN';
         wrapone.POD= 'ITO';wrapone.LOCALITA_FORNITURA= 'ITO';
         wrapone.SERVIZIO_MISURA_RICHIESTO= 'ANN';wrapone.DENOMINAZIONE_ATTUALE_CF= 'ANN';
         wrapone.CODICE_FISCALE_CF= 'ANN';wrapone.DENOMINAZIONE_CF= 'ANN';
         wrapone.PARTITA_IVA_CF= 'ANN';wrapone.INDIRIZZO_FORNITURA = 'ANN';
         wrapone.INDIRIZZO_NUOVA = 'ANN';wrapone.LOCALITA_NUOVA = 'ANN';
         wrapone.NUOVO_INDIRIZZO = 'ANN';wrapone.NUOVA_LOCALITA = 'ANN';
         wrapone.TIPOLOGIA_FORNITURA = 'ANN';wrapone.ATTUALE_OPZIONE = 'ANN';
         wrapone.ATTUALE_POTENZA = 123;wrapone.NUOVA_TENSIONE = 'ANN';
         wrapone.NUOVA_POTENZA_DISPONIBILE = 123;wrapone.PREVENTIVO = 'ANN';
         wrapone.A_Matr_Misu = 'ANN';wrapone.A_Energia_Attiva_F1 = 123;
         wrapone.NUOVA_OPZIONE = 'ANN';wrapone.DATA_DEL = system.today();
         wrapone.ULTIMO_CODICE_FLUSSO = null;wrapone.CODICE_DATA_PUBBLICAZIONE = null;

         String myJSONone = JSON.serialize(wrapone);
         List<PT_F05_csv_Wrapper> cslist = new List<PT_F05_csv_Wrapper>();
         cslist=PT_F05_Search.csvGenerate(ADDRESSQUERTY);
         PT_F05Report__c accRec=[Select id,name from PT_F05Report__c LIMIT: lmt];
         Test.startTest();
         PT_F05_Search.createF05Report('');
         Test.stopTest();
         system.assertEquals( wrapone.NUOVA_OPZIONE, 'ANN');
       }
     }
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search negative scenario to cover Exception
**********************************************************************************************/
static testMethod void pt_F05_SearchTest_Negative() {
       //Test.startTest();
       // User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
       User usr=[Select id,name from User LIMIT: lmt];
       System.runAs(usr) {
         PT_F05_Search.getWrapper();
         PT_F05_Wrapper  wrap = new PT_F05_Wrapper ();
         wrap.Ordinaper = 'Pod__c';
         String myJSON = JSON.serialize(wrap);
         string strfields = 'IdRichiestaFour__c,IdTipoRichiesta__c,IdRichTrader__c';
         string objname = 'PT_Richiesta__c';
         Test.startTest();
         PT_F05_Search.getQueryreqrecord('wraplist', objname, strfields, 100);
         Test.stopTest();
         system.assertEquals(wrap.Ordinaper, 'Pod__c');

       }
     }
   }