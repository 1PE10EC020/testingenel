@isTest
private class ChangeHistoryController_Test {
 static testMethod void testChangeHistoryController()
 {
  Account acc= new Account(Phone='49749',Name='Test');
  insert acc;
  List<SObjectType> sObjList =new List<SObjectType> ();
  sObjList.add(acc.getSObjectType());
  List<SObjectField> sof =new List<SObjectField> ();
  
  Schema.DescribeFieldResult F = Account.Name.getDescribe();
  Schema.sObjectField T = F.getSObjectField();
  sof.add(T);
  
  Schema.DescribeFieldResult g = Account.Phone.getDescribe();
  Schema.sObjectField h = g.getSObjectField();
  sof.add(h);

  Map<String, List<SObjectField>> objectFieldMap= new Map<String, List<SObjectField>>(); 
  objectFieldMap.put('acc',sof);
  
  acc.Name = 'Test New';
  update acc;

  ChangeHistoryController ch = new ChangeHistoryController();
  ch.getPastDate();
  ch.getCurrentDate();
  ch.getHistory();
  ch.getHistoryDetailss();
  String userId = userinfo.getUserId();
  ch.retrieveUserName(userId);
  ch.getFields(acc,objectFieldMap);
  ch.createHistoryDetailsList();
  }
}