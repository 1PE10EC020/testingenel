/*
 * @author: Accenture
 * @date: 2011-09-20
 * Controller class for adding multiple 'Customization__c' records through a single page.
 * It provides functionality for dynamically creating new records and saving them all at once.
 */

public class CustomizationController {

    public static Integer indexGen {get; set;} //index generator
    
    /*
     * Wrapper class for holding a 'Customization__c' record and its row index together.
     * The row index is used to delete the row using commandLink
     */
    public class CustomizationWrapper {
        public Customization__c customization;
        public Integer index;
    
        public CustomizationWrapper() {
            this.customization = new Customization__c(Name__c = 'Enter name');
            this.index = indexGen++;
        }
        
        public Customization__c getCustomization() {
            return customization;
        }
        
        public Integer getIndex() {
            return index;
        }
    }

    public List<CustomizationWrapper> customizationList;
    public Integer numRows {get; set;}
    
    /*
     * Constructor
     * -----------
     * Initializes 'customizationList' and 'numRows'
     */
    public CustomizationController(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;
        
        customizationList = new List<CustomizationWrapper>();
        numRows = 1;
    }
    
    public List<CustomizationWrapper> getCustomizationList() {
        return customizationList;
    }
    
    /*
     * upserts all records currently in the 'customizationList'
     */
    public PageReference save() {
        if(customizationList.size() == 0)
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
        try {
            List<Customization__c> tempList = new List<Customization__c>();
            for(Integer i=0; i<customizationList.size(); ++i)
                tempList.add(customizationList[i].getCustomization());
            upsert(tempList);
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }
    
    /*
     * appends new records to the 'customizationList'.
     * The number of records added is determined by the value of 'numRows'
     * Issues with the method: Not working if mandatory fields are left empty
     */
    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    customizationList.add(new CustomizationWrapper());
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
     public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                CustomizationWrapper customizeWrap = new CustomizationWrapper();
                customizeWrap.customization = [SELECT Accept_Attachments__c,Accept_Email_From_Details__c,Active__c,Advanced_Email_Security_Settings__c,Apex_Class__c,  API_Version__c,Code_Owner_And_Details__c,Configuration_Environment__c,Name__c,  Deactivated_Email_Address_Action__c,Deactivated_Email_Service_Action__c,Description__c,Development_Environment__c,Enter_Code_Details__c,    HTML_Body__c,Is_Valid__c,Label__c,Length_Without_Comments__c,Markup__c, Namespace_Prefix__c,Over_Email_Rate_Limit_Action__c,Prebuild_In_Page__c,Production_Environment__c,  recordtype1__c,Refrence_No__c,Requirement_No__c,SControlName__c,Status__c,Type__c,UAT_Environment__c,Unauthenticated_Sender_Action__c,Unauthorized_Sender_Action__c FROM Customization__c WHERE ID = :CurrentRecordID];
                customizationList.add(customizeWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    /*
     * Clears all records from the 'courseList'
     * Issues with the method: Not working if mandatory fields are left empty
     */
    public void clear() {
        customizationList.clear();
        numRows = 1;
    }
    
    /*
     * deletes a record from 'courseList' depending on the 'index' of 'Customization__c' within the 'CustomizationWrapper' class
     */
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<customizationList.size(); ++i)
                if(customizationList[i].index == delIndex) {
                    customizationList.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}