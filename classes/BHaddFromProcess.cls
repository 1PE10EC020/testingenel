public class BHaddFromProcess{
@InvocableMethod
    public static list<Datetime> AddBusinessHour (list<String> listOfCaseId){
       
        list<Datetime> listDate = new list<Datetime>();
        system.debug(logginglevel.debug, 'case in input : '+listOfCaseId);
        try{
            // retrive the case passed from input
            /*Giorgio risolto bug sul ricalcolo datatime per l'escalation al BO*/
            list<Case> listOfCase = [Select id,status,AssegnatoIILivello__c,lastModifiedDate,createddate,tech_IsStatusIILivello__c,Status_reale__c, Secondo_Livello_Manuale__c, tech_case_aperto_manualmente__c,tech_isFibra__c from case where id in :listOfCaseId];
            if(listOfCase != null && !listOfCase.isEmpty() ){
                system.debug(logginglevel.debug, 'ListOfCaseIvoked '+ listOfCase);
                listDate = BusinessHoursHelper.addHours(listOfCase);
                //update(listOfCase);
            /*  for(Case c : listOfCase)
                    listDate.add(c.TempoDiEscalation__c); */
            }
        }
        catch(Exception e ){
            system.debug(logginglevel.error, 'Exception in Add16BusinessHour :'+e.getMessage());
        }
       return listDate;
    }

	//Giorgio Per essere usato nel ricalcolo del tempo di escalation quando il case passa da SME al II Livello
	public static Datetime AddBusinessHour(Case inputCase){
		list<Datetime> dateList = new list<Datetime>();
		List<String> listOfCaseId = new List<String>();
		System.debug('*** trackEmailSent(Case inputCase)'+inputCase.id);
		listOfCaseId.add(inputCase.id);
		dateList= AddBusinessHour(listOfCaseId);
		System.debug('*** AddBusinessHour(Case inputCase) datetime for tempo di escalation'+dateList.get(0));
		return dateList.get(0);
	}

}