global class KBMS_ArticleHistoryBatchableSheduled implements Schedulable, Database.Batchable<sObject>
{
    //Execute menthod of Schedulable interface
    global void execute(SchedulableContext scMain) 
    {
         KBMS_ArticleHistoryBatchableSheduled b = new KBMS_ArticleHistoryBatchableSheduled();
         database.executebatch(b);
    }
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        String query='SELECT Id FROM  KBMS_ArticleHistory__c';
        return Database.getQueryLocator(query);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<KBMS_ArticleHistory__c> KBMSArticleHistoryList) 
    {
        Delete KBMSArticleHistoryList;
    }
       
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
         KBMS_ArticleFAQViewBatchable b = new KBMS_ArticleFAQViewBatchable();
         database.executebatch(b);
    
    }
}