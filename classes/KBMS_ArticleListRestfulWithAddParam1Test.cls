/*
* @author Atos India Pvt Ltd.
* @date  Sept,2016
* @description This class is test class of KBMS_ArticlesListRestfulWithAddParam1
*/
@isTest(seeAllData= true)
public class KBMS_ArticleListRestfulWithAddParam1Test {
    
    static testMethod void testMethodForFAQ() { 
        
        //Accessing custom Labels
        
        String language = Label.KBMS_Language;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        
        //Faq Records        
        List<FAQ__kav> lstDraftFaq1=KBMS_TestDataFactory.createFAQTestRecords(5);
        Set<Id> setArticleIds=new Set<Id>();
        for(FAQ__kav faqt:lstDraftFaq1){
            
            setArticleIds.add(faqt.id);            
        }
        string draftSts = 'Draft';
        string onlineSts = 'Online';
        string queryFaq = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary,PublishStatus FROM FAQ__kav  WHERE PublishStatus =: draftSts AND Language =: language AND ID in:setArticleIds';
        List<FAQ__kav> lstDraftFaq = Database.query(queryFaq);
        
        FAQ__DataCategorySelection datacatTest =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,GroupDataCategory,dataCategory);        
        insert datacatTest;
        
        FAQ__DataCategorySelection datacatTest1 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,'KBMS','OT');        
        insert datacatTest1;
        
        FAQ__DataCategorySelection datacatTest2 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,GroupDataCategory,dataCategory);        
        insert datacatTest2;
        
        FAQ__DataCategorySelection datacatTest3 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,'KBMS','OT');        
        insert datacatTest3;
        
        FAQ__DataCategorySelection datacatTest4=KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[2].Id,'KBMS_Operai_Tecnici','Connessione');        
        insert datacatTest4;
            
        Set<Id> articleIds=new Set<Id>();
        Set<Id> parentArticleId = new Set<Id>();
        
        for(FAQ__kav fq:lstDraftFaq){
            
            KbManagement.PublishingService.publishArticle(fq.KnowledgeArticleId, true);
        } 
        
        string publishQuery ='SELECT Id,KnowledgeArticleId FROM FAQ__kav  WHERE PublishStatus =: onlineSts AND Language =: language  AND Id IN:setArticleIds';
        List<FAQ__kav> lstFaqPublish= Database.query(publishQuery);
        
        for(FAQ__kav fq:lstFaqPublish){
            
            parentArticleId.add(fq.KnowledgeArticleId);
            articleIds.add(fq.id);            
        }        
        List<Vote> lstFaqVote=new List<Vote>();       
            
        Vote upVot=new Vote();
        upVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[0].KnowledgeArticleId,'Up');
        lstFaqVote.add(upVot);
        
        Vote downVot=new Vote();
        downVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[1].KnowledgeArticleId,'Down');
        lstFaqVote.add(downVot);            
       
        insert lstFaqVote;
        
        List<Vote> Votetest=[SELECT id,Type,ParentId  from vote WHERE Parent.type='FAQ__Ka' ];
        System.assert(Votetest.size()!=0);
        
        PageReference pageRef = new PageReference('/'+lstFaqPublish[0].Id);
        
        Test.setCurrentPage(pageRef);
        
        //testing rest services        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam1';  
        RestRequestobject.httpMethod = 'GET'; 
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0'); 
        RestRequestobject.addParameter('isDESC','ASC');
        RestRequestobject.addParameter('isDESC','FALSE');
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        RestRequestobject.addParameter('articleType','FAQ');        
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','TestFAQ0');
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        
        RestContext.response= restResponseObject; 
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();         
        RestRequestobject.addParameter('orderBy','Title');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','View');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','Like');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','Alphabetic');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        Test.stopTest(); 
    }
    static testMethod void testMethodModuli() { 
        string draftSts = 'Draft';
        string onlineSts = 'Online';
        String language = Label.KBMS_Language;
        List<Moduli__kav> lstModuli = KBMS_TestDataFactory.createModuliTestRecords(5);
        Set<ID>  setModuli=new Set<ID>();
        
        for(Moduli__kav mod:lstModuli){         
            setModuli.add(mod.id);      
        }
        string moduliQ = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary FROM Moduli__kav WHERE PublishStatus =: draftSts AND Language =:language AND ID IN:setModuli ';
        List<Moduli__kav> lstPubllishModuli = Database.query(moduliQ);
        
        for(Moduli__kav mo:lstPubllishModuli){
            
            KbManagement.PublishingService.publishArticle(mo.KnowledgeArticleId, true);
        }
        Set<String> setKnowledgeIdM=new Set<String>();
        List<Vote> lstModVote=new List<Vote>();
       
            
        Vote upVotM=new Vote();
        upVotM=KBMS_TestDataFactory.createVoteTestRecords(lstPubllishModuli[0].KnowledgeArticleId,'Up');
        lstModVote.add(upVotM);
        Vote downVotM=new Vote();
        downVotM=KBMS_TestDataFactory.createVoteTestRecords(lstPubllishModuli[1].KnowledgeArticleId,'Down');
        lstModVote.add(downVotM);   
    
        insert lstModVote;
        
        System.assertEquals(5,lstPubllishModuli.size());
        List<Vote> Votetest=[SELECT id,Type,ParentId  from vote WHERE Parent.type='Moduli__Ka']; 
        System.assertEquals(Votetest[0].id!=null,True); 
        
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam1';  
        RestRequestobject.httpMethod = 'GET'; 
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0'); 
        RestRequestobject.addParameter('isDESC','ASC');
        RestRequestobject.addParameter('isDESC','FALSE');
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        RestRequestobject.addParameter('articleType','Moduli');
        
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+Label.KBMS_DataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');        
        RestRequestobject.addParameter('textSearch','TestModuli0');
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;        
        Test.startTest();
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        RestRequestobject.addParameter('orderBy','Title');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        RestRequestobject.addParameter('orderBy','View');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        RestRequestobject.addParameter('orderBy','Like');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','Alphabetic');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        Test.stopTest();
    }   
    static testMethod void testMethodForProcessi() {
        
        String articleType='Processi';
        String language = Label.KBMS_Language;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        string draftSts = 'Draft';
        string onlineSts = 'Online';
        Set<Id> setIds=new Set<Id>();
        List<Processi__kav> lstProcess = KBMS_TestDataFactory.createProcessTestRecords(5);
        for(Processi__kav protest:lstProcess){          
            setIds.add(protest.id);         
        }
        string processiQ= 'SELECT id,KnowledgeArticleId FROM Processi__kav  WHERE PublishStatus =:draftSts AND Language =:language AND ID IN:setIds' ;
        List<Processi__kav> lstProcesDraft= Database.query(processiQ);
        Processi__DataCategorySelection datacatTestP =KBMS_TestDataFactory.createProcessiDataCategoryTestRecords(lstProcesDraft[0].Id,GroupDataCategory,dataCategory);        
        insert datacatTestP;
        
        Processi__DataCategorySelection datacatTestP1 =KBMS_TestDataFactory.createProcessiDataCategoryTestRecords(lstProcesDraft[0].Id,'KBMS','OT');        
        insert datacatTestP1;
        
        Processi__DataCategorySelection datacatTestP2 =KBMS_TestDataFactory.createProcessiDataCategoryTestRecords(lstProcesDraft[1].Id,GroupDataCategory,dataCategory);        
        insert datacatTestP2;
        
        Processi__DataCategorySelection datacatTestP3 =KBMS_TestDataFactory.createProcessiDataCategoryTestRecords(lstProcesDraft[1].Id,'KBMS','OT');        
        insert datacatTestP3;
        
        for(Processi__kav po:lstProcesDraft){
            
            KbManagement.PublishingService.publishArticle(po.KnowledgeArticleId, true);
        }
        string onlineProcessiQ = 'SELECT id,KnowledgeArticleId FROM Processi__kav  WHERE PublishStatus =:onlineSts AND Language =: language AND ID IN:setIds';
        List<Processi__kav> lstProcesPublish = Database.query(onlineProcessiQ) ;
        
        Set<String> setKnowledgeIdP=new Set<String>();
        List<Vote> lstProcVote=new List<Vote>(); 
        
        Vote downVotP=new Vote();
        Vote upVotP=new Vote(); 
        upVotP=KBMS_TestDataFactory.createVoteTestRecords(lstProcesPublish[0].KnowledgeArticleId,'Up');       
        downVotP=KBMS_TestDataFactory.createVoteTestRecords(lstProcesPublish[1].KnowledgeArticleId,'Down'); 
        lstProcVote.add(upVotP);  
        lstProcVote.add(downVotP);         
        insert lstProcVote;       
        
        System.assertEquals(5,lstProcesPublish.size());
        
        List<Vote> Votetest=[SELECT id,Type,ParentId  from vote WHERE Parent.type='Processi__Ka'];
        
        System.assert(Votetest.size()>0);
               
        PageReference pageRefP = new PageReference('/'+lstProcesPublish[0].Id);
        
        Test.setCurrentPage(pageRefP);
        
        //testing rest services
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam1';  
        RestRequestobject.httpMethod = 'GET'; 
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0'); 
        RestRequestobject.addParameter('isDESC','ASC');
        RestRequestobject.addParameter('isDESC','FALSE');
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        RestRequestobject.addParameter('articleType',articleType);
        
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+Label.KBMS_DataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','Test proces1');
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;      
        
        Test.startTest();       
        
        RestRequestobject.addParameter('orderBy','Title');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','View');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','Like');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('orderBy','Alphabetic');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        Test.stopTest();
    } 
    static testMethod void testMethodNegativeTestCase() { 
        //searching for wrong data
        //Accessing custom Labels
        
        String language = Label.KBMS_Language;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        string draftSts = 'Draft';
        string onlineSts = 'Online';
        
        //Faq Records        
        List<FAQ__kav> lstDraftFaq1=KBMS_TestDataFactory.createFAQTestRecords(5);
        Set<Id> setArticleIds=new Set<Id>();
        for(FAQ__kav faqt:lstDraftFaq1){
            
            setArticleIds.add(faqt.id);            
        }
        
        string draftQ = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary,PublishStatus FROM FAQ__kav  WHERE PublishStatus =: draftSts AND Language =: language AND ID in:setArticleIds' ;
        List<FAQ__kav> lstDraftFaq = Database.query(draftQ);
        
        FAQ__DataCategorySelection datacatTest =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,GroupDataCategory,dataCategory);        
        insert datacatTest;
        
        FAQ__DataCategorySelection datacatTest1 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,'KBMS','OT');        
        insert datacatTest1;
        
        FAQ__DataCategorySelection datacatTest2 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,GroupDataCategory,dataCategory);        
        insert datacatTest2;
        
        FAQ__DataCategorySelection datacatTest3 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,'KBMS','OT');        
        insert datacatTest3;
        
        FAQ__DataCategorySelection datacatTest4=KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[2].Id,'KBMS_Operai_Tecnici','Tutto');        
        insert datacatTest4;
            
        Set<Id> articleIds=new Set<Id>();
        Set<Id> parentArticleId = new Set<Id>();
        
        for(FAQ__kav fq:lstDraftFaq){
            
            KbManagement.PublishingService.publishArticle(fq.KnowledgeArticleId, true);
        } 
        string onlineQ = 'SELECT Id,KnowledgeArticleId FROM FAQ__kav  WHERE PublishStatus =: onlineSts AND Language =: language AND Id IN:setArticleIds' ;
        List<FAQ__kav> lstFaqPublish = Database.query(onlineQ);
        
        for(FAQ__kav fq:lstFaqPublish){
            
            parentArticleId.add(fq.KnowledgeArticleId);
            articleIds.add(fq.id);            
        }        
        List<Vote> lstFaqVote=new List<Vote>();       
            
        Vote upVot=new Vote();
        upVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[0].KnowledgeArticleId,'Up');
        lstFaqVote.add(upVot);
        
        Vote downVot=new Vote();
        downVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[1].KnowledgeArticleId,'Down');
        lstFaqVote.add(downVot);            
       
        insert lstFaqVote;
        
        List<Vote> Votetest=[SELECT id,Type,ParentId  from vote WHERE Parent.type='FAQ__Ka' ];
        System.assert(Votetest.size()!=0);
        
        PageReference pageRef = new PageReference('/'+lstFaqPublish[0].Id);
        
        Test.setCurrentPage(pageRef);
        
        //testing rest services        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam1';  
        RestRequestobject.httpMethod = 'GET'; 
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0'); 
        RestRequestobject.addParameter('isDESC','ASC');
        RestRequestobject.addParameter('isDESC','FALSE');
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        RestRequestobject.addParameter('articleType','FAQ');        
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','TestFAQN0');
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        
        RestContext.response= restResponseObject; 
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();         
        RestRequestobject.addParameter('orderBy','Title');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        Test.stopTest(); 
    }
      static testMethod void testMethodNegativeTestCase2() { 
        //searching for wrong data
        //Accessing custom Labels
        
        String language = Label.KBMS_Language;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        string draftSts = 'Draft';
        string onlineSts = 'Online';
        
        //Faq Records        
        List<FAQ__kav> lstDraftFaq1=KBMS_TestDataFactory.createFAQTestRecords(5);
        Set<Id> setArticleIds=new Set<Id>();
        for(FAQ__kav faqt:lstDraftFaq1){
            
            setArticleIds.add(faqt.id);            
        }
        string draftQ = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate,Summary,PublishStatus FROM FAQ__kav  WHERE PublishStatus =: draftSts AND Language =: language AND ID in:setArticleIds' ;
        List<FAQ__kav> lstDraftFaq =  Database.query(draftQ);
        
        FAQ__DataCategorySelection datacatTest =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,GroupDataCategory,dataCategory);        
        insert datacatTest;
        
        FAQ__DataCategorySelection datacatTest1 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[0].Id,'KBMS','OT');        
        insert datacatTest1;
        
        FAQ__DataCategorySelection datacatTest2 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,GroupDataCategory,dataCategory);        
        insert datacatTest2;
        
        FAQ__DataCategorySelection datacatTest3 =KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[1].Id,'KBMS','OT');        
        insert datacatTest3;
        
        FAQ__DataCategorySelection datacatTest4=KBMS_TestDataFactory.createFAQDataCategoryTestRecords(lstDraftFaq[2].Id,'KBMS_Operai_Tecnici','Tutto');        
        insert datacatTest4;
            
        Set<Id> articleIds=new Set<Id>();
        Set<Id> parentArticleId = new Set<Id>();
        
        for(FAQ__kav fq:lstDraftFaq){
            
            KbManagement.PublishingService.publishArticle(fq.KnowledgeArticleId, true);
        }
        string onlineQ = 'SELECT Id,KnowledgeArticleId FROM FAQ__kav  WHERE PublishStatus =: onlineSts AND Language =: language AND Id IN:setArticleIds';
        List<FAQ__kav> lstFaqPublish =  Database.query(onlineQ) ;
        
        for(FAQ__kav fq:lstFaqPublish){
            
            parentArticleId.add(fq.KnowledgeArticleId);
            articleIds.add(fq.id);            
        }        
        List<Vote> lstFaqVote=new List<Vote>();       
            
        Vote upVot=new Vote();
        upVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[0].KnowledgeArticleId,'Up');
        lstFaqVote.add(upVot);
        
        Vote downVot=new Vote();
        downVot=KBMS_TestDataFactory.createVoteTestRecords(lstFaqPublish[1].KnowledgeArticleId,'Down');
        lstFaqVote.add(downVot);            
       
        insert lstFaqVote;
        
        List<Vote> Votetest=[SELECT id,Type,ParentId  from vote WHERE Parent.type='FAQ__Ka' ];
        System.assert(Votetest.size()!=0);
        
        PageReference pageRef = new PageReference('/'+lstFaqPublish[0].Id);
        
        Test.setCurrentPage(pageRef);
        
        //testing rest services        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticlesListWithAddParam1';  
        RestRequestobject.httpMethod = 'GET'; 
        RestRequestobject.addParameter('limit', '10');
        RestRequestobject.addParameter('from','0'); 
        RestRequestobject.addParameter('isDESC','ASC');
        RestRequestobject.addParameter('isDESC','FALSE');
        Integer noLimits = 10 == null ? 0 : Integer.valueOf(10);
        Integer offsets = 0 == null ? 0 : Integer.valueOf(0);
        RestRequestobject.addParameter('articleType','FAQ');        
        RestRequestobject.addParameter('listCategory','[{"categoryName":"'+dataCategory+'"}]');
        RestRequestobject.addParameter('orderBy','LastModifiedDate');
        RestRequestobject.addParameter('textSearch','TestFAQN0');
        
        RestContext.request = RestRequestobject;    
        RestResponse restResponseObject = new RestResponse(); 
        
        RestContext.response= restResponseObject; 
        Test.startTest();
        
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();         
        RestRequestobject.addParameter('orderBy','ORDER');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList(); 
        RestRequestobject.addParameter('isDESC','FALSE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();        
        RestRequestobject.addParameter('isDESC','TRUE');
        KBMS_ArticlesListRestfulWithAddParam1.getArticlesList();
        Test.stopTest(); 
    }
    
    
    
    
    
    
}