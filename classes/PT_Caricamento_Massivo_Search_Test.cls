/*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search.
**********************************************************************************************/
@isTest
private class PT_Caricamento_Massivo_Search_Test {
  PRIVATE STATIC FINAL STRING STDUSER='Standard User';
  PRIVATE STATIC FINAL STRING UUNAME = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
  PRIVATE STATIC FINAL STRING ALIAS='standt';
  PRIVATE STATIC FINAL STRING EMAIL='standarduser@testorg.com';
  PRIVATE STATIC FINAL STRING EECK='UTF-8';
  PRIVATE STATIC FINAL STRING ENUS='en_US';
  PRIVATE STATIC FINAL STRING TZK='America/Los_Angeles';
  PRIVATE STATIC FINAL STRING TESTS='test23';
  PRIVATE STATIC FINAL INTEGER LMT=1; 
  PRIVATE STATIC FINAL STRING M03='M03';
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test data creation for entire class
**********************************************************************************************/ 
@testSetup static void createTestRecords() {
    list<PT_Caricamento_Massivo__c > lstInsertService = new list<PT_Caricamento_Massivo__c >();
    PT_Caricamento_Massivo__c cmaer= new PT_Caricamento_Massivo__c();
    cmaer.TipoRich__c=M03;
    cmaer.DataDecorr__c=system.today();
    cmaer.CreatedDate__c=system.today();
    lstInsertService.add(cmaer);

    database.insert(lstInsertService);
    Profile pfl = [SELECT Id FROM Profile WHERE Name=:STDUSER LIMIT: lmt];
    User usr = new User(Alias = ALIAS, Email=EMAIL,
      EmailEncodingKey=EECK, LastName=TESTS, LanguageLocaleKey=ENUS,
      LocaleSidKey=ENUS, ProfileId = pfl.Id,
      TimeZoneSidKey=TZK,
      UserName=UUNAME);
    database.insert(usr);
}
    /*********************************************************************************************
@Author : Bharath Sharma
@date : 24 July 2017
@description : Test class for PT_F05_Search possitive scenario
**********************************************************************************************/
static testMethod void pt_Caricamento_Massivo_SearchTest () {
        //List < PT_F05_Search.wrapperClass > wraplist = new List < PT_F05_Search.wrapperClass > ();
        //Test.startTest();
        //User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
        User usr=[Select id,name from User LIMIT: lmt];
        System.runAs(usr) {
            list<PT_Caricamento_Massivo__c> lstInsertService = new list<PT_Caricamento_Massivo__c>();
            PT_Caricamento_Massivo__c objService =[select id,TipoRich__c,DataDecorr__c,CreatedDate__c from 
            PT_Caricamento_Massivo__c LIMIT: lmt];
            PT_Caricamento_Massivo_Search_controller.getWrapper();            
            PT_Caricamento_Massivo_Search_controller.tiporichValues();
            PT_Caricamento_Massivo_Search_controller.wrapperClass wrap = new 
            PT_Caricamento_Massivo_Search_controller.wrapperClass();
            wrap.DataDecorrenzaDa = System.today();
            wrap.DataDecorrenzaA = System.today()+1;
            wrap.TipoRichiesta = 'M03';
            wrap.ContrattodiDisp = 'M03';
            String myJSON = JSON.serialize(wrap);
            string strfields = 'TipoRich__c,DescrDispaccEstesa__c,CreatedDate__c';
            string objname = 'PT_Caricamento_Massivo__c ';
            List<PT_Caricamento_Massivo_Search_controller.WrapperListView> cslist = new 
            List<PT_Caricamento_Massivo_Search_controller.WrapperListView>();
            Test.startTest();
            PT_Caricamento_Massivo_Search_controller.getQueryall(myJSON, 'PT_Caricamento_Massivo__c', strfields, 100);
            Test.stopTest();
            system.assertEquals(wrap.TipoRichiesta , 'M03');
        }
    }

}