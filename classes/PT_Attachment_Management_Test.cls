@isTest
public class PT_Attachment_Management_Test {
    
    @isTest
  	private static void TestCreateAndSetAttachment() {
      
        PT_Allegato__c  allegato = new PT_Allegato__c();
        allegato.FileDisponibile__c = false;
        allegato.FileRichiesto__c = false;
        allegato.recordTypeId = [select id from recordtype where developername = 'Allegati' ].id ;
        allegato.IdAllegato__c = 10;
        insert allegato;  
        Attachment attach = new Attachment();
        attach.parentid = allegato.Id;
        String before = 'testBody Attachment';
        Blob beforeblob = Blob.valueOf(before);
        string paramvalue = EncodingUtil.base64Encode(beforeblob);
		Blob afterblob = EncodingUtil.base64Decode(paramvalue);        
        
        attach.Body = afterblob;
        
        attach.Name = 'Nome Attachment';
        insert attach;             
	}
    
    @isTest
  	private static void TestDeleteAttachment() {
        
        list<PT_Allegato__c> listAllegato = new list<PT_Allegato__c> ();
        PT_Allegato__c  allegato = new PT_Allegato__c();
        allegato.FileDisponibile__c = false;
        allegato.FileRichiesto__c = false;
        allegato.recordTypeId = [select id from recordtype where developername = 'Allegati' ].id ;
        allegato.IdAllegato__c = 15;
        insert allegato;  
        Attachment attach = new Attachment();
        attach.parentid = allegato.Id;
        String before = 'testBody Attachment';
        Blob beforeblob = Blob.valueOf(before);
        string paramvalue = EncodingUtil.base64Encode(beforeblob);
		Blob afterblob = EncodingUtil.base64Decode(paramvalue);        
        
        attach.Body = afterblob;
        
        attach.Name = 'Nome Attachment';
        insert attach; 
        
        listAllegato.add(allegato);
        
        PT_Cancella_Attachment.deleteAttachment_onAllegato(listAllegato);
        
        
	}


}