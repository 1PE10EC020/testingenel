@isTest()
public class KBMS_ContactsList2RestfulTest {

    
    static testMethod void testMethod1(){
        
        List<Sezione_Contatti__c> lstSezione = new List<Sezione_Contatti__c>();
        
        lstSezione=KBMS_TestDataFactory.createSezioneTestRecords(2);
        
        List<Contact> lstconToInsert=new List<Contact>();
        
        for(Sezione_Contatti__c obj:lstSezione){
            
            List<Contact> lstcon=KBMS_TestDataFactory.createContactTestRecords(2);
            
            for(contact con:lstcon){
                
                con.Sezione_Contatti__c=obj.id;
                lstconToInsert.add(con);
            }
        }
        update lstconToInsert;
        
        
        Test.startTest();
        KBMS_ContactsList2Restful.getContactList();
        Test.stopTest();
        
    }

}