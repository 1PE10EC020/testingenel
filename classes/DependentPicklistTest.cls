public class DependentPicklistTest {
@AuraEnabled
public static list<string> getStateList(String rcdTypeName) {
    
PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
obj.pickListValues.put('IdTipoContr__c',PT_Utility.getPickListValues('PT_Richiesta__c','IdTipoContr__c'));
 
    
    map <string,list<string>>  objj = new map<string,list<string>>();
    
    set<string> picklistfield = new set<string>();
    List<Richieste_Picklist_Values__c> mcs = Richieste_Picklist_Values__c.getall().values();
    system.debug('---->'+mcs);
    for(Richieste_Picklist_Values__c richi: mcs){
        if(richi.Controlling_Field_API__c== 'connessione' && 
           richi.record_type__c== 'N02' 				  &&
           richi.controlling_field_value__c== 'PER'){
		   picklistfield.add(richi.picklist_value__c);  
           system.debug('picklistfield-->'+picklistfield);
           }    
    }

  List<PT_PicklistAuraComponent.SelectOptionWrapper> newoptions= new list<PT_PicklistAuraComponent.SelectOptionWrapper>();
 list<PT_PicklistAuraComponent.SelectOptionWrapper> test = PT_Utility.getPickListValues('PT_Richiesta__c','IdTipoContr__c');  
 system.debug('picklistfield-->'+test);
    
    
    
    
Map<String, Richieste_Picklist_Values__c> c = Richieste_Picklist_Values__c.getall(); 
    

list<string> s = new list<string> ();
s.add('Select one');
if(rcdTypeName ==null){
rcdTypeName='';
}
if(c.containskey(rcdTypeName)){
s.clear();
string sm = c.get(rcdTypeName).controlling__c;
for(string st: sm.split(';'))
s.add(st);
}
system.debug('s--<'+s);     
//Map<String,Map<String,List<String>>> mapCustomer = new Map<String,Map<String,List<String>>>();
return s;
    
    }
    
    
 ///
 @AuraEnabled   
public static PT_PicklistAuraComponent getRichiestaPicklistValues(String RecordTypeName, String FieldAPINAME, String ContFieldAPI, String ContFieldVal, String ContField2API, String ContField2Val){
    system.debug('xxxxxxxxx'+RecordTypeName+'yyyyyyyy'+'FieldAPINAME--->>>>>'+FieldAPINAME+'ContFieldAPI>>>>>>>>>'+ContFieldAPI+'ContFieldVal>>>>>>'+ContFieldVal+'ContField2API>>>>>>>>>>'+ContField2API+'ContField2Val>>>>>>>'+ContField2Val);

    	List<Richieste_Picklist_Values__c> reqAllPicklist = Richieste_Picklist_Values__c.getall().values();
 system.debug('xxreqqqqqqqq'+reqAllPicklist);
    	set<string> picklistValueSet = new set<string>();
        
        for(Richieste_Picklist_Values__c reqPicklist:reqAllPicklist){
            if(reqPicklist.Record_Type__c==RecordTypeName && reqPicklist.Picklist_Api_Name__c==FieldAPINAME){
                if(ContFieldAPI!='' && ContFieldVal!=''){
                    if(ContField2API!='' && ContField2Val!=''){
                        if(reqPicklist.Controlling_Field_API__c == ContFieldAPI && reqPicklist.Controlling_Field_Value__c == ContFieldVal &&
                          reqPicklist.Controlling_Field_2_API__c == ContField2API && reqPicklist.Controlling_Field_2_Value__c == ContField2Val)  {
                    		picklistValueSet.add(reqPicklist.Picklist_Value__c); 
 					system.debug('finalpicklistValueSet List@@@@'+picklistValueSet);
                    	}
                    }    
                    else{
                        if(reqPicklist.Controlling_Field_API__c == ContFieldAPI && reqPicklist.Controlling_Field_Value__c == ContFieldVal)  {
                    		picklistValueSet.add(reqPicklist.Picklist_Value__c);  
                    	}
                    }  
                }
                else{    
                	picklistValueSet.add(reqPicklist.Picklist_Value__c);
                }    
            }
        }
        
        List<PT_PicklistAuraComponent.SelectOptionWrapper> picklistValueList = new List<PT_PicklistAuraComponent.SelectOptionWrapper>();
        List<PT_PicklistAuraComponent.SelectOptionWrapper> picklistValueListFinal = new List<PT_PicklistAuraComponent.SelectOptionWrapper>();
        
        picklistValueList = PT_Utility.getPickListValues('PT_Richiesta__c',FieldAPINAME);
        
        for(PT_PicklistAuraComponent.SelectOptionWrapper pickRecord:picklistValueList){
            if(picklistValueSet.contains(pickRecord.value))
            {
                picklistValueListFinal.add(pickRecord);
            }
        }
        
		PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        obj.pickListValues.put(FieldAPINAME,picklistValueListFinal);
        system.debug('final List@@@@'+picklistValueListFinal);
        system.debug('Final map@@@@'+obj);
        
        return obj;
    }



///
@AuraEnabled
 public static PT_PicklistAuraComponent getRichiestaPicklistValue(){     
    List<Richieste_Picklist_Values__c> mcs = Richieste_Picklist_Values__c.getall().values();
      set<string> picklistfield = new set<string>();
    system.debug('---->'+mcs);
    for(Richieste_Picklist_Values__c richi: mcs){
        if(richi.Picklist_Api_Name__c== 'IdConnessione__c' && 
           richi.record_type__c== 'N02' &&
           richi.controlling_field_value__c== null){
		   picklistfield.add(richi.picklist_value__c);  
               system.debug('picklistfield-->'+picklistfield);
           }
     
    }


 List<PT_PicklistAuraComponent.SelectOptionWrapper> newoptions= new list<PT_PicklistAuraComponent.SelectOptionWrapper>();    
 List<PT_PicklistAuraComponent.SelectOptionWrapper> test= new list<PT_PicklistAuraComponent.SelectOptionWrapper>();    
      test = PT_Utility.getPickListValues('PT_Richiesta__c','IdConnessione__c');  
	
      
      for( PT_PicklistAuraComponent.SelectOptionWrapper tes:test){
          if(picklistfield.contains(tes.value))   {
              newoptions.add(tes);
              system.debug('test-->'+newoptions);
          }
      }    
      
      PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
        obj.pickListValues.put('IdTipoContr__c',newoptions);
        system.debug('final List@@@@'+newoptions);
        system.debug('Final map@@@@'+obj);
	return obj;
}
}