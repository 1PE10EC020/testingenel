<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-Idea</tab>
    <tab>standard-IdeaTheme</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>PT_Notifica__c</tab>
    <tab>PT_Allegato__c</tab>
    <tab>PT_Caricamento_Massivo__c</tab>
</CustomApplication>
