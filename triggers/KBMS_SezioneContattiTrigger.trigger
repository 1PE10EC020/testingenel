trigger KBMS_SezioneContattiTrigger on Sezione_Contatti__c (before insert, after update, before delete, after undelete ) {
    
    KBMS_SezioneContattiTrigger triggerClass = new KBMS_SezioneContattiTrigger(); //-------- instantiation of class that has the functionalities for this trigger
    
    if (trigger.isInsert){
        triggerClass.insertMethod(trigger.new);
    }
    
    if (trigger.isUpdate){
        if(trigger.isAfter){
            if(KBMS_SezioneContattiTrigger.runonce()){
                triggerClass.updateMethod(trigger.new, Trigger.oldMap);
            }
        }
    }
    
    if (trigger.isDelete){
        if(KBMS_SezioneContattiTrigger.runonce()){
            triggerClass.deleteMethod(trigger.old) ;
        }
        
    }
    
    if (trigger.isUndelete){
        if(KBMS_SezioneContattiTrigger.runonce()){
            triggerClass.undeleteMethod(trigger.new) ;
        }
        
    }
    
}