trigger ValidateAccount on Account (before insert,after insert, before update,after update) {
    
    if ( Trigger.isInsert || Trigger.isUpdate )
    {
       Account_Helper.manageAccountOnTrigger(Trigger.new);
    }

}