/**
 * @author  rakesh mistry - 'inspire'
 * @date    06-Jun-2012
 *
 * trigger to prevent entries of more than one destination org credential.
 */

trigger PreventMultipleDestOrgEntry on Destination_Org_Credentials__c (before insert) {

    /* Check if a destination org credential record already exists */
    Destination_Org_Credentials__c[] credentials = [SELECT Id
                                                    FROM Destination_Org_Credentials__c
                                                    LIMIT 1];

    /* throw error if record already exists */
    if (credentials != null && credentials.size() > 0) {
        Trigger.new[0].addError('A Destination Org Credential already exists. You are allowed to add only one Destination Org Credential.');
    }
}