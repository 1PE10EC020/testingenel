trigger PT_Richiesta_Create_Trigger on PT_Richiesta__c (before insert) {
    
   if (Trigger.isInsert) {
        if (Trigger.isBefore) { 
                for(PT_Richiesta__c a: Trigger.New) {
                            PT_Richesta_Helper.validateConditions(Trigger.new);
                        }
                     }
                } 

}