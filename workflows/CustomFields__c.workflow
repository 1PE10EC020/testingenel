<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Concat_Object_Field_Name</fullName>
        <field>Concat_Object_Field_Names__c</field>
        <formula>Custom_Objects__r.Object_Name__c &amp;  Field_Name__c</formula>
        <name>Update Concat Object Field Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
