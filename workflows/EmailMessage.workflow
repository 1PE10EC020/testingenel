<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Tech_new_email_case_chiuso_True</fullName>
        <field>tech_new_email_case_chiuso__c</field>
        <literalValue>1</literalValue>
        <name>Tech new email case chiuso True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>WF Flag Auto Response Email Case Chiuso</fullName>
        <actions>
            <name>Tech_new_email_case_chiuso_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Chiuso</value>
        </criteriaItems>
        <description>(Quando il richiedente risponde all&apos;email di chiusura case) attiva il flag necessario per far partire l&apos;auto response che avvisa il richiedente di aprire un nuovo case per eventuali segnalazioni ulteriori.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
