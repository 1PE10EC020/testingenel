<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SendOutbound_False</fullName>
        <field>SendOutbound__c</field>
        <literalValue>0</literalValue>
        <name>SendOutbound_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>PT_RichiediReport</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://hookb.in/Ekqp1QRP</endpointUrl>
        <fields>CFRich__c</fields>
        <fields>CodRichDistribA__c</fields>
        <fields>CodRichDistribDa__c</fields>
        <fields>CodRichVend__c</fields>
        <fields>Cognome__c</fields>
        <fields>ContrattodiDisp__c</fields>
        <fields>CreatedById</fields>
        <fields>DataDecorrenzaA__c</fields>
        <fields>DataDecorrenzaDa__c</fields>
        <fields>DataEvasioneA__c</fields>
        <fields>DataEvasioneDa__c</fields>
        <fields>Eneltel__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Nome__c</fields>
        <fields>NumPresa__c</fields>
        <fields>Ordinaper__c</fields>
        <fields>PIVARich__c</fields>
        <fields>POD__c</fields>
        <fields>ProvinciaPOD__c</fields>
        <fields>RagSociale__c</fields>
        <fields>SottostatoRich__c</fields>
        <fields>StatoRich__c</fields>
        <fields>TipoRich__c</fields>
        <fields>Venditore__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>giorgio.peresempio@edistribuzione.com</integrationUser>
        <name>PT_RichiediReport</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>PT_RichiediReport</fullName>
        <actions>
            <name>SendOutbound_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PT_RichiediReport</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PT_F05Report__c.SendOutbound__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
