<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_CF_UpperCase</fullName>
        <description>trasforma in upper case il cf</description>
        <field>CodiceFiscale__c</field>
        <formula>UPPER( CodiceFiscale__c )</formula>
        <name>Update CF UpperCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>WF CF UpperCase</fullName>
        <actions>
            <name>Update_CF_UpperCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(OR(ISNULL(CodiceFiscale__c  ),  ISBLANK(CodiceFiscale__c)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
