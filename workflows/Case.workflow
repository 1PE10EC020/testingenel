<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_Response_New_Email_Case_Chiuso</fullName>
        <description>Auto-Response New Email Case Chiuso</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Richiedente__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Case_Closure_New_Email_Richiedente</template>
    </alerts>
    <alerts>
        <fullName>EmailAssegnatoDTR</fullName>
        <description>EmailAssegnatoDTR</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_DTR_II_Livello</template>
    </alerts>
    <alerts>
        <fullName>EmailAssegnatoZona</fullName>
        <description>EmailAssegnatoZona</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_II_Livello</template>
    </alerts>
    <alerts>
        <fullName>EmailIILivello</fullName>
        <description>EmailIILivello</description>
        <protected>false</protected>
        <recipients>
            <field>tech_EmailEscalation__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Escalation_Email_Team_Leader</template>
    </alerts>
    <alerts>
        <fullName>EmailToEspertoZona</fullName>
        <description>EmailToEspertoZona</description>
        <protected>false</protected>
        <recipients>
            <field>AssegnatoAllEspertoDiDTRZona__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_Esperto_Zona</template>
    </alerts>
    <alerts>
        <fullName>EmailToEspertoZonaPED</fullName>
        <description>EmailToEspertoZonaPED</description>
        <protected>false</protected>
        <recipients>
            <field>AssegnatoAllEspertoDiDTRZona__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_Esperto_Zona_PED</template>
    </alerts>
    <alerts>
        <fullName>Email_a_Coda_Enel_Info_Piu</fullName>
        <description>Email a Coda Enel Info Piu</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_Info_Piu_II_Livello</template>
    </alerts>
    <alerts>
        <fullName>Email_come_back_Fibra_from_SME</fullName>
        <description>Email come back Fibra from SME</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_Fibra</template>
    </alerts>
    <alerts>
        <fullName>Email_come_back_II_livello_from_SME</fullName>
        <description>Email come back II livello from SME</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_II_Livello</template>
    </alerts>
    <alerts>
        <fullName>Invio_Auto_Reply_PED</fullName>
        <description>Invio Auto Reply PED</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Richiedente_Auto_reply</template>
    </alerts>
    <alerts>
        <fullName>Invio_Questionario_di_Chiusura_Case</fullName>
        <description>Invio Questionario di Chiusura Case</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Richiedente__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Case_Closure_Survey_Richiedente</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_di_chiusura_pending</fullName>
        <description>Invio email di chiusura pending</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Richiedente__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Case_ClosurePending_Survey_Richiedente</template>
    </alerts>
    <alerts>
        <fullName>Notifica_Assegnazione_Case_BO</fullName>
        <description>Notifica Assegnazione Case BO</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Back_Office</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_BO</template>
    </alerts>
    <alerts>
        <fullName>Notifica_Assegnazione_Case_Fibra_e_Contatori</fullName>
        <description>Notifica Assegnazione Case Fibra e Contatori</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Fibra_e_Contatore</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Invia_Email_Fibra</template>
    </alerts>
    <alerts>
        <fullName>Team_Leader_Back_Office</fullName>
        <description>Team Leader Back Office</description>
        <protected>false</protected>
        <recipients>
            <recipient>Team_Leader_BO</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Escalation_Email_Team_Leader</template>
    </alerts>
    <alerts>
        <fullName>Team_Leader_Contatore_e_Fibra</fullName>
        <description>Team Leader Contatore e Fibra</description>
        <protected>false</protected>
        <recipients>
            <recipient>Team_Leader_Contatore_e_Fibra</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>servizioclienti@e-distribuzione.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Case_Management/Support_Escalation_Email_Team_Leader</template>
    </alerts>
    <fieldUpdates>
        <fullName>AggiornaNote_aggiuntive_dell_operatore</fullName>
        <description>se il campo Eventuali Note Richiedente è valorizzato allora questo campo sarà valorizzato con &apos;Note aggiuntive dell&apos;operatore&apos; altrimenti sarà vuoto</description>
        <field>Note_aggiuntive_dell_operatore__c</field>
        <formula>&quot;Note aggiuntive dell&apos; operatore: &quot;</formula>
        <name>AggiornaNote aggiuntive dell&apos;operatore</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AssegnatoIILiv_to_Now</fullName>
        <description>setta a now() la data AssegnatoIILivello__c del case</description>
        <field>AssegnatoIILivello__c</field>
        <formula>NOW()</formula>
        <name>AssegnatoIILiv to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseStateAnnullato</fullName>
        <field>DescrizioneStato__c</field>
        <literalValue>Annullato</literalValue>
        <name>Case State Annullato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseStateChiuso</fullName>
        <field>DescrizioneStato__c</field>
        <literalValue>Chiuso</literalValue>
        <name>Case State Chiuso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseStateInLavorazione</fullName>
        <field>DescrizioneStato__c</field>
        <literalValue>In Lavorazione</literalValue>
        <name>Case State In Lavorazione</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseStatusNuovo</fullName>
        <field>DescrizioneStato__c</field>
        <literalValue>Aperto</literalValue>
        <name>Case Status Nuovo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Escalated_True</fullName>
        <description>On Case, set escalated field to true</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Case_Escalated_True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Time_closed_update</fullName>
        <description>Il campo custom Date/Time closed viene aggiornato quando lo status cambia a chiuso o chiuso pending</description>
        <field>Date_Time_Closed__c</field>
        <formula>NOW()</formula>
        <name>Date/Time closed update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DefaultFamiglia_Info</fullName>
        <field>Famiglia__c</field>
        <literalValue>Info+</literalValue>
        <name>DefaultFamiglia Info+</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Progetti_RT</fullName>
        <description>imposta il record type a Progetti quando il case è aperto o assegnato ad uno user con Ruolo &apos;Contatore e Fibra&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Progetti</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Default Progetti RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Progetti_Tipologia</fullName>
        <description>Imposta di default la tipologia del case a &apos;Progetti&apos; quando viene creato o assegnato ad uno User con role &apos;ontatori e Fibra&apos;</description>
        <field>TipologiaCase__c</field>
        <literalValue>Progetti</literalValue>
        <name>Default Progetti Tipologia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Descrizione</fullName>
        <field>Description</field>
        <formula>&quot;WF User Fibrs to Coda Fibra&quot;</formula>
        <name>Descrizione</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Richiedente</fullName>
        <field>Email_Richiedente__c</field>
        <formula>Account.Email__c</formula>
        <name>Email Richiedente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation40h</fullName>
        <field>Secondo_Livello_Manuale__c</field>
        <literalValue>1</literalValue>
        <name>Escalation40h</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Famigli_Default</fullName>
        <description>In caso di change ower assegnato alla Coda di Contatore e FIbra</description>
        <field>Famiglia__c</field>
        <literalValue>Posa FO</literalValue>
        <name>Famiglia Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FamigliaFibra</fullName>
        <description>Imposta la famiglia a fibra quando si crea un case con profilo fibra</description>
        <field>Famiglia__c</field>
        <literalValue>Fibra e Contatori</literalValue>
        <name>FamigliaFibra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InCaricoAlBO</fullName>
        <field>Status</field>
        <literalValue>In carico al BO</literalValue>
        <name>InCaricoAlBO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InCaricoAlFO</fullName>
        <field>Status</field>
        <literalValue>In carico al FO</literalValue>
        <name>InCaricoAlFO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsOperatoreFO</fullName>
        <field>Rep_OneCallSolutions__c</field>
        <literalValue>1</literalValue>
        <name>IsOperatoreFO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Now_Date_Closed</fullName>
        <field>Date_Closed__c</field>
        <formula>NOW()</formula>
        <name>Now Date Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopolaEmailxQuestionario</fullName>
        <field>EmailChiusraCase__c</field>
        <formula>Account.Email__c</formula>
        <name>PopolaEmailxQuestionario</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Argomenti</fullName>
        <description>Sbianca il campo Argomenti quando cambiamo l&apos;owner da Fibra a BO o II Livello</description>
        <field>Argomenti__c</field>
        <name>Sbianca Argomenti</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Famiglia</fullName>
        <description>Sbianca la famiglia quando il case passa da fibra al II livello o back office</description>
        <field>Famiglia__c</field>
        <name>Sbianca Famiglia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Famiglia_ex_argomenti</fullName>
        <description>Sbaianca la famiglia quando il case torna a contatore o fibra</description>
        <field>Argomenti__c</field>
        <name>Sbianca Famiglia ex argomenti</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Livello_1</fullName>
        <field>Livello1__c</field>
        <name>Sbianca Livello 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Livello_2</fullName>
        <field>Livello2__c</field>
        <name>Sbianca Livello 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Livello_3</fullName>
        <field>Livello3__c</field>
        <name>Sbianca Livello 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_Situazione_Prestazione_sottesa</fullName>
        <description>Sbianca il campo Situazione Prestazione sottesa quando cambiamo l&apos;owner da Fibra a BO o II Livello</description>
        <field>SituazionePrestazioneSottesa__c</field>
        <name>Sbianca Situazione Prestazione sottesa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sbianca_SottoFamiglia</fullName>
        <field>SottofamigliaSituazionePrestazionesot__c</field>
        <name>Sbianca SottoFamiglia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Fibra</fullName>
        <description>Imposta lo Status a &apos;In carico a Fibra&apos;</description>
        <field>Status</field>
        <literalValue>In carico a Fibra</literalValue>
        <name>Set Status Fibra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Settech_IsStatusIILivelloFalse</fullName>
        <description>Set il campo Settech_IsStatusIILivello a false per far partire la rule corretta quando si crea manualmente un case</description>
        <field>tech_IsStatusIILivello__c</field>
        <literalValue>0</literalValue>
        <name>Settech_IsStatusIILivelloFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Settech_IsStatusIILivelloTrue</fullName>
        <description>Set Settech_IsStatusIILivello a true per far partire il wf time dependet wf 16 ore</description>
        <field>tech_case_aperto_manualmente__c</field>
        <literalValue>1</literalValue>
        <name>Settech_IsStatusIILivelloTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SmeAFibra</fullName>
        <description>riporta lo status dall&apos;esperto di zona a Fibra</description>
        <field>Status</field>
        <literalValue>In carico al II Livello</literalValue>
        <name>SmeAFibra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SmeAlIILivello</fullName>
        <description>riporta lo status dall&apos;esperto di zona al II Livello</description>
        <field>Status</field>
        <literalValue>In carico al II Livello</literalValue>
        <name>SmeAlIILivello</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SmeIILivelloStatus</fullName>
        <description>Riporta SendingEmailIILivelloAfterSme a false per far si che il WF parta solo quando da SME si torna all II Livello</description>
        <field>SendingEmailIILivelloAfterSme__c</field>
        <literalValue>0</literalValue>
        <name>SmeIILivelloStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusDefault_IILivello</fullName>
        <field>Status</field>
        <literalValue>In carico al II Livello</literalValue>
        <name>StatusDefault_IILivello</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusDefault_PrimoLivello</fullName>
        <description>Imposta lo stato di default a &quot;In carico al FO&quot; quando viene creato un case manualmente</description>
        <field>Status</field>
        <literalValue>In carico al FO</literalValue>
        <name>StatusDefault_PrimoLivello</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StausScalato</fullName>
        <field>Status</field>
        <literalValue>Scalato</literalValue>
        <name>StausScalato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tipologia_DaDefinire</fullName>
        <description>imposta la tipologia a Da Definire</description>
        <field>TipologiaCase__c</field>
        <literalValue>Da Definire</literalValue>
        <name>Tipologia_DaDefinire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setTempoDiEscalationManuale</fullName>
        <description>update il tempo di escalation a true per startare il calcolo del tempo di scalabilità del case</description>
        <field>tech_case_aperto_manualmente__c</field>
        <literalValue>1</literalValue>
        <name>setTempoDiEscalationManuale</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Coda_BO</fullName>
        <description>forza il valore alla coda di BO</description>
        <field>OwnerId</field>
        <lookupValue>Back_Office</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>set Coda BO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Status_a_Fibra</fullName>
        <description>Forza lo status a Fibra quando la tipologia del case passa da un altro valore a Progetti</description>
        <field>Status</field>
        <literalValue>In carico a Fibra</literalValue>
        <name>set Status a Fibra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Status_al_BO</fullName>
        <description>Forza lo status al BO quando la tipologia del case passa da Progetti ad altro valore</description>
        <field>Status</field>
        <literalValue>In carico al BO</literalValue>
        <name>set Status al BO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Status_al_II_Livello</fullName>
        <description>Forza lo status al II Livello quando l&apos;owner passa da un operatore Fibra a un operatore di II livello</description>
        <field>Status</field>
        <literalValue>In carico al II Livello</literalValue>
        <name>set Status al II Livello</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_FlowBOAlreadyFired_False</fullName>
        <field>tech_FlowBOAlreadyFired__c</field>
        <literalValue>0</literalValue>
        <name>tech_FlowBOAlreadyFired_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_FlowFIBRA_AlreadyFired_False</fullName>
        <field>tech_FlowFIBRA_AlreadyFired__c</field>
        <literalValue>0</literalValue>
        <name>tech_FlowFIBRA_AlreadyFired_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_FlowFIBRA_AlreadyFired_c_false</fullName>
        <field>tech_FlowFIBRA_AlreadyFired__c</field>
        <literalValue>0</literalValue>
        <name>tech_FlowFIBRA_AlreadyFired__c_false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_FlowIILivelloAlreadyFired_False</fullName>
        <field>tech_FlowIIAlreadyFired__c</field>
        <literalValue>0</literalValue>
        <name>tech FlowIILivelloAlreadyFired False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_IsStatusIILivello_True</fullName>
        <description>forzato a true per permetterre l&apos;assegnazione alle code di II livello solo da bottone &quot;Assegna al II livello&quot;</description>
        <field>tech_IsStatusIILivello__c</field>
        <literalValue>1</literalValue>
        <name>tech IsStatusIILivello True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_already_fired_info_piu</fullName>
        <field>tech_already_fired_info_piu__c</field>
        <literalValue>0</literalValue>
        <name>tech already fired info piu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_case_aperto_manualmente_False</fullName>
        <field>tech_case_aperto_manualmente__c</field>
        <literalValue>0</literalValue>
        <name>tech_case_aperto_manualmente_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_createDitta_False</fullName>
        <description>Imposta a false il campo 	tech_createDitta__c per poter accettare il case gli operatori ditta quando questo gli è stato assegnato</description>
        <field>tech_createDitta__c</field>
        <literalValue>0</literalValue>
        <name>tech_createDitta_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_isFibra</fullName>
        <field>tech_isFibra__c</field>
        <literalValue>0</literalValue>
        <name>tech_isFibra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_isFibra_True</fullName>
        <field>tech_isFibra__c</field>
        <literalValue>1</literalValue>
        <name>tech_isFibra_True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_isInfoPiu_false</fullName>
        <field>tech_isInfoPiu__c</field>
        <literalValue>0</literalValue>
        <name>tech_isInfoPiu_false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>tech_isNewCase_False</fullName>
        <field>tech_isNewCase__c</field>
        <literalValue>0</literalValue>
        <name>tech_isNewCase_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>testIdTicket</fullName>
        <field>ID_Ticket_Web__c</field>
        <formula>Status_reale__c</formula>
        <name>testIdTicket</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CM WF Date NO time closed</fullName>
        <actions>
            <name>Now_Date_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>setta il campo data chiusura eff. senza l&apos;ora quando il case viene chiuso</description>
        <formula>OR(AND( ISPICKVAL( Status , &quot;Annullato&quot;),  !ISPICKVAL(PRIORVALUE(Status),&quot;Annullato&quot;)), ISPICKVAL( Status , &quot;Chiuso&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CM_WF_PED_SET_NOW</fullName>
        <actions>
            <name>AssegnatoIILiv_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Setta la Data a now quando il case è assegnato a ped</description>
        <formula>CONTAINS(Owner:Queue.QueueName, &quot;Ped&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Famiglia Info%2B</fullName>
        <actions>
            <name>DefaultFamiglia_Info</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Default_Progetti_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Famiglia_ex_argomenti</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Situazione_Prestazione_sottesa</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_SottoFamiglia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_createDitta_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Imposta tipologia Progetti e famiglia Info+ quando avviene change owner a una coda Info+</description>
        <formula>AND (AND (NOT(Owner:Queue.QueueName = &quot;Piemonte Liguria&quot;), NOT(Owner:Queue.QueueName = &quot;Lombardia&quot;),NOT(Owner:Queue.QueueName = &quot;Emilia Romagna Marche&quot;), NOT(Owner:Queue.QueueName = &quot;Triveneto&quot;), NOT(Owner:Queue.QueueName = &quot;Toscana Umbria&quot;),NOT(Owner:Queue.QueueName = &quot;Lazio Abruzzo e Molise&quot;),NOT(Owner:Queue.QueueName = &quot;Campania&quot;) ,NOT(Owner:Queue.QueueName = &quot;Puglia Basilicata&quot;) , NOT(Owner:Queue.QueueName = &quot;Calabria&quot;) ,NOT(Owner:Queue.QueueName = &quot;Sicilia&quot;) ,NOT(Owner:Queue.QueueName = &quot;Sardegna&quot;) ,NOT(Owner:Queue.QueueName = &quot;Back Office&quot;) ,NOT(Owner:Queue.QueueName = &quot;CallBack&quot;), NOT(Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;), NOT(CONTAINS(Owner:Queue.QueueName, &quot;Ped&quot;)) ), OR((PRIORVALUE( Status ) =&quot;In carico al BO&quot;),(PRIORVALUE( Status ) =&quot;In carico al FO&quot;),  NOT( ISPICKVAL( PRIORVALUE(Famiglia__c) , &quot;Info+&quot; )) ), OR( ISBLANK(Owner:User.Profile.Name), ISNULL(Owner:User.Profile.Name) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Cambio Tipologia</fullName>
        <actions>
            <name>set_Coda_BO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Status_al_BO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowBOAlreadyFired_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Forza l&apos;owner alla coda di BO e lo status &apos;In carico al BO&apos; quando la tipologia passa da Fibra ad altro tipo (es. connessione)</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(TipologiaCase__c), &quot;Progetti&quot;), NOT (ISPICKVAL(TipologiaCase__c, &quot;Progetti&quot;)), ISPICKVAL(Status, &quot;In carico a Fibra&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Case Annullato</fullName>
        <actions>
            <name>CaseStateAnnullato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Annullato</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Case Chiuso</fullName>
        <actions>
            <name>Invio_Questionario_di_Chiusura_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CaseStateChiuso</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Status, &quot;Chiuso&quot;), NOT(ISPICKVAL(Famiglia__c, &quot;Mobilità&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Case In Lavorazione</fullName>
        <actions>
            <name>CaseStateInLavorazione</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status, &quot;In carico al FO&quot;)  ||  ISPICKVAL(Status, &quot;In carico al BO&quot;)  || ISPICKVAL(Status, &quot;In carico al II Livello&quot;)  || ISPICKVAL(Status, &quot;In carico Esperto Zona&quot;) ||  Status_reale__c = &quot;In carico a Fibra&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Change Owner FIto2Livello</fullName>
        <actions>
            <name>Sbianca_Argomenti</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Famiglia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Situazione_Prestazione_sottesa</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tipologia_DaDefinire</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Status_al_II_Livello</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowIILivelloAlreadyFired_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Forza la tipologia a &apos;Da Definire&apos; quando viene fatto un Change Owner manuale da Fibra al II Livello</description>
        <formula>AND ( OR(RecordType.Name=&quot;Misura&quot;, RecordType.Name=&quot;Connessione&quot;, RecordType.Name=&quot;Da Definire&quot;,  RecordType.Name=&quot;Progetti&quot;, RecordType.Name=&quot;Trasporto&quot;, RecordType.Name=&quot;Soggetto Terzo&quot;),  OR(Owner:Queue.QueueName = &quot;Calabria&quot;, Owner:Queue.QueueName = &quot;Campania&quot;,  Owner:Queue.QueueName = &quot;Emilia Romagna Marche&quot;, Owner:Queue.QueueName = &quot;Lazio Abruzzo e Molise&quot;, Owner:Queue.QueueName = &quot;Lombardia&quot;, Owner:Queue.QueueName = &quot;Piemonte Liguria&quot;, Owner:Queue.QueueName = &quot;Puglia Basilicata&quot;, Owner:Queue.QueueName = &quot;Sardegna&quot;, Owner:Queue.QueueName = &quot;Sicilia&quot;, Owner:Queue.QueueName = &quot;Toscana Umbria&quot;, Owner:Queue.QueueName = &quot;Triveneto&quot;), OR((PRIORVALUE( Status_reale__c )= &quot;In carico a Fibra&quot;),ISPICKVAL( Famiglia__c , &quot;Info+&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Change Owner FItoBO</fullName>
        <actions>
            <name>Sbianca_Argomenti</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Famiglia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Situazione_Prestazione_sottesa</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tipologia_DaDefinire</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Status_al_BO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowBOAlreadyFired_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_IsStatusIILivello_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Forza la tipologia a &apos;Da Definire&apos; quando viene fatto un Change Owner manuale da Fibra al BO</description>
        <formula>AND ( Owner:Queue.QueueName = &quot;Back Office&quot;,   ISPICKVAL(PRIORVALUE(Status), &quot;In carico a Fibra&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Change Owner set Famiglia Default</fullName>
        <actions>
            <name>Famigli_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND ( Status_reale__c = &quot;In carico a Fibra&quot;,  ISPICKVAL(Famiglia__c, &quot;&quot;) ), AND(Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;, ISPICKVAL(Famiglia__c, &quot;&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Date closed</fullName>
        <actions>
            <name>Date_Time_closed_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND( ISPICKVAL( Status , &quot;Annullato&quot;),  !ISPICKVAL(PRIORVALUE(Status),&quot;Annullato&quot;)), ISPICKVAL( Status , &quot;Chiuso&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Email BO E2Case</fullName>
        <actions>
            <name>Notifica_Assegnazione_Case_BO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (Owner:Queue.QueueName  = &quot;Back Office&quot;, ISPICKVAL(Origin, &quot;Email&quot;), ISPICKVAL(Status, &quot;Nuovo&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF Email Fibra Liv After SME</fullName>
        <actions>
            <name>Email_come_back_Fibra_from_SME</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SmeAFibra</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>invia l&apos;email a Fibra livello quando il case passa di status da Esperto di Zona al Livello Fibra</description>
        <formula>AND (ISPICKVAL(Status, &quot;In carico Esperto di Zona&quot;)  ,ISBLANK( AssegnatoAllEspertoDiDTRZona__c ),  NOT(ISBLANK(PRIORVALUE(AssegnatoAllEspertoDiDTRZona__c))),  OR( !ISBLANK(Owner:User.Profile.Name), !ISNULL(Owner:User.Profile.Name) ), Owner:User.Profile.Name = &quot;Operatore II Livello Fibra&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Email II Liv After SME</fullName>
        <actions>
            <name>SmeAlIILivello</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>invia l&apos;email al II livello quando il case passa di status da Esperto di Zona al Secondo livello</description>
        <formula>AND (ISPICKVAL(Status, &quot;In carico Esperto di Zona&quot;)  ,ISBLANK( AssegnatoAllEspertoDiDTRZona__c ),  NOT(ISBLANK(PRIORVALUE(AssegnatoAllEspertoDiDTRZona__c))),  OR( !ISBLANK(Owner:User.Profile.Name), !ISNULL(Owner:User.Profile.Name) ), Owner:User.Profile.Name = &quot;Operatore CM II Livello&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Email PedToCase</fullName>
        <actions>
            <name>Invio_Auto_Reply_PED</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notifica_Assegnazione_Case_BO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Invia l&apos;email di autoreply a chi a aperto il case da PED e invia l&apos;email al Back Office</description>
        <formula>AND (Owner:Queue.QueueName = &quot;Back Office&quot;, ISPICKVAL(Origin, &quot;Web&quot;), ISPICKVAL(Status, &quot;Nuovo&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF Email Richiedenti</fullName>
        <actions>
            <name>Email_Richiedente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Campo email sarà valorizzato con l’email, se presente, con quella sull’anagrafica richiedente. Il nuovo campo email sarà valorizzato a null se non esiste una mail del Richiedente.</description>
        <formula>AND(OR(ISNEW() ,ISCHANGED( AccountId )), NOT (AND( ISPICKVAL(Status , &quot;Nuovo&quot;),  ISPICKVAL( Origin , &quot;Web&quot;)  )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Escalation Email 16</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ((3 OR 4) OR (5 AND 6))</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Annullato,Chiuso Pending,Chiuso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_IsStatusIILivello__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In carico al FO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In carico al BO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nuovo</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CallBack</value>
        </criteriaItems>
        <description>Manda l&apos;email al team leader e pone lo stato a scalato dopo 16 ore dall&apos;apertura del case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Team_Leader_Back_Office</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Escalated_True</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>StausScalato</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.TempoDiEscalation__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WF Escalation Email 40</fullName>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 5) OR (6 AND 7) OR (2 AND 7) OR (5 AND 6) ) AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Annullato,Chiuso Pending,Chiuso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_IsStatusIILivello__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status_reale__c</field>
            <operation>equals</operation>
            <value>In carico al II Livello</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In carico Esperto di Zona</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isNewCase__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_IsStatusIILivello__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isNewCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Manda l&apos;email al team leader e pone lo stato a scalato dopo 16 ore dall&apos;apertura del case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EmailIILivello</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Escalated_True</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>StausScalato</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.TempoDiEscalation__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WF Escalation Email Fibra 40</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ( (3 AND 4) OR (5 AND 6) OR (5 AND 7 AND 8 AND 9) OR (3 AND 6) )</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Annullato,Chiuso Pending,Chiuso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status_reale__c</field>
            <operation>equals</operation>
            <value>In carico a Fibra</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_case_aperto_manualmente__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isFibra__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_case_aperto_manualmente__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isFibra__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_IsStatusIILivello__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isNewCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.tech_isFibra__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Team_Leader_Back_Office</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Escalated_True</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>StausScalato</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.TempoDiEscalation__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WF Invio Auto Response Email Case Chiuso</fullName>
        <actions>
            <name>Auto_Response_New_Email_Case_Chiuso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.tech_new_email_case_chiuso__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>(Quando il richiedente risponde all&apos;email di chiusura) Invio di un email di auto response che avvisa il richiedente di aprire un nuovo case qualora voglia inviare nuove segnalazioni</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Torna a Info%2B</fullName>
        <actions>
            <name>tech_already_fired_info_piu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isInfoPiu_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Si starta quando viene cambaito manualmente l&apos;owner del case da coda II Livello a --&gt; Info+</description>
        <formula>AND(OR($Profile.Name = &quot;Operatore CM II Livello&quot;, $Profile.Name = &quot;Operatore CM I Livello&quot;),NOT(Owner:Queue.QueueName = &quot;Piemonte Liguria&quot;), NOT(Owner:Queue.QueueName = &quot;Lombardia&quot;),NOT(Owner:Queue.QueueName = &quot;Emilia Romagna Marche&quot;),NOT(Owner:Queue.QueueName = &quot;Triveneto&quot;),NOT(Owner:Queue.QueueName = &quot;Toscana Umbria&quot;),NOT(Owner:Queue.QueueName = &quot;Lazio Abruzzo e Molise&quot;),NOT(Owner:Queue.QueueName = &quot;Campania&quot;) ,NOT(Owner:Queue.QueueName = &quot;Puglia Basilicata&quot;) ,NOT(Owner:Queue.QueueName = &quot;Calabria&quot;) ,NOT(Owner:Queue.QueueName = &quot;Sicilia&quot;) ,NOT(Owner:Queue.QueueName = &quot;Sardegna&quot;) ,NOT(Owner:Queue.QueueName = &quot;Back Office&quot;) ,NOT(Owner:Queue.QueueName = &quot;CallBack&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Torna a Progetti</fullName>
        <actions>
            <name>Default_Progetti_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalation40h</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Famigli_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Settech_IsStatusIILivelloFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>setTempoDiEscalationManuale</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowFIBRA_AlreadyFired_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_case_aperto_manualmente_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isFibra</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Si starta quando viene cambaito manualmente l&apos;owner del case da coda II Livello a --&gt; Coda &apos;Contatore e Fibra&apos;</description>
        <formula>OR (AND(Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;, Status_reale__c = &quot;In carico a Fibra&quot;),  AND( OR($Profile.Name = &quot;Operatore CM II Livello&quot;,$Profile.Name = &quot;Operatore CM II Livello Info+&quot;),Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;), AND(Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;,NOT(ISPICKVAL( Famiglia__c , &quot;Cambio CE&quot;)),  NOT(ISPICKVAL( Famiglia__c , &quot;Posa FO&quot;))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Torna a Progetti e Sbianca</fullName>
        <actions>
            <name>Sbianca_Famiglia_ex_argomenti</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Livello_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Livello_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Livello_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_Situazione_Prestazione_sottesa</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sbianca_SottoFamiglia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sbianca la Prestazione Sottesa e la Sottofamiglia quando il case torna alla Coda Contatore e Fibra</description>
        <formula>AND(Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;,  Status_reale__c != &quot;In carico a Fibra&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF User Fibrs to Coda Fibra</fullName>
        <actions>
            <name>Default_Progetti_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Descrizione</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalation40h</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Settech_IsStatusIILivelloFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowFIBRA_AlreadyFired_c_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_case_aperto_manualmente_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isFibra</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Quando si passa manualmente da un operatore Fibra alla coda di Contatore e Fibra viene ricalcolato il tempo di escalation</description>
        <formula>AND (Owner:Queue.QueueName = &quot;Contatore e Fibra&quot;, Status_reale__c = &quot;In carico a Fibra&quot;, PRIORVALUE(Status_reale__c) = &quot;In carico a Fibra&quot;, tech_isNewCase__c = false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF User To Coda II Livello</fullName>
        <actions>
            <name>Settech_IsStatusIILivelloFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_FlowIILivelloAlreadyFired_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isNewCase_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Cambio owner manuale da operatore II livello alla Coda dell&apos;operatore stesso.</description>
        <formula>AND ( Owner:Queue.QueueName != &quot;Back Office&quot;, Owner:Queue.QueueName != &quot;Contatore e Fibra&quot;, !CONTAINS(Owner:Queue.QueueName, &quot;Team Leader&quot;) , PRIORVALUE( Status_reale__c )= &quot;In carico al II Livello&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF set Status Fibra</fullName>
        <actions>
            <name>Default_Progetti_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Default_Progetti_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalation40h</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Settech_IsStatusIILivelloFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>setTempoDiEscalationManuale</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Status_al_II_Livello</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_isFibra_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Pone lo staus a &apos;In Carico a Fibra&apos; quando l&apos;owner è una coda o uno user con role &apos;Contatore e Fibra&apos;</description>
        <formula>AND (OR( !ISBLANK(Owner:User.Profile.Name), !ISNULL(Owner:User.Profile.Name) ), Owner:User.Profile.Name = &quot;Operatore II Livello Fibra&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF set Status Info%2B</fullName>
        <actions>
            <name>Default_Progetti_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Default_Progetti_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalation40h</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>setTempoDiEscalationManuale</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Status_al_II_Livello</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>tech_createDitta_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Pone lo staus a &apos;In Carico a Info+&apos; quando l&apos;owner è una coda o uno user con role &apos;Info+&apos;</description>
        <formula>AND (OR( !ISBLANK(Owner:User.Profile.Name), !ISNULL(Owner:User.Profile.Name) ),  Owner:User.Profile.Name = &quot;Operatore CM II Livello Info+&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WFNote aggiuntive per Email chiusura</fullName>
        <actions>
            <name>AggiornaNote_aggiuntive_dell_operatore</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Usato per rendere dinamica le 2 email di chiusura case</description>
        <formula>!ISBLANK(Eventuali_Note_Richiedente__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_CaseChiusoPending</fullName>
        <actions>
            <name>Invio_email_di_chiusura_pending</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sollecito</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status), !ISPICKVAL(PRIORVALUE(Status),&quot;Chiuso Pending&quot;), ISPICKVAL(Status, &quot;Chiuso Pending&quot;), NOT(ISPICKVAL(Famiglia__c, &quot;Mobilità&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_Case_Aperto</fullName>
        <actions>
            <name>CaseStatusNuovo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nuovo</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF_Case_Manuale</fullName>
        <actions>
            <name>InCaricoAlFO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Scatta quando viene creato un nuovo case in modo manuale da un operatore</description>
        <formula>$Profile.Name = &apos;Operatore CM – I Livello&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_Case_Manuale BO</fullName>
        <actions>
            <name>InCaricoAlBO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Scatta quando viene creato un nuovo case in modo manuale da un operatore Back Office</description>
        <formula>AND ($Profile.Name = &apos;Operatore CM I Livello&apos;, $UserRole.Name = &apos;Back Office&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_Case_Manuale FO</fullName>
        <actions>
            <name>InCaricoAlFO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Scatta quando viene creato un nuovo case in modo manuale da un operatore Front Office</description>
        <formula>AND ($Profile.Name = &apos;Operatore CM I Livello&apos;, $UserRole.Name = &apos;Front Office&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_ChiusoDaOperatoreFO</fullName>
        <actions>
            <name>IsOperatoreFO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL( PRIORVALUE(Status),&quot;In carico al FO&quot;), OR (ISPICKVAL(Status , &quot;Chiuso&quot;), ISPICKVAL(Status , &quot;Annullato&quot;), ISPICKVAL(Status , &quot;Chiuso pending&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_PopulateCaseEmail</fullName>
        <actions>
            <name>PopolaEmailxQuestionario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(NOT(  ISBLANK( Account.Email__c )) ,  NOT( ISNULL(Account.Email__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF_Status_IILiv_Default</fullName>
        <actions>
            <name>Escalation40h</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Settech_IsStatusIILivelloFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>StatusDefault_IILivello</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>setTempoDiEscalationManuale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Imposta lo stato di default al profilo dello user loggato quando viene creato un case manualmente</description>
        <formula>AND (OR( !ISBLANK(Owner:User.Profile.Name),  !ISNULL(Owner:User.Profile.Name) ), Owner:User.Profile.Name = &quot;Operatore CM II Livello&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_Status_PimoLiv_Default</fullName>
        <actions>
            <name>Settech_IsStatusIILivelloTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>StatusDefault_PrimoLivello</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>setTempoDiEscalationManuale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Imposta lo stato a &quot;in carico al FO&quot; di default al profilo dello user loggato quando viene creato un case manualmente</description>
        <formula>AND (OR( !ISBLANK(Owner:User.Profile.Name),  !ISNULL(Owner:User.Profile.Name) ), Owner:User.Profile.Name = &quot;Operatore CM I Livello&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_TaskCaseChiusoPending_Richiesta</fullName>
        <actions>
            <name>Sollecito_chiusura_Richiesta</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>crea un task di 15 gg dalla chiusura pending del case per ill tipo Richiesta</description>
        <formula>AND(ISCHANGED(Status), !ISPICKVAL(PRIORVALUE(Status),&quot;Chiuso Pending&quot;), ISPICKVAL(Status, &quot;Chiuso Pending&quot;), NOT(ISPICKVAL(Famiglia__c, &quot;Mobilità&quot;)), ISPICKVAL(Type, &quot;Richiesta&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WS_Case_InCaricoEspertoZona</fullName>
        <actions>
            <name>EmailToEspertoZona</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Status, &quot;In carico Esperto di Zona&quot;),NOT ( ISBLANK ( AssegnatoAllEspertoDiDTRZona__c ) ),  ISCHANGED(AssegnatoAllEspertoDiDTRZona__c), NOT(ISPICKVAL(Origin, &quot;Web&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WS_Case_InCaricoEspertoZona%09_PED</fullName>
        <actions>
            <name>EmailToEspertoZonaPED</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Invia Email allo SME quando il case è del PED</description>
        <formula>AND(ISPICKVAL(Status, &quot;In carico Esperto di Zona&quot;),NOT ( ISBLANK ( AssegnatoAllEspertoDiDTRZona__c ) ),  ISCHANGED(AssegnatoAllEspertoDiDTRZona__c), ISPICKVAL(Origin, &quot;Web&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Sollecito</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sollecito di verifica di chiusura di un Case in Stato Chiuso Pending</description>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.DataPrevistaRisoluzioneCriticita__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sollecito</subject>
    </tasks>
    <tasks>
        <fullName>Sollecito_chiusura_Richiesta</fullName>
        <assignedToType>owner</assignedToType>
        <description>Iniviato agli operatori per ricordare la chiusura del case di tipo Richiesta</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.DataPrevistaRisoluzioneCriticita__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sollecito chiusura Richiesta</subject>
    </tasks>
</Workflow>
